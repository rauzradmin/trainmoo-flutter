class OfferModel {
  int offerId;
  String name;

  OfferModel({this.offerId, this.name});

  static Map<String, dynamic> toMap(OfferModel offerObject) {
    Map<String, dynamic> offerMap = Map();
    offerMap['offerId'] = offerObject.offerId;
    offerMap['name'] = offerObject.name;
    return offerMap;
  }

  static List<Map<String, dynamic>> mapList(List<OfferModel> offers) {
    List<Map<String, dynamic>> listOfOffer = offers
        .map((offer) => {
              "offerId": offer.offerId,
              "name": offer.name,
            })
        .toList();
    return listOfOffer;
  }
}
