import 'FileModel.dart';

class ProgramClassesModel {
  String id;
  String createdAt;
  String updatedAt;
  String name;
  String uniqueid;
  String placeid;
  String summary;
  String from;
  String to;
  ProgramBean program;
  String school;
  String enrollCode;
  String registrationStatus;
  List<dynamic> paymentSchedules;
  GradingBean grading;
  List<SectionsBean> sections;
  List<FileModel> media;
  List<dynamic> medium;
  List<dynamic> category;
  String type;
  int V;
  AvatarBean avatar;
  String status;
  bool displayTotalToStudents;
  bool displayPercentileToStudents;
  String end;
  String start;
  UsersBean users;
  TutorBean tutor;
  PostCountBean postCount;
  int eventCount;

  ProgramClassesModel(
      {this.id,
      this.createdAt,
      this.updatedAt,
      this.name,
      this.uniqueid,
      this.placeid,
      this.summary,
      this.from,
      this.to,
      this.program,
      this.school,
      this.enrollCode,
      this.registrationStatus,
      this.paymentSchedules,
      this.grading,
      this.sections,
      this.media,
      this.medium,
      this.category,
      this.type,
      this.V,
      this.avatar,
      this.status,
      this.displayTotalToStudents,
      this.displayPercentileToStudents,
      this.end,
      this.start,
      this.users,
      this.tutor,
      this.postCount,
      this.eventCount});

  ProgramClassesModel.map(dynamic obj) {
    this.id = obj["_id"];
    this.createdAt = obj["createdAt"];
    this.updatedAt = obj["updatedAt"];
    this.name = obj["name"] != null ? obj["name"] : 'Unknown';
    this.uniqueid = obj["uniqueid"];
    this.placeid = obj["placeid"];
    this.summary = obj["summary"];
    this.from = obj["from"];
    this.to = obj["to"];
    this.program =
        obj["program"] != null ? ProgramBean.map(obj["program"]) : null;
    this.school = obj["school"];
    this.enrollCode = obj["enrollCode"];
    this.registrationStatus = obj["registrationStatus"];
    this.paymentSchedules = obj["paymentSchedules"];
    this.grading =
        obj["grading"] != null ? GradingBean.map(obj["grading"]) : null;
    this.sections = obj["sections"] != null
        ? SectionsBean.fromJsonArray(obj["sections"])
        : [];
    this.media = FileModel.fromJsonArray(obj["media"]);
    this.medium = obj["medium"];
    this.category = obj["category"];
    this.type = obj["type"];
    this.V = obj["V"];
    this.avatar = obj["avatar"] != null ? AvatarBean.map(obj["avatar"]) : null;
    this.status = obj["status"];
    this.displayTotalToStudents = obj["displayTotalToStudents"];
    this.displayPercentileToStudents = obj["displayPercentileToStudents"];
    this.end = obj["end"];
    this.start = obj["start"];
    this.users =
        obj["users"] != null ? UsersBean.map(obj["users"]) : UsersBean(0, 0, 0);
    this.tutor = obj["tutor"] != null ? TutorBean.map(obj["tutor"]) : null;
    this.postCount =
        obj["postCount"] != null ? PostCountBean.map(obj["postCount"]) : null;
    this.eventCount = obj["eventCount"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["_id"] = id;
    map["createdAt"] = createdAt;
    map["updatedAt"] = updatedAt;
    map["name"] = name;
    map["uniqueid"] = uniqueid;
    map["placeid"] = placeid;
    map["summary"] = summary;
    map["from"] = from;
    map["to"] = to;
    map["program"] = program.toMap();
    map["school"] = school;
    map["enrollCode"] = enrollCode;
    map["registrationStatus"] = registrationStatus;
    map["paymentSchedules"] = paymentSchedules;
    map["grading"] = grading.toMap();
    map["sections"] = sections;
    map["media"] = media;
    map["medium"] = medium;
    map["category"] = category;
    map["type"] = type;
    map["V"] = V;
    map["avatar"] = avatar.toMap();
    map["status"] = status;
    map["displayTotalToStudents"] = displayTotalToStudents;
    map["displayPercentileToStudents"] = displayPercentileToStudents;
    map["end"] = end;
    map["start"] = start;
    map["users"] = users.toMap();
    map["tutor"] = tutor.toMap();
    map["postCount"] = postCount.toMap();
    map["eventCount"] = eventCount;
    return map;
  }

  static List<ProgramClassesModel> fromJsonArray(List<dynamic> json) {
    List<ProgramClassesModel> list = json
        .map<ProgramClassesModel>((json) => ProgramClassesModel.map(json))
        .toList();
    return list;
  }

  static List<ProgramClassesModel> fromDatabaseJsonArray(List<dynamic> json) {
    print('converting array data ==> ${json[0]['data']}');

    List<ProgramClassesModel> list = json
        .map<ProgramClassesModel>(
            (data) => ProgramClassesModel.map(data['data']))
        .toList();
    return list;
  }
}

/// total : 40
/// message : 3
/// material : 3
/// assessment : 11
/// assignment : 20
/// question : 2
/// discussion : 1

class PostCountBean {
  int total;
  int message;
  int material;
  int assessment;
  int assignment;
  int question;
  int discussion;

  PostCountBean(this.total, this.message, this.material, this.assessment,
      this.assignment, this.question, this.discussion);

  PostCountBean.map(dynamic obj) {
    this.total = obj["total"];
    this.message = obj["message"];
    this.material = obj["material"];
    this.assessment = obj["assessment"];
    this.assignment = obj["assignment"];
    this.question = obj["question"];
    this.discussion = obj["discussion"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["total"] = total;
    map["message"] = message;
    map["material"] = material;
    map["assessment"] = assessment;
    map["assignment"] = assignment;
    map["question"] = question;
    map["discussion"] = discussion;
    return map;
  }
}

/// id : "5b371b716b994000216d154a"
/// email : "teacher@gmail.com"
/// v : 4
/// updatedat : "2020-01-24T14:23:42.016Z"
/// createdat : "2018-05-15T04:35:13.935Z"
/// name : "anil kumar"
/// type : "teacher"
/// school : "5b371b6c6b994000216d1457"
/// registerUsing : "email"
/// user : "5b371b716b994000216d154a"
/// isActive : true
/// children : []
/// schoolAdmin : false
/// avatarThumbnail : "/api/school/5b371b6c6b994000216d1457/getAttachment?path=image/upload/gface,cthumb,w100,h100/v1545936853/alpha/5b371b6c6b994000216d1457/quhbfpsskdjab8kysfjm.jpg"
/// lastloginat : "2020-01-24T14:23:42.015Z"
/// status : "active"
/// gender : "not specified"
/// accessModules : [{"name":"attendance","value":true,"id":"5d0cc4d576614d0064ac66ed"},{"name":"grades","value":true,"id":"5d0cc4d576614d0064ac66ec"},{"name":"assignment","value":true,"id":"5d0cc4d576614d0064ac66eb"},{"name":"assessment","value":true,"id":"5d0cc4d576614d0064ac66ea"},{"name":"quizbank","value":true,"id":"5d0cc4d576614d0064ac66e9"},{"name":"events","value":true,"id":"5d0cc4d576614d0064ac66e8"},{"name":"survey","value":true,"id":"5d0cc4d576614d0064ac66e7"}]
/// dob : null
/// title : "HOD"

class TutorBean {
  String Id;
  String email;
  int V;
  String updatedAt;
  String createdAt;
  String name;
  String type;
  String school;
  String registerUsing;
  String user;
  bool isActive;
  List<dynamic> children;
  bool schoolAdmin;
  String avatarThumbnail;
  String lastLoginAt;
  String status;
  String gender;
  List<AccessModulesBean> accessModules;
  dynamic dob;
  String title;

  TutorBean(
      this.Id,
      this.email,
      this.V,
      this.updatedAt,
      this.createdAt,
      this.name,
      this.type,
      this.school,
      this.registerUsing,
      this.user,
      this.isActive,
      this.children,
      this.schoolAdmin,
      this.avatarThumbnail,
      this.lastLoginAt,
      this.status,
      this.gender,
      this.accessModules,
      this.dob,
      this.title);

  TutorBean.map(dynamic obj) {
    this.Id = obj["_id"];
    this.email = obj["email"];
    this.V = obj["V"];
    this.updatedAt = obj["updatedAt"];
    this.createdAt = obj["createdAt"];
    this.name = obj["name"];
    this.type = obj["type"];
    this.school = obj["school"];
    this.registerUsing = obj["registerUsing"];
    this.user = obj["user"];
    this.isActive = obj["isActive"];
    this.children = obj["children"];
    this.schoolAdmin = obj["schoolAdmin"];
    this.avatarThumbnail =
        obj["avatarThumbnail"] != null ? obj["avatarThumbnail"] : "";
    this.lastLoginAt = obj["lastLoginAt"];
    this.status = obj["status"];
    this.gender = obj["gender"];
    this.accessModules = AccessModulesBean.fromJsonArray(obj["accessModules"]);
    this.dob = obj["dob"];
    this.title = obj["title"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["_id"] = Id;
    map["email"] = email;
    map["V"] = V;
    map["updatedAt"] = updatedAt;
    map["createdAt"] = createdAt;
    map["name"] = name;
    map["type"] = type;
    map["school"] = school;
    map["registerUsing"] = registerUsing;
    map["user"] = user;
    map["isActive"] = isActive;
    map["children"] = children;
    map["schoolAdmin"] = schoolAdmin;
    map["avatarThumbnail"] = avatarThumbnail;
    map["lastLoginAt"] = lastLoginAt;
    map["status"] = status;
    map["gender"] = gender;
    map["accessModules"] = accessModules;
    map["dob"] = dob;
    map["title"] = title;
    return map;
  }
}

/// name : "attendance"
/// value : true
/// id : "5d0cc4d576614d0064ac66ed"

class AccessModulesBean {
  String name;
  bool value;
  String Id;

  AccessModulesBean(this.name, this.value, this.Id);

  AccessModulesBean.map(dynamic obj) {
    this.name = obj["name"];
    this.value = obj["value"];
    this.Id = obj["_id"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["name"] = name;
    map["value"] = value;
    map["_id"] = Id;
    return map;
  }

  static List<AccessModulesBean> fromJsonArray(List<dynamic> json) {
    List<AccessModulesBean> list = json
        .map<AccessModulesBean>((json) => AccessModulesBean.map(json))
        .toList();
    return list;
  }
}

/// student : 35
/// teacher : 1
/// admin : 1

class UsersBean {
  int student;
  int teacher;
  int admin;

  UsersBean(this.student, this.teacher, this.admin);

  UsersBean.map(dynamic obj) {
    this.student = obj["student"] != null ? obj["student"] : 0;
    this.teacher = obj["teacher"] != null ? obj["teacher"] : 0;
    this.admin = obj["admin"] != null ? obj["admin"] : 0;
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["student"] = student;
    map["teacher"] = teacher;
    map["admin"] = admin;
    return map;
  }
}

/// name : "background.png"
/// size : 7140
/// type : ""
/// src : "https://trainmoo-files.s3.amazonaws.com/upload%2Ftsource%2F65a65039ea576086825b0efb7e4063240d9a7d78ea1d7f16a2be7755d6f6769b.png"
/// id : "5b6990b04653710060486a21"
/// createdat : "2018-08-07T12:29:36.748Z"
/// gdrive : false

class AvatarBean {
  String name;
  int size;
  String type;
  String src;
  String Id;
  String createdAt;
  bool gdrive;

  AvatarBean(this.name, this.size, this.type, this.src, this.Id, this.createdAt,
      this.gdrive);

  AvatarBean.map(dynamic obj) {
    this.name = obj["name"];
    this.size = obj["size"];
    this.type = obj["type"];
    this.src = obj["src"];
    this.Id = obj["_id"];
    this.createdAt = obj["createdAt"];
    this.gdrive = obj["gdrive"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["name"] = name;
    map["size"] = size;
    map["type"] = type;
    map["src"] = src;
    map["_id"] = Id;
    map["createdAt"] = createdAt;
    map["gdrive"] = gdrive;
    return map;
  }
}

/// seq : 1
/// summary : "<p>Focus on grammer</p>"
/// title : "Punctuation"
/// id : "5b3c401550ac2700216f7e9c"
/// active : true
/// files : []

class SectionsBean {
  int seq;
  String summary;
  String title;
  String Id;
  bool active;
  List<dynamic> files;

  SectionsBean(
      this.seq, this.summary, this.title, this.Id, this.active, this.files);

  SectionsBean.map(dynamic obj) {
    this.seq = obj["seq"];
    this.summary = obj["summary"];
    this.title = obj["title"];
    this.Id = obj["_id"];
    this.active = obj["active"];
    this.files = obj["files"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["seq"] = seq;
    map["summary"] = summary;
    map["title"] = title;
    map["_id"] = Id;
    map["active"] = active;
    map["files"] = files;
    return map;
  }

  static List<SectionsBean> fromJsonArray(List<dynamic> json) {
    List<SectionsBean> list =
        json.map<SectionsBean>((json) => SectionsBean.map(json)).toList();
    return list;
  }
}

/// by : "assignment"
/// weightages : []

class GradingBean {
  String by;
  List<dynamic> weightages;

  GradingBean(this.by, this.weightages);

  GradingBean.map(dynamic obj) {
    this.by = obj["by"];
    this.weightages = obj["weightages"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["by"] = by;
    map["weightages"] = weightages;
    return map;
  }
}

/// id : "5b371b6c6b994000216d145c"
/// createdat : "2018-05-15T04:20:48.873Z"
/// updatedat : "2019-05-01T01:51:10.555Z"
/// name : "BE 2 IT A"
/// uniqueid : "6G18"
/// placeid : "ChIJL-k0LnUTrjsRrmqYb6Y0ssI"
/// summary : ""
/// from : "2018-10-24T00:00:00.000Z"
/// to : "2018-10-24T00:00:00.000Z"
/// school : "5b371b6c6b994000216d1457"
/// enrollCode : "COMORO FRANC6G18"
/// registrationStatus : "closed"
/// paymentSchedules : [{"updatedat":"2018-08-19T08:52:49.272Z","createdat":"2018-08-16T18:37:09.447Z","cost":1000,"details":"term","due":"2018-11-29T18:30:00.000Z","seq":0,"id":"5b75c45593f48e00607edacf","currency":"INR"},{"createdat":"2018-12-17T09:51:56.705Z","seq":1,"updatedat":"2018-12-17T09:51:04.473Z","due":"2018-12-26T18:30:00.000Z","cost":45,"details":"exam fees","id":"5c1771bc523065006c82e5ee","currency":"INR"}]
/// grading : {"by":"assignment","weightages":[]}
/// sections : []
/// media : []
/// medium : []
/// category : []
/// type : "program"
/// v : 11
/// status : "active"
/// displayTotalToStudents : true
/// displayPercentileToStudents : true
/// end : "2019-12-28T00:00:00.000Z"
/// start : "2019-08-06T00:00:00.000Z"

class ProgramBean {
  String id;
  String createdAt;
  String updatedAt;
  String name;
  String uniqueid;
  String placeid;
  String summary;
  String from;
  String to;
  String school;
  String enrollCode;
  String registrationStatus;
  List<PaymentSchedulesBean> paymentSchedules;
  GradingBean grading;
  List<dynamic> sections;
  List<dynamic> media;
  List<dynamic> medium;
  List<dynamic> category;
  String type;
  int V;
  String status;
  bool displayTotalToStudents;
  bool displayPercentileToStudents;
  String end;
  String start;

  ProgramBean(
      this.id,
      this.createdAt,
      this.updatedAt,
      this.name,
      this.uniqueid,
      this.placeid,
      this.summary,
      this.from,
      this.to,
      this.school,
      this.enrollCode,
      this.registrationStatus,
      this.paymentSchedules,
      this.grading,
      this.sections,
      this.media,
      this.medium,
      this.category,
      this.type,
      this.V,
      this.status,
      this.displayTotalToStudents,
      this.displayPercentileToStudents,
      this.end,
      this.start);

  ProgramBean.map(dynamic obj) {
    this.id = obj["_id"];
    this.createdAt = obj["createdAt"];
    this.updatedAt = obj["updatedAt"];
    this.name = obj["name"];
    this.uniqueid = obj["uniqueid"];
    this.placeid = obj["placeid"];
    this.summary = obj["summary"];
    this.from = obj["from"];
    this.to = obj["to"];
    this.school = obj["school"];
    this.enrollCode = obj["enrollCode"];
    this.registrationStatus = obj["registrationStatus"];
    this.paymentSchedules =
        PaymentSchedulesBean.fromJsonArray(obj["paymentSchedules"]);
    this.grading =
        obj["grading"] != null ? GradingBean.map(obj["grading"]) : null;
    this.sections = obj["sections"];
    this.media = obj["media"];
    this.medium = obj["medium"];
    this.category = obj["category"];
    this.type = obj["type"];
    this.V = obj["V"];
    this.status = obj["status"];
    this.displayTotalToStudents = obj["displayTotalToStudents"];
    this.displayPercentileToStudents = obj["displayPercentileToStudents"];
    this.end = obj["end"];
    this.start = obj["start"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["_id"] = id;
    map["createdAt"] = createdAt;
    map["updatedAt"] = updatedAt;
    map["name"] = name;
    map["uniqueid"] = uniqueid;
    map["placeid"] = placeid;
    map["summary"] = summary;
    map["from"] = from;
    map["to"] = to;
    map["school"] = school;
    map["enrollCode"] = enrollCode;
    map["registrationStatus"] = registrationStatus;
    map["paymentSchedules"] = paymentSchedules;
    map["grading"] = grading.toMap();
    map["sections"] = sections;
    map["media"] = media;
    map["medium"] = medium;
    map["category"] = category;
    map["type"] = type;
    map["V"] = V;
    map["status"] = status;
    map["displayTotalToStudents"] = displayTotalToStudents;
    map["displayPercentileToStudents"] = displayPercentileToStudents;
    map["end"] = end;
    map["start"] = start;
    return map;
  }
}

/// by : "assignment"
/// weightages : []

/// updatedat : "2018-08-19T08:52:49.272Z"
/// createdat : "2018-08-16T18:37:09.447Z"
/// cost : 1000
/// details : "term"
/// due : "2018-11-29T18:30:00.000Z"
/// seq : 0
/// id : "5b75c45593f48e00607edacf"
/// currency : "INR"

class PaymentSchedulesBean {
  String updatedAt;
  String createdAt;
  int cost;
  String details;
  String due;
  int seq;
  String id;
  String currency;

  PaymentSchedulesBean(this.updatedAt, this.createdAt, this.cost, this.details,
      this.due, this.seq, this.id, this.currency);

  PaymentSchedulesBean.map(dynamic obj) {
    this.updatedAt = obj["updatedAt"];
    this.createdAt = obj["createdAt"];
    this.cost = obj["cost"];
    this.details = obj["details"];
    this.due = obj["due"];
    this.seq = obj["seq"];
    this.id = obj["_id"];
    this.currency = obj["currency"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["updatedAt"] = updatedAt;
    map["createdAt"] = createdAt;
    map["cost"] = cost;
    map["details"] = details;
    map["due"] = due;
    map["seq"] = seq;
    map["_id"] = id;
    map["currency"] = currency;
    return map;
  }

  static List<PaymentSchedulesBean> fromJsonArray(List<dynamic> json) {
    List<PaymentSchedulesBean> list = json
        .map<PaymentSchedulesBean>((json) => PaymentSchedulesBean.map(json))
        .toList();
    return list;
  }
}
