class AssetModel {
  int id;
  String type;
  String tag;
  String file;

  AssetModel({this.id, this.type, this.tag, this.file});

  static Map<String, dynamic> toMap(AssetModel assetObject) {
    Map<String, dynamic> assetMap = Map();
    assetMap['id'] = assetObject.id;
    assetMap['type'] = assetObject.type;
    assetMap['tag'] = assetObject.tag;
    assetMap['file'] = assetObject.file;

    return assetMap;
  }

  static List<Map<String, dynamic>> mapList(List<AssetModel> assets) {
    List<Map<String, dynamic>> listOfAsset = assets
        .map((asset) => {
              "id": asset.id,
              "type": asset.type,
              "tag": asset.tag,
              "file": asset.file,
            })
        .toList();
    return listOfAsset;
  }

  static List<AssetModel> mapToList(Map<int, AssetModel> assets) {
    List<AssetModel> listOfAsset = [];

    assets.forEach((key, model) {
      listOfAsset.add(model);
    });

    return listOfAsset;
  }

  static getAssetArray() {
    AssetModel assetModel = AssetModel(
        id: 101212,
        type: 'image',
        tag: 'media',
        file:
            'https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Google_Images_2015_logo.svg/1200px-Google_Images_2015_logo.svg.png');
    List<AssetModel> assets = [];
    assets.add(assetModel);
    return assets;
  }
}
