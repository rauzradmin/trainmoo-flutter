class Experience {
    String experienceId;
    String company;
    String from;
    String summary;
    String title;
    String to;

    Experience({this.experienceId, this.company, this.from, this.summary, this.title, this.to});

    factory Experience.fromJson(Map<String, dynamic> json) {
        return Experience(
            experienceId: json['_id'],
            company: json['company'], 
            from: json['from'], 
            summary: json['summary'], 
            title: json['title'], 
            to: json['to'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['_id'] = this.experienceId;
        data['company'] = this.company;
        data['from'] = this.from;
        data['summary'] = this.summary;
        data['title'] = this.title;
        data['to'] = this.to;
        return data;
    }
}