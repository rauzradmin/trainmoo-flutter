import 'Address.dart';
import 'Avatar.dart';
import 'Background.dart';
import 'Coords.dart';
import 'Education.dart';
import 'Experience.dart';
import 'Media.dart';
import 'Name.dart';

class Author {
  UserNameObject name;
  Avatar avatar;

  Author({this.name,this.avatar});

  factory Author.fromJson(Map<String, dynamic> json) {
    return Author(
      name: json['name'] != null ? UserNameObject.fromJson(json['name']) : null,
      avatar: json['avatar'] != null ? Avatar.fromJson(json['avatar']) : null,


    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    if (this.avatar != null) {
      data['avatar'] = this.avatar.toJson();
    }

    return data;
  }

  static List<Author> fromJsonArray(List<dynamic> json) {
    List<Author> bannerLists =
    json.map<Author>((json) => Author.fromJson(json)).toList();
    return bannerLists;
  }
}
