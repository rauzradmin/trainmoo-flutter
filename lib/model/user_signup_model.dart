import 'package:trainmoo/model/Name.dart';
import 'package:trainmoo/model/User.dart';

class UserSignUpModel {
  User user;
  UserNameObject userNameObject;
  String shortName;
  String fullName;
  String id;
  String gender;
  bool isActive;
  List<String> message;

  UserSignUpModel(
      {this.userNameObject,
      this.user,
      this.shortName,
      this.fullName,
      this.isActive,
      this.id,
      this.gender,
      this.message});

  factory UserSignUpModel.fromJson(Map<String, dynamic> json) {
    if (json['user'] != null) {
      return UserSignUpModel(
        user: json['user'] != null ? User.fromJson(json['user']) : null,
        userNameObject:
            json['name'] != null ? UserNameObject.fromJson(json['name']) : null,
        shortName: json['shortname'],
        fullName: json['fullname'],
        id: json['_id'],
        gender: json['gender'],
        isActive: json['isActive'],
      );
    } else {
      return UserSignUpModel(
        message: json['message'] != null
            ? new List<String>.from(json['message'])
            : null,
      );
    }
  }
}

class ErrorModel {
  List<String> message;

  ErrorModel({this.message});

  factory ErrorModel.fromJson(Map<String, dynamic> json) {
    return ErrorModel(
      message: json['loginMessage'] != null
          ? new List<String>.from(json['loginMessage'])
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.message != null) {
      data['loginMessage'] = this.message;
    }
    return data;
  }
}
