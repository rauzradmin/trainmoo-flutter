import 'package:trainmoo/model/StudentModel.dart';

class AttendanceModel {
  String id;
  String school;
  String classId;
  StudentModel schoolUser;
  bool present;
  String eventDate;
  bool selfMarked;
  String attendanceSession;
  String event;
  String createdAt;
  String updatedAt;
  String createdBy;
  String attendanceTime;

  AttendanceModel(
      this.id,
      this.school,
      this.classId,
      this.schoolUser,
      this.present,
      this.eventDate,
      this.selfMarked,
      this.attendanceSession,
      this.event,
      this.createdAt,
      this.updatedAt,
      this.createdBy,
      this.attendanceTime);

  AttendanceModel.map(dynamic obj) {
    this.id = obj["_id"];
    this.school = obj["school"];
    this.classId = obj["class"];
    this.schoolUser = StudentModel.map(obj["schoolUser"]);
    this.present = obj["present"];
    this.eventDate = obj["eventDate"];
    this.selfMarked = obj["selfMarked"];
    this.attendanceSession = obj["attendanceSession"];
    this.event = obj["event"];
    this.createdAt = obj["createdAt"];
    this.updatedAt = obj["updatedAt"];
    this.createdBy = obj["createdBy"];
    this.attendanceTime = obj["attendanceTime"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["_id"] = id;
    map["school"] = school;
    map["class"] = classId;
    map["schoolUser"] = schoolUser;
    map["present"] = present;
    map["eventDate"] = eventDate;
    map["selfMarked"] = selfMarked;
    map["attendanceSession"] = attendanceSession;
    map["event"] = event;
    map["createdAt"] = createdAt;
    map["updatedAt"] = updatedAt;
    map["createdBy"] = createdBy;
    map["attendanceTime"] = attendanceTime;
    return map;
  }

  static List<AttendanceModel> fromJsonArray(List<dynamic> json) {
    List<AttendanceModel> list =
        json.map<AttendanceModel>((json) => AttendanceModel.map(json)).toList();
    return list;
  }
}
