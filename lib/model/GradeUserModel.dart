class GradeUserModel {
  String id;
  int V;
  String email;
  String updatedAt;
  String createdAt;
  String name;
  String type;
  String registerUsing;
  String school;
  String user;
  bool isActive;
  bool schoolAdmin;
  String bus;
  String mobile;
  String avatarThumbnail;
  String gender;
  String lastLoginAt;
  String status;
  List<AccessModulesBean> accessModules;

  GradeUserModel(
      this.id,
      this.V,
      this.email,
      this.updatedAt,
      this.createdAt,
      this.name,
      this.type,
      this.registerUsing,
      this.school,
      this.user,
      this.isActive,
      this.schoolAdmin,
      this.bus,
      this.mobile,
      this.avatarThumbnail,
      this.gender,
      this.lastLoginAt,
      this.status,
      this.accessModules);

  GradeUserModel.map(dynamic obj) {
    this.id = obj["_id"];
    this.V = obj["__V"];
    this.email = obj["email"];
    this.updatedAt = obj["updatedAt"];
    this.createdAt = obj["createdAt"];
    this.name = obj["name"];
    this.type = obj["type"];
    this.registerUsing = obj["registerUsing"];
    this.school = obj["school"];
    this.user = obj["user"];
    this.isActive = obj["isActive"];
    this.schoolAdmin = obj["schoolAdmin"];
    this.bus = obj["bus"];
    this.mobile = obj["mobile"];
    this.avatarThumbnail = obj["avatarThumbnail"];
    this.gender = obj["gender"];
    this.lastLoginAt = obj["lastLoginAt"];
    this.status = obj["status"];
    this.accessModules = AccessModulesBean.fromJsonArray(obj["accessModules"]);
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["_id"] = id;
    map["__V"] = V;
    map["email"] = email;
    map["updatedAt"] = updatedAt;
    map["createdAt"] = createdAt;
    map["name"] = name;
    map["type"] = type;
    map["registerUsing"] = registerUsing;
    map["school"] = school;
    map["user"] = user;
    map["isActive"] = isActive;
    map["schoolAdmin"] = schoolAdmin;
    map["bus"] = bus;
    map["mobile"] = mobile;
    map["avatarThumbnail"] = avatarThumbnail;
    map["gender"] = gender;
    map["lastLoginAt"] = lastLoginAt;
    map["status"] = status;
    map["accessModules"] = accessModules;
    return map;
  }
}

/// name : "attendance"
/// value : true

class AccessModulesBean {
  String name;
  bool value;

  AccessModulesBean(this.name, this.value);

  AccessModulesBean.map(dynamic obj) {
    this.name = obj["name"];
    this.value = obj["value"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["name"] = name;
    map["value"] = value;
    return map;
  }

  static List<AccessModulesBean> fromJsonArray(List<dynamic> json) {
    List<AccessModulesBean> list = json
        .map<AccessModulesBean>((json) => AccessModulesBean.map(json))
        .toList();
    return list;
  }
}
