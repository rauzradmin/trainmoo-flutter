/// url : "https://edition.cnn.com/2020/03/16/tech/google-coronavirus-website-rollout/index.html"
/// publisher : "CNN"
/// logo : "https://edition.cnn.com//favicon.business.ico"
/// description : "Confusion greeted the launch of a coronavirus testing website built by Verily, a subsidiary of Alphabet and sister company to Google. The site launched Sunday night and is initially intended for people in the San Francisco Bay Area."
/// image : "https://cdn.cnn.com/cnnnext/dam/assets/200316131625-verily-headquarters-super-tease.jpg"
/// title : "Google sister-company's coronavirus website rolls out to confusion"

class LinkPreview {
  String _url;
  String _publisher;
  String _logo;
  String _description;
  String _image;
  String _title;

  String get url => _url;
  String get publisher => _publisher;
  String get logo => _logo;
  String get description => _description;
  String get image => _image;
  String get title => _title;

  LinkPreview(this._url, this._publisher, this._logo, this._description, this._image, this._title);

  LinkPreview.map(dynamic obj) {
    print('Linke preview object ==> $obj');
    this._url = obj["url"];
    this._publisher = obj["publisher"];
    this._logo = obj["logo"];
    this._description = obj["description"];
    this._image = obj["image"];
    this._title = obj["title"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["url"] = _url;
    map["publisher"] = _publisher;
    map["logo"] = _logo;
    map["description"] = _description;
    map["image"] = _image;
    map["title"] = _title;
    return map;
  }

}