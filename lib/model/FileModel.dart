class FileModel {
  String title;
  String name;
  String src;
  String thumbnail;
  String type;
  int size;
  int h;
  int w;
  String id;
  String createdAt;
  bool gdrive;

  FileModel(this.title, this.name, this.src, this.thumbnail, this.type,
      this.size, this.h, this.w, this.id, this.createdAt, this.gdrive);

  FileModel.map(dynamic obj) {
    //print('filemode obj $obj');
    this.title = obj["title"];
    this.name = obj["name"];
    this.src = obj["src"];
    this.thumbnail = obj["thumbnail"];
    this.type = obj["type"];
    this.size = obj["size"];
    this.h = obj["h"];
    this.w = obj["w"];
    this.id = obj["_id"];
    this.createdAt = obj["created_at"];
    this.gdrive = obj["gdrive"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["title"] = title;
    map["name"] = name;
    map["src"] = src;
    map["thumbnail"] = thumbnail;
    map["type"] = type;
    map["size"] = size;
//    map["h"] = h;
//    map["w"] = w;
//    map["_id"] = id;
//    map["created_at"] = createdAt;
//    map["gdrive"] = gdrive;
    return map;
  }

  static List<FileModel> fromJsonArray(List<dynamic> json) {
    //print('json array $json');
    if (json == null || json.length <= 0) return [];
    List<FileModel> list =
        json.map<FileModel>((json) => FileModel.map(json)).toList();
    return list;
  }
}
