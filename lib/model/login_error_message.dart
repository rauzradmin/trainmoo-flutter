
class LoginErrorMessage
{
  final String loginMessage;

  LoginErrorMessage(this.loginMessage);
  factory LoginErrorMessage.fromJson(Map<String, dynamic> json) {
    return LoginErrorMessage(json['loginMessage']!=null?json['loginMessage']:null);
  }


}