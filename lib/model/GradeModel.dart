import 'package:trainmoo/model/GradeDetailObject.dart';

import 'GradeUserModel.dart';

class GradeModel {
  List<HeadersBean> headers;
  int totalStudents;
  List<GradeDetails> gradeDetails;

  GradeModel(this.headers, this.totalStudents, this.gradeDetails);

  GradeModel.map(dynamic obj) {
    this.headers = HeadersBean.fromJsonArray(obj["headers"]);
    this.totalStudents = obj["totalStudents"];
    this.gradeDetails = GradeDetails.fromJsonArray(obj["grades"], this.headers);
  }
}

class GradeDetails {
  GradeUserModel studentModel;
  GradeDetailObject gradeDetailsObject;

  GradeDetails(this.studentModel, this.gradeDetailsObject);

  GradeDetails.map(dynamic obj, List<HeadersBean> headers) {
    this.studentModel = GradeUserModel.map(obj['schoolUser']);

    Map<String, dynamic> mapData = obj;
    print('keys ==> ${mapData.keys}');
    List<String> keys = mapData.keys.toList();
    for (String key in keys) {
      for (HeadersBean headersBean in headers) {
        if (headersBean != null &&
            headersBean.id == key &&
//            grades[grades.keys.first]['approvedMarks'] != null &&
//            grades[grades.keys.first]['marksObtained'] != null &&
//            grades[grades.keys.first]['status'] != null &&
            mapData[key]['schoolUser'] != null) {
          this.gradeDetailsObject =
              GradeDetailObject.map(mapData[key], headersBean);
        }
      }
    }
  }

  static List<GradeDetails> fromJsonArray(
      List<dynamic> json, List<HeadersBean> headers) {
    List<GradeDetails> list = json
        .map<GradeDetails>((json) => GradeDetails.map(json, headers))
        .toList();
    return list;
  }
}

class HeadersBean {
  String id;
  String name;
  String classId;
  dynamic weightage;
  String type;
  String colType;
  String sort;
  int maxMarks;

  HeadersBean(this.id, this.name, this.classId, this.weightage, this.type,
      this.colType, this.sort, this.maxMarks);

  HeadersBean.map(dynamic obj) {
    if (obj != null) {
      try {
        this.id = obj["id"];
        this.name = obj["name"];
        this.classId = obj["class"];
        this.weightage = obj["weightage"];
        this.type = obj["type"];
        this.colType = obj["colType"];
        this.sort = obj["sort"];
        this.maxMarks = obj["maxMarks"] == null ? 0 : obj["maxMarks"];
      } catch (e) {}
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["name"] = name;
    map["class"] = classId;
    map["weightage"] = weightage;
    map["type"] = type;
    map["colType"] = colType;
    map["sort"] = sort;
    map["maxMarks"] = maxMarks;
    return map;
  }

  static List<HeadersBean> fromJsonArray(List<dynamic> json) {
    List<HeadersBean> list =
        json.map<HeadersBean>((json) => HeadersBean.map(json)).toList();
    return list;
  }
}
