class Education {
    String educationId;
    String degree;
    String fieldOfStudy;
    String from;
    String school;
    String summary;
    String to;

    Education({this.educationId, this.degree, this.fieldOfStudy, this.from, this.school, this.summary, this.to});

    factory Education.fromJson(Map<String, dynamic> json) {
        return Education(
            educationId: json['_id'],
            degree: json['degree'], 
            fieldOfStudy: json['fieldOfStudy'], 
            from: json['from'], 
            school: json['school'], 
            summary: json['summary'], 
            to: json['to'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['_id'] = this.educationId;
        data['degree'] = this.degree;
        data['fieldOfStudy'] = this.fieldOfStudy;
        data['from'] = this.from;
        data['school'] = this.school;
        data['summary'] = this.summary;
        data['to'] = this.to;
        return data;
    }
}