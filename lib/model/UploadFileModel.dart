import 'dart:io';

import 'FileModel.dart';

class UploadFileModel {
  File localFileUrl;
  bool isDownloaded = false;
  int downloadProgress = 0;
  String serverFileURl;
  FileModel fileModel;

  UploadFileModel(File file) {
    this.localFileUrl = file;
  }
}
