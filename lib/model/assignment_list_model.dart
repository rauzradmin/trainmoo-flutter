import 'package:json_annotation/json_annotation.dart';
import 'package:trainmoo/model/Assessment.dart';
import 'package:trainmoo/model/Author.dart';
import 'package:trainmoo/model/School.dart';
import 'package:trainmoo/model/Task.dart';
import 'package:trainmoo/model/User.dart';




@JsonSerializable(nullable: true)
class GetAssignmentListModel {
  String id;
  String minified;
  String submittable;
  String type;
  String created_at;
  String updated_at;
  Author author;
  School school;
  User user;
  bool commentsDisabled;
  Assessment assessment;
  Task task;
  String graded;
  int solutions;
  int comments;
  int submissions;
  int views;
  int likes;
  bool anonymous;
  bool starred;
  bool liked;
  bool pending;
  String displayTime;

  GetAssignmentListModel(
      {this.id,
        this.minified,
        this.submittable,
        this.type,
        this.school,
        this.user,
        this.commentsDisabled,
        this.assessment,
        this.task,
        this.graded,
      this.solutions,
      this.views,
      this.likes,
      this.anonymous,
      this.starred,
      this.liked,
      this.pending,
      this.displayTime});

  factory GetAssignmentListModel.fromJson(Map<String, dynamic> json) {
    return GetAssignmentListModel(
      id: json['_id'],
      minified: json['minified'],
      submittable: json['submittable'],
      type: json['type'],
      commentsDisabled: json['commentsDisabled'],
      graded: json['graded'],
      solutions: json['solutions'],
      views: json['views'],
      likes: json['likes'],
      anonymous: json['anonymous'],
      starred: json['starred'],
      liked: json['liked'],
      pending: json['pending'],
      displayTime: json['displayTime'],
      school: json['school'] != null ? School.fromJson(json['school']) : null,
      user: json['user'] != null ? User.fromJson(json['user']) : null,
      assessment: json['assessment'] != null ? Assessment.fromJson(json['assessment']) : null,
      task: json['task'] != null ? Task.fromJson(json['task']) : null,

    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.id;
    data['minified'] = this.minified;
    data['submittable'] = this.submittable;
    data['type'] = this.type;
    data['commentsDisabled'] = this.commentsDisabled;
    data['graded'] = this.graded;
    data['solutions'] = this.solutions;
    data['views'] = this.views;
    data['likes'] = this.likes;
    data['anonymous'] = this.anonymous;
    data['starred'] = this.starred;
    data['liked'] = this.liked;
    data['pending'] = this.pending;
    data['displayTime'] = this.displayTime;

    if (this.school != null) {
      data['school'] = this.school.toJson();
    }
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (this.assessment != null) {
      data['assessment'] = this.assessment.toJson();
    }
    if (this.task != null) {
      data['task'] = this.task.toJson();
    }

    return data;
  }

  static List<GetAssignmentListModel> fromJsonArray(List<dynamic> json) {
    List<GetAssignmentListModel> bannerLists =
    json.map<GetAssignmentListModel>((json) => GetAssignmentListModel.fromJson(json)).toList();
    return bannerLists;
  }
}

