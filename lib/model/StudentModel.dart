class StudentModel {
  String id;
  dynamic V;
  String email;
  String updatedAt;
  String createdAt;
  String name;
  String type;
  String registerUsing;
  String school;
  UserBean user;
  bool isActive;
  List<dynamic> children;
  bool schoolAdmin;
  String bus;
  String mobile;
  String avatarThumbnail;
  String gender;
  String lastLoginAt;
  String status;
  bool isPresent = false;
  List<AccessModulesBean> accessModules;

  static List<StudentModel> fromJsonArray(List<dynamic> json) {
    List<StudentModel> list =
        json.map<StudentModel>((json) => StudentModel.map(json)).toList();
    print("This is List: ======>>>> $list");
    return list;
  }

  StudentModel(
      this.id,
      this.V,
      this.email,
      this.updatedAt,
      this.createdAt,
      this.name,
      this.type,
      this.registerUsing,
      this.school,
      this.user,
      this.isActive,
      this.children,
      this.schoolAdmin,
      this.bus,
      this.mobile,
      this.avatarThumbnail,
      this.gender,
      this.lastLoginAt,
      this.status,
      this.accessModules,
      this.isPresent);

  StudentModel.map(dynamic obj) {
    this.id = obj["_id"];
    this.V = obj["__V"] != null ? obj["__V"] : null;
    this.email = obj["email"];
    this.updatedAt = obj["updatedAt"] != null ? obj["updatedAt"] : null;
    this.createdAt = obj["createdAt"] != null ? obj["createdAt"] : null;
    this.name = obj["name"];
    this.type = obj["type"];
    this.registerUsing = obj["registerUsing"];
    this.school = obj["school"];
    this.user = UserBean.map(
        (obj["user"] is String) ? {"_id": obj["user"]} : obj["user"]);
    this.isActive = obj["isActive"];
    this.children = obj["children"];
    this.schoolAdmin = obj["schoolAdmin"];
    this.bus = obj["bus"];
    this.mobile = obj["mobile"];
    this.avatarThumbnail = obj["avatarThumbnail"];
    this.gender = obj["gender"];
    this.lastLoginAt = obj["lastLoginAt"];
    this.status = obj["status"];
    this.accessModules = AccessModulesBean.fromJsonArray(obj["accessModules"]);
  }

  StudentModel.mapData(dynamic obj) {
    this.id = obj["_id"];
    this.V = obj["__V"] != null ? obj["__V"] : null;
    this.email = obj["email"];
    this.updatedAt = obj["updatedAt"] != null ? obj["updatedAt"] : null;
    this.createdAt = obj["createdAt"] != null ? obj["createdAt"] : null;
    this.name = obj["name"];
    this.type = obj["type"];
    this.registerUsing = obj["registerUsing"];
    this.school = obj["school"];
    this.isActive = obj["isActive"];
    this.children = obj["children"];
    this.schoolAdmin = obj["schoolAdmin"];
    this.bus = obj["bus"];
    this.mobile = obj["mobile"];
    this.avatarThumbnail = obj["avatarThumbnail"];
    this.gender = obj["gender"];
    this.lastLoginAt = obj["lastLoginAt"];
    this.status = obj["status"];
  }

  StudentModel.fromJson(Map<String, dynamic> json) {
    id = json['_id'];
    V = json['__v'];
    email = json['email'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    name = json['name'];
    type = json['type'];
    registerUsing = json['registerUsing'];
    school = json['school'];
    user = json['user'];
    isActive = json['isActive'];

    schoolAdmin = json['schoolAdmin'];
    bus = json['bus'];
    mobile = json['mobile'];
    avatarThumbnail = json['avatarThumbnail'];
    gender = json['gender'];
    lastLoginAt = json['last_login_at'];
    status = json['status'];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.id;
    data['__v'] = this.V;
    data['email'] = this.email;
    data['updated_at'] = this.updatedAt;
    data['created_at'] = this.createdAt;
    data['name'] = this.name;
    data['type'] = this.type;
    data['registerUsing'] = this.registerUsing;
    data['school'] = this.school;
    data['isActive'] = this.isActive;
    data['schoolAdmin'] = this.schoolAdmin;
    data['bus'] = this.bus;
    data['mobile'] = this.mobile;
    data['avatarThumbnail'] = this.avatarThumbnail;
    data['gender'] = this.gender;
    data['last_login_at'] = this.lastLoginAt;
    data['status'] = this.status;

    return data;
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["_id"] = id;
    map["__V"] = V;
    map["email"] = email;
    map["updatedAt"] = updatedAt;
    map["createdAt"] = createdAt;
    map["name"] = name;
    map["type"] = type;
    map["registerUsing"] = registerUsing;
    map["school"] = school;
    map["user"] = user;
    map["isActive"] = isActive;
    map["children"] = children;
    map["schoolAdmin"] = schoolAdmin;
    map["bus"] = bus;
    map["mobile"] = mobile;
    map["avatarThumbnail"] = avatarThumbnail;
    map["gender"] = gender;
    map["lastLoginAt"] = lastLoginAt;
    map["status"] = status;
    map["accessModules"] = accessModules;
    return map;
  }
}

/// name : "attendance"
/// value : true

class AccessModulesBean {
  String name;
  bool value;

  AccessModulesBean(this.name, this.value);

  AccessModulesBean.map(dynamic obj) {
    this.name = obj["name"];
    this.value = obj["value"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["name"] = name;
    map["value"] = value;
    return map;
  }

  static List<AccessModulesBean> fromJsonArray(List<dynamic> json) {
    List<AccessModulesBean> list = json
        .map<AccessModulesBean>((json) => AccessModulesBean.map(json))
        .toList();
    return list;
  }
}

/// id : "5b371b6d6b994000216d1496"
/// password : "$2a$08$gEoWZbymD1/vNMYn09tmL.PpB/TE1YyZLFYN1CtmgSyaAvWLOxaZu"
/// email : "adrian.simonis@rauzrgmail.com"
/// v : 23
/// avatar : {"src":"https://s3.amazonaws.com/uifaces/faces/twitter/wtrsld/128.jpg","id":"5b371b6d6b994000216d149b","createdat":"2018-06-30T05:55:57.764Z","gdrive":false}
/// background : {"src":"http://res.cloudinary.com/trainmoo/image/upload/v1526982127/wo7ekeesvjozuawedzdy.jpg","id":"5b371b6d6b994000216d149c","createdat":"2018-06-30T05:55:57.765Z","gdrive":false}
/// mobile : "00913232323223"
/// lastactiveat : "2019-06-15T15:34:08.487Z"
/// badges : [{"src":"https://res.cloudinary.com/trainmoo/image/upload/v1535137027/beta/5b023ce61c2474001b8c963b/h0vgi7sel8cwco3rmxkn.png","description":"GOLD","badgeType":"POINTS","id":"5b82c2475e71dd0698306c33"}]
/// isMobileVerified : false
/// isEmailVerified : false
/// updatedat : "2019-06-15T15:34:08.492Z"
/// createdat : "2018-06-30T05:55:57.569Z"
/// userType : "Admin"
/// ssoLogin : false
/// googleLogin : false
/// skills : []
/// education : []
/// experience : []
/// trainingStyle : []
/// media : []
/// coords : {"lon":1,"lat":1}
/// gender : "not specified"
/// name : {"firstname":"Adrian","lastname":"Simonis"}
/// shortname : "Adrian S"
/// fullname : "Adrian Simonis"
/// isActive : false
/// id : "5b371b6d6b994000216d1496"

class UserBean {
  String id;
  String password;
  String email;
  int V;
  AvatarBean avatar;
  BackgroundBean background;
  String mobile;
  String lastActiveAt;
  List<BadgesBean> badges;
  bool isMobileVerified;
  bool isEmailVerified;
  String updatedAt;
  String createdAt;
  String userType;
  bool ssoLogin;
  bool googleLogin;
  List<dynamic> skills;
  List<dynamic> education;
  List<dynamic> experience;
  List<dynamic> trainingStyle;
  List<dynamic> media;
  CoordsBean coords;
  String gender;
  NameBean name;
  String shortname;
  String fullname;
  bool isActive;

  UserBean(
      this.id,
      this.password,
      this.email,
      this.V,
      this.avatar,
      this.background,
      this.mobile,
      this.lastActiveAt,
      this.badges,
      this.isMobileVerified,
      this.isEmailVerified,
      this.updatedAt,
      this.createdAt,
      this.userType,
      this.ssoLogin,
      this.googleLogin,
      this.skills,
      this.education,
      this.experience,
      this.trainingStyle,
      this.media,
      this.coords,
      this.gender,
      this.name,
      this.shortname,
      this.fullname,
      this.isActive);

  UserBean.map(dynamic obj) {
    try {
      if (obj != null && obj['_id'] != null) {
        this.id = obj["_id"];
        this.password = obj["password"];
        this.email = obj["email"];
        this.V = obj["__V"];
        this.avatar = AvatarBean.map(obj["avatar"]);
        this.background = BackgroundBean.map(obj["background"]);
        this.mobile = obj["mobile"];
        this.lastActiveAt = obj["lastActiveAt"];
        this.badges = BadgesBean.fromJsonArray(obj["badges"]);
        this.isMobileVerified = obj["isMobileVerified"];
        this.isEmailVerified = obj["isEmailVerified"];
        this.updatedAt = obj["updatedAt"];
        this.createdAt = obj["createdAt"];
        this.userType = obj["userType"];
        this.ssoLogin = obj["ssoLogin"];
        this.googleLogin = obj["googleLogin"];
        this.skills = obj["skills"];
        this.education = obj["education"];
        this.experience = obj["experience"];
        this.trainingStyle = obj["trainingStyle"];
        this.media = obj["media"];
        this.coords = CoordsBean.map(obj["coords"]);
        this.gender = obj["gender"];
        this.name = NameBean.map(obj["name"]);
        this.shortname = obj["shortname"];
        this.fullname = obj["fullname"];
        this.isActive = obj["isActive"];
      }
    } catch (e) {}
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["_id"] = id;
    map["password"] = password;
    map["email"] = email;
    map["__V"] = V;
    map["avatar"] = avatar;
    map["background"] = background;
    map["mobile"] = mobile;
    map["lastActiveAt"] = lastActiveAt;
    map["badges"] = badges;
    map["isMobileVerified"] = isMobileVerified;
    map["isEmailVerified"] = isEmailVerified;
    map["updatedAt"] = updatedAt;
    map["createdAt"] = createdAt;
    map["userType"] = userType;
    map["ssoLogin"] = ssoLogin;
    map["googleLogin"] = googleLogin;
    map["skills"] = skills;
    map["education"] = education;
    map["experience"] = experience;
    map["trainingStyle"] = trainingStyle;
    map["media"] = media;
    map["coords"] = coords;
    map["gender"] = gender;
    map["name"] = name;
    map["shortname"] = shortname;
    map["fullname"] = fullname;
    map["isActive"] = isActive;
    return map;
  }
}

/// firstname : "Adrian"
/// lastname : "Simonis"

class NameBean {
  String firstname;
  String lastname;

  NameBean(this.firstname, this.lastname);

  NameBean.map(dynamic obj) {
    if (obj != null) {
      this.firstname = obj["firstname"];
      this.lastname = obj["lastname"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["firstname"] = firstname;
    map["lastname"] = lastname;
    return map;
  }
}

/// lon : 1
/// lat : 1

class CoordsBean {
  int lon;
  int lat;

  CoordsBean(this.lon, this.lat);

  CoordsBean.map(dynamic obj) {
    if (obj != null) {
      this.lon = obj["lon"];
      this.lat = obj["lat"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["lon"] = lon;
    map["lat"] = lat;
    return map;
  }
}

/// src : "https://res.cloudinary.com/trainmoo/image/upload/v1535137027/beta/5b023ce61c2474001b8c963b/h0vgi7sel8cwco3rmxkn.png"
/// description : "GOLD"
/// badgeType : "POINTS"
/// id : "5b82c2475e71dd0698306c33"

class BadgesBean {
  String src;
  String description;
  String badgeType;
  String id;

  BadgesBean(this.src, this.description, this.badgeType, this.id);

  BadgesBean.map(dynamic obj) {
    if (obj != null) {
      this.src = obj["src"];
      this.description = obj["description"];
      this.badgeType = obj["badgeType"];
      this.id = obj["_id"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["src"] = src;
    map["description"] = description;
    map["badgeType"] = badgeType;
    map["_id"] = id;
    return map;
  }

  static List<BadgesBean> fromJsonArray(List<dynamic> json) {
    List<BadgesBean> list =
        json.map<BadgesBean>((json) => BadgesBean.map(json)).toList();
    return list;
  }
}

/// src : "http://res.cloudinary.com/trainmoo/image/upload/v1526982127/wo7ekeesvjozuawedzdy.jpg"
/// id : "5b371b6d6b994000216d149c"
/// createdat : "2018-06-30T05:55:57.765Z"
/// gdrive : false

class BackgroundBean {
  String src;
  String id;
  String createdAt;
  bool gdrive;

  BackgroundBean(this.src, this.id, this.createdAt, this.gdrive);

  BackgroundBean.map(dynamic obj) {
    if (obj != null) {
      this.src = obj["src"];
      this.id = obj["_id"];
      this.createdAt = obj["createdAt"];
      this.gdrive = obj["gdrive"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["src"] = src;
    map["_id"] = id;
    map["createdAt"] = createdAt;
    map["gdrive"] = gdrive;
    return map;
  }
}

/// src : "https://s3.amazonaws.com/uifaces/faces/twitter/wtrsld/128.jpg"
/// id : "5b371b6d6b994000216d149b"
/// createdat : "2018-06-30T05:55:57.764Z"
/// gdrive : false

class AvatarBean {
  String src;
  String id;
  String createdAt;
  bool gdrive;

  AvatarBean(this.src, this.id, this.createdAt, this.gdrive);

  AvatarBean.map(dynamic obj) {
    if (obj != null) {
      this.src = obj["src"];
      this.id = obj["_id"];
      this.createdAt = obj["createdAt"];
      this.gdrive = obj["gdrive"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["src"] = src;
    map["_id"] = id;
    map["createdAt"] = createdAt;
    map["gdrive"] = gdrive;
    return map;
  }
}
