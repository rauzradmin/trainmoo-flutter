class AttendanceClassModel {
  String className;
  String eventDate;
  String eventStart;
  String eventEnd;
  String eventTime;
  String eventDetails;
  String sessionStartTime;
  bool present;
  int totalNum;
  int presentNum;

  AttendanceClassModel(
      {this.className,
      this.eventStart,
      this.eventEnd,
      this.totalNum,
      this.presentNum,
      this.eventDate,
      this.eventDetails,
      this.eventTime,
      this.present,
      this.sessionStartTime});

  AttendanceClassModel.map(dynamic obj) {
    this.className = obj['class'];
    this.eventDate = obj['eventDate'];
    this.eventStart = obj['eventStart'];
    this.eventTime = obj['eventTime'];
    this.eventDetails = obj['eventDetails'];
    this.sessionStartTime = obj['sessionStartTime'];
    this.present = obj['present'];
    this.eventEnd = obj['eventEnd'];
    this.totalNum = obj['totalNum'];
    this.presentNum = obj['presentNum'];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map['class'] = className;
    map['eventStart'] = eventStart;
    map['eventEnd'] = eventEnd;
    map['totalNum'] = totalNum;
    map['presentNum'] = presentNum;
    map['eventDate'] = eventDate;
    map['eventDetails'] = eventDetails;
    map['sessionStartTime'] = sessionStartTime;
    map['present'] = present;
    map['eventTime'] = eventTime;
    return map;
  }

  static List<AttendanceClassModel> fromDatabaseJsonArray(List<dynamic> json) {
    List<AttendanceClassModel> list = json
        .map<AttendanceClassModel>((data) => AttendanceClassModel.map(data))
        .toList();
    return list;
  }
}
