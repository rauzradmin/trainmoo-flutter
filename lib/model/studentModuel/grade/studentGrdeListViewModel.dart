import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';

import 'StudentGradeModel.dart';

class StudentGradeListViewModel extends BaseModel {
  bool isFirstTime = false;
  List<StudentGradeModel> studentsGradeList = [];
  //String selectDroupId;
  var selectGroupDetails;
  String classId;

  init(String classId) async {
    if (!isFirstTime) {
      this.classId = classId;
      isFirstTime = true;
      getGradeList();
    }
  }

  void setGoup(groupData) {
    this.selectGroupDetails = groupData;
    notifyListeners();
  }

  void getGradeList() {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
        studentsGradeList = await API.getStudentGrade(classId);

        setState(ViewState.Idle);
      } else {
        showToast(AppLocalizations.of(context).internetMessage, context);
      }
      setState(ViewState.Idle);
    });
  }
}
