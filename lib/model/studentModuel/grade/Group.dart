import 'package:trainmoo/model/studentModuel/grade/Tests.dart';

class Group {
  String groupId;
  int maxMarks;
  String name;
  int score;
  List<Tests> tests;

  Group({this.groupId, this.maxMarks, this.name, this.tests, this.score});

  factory Group.fromJson(Map<String, dynamic> json) {
    return Group(
        groupId: json['_id'],
        maxMarks: json['maxMarks'],
        name: json['name'],
        score: json['score'],
        tests: json['tests'] != null
            ? (json['tests'] as List).map((i) => Tests.fromJson(i)).toList()
            : []);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.groupId;
    data['maxMarks'] = this.maxMarks;
    data['name'] = this.name;
    data['score'] = this.score;
    data['tests'] = this.tests;
    return data;
  }
}
