import 'Group.dart';

class GradeClassModel {
    String classId;
    bool displayPercentileToStudents;
    bool displayTotalToStudents;
    List<Group> groups;
    int maxMarks;
    String name;
    String program;
    int score;
    String type;

    GradeClassModel({this.classId, this.displayPercentileToStudents, this.displayTotalToStudents, this.groups, this.maxMarks, this.name, this.program, this.score, this.type});

    factory GradeClassModel.fromJson(Map<String, dynamic> json) {
        return GradeClassModel(
            classId: json['_id'],
            displayPercentileToStudents: json['displayPercentileToStudents'], 
            displayTotalToStudents: json['displayTotalToStudents'], 
            groups: json['groups'] != null ? (json['groups'] as List).map((i) => Group.fromJson(i)).toList() : null, 
            maxMarks: json['maxMarks'], 
            name: json['name'], 
            program: json['program'], 
            score: json['score'], 
            type: json['type'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['_id'] = this.classId;
        data['displayPercentileToStudents'] = this.displayPercentileToStudents;
        data['displayTotalToStudents'] = this.displayTotalToStudents;
        data['maxMarks'] = this.maxMarks;
        data['name'] = this.name;
        data['program'] = this.program;
        data['score'] = this.score;
        data['type'] = this.type;
        if (this.groups != null) {
            data['groups'] = this.groups.map((v) => v.toJson()).toList();
        }
        return data;
    }
}