class Tests {
  String id;
  int maxMarks;
  String name;
  String score;
  String testDate;

  Tests({this.id, this.maxMarks, this.name, this.testDate, this.score});

  factory Tests.fromJson(Map<String, dynamic> json) {
    return Tests(
        id: json['_id'],
        maxMarks: json['maxMarks'],
        name: json['name'],
        score: json.containsKey("score") ? json['score'].toString() : "_",
        testDate: json['testDate']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.id;
    data['maxMarks'] = this.maxMarks;
    data['name'] = this.name;
    data['score'] = this.score;
    data['testDate'] = this.testDate;
    return data;
  }
}
