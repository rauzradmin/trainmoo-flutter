import 'Classe.dart';

class StudentGradeModel {
  String gradeID;
  List<GradeClassModel> classes;
  bool displayPercentileToStudents;
  bool displayTotalToStudents;
  int maxMarks;
  String name;
  int score;
  String type;

  StudentGradeModel({
    this.gradeID,
    this.classes,
    this.displayPercentileToStudents,
    this.displayTotalToStudents,
    this.maxMarks,
    this.name,
    this.score,
    this.type,
  });

  static List<StudentGradeModel> fromJsonArray(List<dynamic> json) {
    List<StudentGradeModel> list = json
        .map<StudentGradeModel>((json) => StudentGradeModel.map(json))
        .toList();
    return list;
  }

  factory StudentGradeModel.fromJson(Map<String, dynamic> json) {
    return StudentGradeModel(
      gradeID: json['_id'],
      classes: json['classes'] != null
          ? (json['classes'] as List)
              .map((i) => GradeClassModel.fromJson(i))
              .toList()
          : null,
      displayPercentileToStudents: json['displayPercentileToStudents'],
      displayTotalToStudents: json['displayTotalToStudents'],
      maxMarks: json['maxMarks'],
      name: json['name'],
      score: json['score'],
      type: json['type'],
    );
  }

  StudentGradeModel.map(dynamic obj) {
    this.gradeID = obj['_id'];
    this.displayPercentileToStudents = obj['displayPercentileToStudents'];
    this.displayTotalToStudents = obj['displayTotalToStudents'];
    this.maxMarks = obj['maxMarks'];
    this.name = obj['name'];
    this.score = obj['score'];
    this.type = obj['type'];

    if (this.classes != null) {
      obj['classes'] = this.classes.map((v) => v.toJson()).toList();
    }
  }
}
