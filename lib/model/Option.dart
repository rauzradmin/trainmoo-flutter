class Option {
  String optionId;
  bool answer;
  List<Object> files;
  bool isCode;
  String seqno;
  String text;

  Option(
      {this.optionId,
      this.answer,
      this.files,
      this.isCode,
      this.seqno,
      this.text});

  factory Option.fromJson(Map<String, dynamic> json) {
    return Option(
      optionId: json['_id'],
      answer: json['answer'],
      isCode: json['isCode'],
      seqno: json['seqno'],
      text: json['text'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.optionId;
    data['answer'] = this.answer;
    data['isCode'] = this.isCode;
    data['seqno'] = this.seqno;
    data['text'] = this.text;

    return data;
  }

  static List<Option> fromJsonArray(List<dynamic> json) {
    List<Option> list =
        json.map<Option>((json) => Option.fromJson(json)).toList();
    return list;
  }
}
