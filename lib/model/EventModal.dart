class EventModel {
  String id;
  String classId;
  String title;

  EventModel(this.id, this.title, this.classId);

  EventModel.map(dynamic obj) {
    this.id = obj["event"];
    this.classId = obj["classid"];
    this.title = obj["title"];
  }
  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["event"] = id;
    map["classid"] = classId;
    map["title"] = title;
  }

  static List<EventModel> fromJsonArray(List<dynamic> json) {
    List<EventModel> list =
        json.map<EventModel>((json) => EventModel.map(json)).toList();
    return list;
  }

  static List<EventModel> fromDatabaseJsonArray(List<dynamic> json) {
    print('converting array data ==> ${json[0]['data']}');
    List<EventModel> list =
        json.map<EventModel>((data) => EventModel.map(data['data'])).toList();
    return list;
  }
}
