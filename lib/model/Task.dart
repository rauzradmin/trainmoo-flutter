class Task
{
  int title;
  String dueby;
  String maxmarks;
  String additionalMarks;
  String test;

  Task({this.title, this.dueby,this.maxmarks,this.additionalMarks,this.test});

  factory Task.fromJson(Map<String, dynamic> json) {
    return Task(
      title: json['title'],
      dueby: json['dueby'],
      maxmarks: json['maxmarks'],
      additionalMarks: json['additionalMarks'],
      test: json['test'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['dueby'] = this.dueby;
    data['maxmarks'] = this.maxmarks;
    data['additionalMarks'] = this.additionalMarks;
    data['test'] = this.test;

    return data;
  }

  static List<Task> fromJsonArray(List<dynamic> json) {
    List<Task> bannerLists =
    json.map<Task>((json) => Task.fromJson(json)).toList();
    return bannerLists;
  }
}