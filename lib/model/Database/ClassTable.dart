class ClassTable {
  String id;
  String data;

  ClassTable({this.id, this.data});

  factory ClassTable.fromJson(Map<String, dynamic> json) {
    return ClassTable(id: json['id'], data: json['data']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['data'] = this.data;
    return data;
  }

  ClassTable.map(dynamic obj) {
    id = obj["id"];
    data = obj["data"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map['id'] = this.id;
    map['data'] = this.data;
    return map;
  }
}
