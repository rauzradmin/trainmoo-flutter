import 'dart:async';
import 'dart:convert';
import 'dart:io' as io;

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:trainmoo/model/Database/AttendanceTable.dart';
import 'package:trainmoo/model/Database/ClassTable.dart';
import 'package:trainmoo/model/Database/EventTable.dart';
import 'package:trainmoo/model/Database/StudentTable.dart';
import 'package:trainmoo/model/SessionModel.dart';
import 'package:trainmoo/model/StudentModel.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:intl/intl.dart';
import '../ProgramClassesModel.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;
  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "trainmoo.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return theDb;
  }

  void _onCreate(Database db, int version) async {
    await db.execute(
        "CREATE TABLE IF NOT EXISTS ClassTable(id TEXT PRIMARY KEY,data TEXT)");

    await db.execute(
        "CREATE TABLE IF NOT EXISTS EventTable(id TEXT PRIMARY KEY,classId TEXT,data TEXT)");

    await db.execute(
        "CREATE TABLE IF NOT EXISTS StudentTable(id TEXT PRIMARY KEY, data TEXT )");

    await db.execute(
        "CREATE TABLE IF NOT EXISTS AttendanceTable(id INT PRIMARY KEY, data TEXT, date TEXT, class_id TEXT, event_id TEXT, submitted INT )");
  }

  Future<void> saveAttendanceData(AttendanceTable attendance) async {
    var dbClient = await db;
    try {
      int id = await dbClient.insert("AttendanceTable", attendance.toJson());
      print('Database insert for the attendance $id');
    } catch (error) {
      throw Exception('DbBase.cleanDatabase: ' + error.toString());
    }
  }

  Future<List<AttendanceTable>> getAttendanceData() async {
    var dbClient = await db;
    List<dynamic> list =
        await dbClient.rawQuery('SELECT * FROM AttendanceTable');
    print('Attendance data length ==> ${list.length}');

    List<AttendanceTable> studentList = [];
    if (list != null && list.length > 0) {
      studentList = AttendanceTable.fromJsonArray(list);
    }
    return studentList;
  }

  // Future<void> getEventData(classList, length, i) async {
  //   if (classList.length > i) {
  //     print("classList[i]['_id'] ${classList[i]['_id']}");
  //     var eventList =
  //         await API.getEventList(DateTime.now(), classList[i]['_id']);
  //     print("eventList : ${eventList.length}");
  //     if (eventList.length > 0) {
  //       print("eventList inserted ");
  //       saveEventData(eventList, classList[i]['_id']);
  //     }
  //     getEventData(classList, classList.length, i + 1);
  //   }
  // }

  Future<void> saveClassData(List<dynamic> classes) async {
    var dbClient = await db;
    try {
      var batch = dbClient.batch();
      batch.delete('ClassTable');
      //getEventData(classes, classes.length, 0);
      for (dynamic dynamicClassData in classes) {
        batch.insert(
            "ClassTable",
            new ClassTable(
                    id: dynamicClassData['_id'],
                    data: jsonEncode(dynamicClassData))
                .toMap());
      }
//        int res = await dbClient.insert("ClassTable", classTable.toMap());
      await batch.commit(noResult: true);
    } catch (error) {
      throw Exception('DbBase.cleanDatabase: ' + error.toString());
    }
  }

  Future<void> saveEventData(List<SessionModel> events) async {
    var dbClient = await db;
    try {
      var batch = dbClient.batch();
      batch.delete('EventTable');
      var eventIdList = [];
      for (SessionModel dynamicEventData in events) {
        print("eventDBitem : ${dynamicEventData.toMap()}");
        if (eventIdList.indexOf(dynamicEventData.event) == -1) {
          batch.insert(
              "EventTable",
              new EventTable(
                      id: dynamicEventData.event,
                      classId: dynamicEventData.classId,
                      data: jsonEncode(dynamicEventData.toMap()))
                  .toMap());
          eventIdList.add(dynamicEventData.event);
        }
      }
//        int res = await dbClient.insert("ClassTable", classTable.toMap());
      await batch.commit(noResult: true);
    } catch (error) {
      throw Exception('DbBase.cleanDatabase: ' + error.toString());
    }
  }

  Future<bool> isClassAvailable(String classId) async {
    var dbClient = await db;
    List<Map> list = await dbClient
        .rawQuery('SELECT * FROM ClassTable WHERE id = ?', [classId]);
    if (list.length > 0 && list[0]["resId"] == classId)
      return true;
    else
      return false;
  }

  Future<int> saveStudentData(dynamic data, String classId) async {
    var dbClient = await db;
    StudentTable studentTable =
        new StudentTable(id: classId, data: jsonEncode(data));
    int res = 0;
    try {
      res = await dbClient.insert("StudentTable", studentTable.toMap());
    } catch (e) {
      res = await dbClient.update("StudentTable", studentTable.toMap(),
          where: "id = ?", whereArgs: <String>[studentTable.id]);
    }
    return res;
  }

  Future<List<ProgramClassesModel>> getClasses() async {
    var dbClient = await db;
    List<dynamic> list = await dbClient.rawQuery('SELECT * FROM ClassTable');
    List<dynamic> classObjList = [];
    {
      for (dynamic dynamicObj in list) {
        classObjList.add(jsonDecode(dynamicObj['data']));
      }
    }
    List<ProgramClassesModel> dealList =
        ProgramClassesModel.fromJsonArray(classObjList);

    List<ProgramClassesModel> classList = [];
    if (dealList.length > 0) {
      for (ProgramClassesModel classesModel in dealList) {
        if (classesModel.users.student > 0 && classesModel.type == "class") {
          classList.add(classesModel);
        }
      }
    }
    return classList;
  }

  Future<List<dynamic>> getEvent(dateTime, classId) async {
    print("classId $classId");
    var dbClient = await db;
    var list = await dbClient.rawQuery('SELECT * FROM EventTable');
    var eventList = [];
    print("event length : ${list}");
    if (list != null && list.length > 0) {
      for (var event in list) {
        var eventData = jsonDecode(event['data']);
        var startDate = DateTime.parse(eventData['eventDate']);
        // var endDate = DateTime.parse(eventData['eventEndDate']);

        var d1 = DateFormat("dd-MMM-yyyy")
            .format(DateTime.parse(eventData['eventDate']));
        // var d3 = DateFormat("dd-MMM-yyyy")
        //     .format(DateTime.parse(eventData['eventEndDate']));
        var d2 = DateFormat("dd-MMM-yyyy").format(dateTime);
        print(
            "eventDate : ${d1} date 2 $d2  startDate :$startDate  endDate : ${eventData['eventEndDate']}");
        if (d2.compareTo(d1) == 0) {
          eventList.add(eventData);
        } else {
          print('false');
        }
        print("eventDate : ${eventData['eventDate']}");
        //if(eventData['eventDate'])

      }
    }
    return eventList;
  }

  Future<List<StudentModel>> getStudents(String id) async {
    var dbClient = await db;
    List<dynamic> list = await dbClient
        .rawQuery('SELECT * FROM StudentTable WHERE id = ?', [id]);
    List<StudentModel> studentList = [];
    if (list != null && list.length > 0) {
      studentList = StudentModel.fromJsonArray(jsonDecode(list[0]['data']));
    }

    return studentList;
  }

  Future<void> deleteAttendanceFromTable(int id) async {
    var dbClient = await db;
    await dbClient.rawQuery('DELETE FROM AttendanceTable WHERE id = ?', [id]);
    return;
  }
}
