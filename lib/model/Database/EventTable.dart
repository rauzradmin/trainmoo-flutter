class EventTable {
  String id;
  String classId;
  String data;

  EventTable({this.id, this.classId, this.data});

  factory EventTable.fromJson(Map<String, dynamic> json) {
    return EventTable(
        id: json['id'], classId: json['classId'], data: json['data']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['classId'] = this.classId;
    data['data'] = this.data;
    return data;
  }

  EventTable.map(dynamic obj) {
    id = obj["id"];
    data = obj["data"];
    classId = obj['classId'];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map['id'] = this.id;
    map['data'] = this.data;
    map['classId'] = this.classId;
    return map;
  }
}
