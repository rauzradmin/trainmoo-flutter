class AttendanceTable {
  int id;
  String data;
  String date;
  String classID;
  String eventId;
  int isSubmitted;

  AttendanceTable(
      {this.id,
      this.data,
      this.date,
      this.classID,
      this.eventId,
      this.isSubmitted});

  factory AttendanceTable.fromJson(Map<String, dynamic> json) {
    return AttendanceTable(
        id: json['id'],
        data: json['data'],
        date: json['date'],
        classID: json['class_id'],
        eventId: json['event_id'],
        isSubmitted: json['submitted'] as int);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['data'] = this.data;
    data['date'] = this.date;

    data['class_id'] = this.classID;
    data['event_id'] = this.eventId;
    data['submitted'] = this.isSubmitted;
    return data;
  }

  AttendanceTable.map(dynamic obj) {
    id = obj["id"];
    data = obj["data"];
    date = obj["date"];

    classID = obj["class_id"];
    eventId = obj["event_id"];
    isSubmitted = obj["submitted"];
  }

  static List<AttendanceTable> fromJsonArray(List<dynamic> json) {
    List<AttendanceTable> bannerLists = json
        .map<AttendanceTable>((json) => AttendanceTable.fromJson(json))
        .toList();
    return bannerLists;
  }
}
