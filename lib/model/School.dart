class School
{
  int id;
  String name;

  School({this.id, this.name});

  factory School.fromJson(Map<String, dynamic> json) {
    return School(
      id: json['_id'],
      name: json['name'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;

    return data;
  }

  static List<School> fromJsonArray(List<dynamic> json) {
    List<School> bannerLists =
    json.map<School>((json) => School.fromJson(json)).toList();
    return bannerLists;
  }
}