import 'package:flutter/cupertino.dart';
import 'package:trainmoo/model/LinkPreview.dart';
import 'package:trainmoo/ui/util/util.dart';

import 'FileModel.dart';

class MaterialModel {
  String id;
  bool minified;
  String type;
  String title;
  Color color;
  SchoolBean school;
  ClassBean classBean;
  String text;
  bool submittable;
  String createdAt;
  int createTimeStamp;
  String updatedAt;
  AuthorBean author;
  SubmissionDetailsBean submissionDetails;
  List<dynamic> users;
  LinkPreview linkPreview;
  PollBean poll;
  bool commentsDisabled;
  AssessmentBean assessment;
  TaskBean task;
  bool isCode;
  bool pinned;
  bool locked;
  List<dynamic> replies;
  int graded;
  int solutions;
  int comments;
  int submissions;
  int views;
  int likes;
  List<FileModel> files;
  bool anonymous;
  int V;
  bool starred;
  bool liked;
  int pending;
  String displayTime;

  MaterialModel(
      this.id,
      this.minified,
      this.type,
      this.school,
      this.classBean,
      this.text,
      this.title,
      this.submittable,
      this.createdAt,
      this.createTimeStamp,
      this.updatedAt,
      this.author,
      this.submissionDetails,
      this.users,
      this.poll,
      this.commentsDisabled,
      this.assessment,
      this.task,
      this.isCode,
      this.pinned,
      this.locked,
      this.replies,
      this.graded,
      this.solutions,
      this.comments,
      this.submissions,
      this.views,
      this.likes,
      this.files,
      this.anonymous,
      this.V,
      this.starred,
      this.liked,
      this.displayTime,
      this.pending,
      this.linkPreview);

  MaterialModel.map(dynamic obj) {
    // print("submissionDetails ----------->>> ${obj["submissionDetails"]}");
    // if (obj["class"] != null) {
    //   print("class null object id :====> ${obj["_id"]}");
    // }
    this.id = obj["_id"];
    this.minified = obj["minified"];
    this.type = obj["type"];
    this.linkPreview =
        obj["linkPreview"] == null ? null : LinkPreview.map(obj["linkPreview"]);

    this.school = SchoolBean.map(obj["school"]);
    this.classBean = obj["class"] != null ? ClassBean.map(obj["class"]) : null;
    this.text = obj["text"];
    this.title = obj['task'] != null ? obj['task']['title'] : "";
    this.submittable = obj["submittable"];
    this.createdAt = obj["created_at"];
    this.createTimeStamp =
        Utility.getServerDateTimeWithOffset(obj["created_at"])
            .millisecondsSinceEpoch;
    this.updatedAt = obj["updated_at"];
    this.author = AuthorBean.map(obj["author"]);
    this.submissionDetails =
        SubmissionDetailsBean.map(obj["submissionDetails"]);
    this.users = obj["users"];
    this.poll = PollBean.map(obj["poll"]);
    this.commentsDisabled = obj["commentsDisabled"];
    this.assessment = obj["assessment"] == null
        ? AssessmentBean(false, false, false, 0)
        : AssessmentBean.map(obj["assessment"]);
    this.task = TaskBean.map(obj["task"]);
    this.isCode = obj["isCode"];
    this.pinned = obj["pinned"];
    this.locked = obj["locked"];
    this.replies = obj["replies"];
    this.graded = obj["graded"];
    this.solutions = obj["solutions"];
    this.comments = obj["comments"];
    this.submissions = obj["submissions"];
    this.views = obj["views"];
    this.likes = obj["likes"];
    this.files = FileModel.fromJsonArray(obj["files"]);
    this.anonymous = obj["anonymous"];
    this.V = obj["V"];
    this.starred = obj["starred"];
    this.liked = obj["liked"];
    this.displayTime = obj["displayTime"];
    this.pending = obj["pending"] == null ? 0 : obj["pending"] as int;
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["_id"] = id;
    map["minified"] = minified;
    map["type"] = type;
    map["school"] = school;
    map["class"] = classBean;
    map["text"] = text;
    map['title'] = title;
    map["submittable"] = submittable;
    map["createdAt"] = createdAt;
    map["updatedAt"] = updatedAt;
    map["author"] = author;
    map["submissionDetails"] = submissionDetails;
    map["users"] = users;
    map["poll"] = poll;
    map["commentsDisabled"] = commentsDisabled;
    map["assessment"] = assessment;
    map["task"] = task;
    map["isCode"] = isCode;
    map["pinned"] = pinned;
    map["locked"] = locked;
    map["replies"] = replies;
    map["graded"] = graded;
    map["solutions"] = solutions;
    map["comments"] = comments;
    map["submissions"] = submissions;
    map["views"] = views;
    map["likes"] = likes;
    map["files"] = files;
    map["anonymous"] = anonymous;
    map["V"] = V;
    map["starred"] = starred;
    map["liked"] = liked;
    map["displayTime"] = displayTime;
    return map;
  }

  static List<MaterialModel> fromJsonArray(List<dynamic> json) {
    List<MaterialModel> list =
        json.map<MaterialModel>((json) => MaterialModel.map(json)).toList();
    return list;
  }
}

/// additionalMarks : 0
/// additionalMaxMarks : 0

class TaskBean {
  int additionalMarks;
  int additionalMaxMarks;
  String start;
  String dueby;
  String startTime;
  String duebyTime;

  TaskBean(this.additionalMarks, this.additionalMaxMarks, this.start,
      this.dueby, this.startTime, this.duebyTime);

  TaskBean.map(dynamic obj) {
    this.additionalMarks = obj == null ? 0 : obj["additionalMarks"] as int;
    this.additionalMaxMarks =
        obj == null ? 0 : obj["additionalMaxMarks"] as int;
    this.start = obj != null && obj.containsKey('start') ? obj["start"] : "";
    this.dueby = obj != null && obj.containsKey('dueby') ? obj["dueby"] : "";
    this.startTime = obj != null && obj.containsKey('start')
        ? Utility.getTimeOfSession(obj["start"])
        : "";
    this.duebyTime = obj != null && obj.containsKey('dueby')
        ? Utility.getTimeOfSession(obj["dueby"])
        : "";
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["additionalMarks"] = additionalMarks;
    map["additionalMaxMarks"] = additionalMaxMarks;
    map['start'] = start;
    map['dueby'] = dueby;
    map['startTime'] = startTime;
    map['duebyTime'] = duebyTime;
    return map;
  }
}

/// passageView : false
/// random : true
/// retake : false
/// attempts : 1

class AssessmentBean {
  bool passageView;
  bool random;
  bool retake;
  int attempts;

  AssessmentBean(this.passageView, this.random, this.retake, this.attempts);

  AssessmentBean.map(dynamic obj) {
    this.passageView = obj["passageView"];
    this.random = obj["random"];
    this.retake = obj["retake"];
    this.attempts = obj["attempts"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["passageView"] = passageView;
    map["random"] = random;
    map["retake"] = retake;
    map["attempts"] = attempts;
    return map;
  }
}

/// options : []

class PollBean {
  List<dynamic> options;

  PollBean(this.options);

  PollBean.map(dynamic obj) {
    this.options = obj == null ? [] : obj["options"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["options"] = options;
    return map;
  }
}

/// results : []

class SubmissionDetailsBean {
  List<dynamic> results;

  SubmissionDetailsBean(this.results);

  SubmissionDetailsBean.map(dynamic obj) {
    this.results = obj == null ? [] : obj["results"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["results"] = results;
    return map;
  }
}

/// name : {"firstname":"pranav","lastname":"singhania"}
/// avatar : {"src":"https://s3.amazonaws.com/uifaces/faces/twitter/magoo04/128.jpg","id":"5b371b6c6b994000216d1452","createdat":"2018-06-30T05:55:56.523Z","gdrive":false}
/// id : "5b371b6c6b994000216d144e"

class AuthorBean {
  NameBean name;
  AvatarBean avatar;
  String id;

  AuthorBean(this.name, this.avatar, this.id);

  AuthorBean.map(dynamic obj) {
    this.name = NameBean.map(obj["name"]);
    this.avatar = AvatarBean.map(obj["avatar"]);
    this.id = obj["_id"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["name"] = name;
    map["avatar"] = avatar;
    map["_id"] = id;
    return map;
  }
}

/// src : "https://s3.amazonaws.com/uifaces/faces/twitter/magoo04/128.jpg"
/// id : "5b371b6c6b994000216d1452"
/// createdat : "2018-06-30T05:55:56.523Z"
/// gdrive : false

class AvatarBean {
  String src;
  String id;
  String createdAt;
  bool gdrive;

  AvatarBean(this.src, this.id, this.createdAt, this.gdrive);

  AvatarBean.map(dynamic obj) {
    this.src = obj["src"];
    this.id = obj["_id"];
    this.createdAt = obj["createdAt"];
    this.gdrive = obj["gdrive"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["src"] = src;
    map["_id"] = id;
    map["createdAt"] = createdAt;
    map["gdrive"] = gdrive;
    return map;
  }
}

/// firstname : "pranav"
/// lastname : "singhania"

class NameBean {
  String firstname;
  String lastname;

  NameBean(this.firstname, this.lastname);

  NameBean.map(dynamic obj) {
    this.firstname = obj["firstname"];
    this.lastname = obj["lastname"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["firstname"] = firstname;
    map["lastname"] = lastname;
    return map;
  }
}

/// id : "5bef311b53b74d006742bca8"
/// name : "Project ML"
/// users : {"student":7,"teacher":2}

class ClassBean {
  String id;
  String name;
  UsersBean users;

  ClassBean(this.id, this.name, this.users);

  ClassBean.map(dynamic obj) {
    this.id = obj["_id"];
    this.name = obj["name"];
    this.users = UsersBean.map(obj["users"]);
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["_id"] = id;
    map["name"] = name;
    map["users"] = users;
    return map;
  }
}

/// student : 7
/// teacher : 2

class UsersBean {
  int student;
  int teacher;
  int admin;

  UsersBean(this.student, this.teacher, this.admin);

  UsersBean.map(dynamic obj) {
    this.student = obj["student"];
    this.teacher = obj["teacher"];
    this.admin = obj['admin'] == null ? 0 : obj['admin'] as int;
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["student"] = student;
    map["teacher"] = teacher;
    return map;
  }
}

/// id : "5b371b6c6b994000216d1457"
/// name : "Modern University"

class SchoolBean {
  String id;
  String name;

  SchoolBean(this.id, this.name);

  SchoolBean.map(dynamic obj) {
    this.id = obj["_id"];
    this.name = obj["name"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["name"] = name;
    return map;
  }
}
