import 'GetCommentModel.dart';
import 'RepliesModel.dart';
import 'StudentModel.dart';

class QuizUserModel {
  List<StudentModel> submittedUsers;
  List<StudentModel> pendingUsers;
  List<RepliesModel> replies;
  List<Replies> repliesData;

  QuizUserModel(this.submittedUsers, this.pendingUsers, this.replies);

  QuizUserModel.map(dynamic obj) {
    this.submittedUsers = StudentModel.fromJsonArray(obj["submittedUsers"]);
    this.pendingUsers = StudentModel.fromJsonArray(obj["pendingUsers"]);
    this.replies = RepliesModel.fromJsonArray(obj['replies']);
    if (obj['replies'] != null) {
      repliesData = new List<Replies>();
      obj['replies'].forEach((v) {
        repliesData.add(new Replies.fromJson(v));
      });
    }

  }
}
