import 'FileModel.dart';
import 'GetCommentModel.dart';

class RepliesModel {
  String id;
  bool minified;
  ParentBean parent;
  String type;
  String responseType;
  String school;
  String classID;
  String text;
  bool submittable;
  String createdAt;
  String updatedAt;
  AuthorBean author;
  SubmissionDetailsBean submissionDetails;
  List<dynamic> users;
  bool commentsDisabled;
  TaskBean task;
  bool isCode;
  bool pinned;
  bool locked;
  List<dynamic> replies;
  int graded;
  int solutions;
  int comments;
  int submissions;
  int views;
  int likes;
  Assessment assessment;
  List<Replies> repliesData;
  List<FileModel> files;
  bool anonymous;
  int V;
  int marks;

  RepliesModel(
      this.id,
      this.minified,
      this.parent,
      this.type,
      this.responseType,
      this.school,
      this.classID,
      this.text,
      this.submittable,
      this.createdAt,
      this.updatedAt,
      this.author,
      this.submissionDetails,
      this.users,
      this.commentsDisabled,
      this.task,
      this.isCode,
      this.pinned,
      this.locked,
      this.replies,
      this.graded,
      this.solutions,
      this.comments,
      this.submissions,
      this.assessment,
      this.views,
      this.likes,
      this.files,
      this.anonymous,
      this.marks,
      this.V);

  RepliesModel.map(dynamic obj) {
    print("value of mark =========> ${obj["assessment"]}");
    if (obj != null) {
      this.id = obj["_id"];
      this.minified = obj["minified"];
      this.parent = ParentBean.map(obj["parent"]);
      this.type = obj["type"];
      this.responseType = obj["responseType"];
      this.school = obj["school"];
      this.classID = obj["class"];
      this.text = obj["text"];
      this.submittable = obj["submittable"];
      this.createdAt = obj["created_at"];
      this.updatedAt = obj["updated_at"];
      this.author = AuthorBean.map(obj["author"]);
      this.submissionDetails =
          SubmissionDetailsBean.map(obj["submissionDetails"]);
      this.users = obj["users"];
      this.commentsDisabled = obj["commentsDisabled"];
      this.task = TaskBean.map(obj["task"]);
      this.isCode = obj["isCode"];
      this.pinned = obj["pinned"];
      this.locked = obj["locked"];
      this.replies = obj["replies"];
      this.graded = obj["graded"];
      this.solutions = obj["solutions"];
      this.comments = obj["comments"];
      this.submissions = obj["submissions"];
      this.views = obj["views"];
      this.likes = obj["likes"];
      this.assessment = obj['assessment'] != null
          ? new Assessment.fromJson(obj['assessment'])
          : null;
      this.files = FileModel.fromJsonArray(obj["files"]);
      this.anonymous = obj["anonymous"];
      this.V = obj["__v"];
      this.marks = obj["assessment"] != null
          ? obj["assessment"]["correct"] != null
              ? obj["assessment"]["correct"] * obj["parent"]["task"]["maxmarks"]
              : 0
          : 0;
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["_id"] = id;
    map["minified"] = minified;
    map["parent"] = parent;
    map["type"] = type;
    map["responseType"] = responseType;
    map["school"] = school;
    map["class"] = classID;
    map["text"] = text;
    map["submittable"] = submittable;
    map["created_at"] = createdAt;
    map["updated_at"] = updatedAt;
    map["author"] = author;
    map["submissionDetails"] = submissionDetails;
    map["users"] = users;
    map["commentsDisabled"] = commentsDisabled;
    map["task"] = task;
    map["isCode"] = isCode;
    map["pinned"] = pinned;
    map["locked"] = locked;
    map["replies"] = replies;
    map["graded"] = graded;
    map["solutions"] = solutions;
    map["comments"] = comments;
    map["submissions"] = submissions;
    map["views"] = views;
    map["likes"] = likes;
    map["files"] = files;
    map['assessment'] = this.assessment;
    map["anonymous"] = anonymous;
    map["__v"] = V;
    map["marks"] = marks;
    if (map['replies'] != null) {
      repliesData = new List<Replies>();
      map['replies'].forEach((v) {
        repliesData.add(new Replies.fromJson(v));
      });
    }
    return map;
  }

  static List<RepliesModel> fromJsonArray(List<dynamic> json) {
    if (json == null) return [];
    List<RepliesModel> list =
        json.map<RepliesModel>((json) => RepliesModel.map(json)).toList();
    return list;
  }
}

class ParentBean {
  String id;
  TaskBean task;
  AuthorBean author;

  ParentBean(this.id, this.task, this.author);

  ParentBean.map(dynamic obj) {
    if (obj != null) {
      this.id = obj["_id"];
      this.task = TaskBean.map(obj["task"]);
      this.author = AuthorBean.map(obj["author"]);
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["_id"] = id;
    map["task"] = task;
    map["author"] = author;
    return map;
  }
}

class AuthorBean {
  String id;
  String password;
  String email;
  bool isMobileVerified;
  bool isEmailVerified;
  String updatedAt;
  String createdAt;
  String userType;
  bool googleLogin;
  List<String> skills;
  List<EducationBean> education;
  List<ExperienceBean> experience;
  List<dynamic> trainingStyle;
  List<MediaBean> media;
  CoordsBean coords;
  NameBean name;
  int V;
  AddressBean address;
  AvatarBean avatar;
  BackgroundBean background;
  String headline;
  String summary;
  dynamic dob;
  String gender;
  String lastActiveAt;
  String mobile;
  List<dynamic> badges;

  AuthorBean(
      this.id,
      this.password,
      this.email,
      this.isMobileVerified,
      this.isEmailVerified,
      this.updatedAt,
      this.createdAt,
      this.userType,
      this.googleLogin,
      this.skills,
      this.education,
      this.experience,
      this.trainingStyle,
      this.media,
      this.coords,
      this.name,
      this.V,
      this.address,
      this.avatar,
      this.background,
      this.headline,
      this.summary,
      this.dob,
      this.gender,
      this.lastActiveAt,
      this.mobile,
      this.badges);

  AuthorBean.map(dynamic obj) {
    if (obj != null) {
      this.id = obj["_id"];
      this.password = obj["password"];
      this.email = obj["email"];
      this.isMobileVerified = obj["isMobileVerified"];
      this.isEmailVerified = obj["isEmailVerified"];
      this.updatedAt = obj["updated_at"];
      this.createdAt = obj["created_at"];
      this.userType = obj["userType"];
      this.googleLogin = obj["googleLogin"];
//    this.skills = obj["skills"] as List<String>;
      this.education = EducationBean.fromJsonArray(obj["education"]);
      this.experience = ExperienceBean.fromJsonArray(obj["experience"]);
      this.trainingStyle = obj["trainingStyle"];
      this.media = MediaBean.fromJsonArray(obj["media"]);
//    this.coords = CoordsBean.map(obj["coords"]);
      this.name = NameBean.map(obj["name"]);
      this.V = obj["__v"];
      this.address = AddressBean.map(obj["address"]);
      this.avatar = AvatarBean.map(obj["avatar"]);
      this.background = BackgroundBean.map(obj["background"]);
      this.headline = obj["headline"];
      this.summary = obj["summary"];
      this.dob = obj["dob"];
      this.gender = obj["gender"];
      this.lastActiveAt = obj["last_active_at"];
      this.mobile = obj["mobile"];
      this.badges = obj["badges"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["_id"] = id;
    map["password"] = password;
    map["email"] = email;
    map["isMobileVerified"] = isMobileVerified;
    map["isEmailVerified"] = isEmailVerified;
    map["updated_at"] = updatedAt;
    map["created_at"] = createdAt;
    map["userType"] = userType;
    map["googleLogin"] = googleLogin;
    map["skills"] = skills;
    map["education"] = education;
    map["experience"] = experience;
    map["trainingStyle"] = trainingStyle;
    map["media"] = media;
    map["coords"] = coords;
    map["name"] = name;
    map["__v"] = V;
    map["address"] = address;
    map["avatar"] = avatar;
    map["background"] = background;
    map["headline"] = headline;
    map["summary"] = summary;
    map["dob"] = dob;
    map["gender"] = gender;
    map["last_active_at"] = lastActiveAt;
    map["mobile"] = mobile;
    map["badges"] = badges;
    return map;
  }
}

class SubmissionDetailsBean {
  String language;
  List<dynamic> results;

  SubmissionDetailsBean(this.language, this.results);

  SubmissionDetailsBean.map(dynamic obj) {
    if (obj != null) {
      this.language = obj["language"];
      this.results = obj["results"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["language"] = language;
    map["results"] = results;
    return map;
  }
}

class TaskBean {
  String title;
  String start;
  String dueby;
  int maxmarks;
  int additionalMarks;
  int additionalMaxMarks;
  int grade;
  String comment;

  TaskBean(this.title, this.start, this.dueby, this.additionalMarks,
      this.additionalMaxMarks, this.grade, this.comment, this.maxmarks);

  TaskBean.map(dynamic obj) {
    if (obj != null) {
      this.title = obj["title"];
      this.start = obj["start"];
      this.dueby = obj["dueby"];
      this.additionalMarks = obj["additionalMarks"];
      this.additionalMaxMarks = obj["additionalMaxMarks"];
      this.grade = obj["grade"];
      this.comment = obj["comment"];
      this.maxmarks = obj["maxmarks"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["title"] = title;
    map["start"] = start;
    map["dueby"] = dueby;
    map["additionalMarks"] = additionalMarks;
    map["additionalMaxMarks"] = additionalMaxMarks;
    map["grade"] = grade;
    map["comment"] = comment;
    map["maxmarks"] = maxmarks;
    return map;
  }
}

class BackgroundBean {
  String src;
  String id;
  String createdAt;
  bool gdrive;

  BackgroundBean(this.src, this.id, this.createdAt, this.gdrive);

  BackgroundBean.map(dynamic obj) {
    if (obj != null) {
      this.src = obj["src"];
      this.id = obj["_id"];
      this.createdAt = obj["created_at"];
      this.gdrive = obj["gdrive"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["src"] = src;
    map["_id"] = id;
    map["created_at"] = createdAt;
    map["gdrive"] = gdrive;
    return map;
  }
}

class AddressBean {
  String city;
  String state;
  String country;
  int zip;

  AddressBean(this.city, this.state, this.country, this.zip);

  AddressBean.map(dynamic obj) {
    if (obj != null) {
      this.city = obj["city"];
      this.state = obj["state"];
      this.country = obj["country"];
      this.zip = obj["zip"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["city"] = city;
    map["state"] = state;
    map["country"] = country;
    map["zip"] = zip;
    return map;
  }
}

class CoordsBean {
  int lon;
  int lat;

  CoordsBean(this.lon, this.lat);

  CoordsBean.map(dynamic obj) {
    if (obj != null) {
      this.lon = obj["lon"];
      this.lat = obj["lat"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["lon"] = lon;
    map["lat"] = lat;
    return map;
  }
}

class MediaBean {
  String title;
  String name;
  String src;
  String type;
  int size;
  String id;
  String createdAt;
  bool gdrive;

  MediaBean(this.title, this.name, this.src, this.type, this.size, this.id,
      this.createdAt, this.gdrive);

  MediaBean.map(dynamic obj) {
    if (obj != null) {
      this.title = obj["title"];
      this.name = obj["name"];
      this.src = obj["src"];
      this.type = obj["type"];
      this.size = obj["size"];
      this.id = obj["_id"];
      this.createdAt = obj["created_at"];
      this.gdrive = obj["gdrive"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["title"] = title;
    map["name"] = name;
    map["src"] = src;
    map["type"] = type;
    map["size"] = size;
    map["_id"] = id;
    map["created_at"] = createdAt;
    map["gdrive"] = gdrive;
    return map;
  }

  static List<MediaBean> fromJsonArray(List<dynamic> json) {
    if (json == null) return [];
    List<MediaBean> list =
        json.map<MediaBean>((json) => MediaBean.map(json)).toList();
    return list;
  }
}

class ExperienceBean {
  String company;
  String from;
  String to;
  String title;
  String summary;
  String id;

  ExperienceBean(
      this.company, this.from, this.to, this.title, this.summary, this.id);

  ExperienceBean.map(dynamic obj) {
    if (obj != null) {
      this.company = obj["company"];
      this.from = obj["from"];
      this.to = obj["to"];
      this.title = obj["title"];
      this.summary = obj["summary"];
      this.id = obj["_id"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["company"] = company;
    map["from"] = from;
    map["to"] = to;
    map["title"] = title;
    map["summary"] = summary;
    map["_id"] = id;
    return map;
  }

  static List<ExperienceBean> fromJsonArray(List<dynamic> json) {
    if (json == null) return [];
    List<ExperienceBean> list =
        json.map<ExperienceBean>((json) => ExperienceBean.map(json)).toList();
    return list;
  }
}

class EducationBean {
  String school;
  String from;
  String to;
  String summary;
  String degree;
  String fieldOfStudy;
  String id;

  EducationBean(this.school, this.from, this.to, this.summary, this.degree,
      this.fieldOfStudy, this.id);

  EducationBean.map(dynamic obj) {
    if (obj != null) {
      this.school = obj["school"];
      this.from = obj["from"];
      this.to = obj["to"];
      this.summary = obj["summary"];
      this.degree = obj["degree"];
      this.fieldOfStudy = obj["fieldOfStudy"];
      this.id = obj["_id"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["school"] = school;
    map["from"] = from;
    map["to"] = to;
    map["summary"] = summary;
    map["degree"] = degree;
    map["fieldOfStudy"] = fieldOfStudy;
    map["_id"] = id;
    return map;
  }

  static List<EducationBean> fromJsonArray(List<dynamic> json) {
    if (json == null) return [];
    List<EducationBean> list =
        json.map<EducationBean>((json) => EducationBean.map(json)).toList();
    return list;
  }
}

class SubmissionOptionsBean {
  String type;
  String codeEvaluationType;
  String inputFormat;
  String outputFormat;
  String id;
  List<dynamic> uploadSpecifications;
  List<TestcasesBean> testcases;
  List<SampleTestcasesBean> sampleTestcases;

  SubmissionOptionsBean(
      this.type,
      this.codeEvaluationType,
      this.inputFormat,
      this.outputFormat,
      this.id,
      this.uploadSpecifications,
      this.testcases,
      this.sampleTestcases);

  SubmissionOptionsBean.map(dynamic obj) {
    if (obj != null) {
      this.type = obj["type"];
      this.codeEvaluationType = obj["codeEvaluationType"];
      this.inputFormat = obj["inputFormat"];
      this.outputFormat = obj["outputFormat"];
      this.id = obj["_id"];
      this.uploadSpecifications = obj["uploadSpecifications"];
      this.testcases = TestcasesBean.fromJsonArray(obj["testcases"]);
      this.sampleTestcases =
          SampleTestcasesBean.fromJsonArray(obj["sampleTestcases"]);
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["type"] = type;
    map["codeEvaluationType"] = codeEvaluationType;
    map["inputFormat"] = inputFormat;
    map["outputFormat"] = outputFormat;
    map["_id"] = id;
    map["uploadSpecifications"] = uploadSpecifications;
    map["testcases"] = testcases;
    map["sampleTestcases"] = sampleTestcases;
    return map;
  }
}

class SampleTestcasesBean {
  String output;
  String input;
  String id;

  SampleTestcasesBean(this.output, this.input, this.id);

  SampleTestcasesBean.map(dynamic obj) {
    if (obj != null) {
      this.output = obj["output"];
      this.input = obj["input"];
      this.id = obj["_id"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["output"] = output;
    map["input"] = input;
    map["_id"] = id;
    return map;
  }

  static List<SampleTestcasesBean> fromJsonArray(List<dynamic> json) {
    if (json == null) return [];
    List<SampleTestcasesBean> list = json
        .map<SampleTestcasesBean>((json) => SampleTestcasesBean.map(json))
        .toList();
    return list;
  }
}

class TestcasesBean {
  int maxmarks;
  String input;
  String output;
  String id;

  TestcasesBean(this.maxmarks, this.input, this.output, this.id);

  TestcasesBean.map(dynamic obj) {
    if (obj != null) {
      this.maxmarks = obj["maxmarks"];
      this.input = obj["input"];
      this.output = obj["output"];
      this.id = obj["_id"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["maxmarks"] = maxmarks;
    map["input"] = input;
    map["output"] = output;
    map["_id"] = id;
    return map;
  }

  static List<TestcasesBean> fromJsonArray(List<dynamic> json) {
    if (json == null) return [];
    List<TestcasesBean> list =
        json.map<TestcasesBean>((json) => TestcasesBean.map(json)).toList();
    return list;
  }
}

class ClassBean {
  String id;
  String createdAt;
  String updatedAt;
  String name;
  String uniqueid;
  String placeid;
  String summary;
  dynamic from;
  dynamic to;
  ProgramBean program;
  String school;
  String enrollCode;
  String registrationStatus;
  List<PaymentSchedulesBean> paymentSchedules;
  GradingBean grading;
  List<SectionsBean> sections;
  List<dynamic> media;
  List<dynamic> medium;
  List<dynamic> category;
  String type;
  int V;
  String status;
  bool displayTotalToStudents;
  bool displayPercentileToStudents;
  String end;
  String start;

  ClassBean(
      this.id,
      this.createdAt,
      this.updatedAt,
      this.name,
      this.uniqueid,
      this.placeid,
      this.summary,
      this.from,
      this.to,
      this.program,
      this.school,
      this.enrollCode,
      this.registrationStatus,
      this.paymentSchedules,
      this.grading,
      this.sections,
      this.media,
      this.medium,
      this.category,
      this.type,
      this.V,
      this.status,
      this.displayTotalToStudents,
      this.displayPercentileToStudents,
      this.end,
      this.start);

  ClassBean.map(dynamic obj) {
    if (obj != null) {
      this.id = obj["_id"];
      this.createdAt = obj["created_at"];
      this.updatedAt = obj["updated_at"];
      this.name = obj["name"];
      this.uniqueid = obj["uniqueid"];
      this.placeid = obj["placeid"];
      this.summary = obj["summary"];
      this.from = obj["from"];
      this.to = obj["to"];
      this.program = ProgramBean.map(obj["program"]);
      this.school = obj["school"];
      this.enrollCode = obj["enrollCode"];
      this.registrationStatus = obj["registrationStatus"];
      this.paymentSchedules =
          PaymentSchedulesBean.fromJsonArray(obj["paymentSchedules"]);
      this.grading = GradingBean.map(obj["grading"]);
      this.sections = SectionsBean.fromJsonArray(obj["sections"]);
      this.media = obj["media"];
      this.medium = obj["medium"];
      this.category = obj["category"];
      this.type = obj["type"];
      this.V = obj["__v"];
      this.status = obj["status"];
      this.displayTotalToStudents = obj["displayTotalToStudents"];
      this.displayPercentileToStudents = obj["displayPercentileToStudents"];
      this.end = obj["end"];
      this.start = obj["start"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["_id"] = id;
    map["created_at"] = createdAt;
    map["updated_at"] = updatedAt;
    map["name"] = name;
    map["uniqueid"] = uniqueid;
    map["placeid"] = placeid;
    map["summary"] = summary;
    map["from"] = from;
    map["to"] = to;
    map["program"] = program;
    map["school"] = school;
    map["enrollCode"] = enrollCode;
    map["registrationStatus"] = registrationStatus;
    map["paymentSchedules"] = paymentSchedules;
    map["grading"] = grading;
    map["sections"] = sections;
    map["media"] = media;
    map["medium"] = medium;
    map["category"] = category;
    map["type"] = type;
    map["__v"] = V;
    map["status"] = status;
    map["displayTotalToStudents"] = displayTotalToStudents;
    map["displayPercentileToStudents"] = displayPercentileToStudents;
    map["end"] = end;
    map["start"] = start;
    return map;
  }
}

class SectionsBean {
  String summary;
  int seq;
  String title;
  String id;
  bool active;
  List<dynamic> files;

  SectionsBean(
      this.summary, this.seq, this.title, this.id, this.active, this.files);

  SectionsBean.map(dynamic obj) {
    if (obj != null) {
      this.summary = obj["summary"];
      this.seq = obj["seq"];
      this.title = obj["title"];
      this.id = obj["_id"];
      this.active = obj["active"];
      this.files = obj["files"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["summary"] = summary;
    map["seq"] = seq;
    map["title"] = title;
    map["_id"] = id;
    map["active"] = active;
    map["files"] = files;
    return map;
  }

  static List<SectionsBean> fromJsonArray(List<dynamic> json) {
    if (json == null) return [];
    List<SectionsBean> list =
        json.map<SectionsBean>((json) => SectionsBean.map(json)).toList();
    return list;
  }
}

class GradingBean {
  String by;
  List<dynamic> weightages;

  GradingBean(this.by, this.weightages);

  GradingBean.map(dynamic obj) {
    if (obj != null) {
      this.by = obj["by"];
      this.weightages = obj["weightages"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["by"] = by;
    map["weightages"] = weightages;
    return map;
  }
}

class PaymentSchedulesBean {
  String createdAt;
  int seq;
  String updatedAt;
  String due;
  int cost;
  String details;
  String id;
  String currency;

  PaymentSchedulesBean(this.createdAt, this.seq, this.updatedAt, this.due,
      this.cost, this.details, this.id, this.currency);

  PaymentSchedulesBean.map(dynamic obj) {
    if (obj != null) {
      this.createdAt = obj["created_at"];
      this.seq = obj["seq"];
      this.updatedAt = obj["updated_at"];
      this.due = obj["due"];
      this.cost = obj["cost"];
      this.details = obj["details"];
      this.id = obj["_id"];
      this.currency = obj["currency"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["created_at"] = createdAt;
    map["seq"] = seq;
    map["updated_at"] = updatedAt;
    map["due"] = due;
    map["cost"] = cost;
    map["details"] = details;
    map["_id"] = id;
    map["currency"] = currency;
    return map;
  }

  static List<PaymentSchedulesBean> fromJsonArray(List<dynamic> json) {
    if (json == null) return [];
    List<PaymentSchedulesBean> list = json
        .map<PaymentSchedulesBean>((json) => PaymentSchedulesBean.map(json))
        .toList();
    return list;
  }
}

class ProgramBean {
  String id;
  String createdAt;
  String updatedAt;
  String name;
  String uniqueid;
  String placeid;
  String summary;
  String from;
  String to;
  String school;
  String enrollCode;
  String registrationStatus;
  List<PaymentSchedulesBean> paymentSchedules;
  GradingBean grading;
  List<dynamic> sections;
  List<dynamic> media;
  List<dynamic> medium;
  List<dynamic> category;
  String type;
  int V;
  String status;
  bool displayTotalToStudents;
  bool displayPercentileToStudents;
  String end;
  String start;

  ProgramBean(
      this.id,
      this.createdAt,
      this.updatedAt,
      this.name,
      this.uniqueid,
      this.placeid,
      this.summary,
      this.from,
      this.to,
      this.school,
      this.enrollCode,
      this.registrationStatus,
      this.paymentSchedules,
      this.grading,
      this.sections,
      this.media,
      this.medium,
      this.category,
      this.type,
      this.V,
      this.status,
      this.displayTotalToStudents,
      this.displayPercentileToStudents,
      this.end,
      this.start);

  ProgramBean.map(dynamic obj) {
    if (obj != null) {
      this.id = obj["_id"];
      this.createdAt = obj["created_at"];
      this.updatedAt = obj["updated_at"];
      this.name = obj["name"];
      this.uniqueid = obj["uniqueid"];
      this.placeid = obj["placeid"];
      this.summary = obj["summary"];
      this.from = obj["from"];
      this.to = obj["to"];
      this.school = obj["school"];
      this.enrollCode = obj["enrollCode"];
      this.registrationStatus = obj["registrationStatus"];
      this.paymentSchedules =
          PaymentSchedulesBean.fromJsonArray(obj["paymentSchedules"]);
      this.grading = GradingBean.map(obj["grading"]);
      this.sections = obj["sections"];
      this.media = obj["media"];
      this.medium = obj["medium"];
      this.category = obj["category"];
      this.type = obj["type"];
      this.V = obj["__v"];
      this.status = obj["status"];
      this.displayTotalToStudents = obj["displayTotalToStudents"];
      this.displayPercentileToStudents = obj["displayPercentileToStudents"];
      this.end = obj["end"];
      this.start = obj["start"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["_id"] = id;
    map["created_at"] = createdAt;
    map["updated_at"] = updatedAt;
    map["name"] = name;
    map["uniqueid"] = uniqueid;
    map["placeid"] = placeid;
    map["summary"] = summary;
    map["from"] = from;
    map["to"] = to;
    map["school"] = school;
    map["enrollCode"] = enrollCode;
    map["registrationStatus"] = registrationStatus;
    map["paymentSchedules"] = paymentSchedules;
    map["grading"] = grading;
    map["sections"] = sections;
    map["media"] = media;
    map["medium"] = medium;
    map["category"] = category;
    map["type"] = type;
    map["__v"] = V;
    map["status"] = status;
    map["displayTotalToStudents"] = displayTotalToStudents;
    map["displayPercentileToStudents"] = displayPercentileToStudents;
    map["end"] = end;
    map["start"] = start;
    return map;
  }
}

class SchoolBean {
  String id;
  String name;

  SchoolBean(this.id, this.name);

  SchoolBean.map(dynamic obj) {
    if (obj != null) {
      this.id = obj["_id"];
      this.name = obj["name"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["_id"] = id;
    map["name"] = name;
    return map;
  }
}

class AvatarBean {
  String src;
  String id;
  String createdAt;
  bool gdrive;

  AvatarBean(this.src, this.id, this.createdAt, this.gdrive);

  AvatarBean.map(dynamic obj) {
    if (obj != null) {
      this.src = obj["src"];
      this.id = obj["_id"];
      this.createdAt = obj["created_at"];
      this.gdrive = obj["gdrive"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["src"] = src;
    map["_id"] = id;
    map["created_at"] = createdAt;
    map["gdrive"] = gdrive;
    return map;
  }
}

class NameBean {
  String firstname;
  String lastname;

  NameBean(this.firstname, this.lastname);

  NameBean.map(dynamic obj) {
    if (obj != null) {
      this.firstname = obj["firstname"];
      this.lastname = obj["lastname"];
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["firstname"] = firstname;
    map["lastname"] = lastname;
    return map;
  }
}

class Assessment {
  int answered;
  int correct;
  String status;
  int timeSpent;
  bool retake;
  int attempts;

  Assessment(
      {this.answered,
      this.correct,
      this.status,
      this.timeSpent,
      this.retake,
      this.attempts});

  Assessment.fromJson(Map<String, dynamic> json) {
    answered = json['answered'];
    correct = json['correct'];
    status = json['status'];
    timeSpent = json['timeSpent'];
    retake = json['retake'];
    attempts = json['attempts'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['answered'] = this.answered;
    data['correct'] = this.correct;
    data['status'] = this.status;
    data['timeSpent'] = this.timeSpent;
    data['retake'] = this.retake;
    data['attempts'] = this.attempts;
    return data;
  }
}
