class GroupModel {
  int groupId;
  String groupName;
  List<Members> members;
  String type;
  int creationDate;
  int createdBy;
  bool isActive;
  String aboutGroup;
  String otherInfo;
  String groupImage;
  int roleId;

  GroupModel(
      {this.groupId,
      this.groupName,
      this.members,
      this.type,
      this.creationDate,
      this.createdBy,
      this.isActive,
      this.aboutGroup,
      this.otherInfo,
      this.groupImage,
      this.roleId});

  GroupModel.fromJson(Map<String, dynamic> json) {
    groupId = json['groupId'];
    groupName = json['groupName'];
    if (json['members'] != null) {
      members = new List<Members>();
      json['members'].forEach((v) {
        members.add(new Members.fromJson(v));
      });
    }
    type = json['type'];
    creationDate = json['creationDate'];
    createdBy = json['createdBy'];
    isActive = json['isActive'];
    aboutGroup = json['aboutGroup'];
    otherInfo = json['otherInfo'];
    groupImage = json['groupImage'];
    roleId = json['roleId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['groupId'] = this.groupId;
    data['groupName'] = this.groupName;
    if (this.members != null) {
      data['members'] = this.members.map((v) => v.toJson()).toList();
    }
    data['type'] = this.type;
    data['creationDate'] = this.creationDate;
    data['createdBy'] = this.createdBy;
    data['isActive'] = this.isActive;
    data['aboutGroup'] = this.aboutGroup;
    data['otherInfo'] = this.otherInfo;
    data['groupImage'] = this.groupImage;
    data['roleId'] = this.roleId;
    return data;
  }

  static Map<String, dynamic> toMap(GroupModel model) {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['groupId'] = model.groupId;
    data['groupName'] = model.groupName;
    if (model.members != null) {
      data['members'] = model.members.map((v) => v.toJson()).toList();
    }
    data['type'] = model.type;
    data['creationDate'] = model.creationDate;
    data['createdBy'] = model.createdBy;
    data['isActive'] = model.isActive;
    data['aboutGroup'] = model.aboutGroup;
    data['otherInfo'] = model.otherInfo;
    data['groupImage'] = model.groupImage;
    data['roleId'] = model.roleId;
    return data;
  }

  static List<Map<String, dynamic>> mapList(List<GroupModel> model) {
    List<Map<String, dynamic>> listOfOffer = model
        .map((group) => {
              "groupId": group.groupId,
              "groupName": group.groupName,
              "members": group.members,
              "type": group.type,
              "creationDate": group.creationDate,
              "createdBy": group.createdBy,
              "isActive": group.isActive,
              "aboutGroup": group.aboutGroup,
              "otherInfo": group.otherInfo,
              "groupImage": group.groupImage,
              "roleId": group.roleId,
            })
        .toList();
    return listOfOffer;
  }
}

class Members {
  int dateTime;
  String status;
  bool isAdmin;
  int userId;

  Members({this.dateTime, this.status, this.isAdmin, this.userId});

  Members.fromJson(Map<String, dynamic> json) {
    dateTime = json['dateTime'];
    status = json['status'];
    isAdmin = json['isAdmin'];
    userId = json['userId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dateTime'] = this.dateTime;
    data['status'] = this.status;
    data['isAdmin'] = this.isAdmin;
    data['userId'] = this.userId;
    return data;
  }
}
