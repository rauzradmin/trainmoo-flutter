class UserNameObject {
    String firstname;
    String lastname;

    UserNameObject({this.firstname, this.lastname});

    factory UserNameObject.fromJson(Map<String, dynamic> json) {
        return UserNameObject(
            firstname: json['firstname'], 
            lastname: json['lastname'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['firstname'] = this.firstname;
        data['lastname'] = this.lastname;
        return data;
    }
}