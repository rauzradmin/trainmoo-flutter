import 'User.dart';

class UserLoginModel {
  User user;
  List<String> loginMessage;

  UserLoginModel({this.user, this.loginMessage});

  factory UserLoginModel.fromJson(Map<String, dynamic> json) {
    if (json['user'] != null) {
      return UserLoginModel(
        user: json['user'] != null ? User.fromJson(json['user']) : null,
      );
    } else {
      return   UserLoginModel(
        loginMessage: json['loginMessage'] != null
            ? new List<String>.from(json['loginMessage'])
            : null,
      );
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}

class ErrorModel {
  List<String> loginMessage;

  ErrorModel({this.loginMessage});

  factory ErrorModel.fromJson(Map<String, dynamic> json) {
    return ErrorModel(
      loginMessage: json['loginMessage'] != null
          ? new List<String>.from(json['loginMessage'])
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.loginMessage != null) {
      data['loginMessage'] = this.loginMessage;
    }
    return data;
  }
}
