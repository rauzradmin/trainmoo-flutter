class Background {
    String backgroundId;
    String created_at;
    bool gdrive;
    String src;

    Background({this.backgroundId, this.created_at, this.gdrive, this.src});

    factory Background.fromJson(Map<String, dynamic> json) {
        return Background(
            backgroundId: json['_id'],
            created_at: json['created_at'], 
            gdrive: json['gdrive'], 
            src: json['src'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['_id'] = this.backgroundId;
        data['created_at'] = this.created_at;
        data['gdrive'] = this.gdrive;
        data['src'] = this.src;
        return data;
    }
}