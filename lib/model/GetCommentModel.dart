import 'dart:core';

import 'package:trainmoo/model/FileModel.dart';

class GetCommonModel {
  String sId;
  bool minified;
  String type;
  School school;
  ClassModel classModel;
  String text;
  bool submittable;
  String createdAt;
  String updatedAt;

  Author author;
  bool commentsDisabled;
  bool isCode;
  bool pinned;
  bool locked;
  List<Replies> replies;
  int graded;
  int solutions;
  int comments;
  int submissions;
  int views;
  int likes;
  bool anonymous;
  int iV;
  bool starred;
  bool liked;
  String displayTime;
  int pending;

  GetCommonModel(
      {this.sId,
      this.minified,
      this.type,
      this.school,
      this.classModel,
      this.text,
      this.submittable,
      this.createdAt,
      this.updatedAt,
      this.author,
      this.commentsDisabled,
      this.isCode,
      this.pinned,
      this.locked,
      this.replies,
      this.graded,
      this.solutions,
      this.comments,
      this.submissions,
      this.views,
      this.likes,
      this.anonymous,
      this.iV,
      this.starred,
      this.liked,
      this.displayTime,
      this.pending});

  GetCommonModel.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    minified = json['minified'];
    type = json['type'];
    school =
        json['school'] != null ? new School.fromJson(json['school']) : null;
    classModel =
        json['class'] != null ? new ClassModel.fromJson(json['class']) : null;
    text = json['text'];
    submittable = json['submittable'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    author =
        json['author'] != null ? new Author.fromJson(json['author']) : null;

    commentsDisabled = json['commentsDisabled'];
    isCode = json['isCode'];
    pinned = json['pinned'];
    locked = json['locked'];
    if (json['replies'] != null) {
      replies = new List<Replies>();
      json['replies'].forEach((v) {
        replies.add(new Replies.fromJson(v));
      });
    }
    graded = json['graded'];
    solutions = json['solutions'];
    comments = json['comments'];
    submissions = json['submissions'];
    views = json['views'];
    likes = json['likes'];

    anonymous = json['anonymous'];
    iV = json['__v'];
    starred = json['starred'];
    liked = json['liked'];
    displayTime = json['displayTime'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['minified'] = this.minified;
    data['type'] = this.type;
    if (this.school != null) {
      data['school'] = this.school.toJson();
    }

    data['text'] = this.text;
    data['submittable'] = this.submittable;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.author != null) {
      data['author'] = this.author.toJson();
    }

    data['commentsDisabled'] = this.commentsDisabled;

    data['isCode'] = this.isCode;
    data['pinned'] = this.pinned;
    data['locked'] = this.locked;
    if (this.replies != null) {
      data['replies'] = this.replies.map((v) => v.toJson()).toList();
    }
    data['graded'] = this.graded;
    data['solutions'] = this.solutions;
    data['comments'] = this.comments;
    data['submissions'] = this.submissions;
    data['views'] = this.views;
    data['likes'] = this.likes;

    data['anonymous'] = this.anonymous;
    data['__v'] = this.iV;
    data['starred'] = this.starred;
    data['liked'] = this.liked;
    data['displayTime'] = this.displayTime;
    data['pending'] = this.pending;
    return data;
  }
}

class ClassModel {
  String sId;

  ClassModel({this.sId});
  ClassModel.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
  }
}

class School {
  String sId;
  String name;

  School({this.sId, this.name});

  School.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['name'] = this.name;
    return data;
  }
}

class Name {
  String firstname;
  String lastname;

  Name({this.firstname, this.lastname});

  Name.fromJson(Map<String, dynamic> json) {
    firstname = json['firstname'];
    lastname = json['lastname'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    return data;
  }
}

class Replies {
  String sId;
  bool minified;
  Parent parent;
  String type;
  String responseType;
  String school;
  String className;
  String text;
  bool submittable;
  String createdAt;
  String updatedAt;
  Author author;
  List<Null> users;
  bool commentsDisabled;
  bool isCode;
  bool pinned;
  bool locked;
  List<Null> replies;
  int graded;
  int solutions;
  int comments;
  int submissions;
  int views;
  int likes;
  Assessment assessment;
  List<FileModel> files;
  bool anonymous;
  int iV;

  Replies(
      {this.sId,
      this.minified,
      this.parent,
      this.type,
      this.responseType,
      this.school,
      this.className,
      this.text,
      this.submittable,
      this.createdAt,
      this.updatedAt,
      this.author,
      this.users,
      this.commentsDisabled,
      this.isCode,
      this.pinned,
      this.locked,
      this.replies,
      this.graded,
      this.solutions,
      this.comments,
      this.submissions,
      this.views,
      this.likes,
      this.files,
      this.anonymous,
      this.assessment,
      this.iV});

  Replies.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    minified = json['minified'];
    parent =
        json['parent'] != null ? new Parent.fromJson(json['parent']) : null;
    type = json['type'];
    responseType = json['responseType'];
    school = json['school'];
    className = json['class'];
    text = json['text'];
    submittable = json['submittable'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    author =
        json['author'] != null ? new Author.fromJson(json['author']) : null;
    commentsDisabled = json['commentsDisabled'];
    isCode = json['isCode'];
    pinned = json['pinned'];
    locked = json['locked'];
    graded = json['graded'];
    solutions = json['solutions'];
    comments = json['comments'];
    submissions = json['submissions'];
    views = json['views'];
    likes = json['likes'];
    assessment = json['assessment'] != null
        ? new Assessment.fromJson(json['assessment'])
        : null;
    files = FileModel.fromJsonArray(json['files']);

    anonymous = json['anonymous'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['minified'] = this.minified;
    if (this.parent != null) {
      data['parent'] = this.parent.toJson();
    }
    data['type'] = this.type;
    data['responseType'] = this.responseType;
    data['school'] = this.school;
    data['class'] = this.className;
    data['text'] = this.text;
    data['submittable'] = this.submittable;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.author != null) {
      data['author'] = this.author.toJson();
    }

    data['commentsDisabled'] = this.commentsDisabled;

    data['isCode'] = this.isCode;
    data['pinned'] = this.pinned;
    data['locked'] = this.locked;

    data['graded'] = this.graded;
    data['solutions'] = this.solutions;
    data['comments'] = this.comments;
    data['submissions'] = this.submissions;
    data['views'] = this.views;
    data['likes'] = this.likes;
    data['assessment'] = this.assessment;
    data['anonymous'] = this.anonymous;
    data['__v'] = this.iV;
    return data;
  }
}

class Parent {
  String sId;

  Author author;

  Parent({this.sId, this.author});

  Parent.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    author =
        json['author'] != null ? new Author.fromJson(json['author']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;

    if (this.author != null) {
      data['author'] = this.author.toJson();
    }
    return data;
  }
}

class AccessModules {
  String name;
  bool value;
  String sId;

  AccessModules({this.name, this.value, this.sId});

  AccessModules.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    data['_id'] = this.sId;
    return data;
  }
}

class Author {
  String sId;
  Name name;
  Files avatar;

  Author({this.sId, this.name, this.avatar});

  Author.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    name = json['name'] != null ? new Name.fromJson(json['name']) : null;
    avatar = json['avatar'] != null ? new Files.fromJson(json['avatar']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    if (this.avatar != null) {
      data['avatar'] = this.avatar.toJson();
    }
    return data;
  }
}

class Files {
  String title;
  String name;
  String src;
  String thumbnail;
  String type;
  int size;
  int h;
  int w;
  String sId;
  String createdAt;
  bool gdrive;

  Files(
      {this.title,
      this.name,
      this.src,
      this.thumbnail,
      this.type,
      this.size,
      this.h,
      this.w,
      this.sId,
      this.createdAt,
      this.gdrive});

  Files.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    name = json['name'];
    src = json['src'];
    thumbnail = json['thumbnail'];
    type = json['type'];
    size = json['size'];
    h = json['h'];
    w = json['w'];
    sId = json['_id'];
    createdAt = json['created_at'];
    gdrive = json['gdrive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['name'] = this.name;
    data['src'] = this.src;
    data['thumbnail'] = this.thumbnail;
    data['type'] = this.type;
    data['size'] = this.size;
    data['h'] = this.h;
    data['w'] = this.w;
    data['_id'] = this.sId;
    data['created_at'] = this.createdAt;
    data['gdrive'] = this.gdrive;
    return data;
  }
}

class Assessment {
  int answered;
  int correct;
  String status;
  int timeSpent;
  bool retake;
  int attempts;

  Assessment(
      {this.answered,
      this.correct,
      this.status,
      this.timeSpent,
      this.retake,
      this.attempts});

  Assessment.fromJson(Map<String, dynamic> json) {
    answered = json['answered'];
    correct = json['correct'];
    status = json['status'];
    timeSpent = json['timeSpent'];
    retake = json['retake'];
    attempts = json['attempts'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['answered'] = this.answered;
    data['correct'] = this.correct;
    data['status'] = this.status;
    data['timeSpent'] = this.timeSpent;
    data['retake'] = this.retake;
    data['attempts'] = this.attempts;
    return data;
  }
}
