class Avatar {
    String avtarId;
    String created_at;
    bool gDrive;
    String src;

    Avatar({this.avtarId, this.created_at, this.gDrive, this.src});

    factory Avatar.fromJson(Map<String, dynamic> json) {
        return Avatar(
            avtarId: json['_id'],
            created_at: json['created_at'], 
            gDrive: json['gdrive'],
            src: json['src'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['_id'] = this.avtarId;
        data['created_at'] = this.created_at;
        data['gdrive'] = this.gDrive;
        data['src'] = this.src;
        return data;
    }
}