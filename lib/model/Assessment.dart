class Assessment
{
  bool passageView;
  bool random;
  bool retake;
  int attempt;

  Assessment({this.passageView, this.random,this.retake,this.attempt});

  factory Assessment.fromJson(Map<String, dynamic> json) {
    return Assessment(
      passageView: json['passageView'],
      random: json['random'],
      retake: json['retake'],
      attempt: json['attempts'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['passageView'] = this.passageView;
    data['random'] = this.random;
    data['retake'] = this.retake;
    data['attempts'] = this.attempt;

    return data;
  }
  static List<Assessment> fromJsonArray(List<dynamic> json) {
    List<Assessment> bannerLists =
    json.map<Assessment>((json) => Assessment.fromJson(json)).toList();
    return bannerLists;
  }

}