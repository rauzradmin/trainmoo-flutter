class Coords {
    int lat;
    int lon;

    Coords({this.lat, this.lon});

    factory Coords.fromJson(Map<String, dynamic> json) {
        return Coords(
            lat: json['lat'], 
            lon: json['lon'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['lat'] = this.lat;
        data['lon'] = this.lon;
        return data;
    }
}