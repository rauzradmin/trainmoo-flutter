import 'Address.dart';
import 'Avatar.dart';
import 'Background.dart';
import 'Coords.dart';
import 'Education.dart';
import 'Experience.dart';
import 'Media.dart';
import 'Name.dart';

class User {
  int version;
  String userId;
  Address address;
  Avatar avatar;
  Background background;
  List<BadgesObject> badges;
  Coords coords;
  String created_at;
  String dob;
  List<Education> education;
  String email;
  List<Experience> experience;
  String fullname;
  String gender;
  bool googleLogin;
  String headline;
  String id;
  bool isActive;
  bool isEmailVerified;
  bool isMobileVerified;
  String last_active_at;
  List<Media> media;
  String mobile;
  UserNameObject name;
  String password;
  String shortName;
  List<String> skills;
  bool ssoLogin;
  String summary;
  List<Object> trainingStyle;
  String updated_at;
  String userType;
  String userImage;

  User(
      {this.version,
      this.userId,
      this.address,
      this.avatar,
      this.background,
      this.badges,
      this.coords,
      this.created_at,
      this.dob,
      this.education,
      this.email,
      this.experience,
      this.fullname,
      this.gender,
      this.googleLogin,
      this.headline,
      this.id,
      this.isActive,
      this.isEmailVerified,
      this.isMobileVerified,
      this.last_active_at,
      this.media,
      this.mobile,
      this.name,
      this.password,
      this.shortName,
      this.skills,
      this.ssoLogin,
      this.summary,
      this.trainingStyle,
      this.updated_at,
      this.userType});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      version: json['__v'],
      userId: json['_id'],
      address:
          json['address'] != null ? Address.fromJson(json['address']) : null,
      avatar: json['avatar'] != null ? Avatar.fromJson(json['avatar']) : null,
      background: json['background'] != null
          ? Background.fromJson(json['background'])
          : null,
//            badges: json['badges'] != null ? (json['badges'] as List).map((i) => BadgesObject.fromJson(i)).toList() : null,
      coords: json['coords'] != null ? Coords.fromJson(json['coords']) : null,
      created_at: json['created_at'],
      dob: json['dob'] != null ? json['dob'] : null,
      education: json['education'] != null
          ? (json['education'] as List)
              .map((i) => Education.fromJson(i))
              .toList()
          : null,
      email: json['email'],
      experience: json['experience'] != null
          ? (json['experience'] as List)
              .map((i) => Experience.fromJson(i))
              .toList()
          : null,
      fullname: json['fullname'],
      gender: json['gender'],
      googleLogin: json['googleLogin'],
      headline: json['headline'],
      id: json['id'],
      isActive: json['isActive'],
      isEmailVerified: json['isEmailVerified'],
      isMobileVerified: json['isMobileVerified'],
      last_active_at: json['last_active_at'],
      media: json['media'] != null
          ? (json['media'] as List).map((i) => Media.fromJson(i)).toList()
          : null,
      mobile: json['mobile'],
      name: json['name'] != null ? UserNameObject.fromJson(json['name']) : null,
      password: json['password'],
      shortName: json['shortname'],
      skills:
          json['skills'] != null ? new List<String>.from(json['skills']) : null,
      ssoLogin: json['ssoLogin'],
      summary: json['summary'],
//            trainingStyle: json['trainingStyle'] != null ? (json['trainingStyle'] as List).map((i) => Object.fromJson(i)).toList() : null,
      updated_at: json['updated_at'],
      userType: json['userType'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['__v'] = this.version;
    data['_id'] = this.userId;
    data['created_at'] = this.created_at;
    data['email'] = this.email;
    data['fullname'] = this.fullname;
    data['gender'] = this.gender;
    data['googleLogin'] = this.googleLogin;
    data['headline'] = this.headline;
    data['id'] = this.id;
    data['isActive'] = this.isActive;
    data['isEmailVerified'] = this.isEmailVerified;
    data['isMobileVerified'] = this.isMobileVerified;
    data['last_active_at'] = this.last_active_at;
    data['mobile'] = this.mobile;
    data['password'] = this.password;
    data['shortname'] = this.shortName;
    data['ssoLogin'] = this.ssoLogin;
    data['summary'] = this.summary;
    data['updated_at'] = this.updated_at;
    data['userType'] = this.userType;
    if (this.address != null) {
      data['address'] = this.address.toJson();
    }
    if (this.avatar != null) {
      data['avatar'] = this.avatar.toJson();
    }
    if (this.background != null) {
      data['background'] = this.background.toJson();
    }
//        if (this.badges != null) {
//            data['badges'] = this.badges.map((v) => v.toJson()).toList();
//        }
    if (this.coords != null) {
      data['coords'] = this.coords.toJson();
    }
    if (this.dob != null) {
//            data['dob'] = this.dob.toJson();
    }
    if (this.education != null) {
      data['education'] = this.education.map((v) => v.toJson()).toList();
    }
    if (this.experience != null) {
      data['experience'] = this.experience.map((v) => v.toJson()).toList();
    }
    if (this.media != null) {
      data['media'] = this.media.map((v) => v.toJson()).toList();
    }
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    if (this.skills != null) {
      data['skills'] = this.skills;
    }
    if (this.trainingStyle != null) {
//            data['trainingStyle'] = this.trainingStyle.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class BadgesObject {}
