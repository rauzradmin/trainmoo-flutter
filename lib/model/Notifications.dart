import 'package:trainmoo/model/FileModel.dart';
import 'package:trainmoo/ui/util/util.dart';

class NotificationModel {
  String id;
  String created_at;
  String updated_at;
  String type;
  String source;
  String sourceid;
  String to;
  String school;
  String classid;
  String created;
  String text;
  int V;
  bool read;
  int submissions;
  int comments;

  NotificationModel(
      this.id,
      this.created_at,
      this.updated_at,
      this.type,
      this.source,
      this.sourceid,
      this.to,
      this.classid,
      this.created,
      this.text,
      this.V,
      this.school,
      this.read,
      this.comments,
      this.submissions);

  NotificationModel.map(dynamic obj) {
    this.id = obj["_id"];
    this.created_at = obj["created_at"];
    this.updated_at = obj["updated_at"];
    this.type = obj["type"];
    this.source = obj["source"];
    this.sourceid = obj["sourceid"];
    this.to = obj["to"];
    this.school = obj["school"];
    this.classid = obj["class"];
    this.created = obj["created"];
    this.text = obj["text"];
    this.V = obj["__v"];
    this.read = obj["read"];
    this.submissions = obj["submissions"];
    this.comments = obj["comments"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["created_at"] = created_at;
    map["updated_at"] = updated_at;
    map["type"] = type;
    map["source"] = source;
    map["sourceid"] = sourceid;
    map["to"] = to;
    map["school"] = school;
    map["classid"] = classid;
    map["created"] = created;
    map["text"] = text;
    map["V"] = V;
    map["read"] = read;
    map["submissions"] = submissions;
    map["comments"] = comments;

    return map;
  }

  static List<NotificationModel> fromJsonArray(List<dynamic> json) {
    if (json == null || json.length <= 0) return [];
    List<NotificationModel> list = json
        .map<NotificationModel>((json) => NotificationModel.map(json))
        .toList();
    return list;
  }
}
