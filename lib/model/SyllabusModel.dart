import 'FileModel.dart';

class SyllabusModel {
  List<SectionsBean> sections;

  SyllabusModel(this.sections);

  SyllabusModel.map(dynamic obj) {
    this.sections = SectionsBean.fromJsonArray(obj["sections"]);
  }
}

class SectionsBean {
  String summary;
  int seq;
  String title;
  String Id;
  bool active;
  List<FileModel> files;
  bool visible;
  bool activeNow;
  String availability;

  SectionsBean(this.summary, this.seq, this.title, this.Id, this.active,
      this.files, this.visible, this.activeNow, this.availability);

  SectionsBean.map(dynamic obj) {
    this.summary = obj["summary"];
    this.seq = obj["seq"];
    this.title = obj["title"];
    this.Id = obj["_id"];
    this.active = obj["active"];
    this.files = FileModel.fromJsonArray(obj["files"]);
    this.visible = obj["visible"];
    this.activeNow = obj["activeNow"];
    this.availability = obj["availability"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["summary"] = summary;
    map["seq"] = seq;
    map["title"] = title;
    map["_id"] = Id;
    map["active"] = active;
    map["files"] = files;
    map["visible"] = visible;
    map["activeNow"] = activeNow;
    map["availability"] = availability;
    return map;
  }

  static List<SectionsBean> fromJsonArray(List<dynamic> json) {
    if (json == null || json.length <= 0) return [];
    List<SectionsBean> list =
        json.map<SectionsBean>((json) => SectionsBean.map(json)).toList();
    return list;
  }
}
