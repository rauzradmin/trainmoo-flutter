import 'package:trainmoo/model/GradeModel.dart';

/// id : "dfdfdfsrewrWE"
/// approvedMarks : 14
/// marksObtained : 14
/// percentile : "83.33"
/// status : "approved"

class GradeDetailObject {
  HeadersBean headersBean;
  int approvedMarks;
  int marksObtained;
  String percentile;
  String status;

  GradeDetailObject(this.headersBean, this.approvedMarks, this.marksObtained,
      this.percentile, this.status);

  GradeDetailObject.map(dynamic obj, HeadersBean headersBean) {
    this.headersBean = headersBean;
    this.approvedMarks = obj["approvedMarks"];
    this.marksObtained = obj["marksObtained"];
    this.percentile = getPercentile(obj);
    this.status = obj["status"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["approvedMarks"] = approvedMarks;
    map["marksObtained"] = marksObtained;
    map["percentile"] = percentile;
    map["status"] = status;
    return map;
  }

  String getPercentile(dynamic data) {
    try {
      int percentAsInt = data['percentile'] as int;
      return percentAsInt.toString();
    } catch (e) {
      try {
        double percentAsDouble = data['percentile'] as double;
        return percentAsDouble.toString();
      } catch (e) {
        return data['percentile'];
      }
    }
  }
}
