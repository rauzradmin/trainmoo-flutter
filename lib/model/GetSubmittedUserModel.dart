import 'dart:core';

import 'package:trainmoo/model/GetCommentModel.dart';

class GetSubmittedUserModel {
  String sId;
  bool minified;
  bool submittable;
  String type;
  School school;
  ClassModel classModel;
  SubmissionOptions submissionOptions;
  String text;
  String createdAt;
  String updatedAt;
  Author author;
  SubmissionDetails submissionDetails;
  bool commentsDisabled;
  Assessment assessment;
  Task task;
  bool isCode;
  bool pinned;
  bool locked;
  List<Replies> replies;
  int graded;
  int solutions;
  int comments;
  int submissions;
  int views;
  int likes;
  List<Null> files;
  bool anonymous;
  int iV;
  bool starred;
  bool liked;
  String displayTime;
  List<PendingUsers> pendingUsers;
  List<SubmittedUsers> submittedUsers;
  int pending;

  GetSubmittedUserModel(
      {this.sId,
      this.minified,
      this.submittable,
      this.type,
      this.school,
      this.classModel,
      this.submissionOptions,
      this.text,
      this.createdAt,
      this.updatedAt,
      this.author,
      this.submissionDetails,
      this.commentsDisabled,
      this.assessment,
      this.task,
      this.isCode,
      this.pinned,
      this.locked,
      this.replies,
      this.graded,
      this.solutions,
      this.comments,
      this.submissions,
      this.views,
      this.likes,
      this.files,
      this.anonymous,
      this.iV,
      this.starred,
      this.liked,
      this.displayTime,
      this.pendingUsers,
      this.submittedUsers,
      this.pending});

  GetSubmittedUserModel.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    minified = json['minified'];
    submittable = json['submittable'];
    type = json['type'];
    school =
        json['school'] != null ? new School.fromJson(json['school']) : null;
    classModel =
        json['class'] != null ? new ClassModel.fromJson(json['class']) : null;
    submissionOptions = json['submissionOptions'] != null
        ? new SubmissionOptions.fromJson(json['submissionOptions'])
        : null;
    text = json['text'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    author =
        json['author'] != null ? new Author.fromJson(json['author']) : null;
    submissionDetails = json['submissionDetails'] != null
        ? new SubmissionDetails.fromJson(json['submissionDetails'])
        : null;

    commentsDisabled = json['commentsDisabled'];
    assessment = json['assessment'] != null
        ? new Assessment.fromJson(json['assessment'])
        : null;
    task = json['task'] != null ? new Task.fromJson(json['task']) : null;
    isCode = json['isCode'];
    pinned = json['pinned'];
    locked = json['locked'];
    if (json['replies'] != null) {
      replies = new List<Replies>();
      json['replies'].forEach((v) {
        replies.add(new Replies.fromJson(v));
      });
    }
    graded = json['graded'];
    solutions = json['solutions'];
    comments = json['comments'];
    submissions = json['submissions'];
    views = json['views'];
    likes = json['likes'];
    anonymous = json['anonymous'];
    iV = json['__v'];
    starred = json['starred'];
    liked = json['liked'];
    displayTime = json['displayTime'];
    if (json['pendingUsers'] != null) {
      pendingUsers = new List<PendingUsers>();
      json['pendingUsers'].forEach((v) {
        pendingUsers.add(new PendingUsers.fromJson(v));
      });
    }
    if (json['submittedUsers'] != null) {
      submittedUsers = new List<SubmittedUsers>();
      json['submittedUsers'].forEach((v) {
        submittedUsers.add(new SubmittedUsers.fromJson(v));
      });
    }
    pending = json['pending'];
  }
}

class School {
  String sId;
  String name;

  School({this.sId, this.name});

  School.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['name'] = this.name;
    return data;
  }
}

class Files {
  String title;
  String name;
  String src;
  String thumbnail;
  String type;
  int size;
  int h;
  int w;
  String sId;
  String createdAt;
  bool gdrive;

  Files(
      {this.title,
      this.name,
      this.src,
      this.thumbnail,
      this.type,
      this.size,
      this.h,
      this.w,
      this.sId,
      this.createdAt,
      this.gdrive});

  Files.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    name = json['name'];
    src = json['src'];
    thumbnail = json['thumbnail'];
    type = json['type'];
    size = json['size'];
    h = json['h'];
    w = json['w'];
    sId = json['_id'];
    createdAt = json['created_at'];
    gdrive = json['gdrive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['name'] = this.name;
    data['src'] = this.src;
    data['thumbnail'] = this.thumbnail;
    data['type'] = this.type;
    data['size'] = this.size;
    data['h'] = this.h;
    data['w'] = this.w;
    data['_id'] = this.sId;
    data['created_at'] = this.createdAt;
    data['gdrive'] = this.gdrive;
    return data;
  }
}

class SubmissionOptions {
  String type;
  String codeEvaluationType;
  String inputFormat;
  String outputFormat;
  String sId;

  SubmissionOptions({
    this.type,
    this.codeEvaluationType,
    this.inputFormat,
    this.outputFormat,
    this.sId,
  });

  SubmissionOptions.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    codeEvaluationType = json['codeEvaluationType'];
    inputFormat = json['inputFormat'];
    outputFormat = json['outputFormat'];
    sId = json['_id'];
  }
}

class Name {
  String firstname;
  String lastname;

  Name({this.firstname, this.lastname});

  Name.fromJson(Map<String, dynamic> json) {
    firstname = json['firstname'];
    lastname = json['lastname'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    return data;
  }
}

class Address {
  String city;
  String state;
  String country;
  int zip;

  Address({this.city, this.state, this.country, this.zip});

  Address.fromJson(Map<String, dynamic> json) {
    city = json['city'];
    state = json['state'];
    country = json['country'];
    zip = json['zip'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['city'] = this.city;
    data['state'] = this.state;
    data['country'] = this.country;
    data['zip'] = this.zip;
    return data;
  }
}

class Avatar {
  String src;
  String sId;
  String createdAt;
  bool gdrive;

  Avatar({this.src, this.sId, this.createdAt, this.gdrive});

  Avatar.fromJson(Map<String, dynamic> json) {
    src = json['src'];
    sId = json['_id'];
    createdAt = json['created_at'];
    gdrive = json['gdrive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['src'] = this.src;
    data['_id'] = this.sId;
    data['created_at'] = this.createdAt;
    data['gdrive'] = this.gdrive;
    return data;
  }
}

class Assessment {
  bool retake;
  int attempts;
  bool random;

  Assessment({this.retake, this.attempts, this.random});

  Assessment.fromJson(Map<String, dynamic> json) {
    retake = json['retake'];
    attempts = json['attempts'];
    random = json['random'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['retake'] = this.retake;
    data['attempts'] = this.attempts;
    data['random'] = this.random;
    return data;
  }
}

class Task {
  String title;
  String start;
  String dueby;
  bool graded;
  int maxmarks;
  int additionalMarks;
  int additionalMaxMarks;
  String test;

  Task(
      {this.title,
      this.start,
      this.dueby,
      this.graded,
      this.maxmarks,
      this.additionalMarks,
      this.additionalMaxMarks,
      this.test});

  Task.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    start = json['start'];
    dueby = json['dueby'];
    graded = json['graded'];
    maxmarks = json['maxmarks'];
    additionalMarks = json['additionalMarks'];
    additionalMaxMarks = json['additionalMaxMarks'];
    test = json['test'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['start'] = this.start;
    data['dueby'] = this.dueby;
    data['graded'] = this.graded;
    data['maxmarks'] = this.maxmarks;
    data['additionalMarks'] = this.additionalMarks;
    data['additionalMaxMarks'] = this.additionalMaxMarks;
    data['test'] = this.test;
    return data;
  }
}

class Parent {
  String sId;
  Task task;
  Author author;

  Parent({this.sId, this.task, this.author});

  Parent.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    task = json['task'] != null ? new Task.fromJson(json['task']) : null;
    author =
        json['author'] != null ? new Author.fromJson(json['author']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    if (this.task != null) {
      data['task'] = this.task.toJson();
    }
    if (this.author != null) {
      data['author'] = this.author.toJson();
    }
    return data;
  }
}

class Author {
  String sId;
  Name name;

  Author({this.sId, this.name});

  Author.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    name = json['name'] != null ? new Name.fromJson(json['name']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    if (this.name != null) {
      data['name'] = this.name.toJson();
    }
    return data;
  }
}

class SubmissionDetails {
  String language;

  SubmissionDetails({this.language});

  SubmissionDetails.fromJson(Map<String, dynamic> json) {
    language = json['language'];
  }
}

class PendingUsers {
  String sId;
  int iV;
  String email;
  String updatedAt;
  String createdAt;
  String name;
  String type;
  String registerUsing;
  String school;
  String user;
  bool isActive;
  List<Null> children;
  bool schoolAdmin;
  String bus;
  String mobile;
  String avatarThumbnail;
  String gender;
  String lastLoginAt;
  String status;
  List<AccessModules> accessModules;
  String title;
  String rollNo;
  String parentEmail;
  String parentMobile;

  PendingUsers(
      {this.sId,
      this.iV,
      this.email,
      this.updatedAt,
      this.createdAt,
      this.name,
      this.type,
      this.registerUsing,
      this.school,
      this.user,
      this.isActive,
      this.children,
      this.schoolAdmin,
      this.bus,
      this.mobile,
      this.avatarThumbnail,
      this.gender,
      this.lastLoginAt,
      this.status,
      this.accessModules,
      this.title,
      this.rollNo,
      this.parentEmail,
      this.parentMobile});

  PendingUsers.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    iV = json['__v'];
    email = json['email'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    name = json['name'];
    type = json['type'];
    registerUsing = json['registerUsing'];
    school = json['school'];
    user = json['user'];
    isActive = json['isActive'];
    schoolAdmin = json['schoolAdmin'];
    bus = json['bus'];
    mobile = json['mobile'];
    avatarThumbnail = json['avatarThumbnail'];
    gender = json['gender'];
    lastLoginAt = json['last_login_at'];
    status = json['status'];
    if (json['accessModules'] != null) {
      accessModules = new List<AccessModules>();
      json['accessModules'].forEach((v) {
        accessModules.add(new AccessModules.fromJson(v));
      });
    }
    title = json['title'];
    rollNo = json['rollNo'];
    parentEmail = json['parentEmail'];
    parentMobile = json['parentMobile'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['__v'] = this.iV;
    data['email'] = this.email;
    data['updated_at'] = this.updatedAt;
    data['created_at'] = this.createdAt;
    data['name'] = this.name;
    data['type'] = this.type;
    data['registerUsing'] = this.registerUsing;
    data['school'] = this.school;
    data['user'] = this.user;
    data['isActive'] = this.isActive;

    data['schoolAdmin'] = this.schoolAdmin;
    data['bus'] = this.bus;
    data['mobile'] = this.mobile;
    data['avatarThumbnail'] = this.avatarThumbnail;
    data['gender'] = this.gender;
    data['last_login_at'] = this.lastLoginAt;
    data['status'] = this.status;
    if (this.accessModules != null) {
      data['accessModules'] =
          this.accessModules.map((v) => v.toJson()).toList();
    }
    data['title'] = this.title;
    data['rollNo'] = this.rollNo;
    data['parentEmail'] = this.parentEmail;
    data['parentMobile'] = this.parentMobile;
    return data;
  }
}

class AccessModules {
  String name;
  bool value;
  String sId;

  AccessModules({this.name, this.value, this.sId});

  AccessModules.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    data['_id'] = this.sId;
    return data;
  }
}

class SubmittedUsers {
  String sId;
  String mobile;
  String email;
  int iV;
  String updatedAt;
  String createdAt;
  String name;
  String type;
  String school;
  String registerUsing;
  String user;
  bool isActive;
  List<Null> children;
  bool schoolAdmin;
  String avatarThumbnail;
  String lastLoginAt;
  String bus;
  String status;
  String gender;
  List<AccessModules> accessModules;

  SubmittedUsers(
      {this.sId,
      this.mobile,
      this.email,
      this.iV,
      this.updatedAt,
      this.createdAt,
      this.name,
      this.type,
      this.school,
      this.registerUsing,
      this.user,
      this.isActive,
      this.children,
      this.schoolAdmin,
      this.avatarThumbnail,
      this.lastLoginAt,
      this.bus,
      this.status,
      this.gender,
      this.accessModules});

  SubmittedUsers.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    mobile = json['mobile'];
    email = json['email'];
    iV = json['__v'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    name = json['name'];
    type = json['type'];
    school = json['school'];
    registerUsing = json['registerUsing'];
    user = json['user'];
    isActive = json['isActive'];
    schoolAdmin = json['schoolAdmin'];
    avatarThumbnail = json['avatarThumbnail'];
    lastLoginAt = json['last_login_at'];
    bus = json['bus'];
    status = json['status'];
    gender = json['gender'];
    if (json['accessModules'] != null) {
      accessModules = new List<AccessModules>();
      json['accessModules'].forEach((v) {
        accessModules.add(new AccessModules.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['mobile'] = this.mobile;
    data['email'] = this.email;
    data['__v'] = this.iV;
    data['updated_at'] = this.updatedAt;
    data['created_at'] = this.createdAt;
    data['name'] = this.name;
    data['type'] = this.type;
    data['school'] = this.school;
    data['registerUsing'] = this.registerUsing;
    data['user'] = this.user;
    data['isActive'] = this.isActive;

    data['schoolAdmin'] = this.schoolAdmin;
    data['avatarThumbnail'] = this.avatarThumbnail;
    data['last_login_at'] = this.lastLoginAt;
    data['bus'] = this.bus;
    data['status'] = this.status;
    data['gender'] = this.gender;
    if (this.accessModules != null) {
      data['accessModules'] =
          this.accessModules.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
