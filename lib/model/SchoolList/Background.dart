class Background {
    String id;
    String created_at;
    bool gdrive;
    int h;
    String name;
    int size;
    String src;
    String thumbnail;
    String title;
    String type;
    int w;

    Background({this.id, this.created_at, this.gdrive, this.h, this.name, this.size, this.src, this.thumbnail, this.title, this.type, this.w});

    factory Background.fromJson(Map<String, dynamic> json) {
        return Background(
            id: json['_id'],
            created_at: json['created_at'], 
            gdrive: json['gdrive'], 
            h: json['h'], 
            name: json['name'], 
            size: json['size'], 
            src: json['src'], 
            thumbnail: json['thumbnail'], 
            title: json['title'], 
            type: json['type'], 
            w: json['w'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['_id'] = this.id;
        data['created_at'] = this.created_at;
        data['gdrive'] = this.gdrive;
        data['h'] = this.h;
        data['name'] = this.name;
        data['size'] = this.size;
        data['src'] = this.src;
        data['thumbnail'] = this.thumbnail;
        data['title'] = this.title;
        data['type'] = this.type;
        data['w'] = this.w;
        return data;
    }
}