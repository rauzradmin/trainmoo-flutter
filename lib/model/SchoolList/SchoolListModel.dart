import 'package:trainmoo/model/SchoolList/Address.dart';
import 'package:trainmoo/model/SchoolList/AttendanceReportConf.dart';
import 'package:trainmoo/model/SchoolList/Background.dart';
import 'package:trainmoo/model/SchoolList/Avatar.dart';


class SchoolListModel {
    int v;
    String id;
    Address address;
    int attendanceCutoffTime;
    AttendanceReportConf attendanceReportConf;
    Avatar avatar;
    Background background;
    List<String> category;
    String created_at;
    String email;
    String googlePlayUrl;
    String gradeSyncingUrl;
    String homeUrl;
    bool includeParentsForNotifications;
    int lastHour;
    List<Object> licenses;
    List<Object> media;
    List<String> medium;
    String name;
    String phone;
    String placeid;
    bool quizbankPublic;
    bool receiveAttendanceReport;
    String schoolSize;
    bool sendAttendanceAlerts;
    bool sendAttendanceAlertsToParent;
    bool sendAttendanceMarkAlertsToStudent;
    bool sendAttendanceMarkMails;
    bool sendGradePublishNotification;
    bool showAttendancePctToStudents;
    bool showOnlySurvey;
    int startHour;
    bool stopNotifications;
    String summary;
    String type;
    String uniqueid;
    String updated_at;
    String website;

    SchoolListModel({this.v, this.id, this.address, this.attendanceCutoffTime, this.attendanceReportConf, this.avatar, this.background, this.category, this.created_at, this.email, this.googlePlayUrl, this.gradeSyncingUrl, this.homeUrl, this.includeParentsForNotifications, this.lastHour, this.licenses, this.media, this.medium, this.name, this.phone, this.placeid, this.quizbankPublic, this.receiveAttendanceReport, this.schoolSize, this.sendAttendanceAlerts, this.sendAttendanceAlertsToParent, this.sendAttendanceMarkAlertsToStudent, this.sendAttendanceMarkMails, this.sendGradePublishNotification, this.showAttendancePctToStudents, this.showOnlySurvey, this.startHour, this.stopNotifications, this.summary, this.type, this.uniqueid, this.updated_at, this.website});

    factory SchoolListModel.fromJson(Map<String, dynamic> json) {
        return SchoolListModel(
            v: json['__v'],
            id: json['_id'],
            address: json['address'] != null ? Address.fromJson(json['address']) : null, 
            attendanceCutoffTime: json['attendanceCutoffTime'], 
            attendanceReportConf: json['attendanceReportConf'] != null ? AttendanceReportConf.fromJson(json['attendanceReportConf']) : null, 
            avatar: json['avatar'] != null ? Avatar.fromJson(json['avatar']) : null, 
            background: json['background'] != null ? Background.fromJson(json['background']) : null, 
            category: json['category'] != null ? new List<String>.from(json['category']) : null, 
            created_at: json['created_at'], 
            email: json['email'], 
            googlePlayUrl: json['googlePlayUrl'], 
            gradeSyncingUrl: json['gradeSyncingUrl'], 
            homeUrl: json['homeUrl'], 
            includeParentsForNotifications: json['includeParentsForNotifications'], 
            lastHour: json['lastHour'], 
//            licenses: json['licenses'] != null ? (json['licenses'] as List).map((i) => Object.fromJson(i)).toList() : null,
//            media: json['media'] != null ? (json['media'] as List).map((i) => Object.fromJson(i)).toList() : null,
            medium: json['medium'] != null ? new List<String>.from(json['medium']) : null, 
            name: json['name'], 
            phone: json['phone'], 
            placeid: json['placeid'], 
            quizbankPublic: json['quizbankPublic'], 
            receiveAttendanceReport: json['receiveAttendanceReport'], 
            schoolSize: json['schoolSize'], 
            sendAttendanceAlerts: json['sendAttendanceAlerts'], 
            sendAttendanceAlertsToParent: json['sendAttendanceAlertsToParent'], 
            sendAttendanceMarkAlertsToStudent: json['sendAttendanceMarkAlertsToStudent'], 
            sendAttendanceMarkMails: json['sendAttendanceMarkMails'], 
            sendGradePublishNotification: json['sendGradePublishNotification'], 
            showAttendancePctToStudents: json['showAttendancePctToStudents'], 
            showOnlySurvey: json['showOnlySurvey'], 
            startHour: json['startHour'], 
            stopNotifications: json['stopNotifications'], 
            summary: json['summary'], 
            type: json['type'], 
            uniqueid: json['uniqueid'], 
            updated_at: json['updated_at'], 
            website: json['website'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['__v'] = this.v;
        data['_id'] = this.id;
        data['attendanceCutoffTime'] = this.attendanceCutoffTime;
        data['created_at'] = this.created_at;
        data['email'] = this.email;
        data['googlePlayUrl'] = this.googlePlayUrl;
        data['gradeSyncingUrl'] = this.gradeSyncingUrl;
        data['homeUrl'] = this.homeUrl;
        data['includeParentsForNotifications'] = this.includeParentsForNotifications;
        data['lastHour'] = this.lastHour;
        data['name'] = this.name;
        data['phone'] = this.phone;
        data['placeid'] = this.placeid;
        data['quizbankPublic'] = this.quizbankPublic;
        data['receiveAttendanceReport'] = this.receiveAttendanceReport;
        data['schoolSize'] = this.schoolSize;
        data['sendAttendanceAlerts'] = this.sendAttendanceAlerts;
        data['sendAttendanceAlertsToParent'] = this.sendAttendanceAlertsToParent;
        data['sendAttendanceMarkAlertsToStudent'] = this.sendAttendanceMarkAlertsToStudent;
        data['sendAttendanceMarkMails'] = this.sendAttendanceMarkMails;
        data['sendGradePublishNotification'] = this.sendGradePublishNotification;
        data['showAttendancePctToStudents'] = this.showAttendancePctToStudents;
        data['showOnlySurvey'] = this.showOnlySurvey;
        data['startHour'] = this.startHour;
        data['stopNotifications'] = this.stopNotifications;
        data['summary'] = this.summary;
        data['type'] = this.type;
        data['uniqueid'] = this.uniqueid;
        data['updated_at'] = this.updated_at;
        data['website'] = this.website;
        if (this.address != null) {
            data['address'] = this.address.toJson();
        }
        if (this.attendanceReportConf != null) {
            data['attendanceReportConf'] = this.attendanceReportConf.toJson();
        }
        if (this.avatar != null) {
            data['avatar'] = this.avatar.toJson();
        }
        if (this.background != null) {
            data['background'] = this.background.toJson();
        }
        if (this.category != null) {
            data['category'] = this.category;
        }
//        if (this.licenses != null) {
//            data['licenses'] = this.licenses.map((v) => v.toJson()).toList();
//        }
//        if (this.media != null) {
//            data['media'] = this.media.map((v) => v.toJson()).toList();
//        }
        if (this.medium != null) {
            data['medium'] = this.medium;
        }
        return data;
    }

    static List<SchoolListModel> fromJsonArray(List<dynamic> json) {
        List<SchoolListModel> bannerLists =
        json.map<SchoolListModel>((json) => SchoolListModel.fromJson(json)).toList();
        return bannerLists;
    }
}