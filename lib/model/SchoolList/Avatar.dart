class Avatar {
    String id;
    String created_at;
    bool gdrive;
    String name;
    int size;
    String src;
    String type;

    Avatar({this.id, this.created_at, this.gdrive, this.name, this.size, this.src, this.type});

    factory Avatar.fromJson(Map<String, dynamic> json) {
        return Avatar(
            id: json['_id'],
            created_at: json['created_at'], 
            gdrive: json['gdrive'], 
            name: json['name'], 
            size: json['size'], 
            src: json['src'], 
            type: json['type'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['_id'] = this.id;
        data['created_at'] = this.created_at;
        data['gdrive'] = this.gdrive;
        data['name'] = this.name;
        data['size'] = this.size;
        data['src'] = this.src;
        data['type'] = this.type;
        return data;
    }
}