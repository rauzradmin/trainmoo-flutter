class Address {
    String city;
    String country;
    String location;
    String state;
    int zip;

    Address({this.city, this.country, this.location, this.state, this.zip});

    factory Address.fromJson(Map<String, dynamic> json) {
        return Address(
            city: json['city'], 
            country: json['country'], 
            location: json['location'], 
            state: json['state'], 
            zip: json['zip'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['city'] = this.city;
        data['country'] = this.country;
        data['location'] = this.location;
        data['state'] = this.state;
        data['zip'] = this.zip;
        return data;
    }
}