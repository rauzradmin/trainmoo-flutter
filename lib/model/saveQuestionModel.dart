class SaveQuestion
{
  int id;
  String name;

  SaveQuestion({this.id, this.name});

  factory SaveQuestion.fromJson(Map<String, dynamic> json) {
    return SaveQuestion(
      id: json['_id'],
      name: json['name'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;

    return data;
  }

  static List<SaveQuestion> fromJsonArray(List<dynamic> json) {
    List<SaveQuestion> bannerLists =
    json.map<SaveQuestion>((json) => SaveQuestion.fromJson(json)).toList();
    return bannerLists;
  }
}