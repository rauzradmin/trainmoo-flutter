/// id : "Science"
/// category : "Science"
/// count : 1

class QuestionCategory {
  String id;
//  String category;

  QuestionCategory(this.id);

  QuestionCategory.map(dynamic obj) {
    this.id = obj["_id"];
//    this.category = obj["category"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["_id"] = id;
//    map["category"] = category;
    return map;
  }

  static List<QuestionCategory> fromJsonArray(List<dynamic> json) {
    List<QuestionCategory> list = json
        .map<QuestionCategory>((json) => QuestionCategory.map(json))
        .toList();
    return list;
  }
}
