import 'package:trainmoo/model/FileModel.dart';
import 'package:trainmoo/ui/util/util.dart';

class SessionModel {
  String title;
  String start;
  String eventDate;
  String eventEndDate;
  String end;
  bool isAllDay;
  String event;
  String type;
  String school;
  String details;
  List<FileModel> files;
  bool isBreak;
  String classId;
  bool attendanceDone = false;

  SessionModel(
      this.title,
      this.start,
      this.end,
      this.isAllDay,
      this.event,
      this.type,
      this.school,
      this.details,
      this.files,
      this.isBreak,
      this.classId);

  SessionModel.map(dynamic obj) {
    this.title = obj["title"];
    this.eventDate =
        obj.containsKey('eventDate') ? obj["eventDate"] : obj["start"];
    this.eventEndDate =
        obj.containsKey('eventEndDate') ? obj["eventEndDate"] : obj['end'];
    String time;
    List<String> timeSlot;
    if (!obj.containsKey('eventDate')) {
      time = Utility.getTimeOfSession(obj["start"]);
      timeSlot = time.split(' ');
    }
    this.start = obj.containsKey('eventDate') ? obj["start"] : timeSlot[0];
    this.end = obj.containsKey('eventDate') ? obj["end"] : timeSlot[1];
    this.isAllDay = obj["isAllDay"];
    this.event = obj["event"];
    this.type = obj["type"];
    this.school = obj["school"];
    this.details = obj["details"];
    this.files = FileModel.fromJsonArray(obj["files"]);
    this.isBreak = obj["isbreak"];
    this.classId = obj["classid"];
    this.attendanceDone = obj.containsKey('attendanceDone')
        ? obj["attendanceDone"]
        : obj["presentNum"] != null
            ? obj["presentNum"] > 0 ? true : false
            : false;
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["title"] = title;
    map["eventDate"] = eventDate;
    map['eventEndDate'] = eventEndDate;
    map["start"] = start;
    map["end"] = end;
    map["isAllDay"] = isAllDay;
    map["event"] = event;
    map["type"] = type;
    map["school"] = school;
    map["details"] = details;
    map["files"] = files;
    map["isbreak"] = isBreak;
    map["classid"] = classId;
    map["attendanceDone"] = attendanceDone;
    return map;
  }

  static List<SessionModel> fromJsonArray(List<dynamic> json) {
    if (json == null || json.length <= 0) return [];
    List<SessionModel> list =
        json.map<SessionModel>((json) => SessionModel.map(json)).toList();
    return list;
  }
}
