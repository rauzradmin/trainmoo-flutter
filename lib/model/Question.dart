import 'Option.dart';

class Question {
  int v;
  String id;
  String category;
  String created_at;
  String explanation;
  bool explanationIsCode;
  bool isCode;
  String level;
  List<Option> options;
  String question;
  String school;
  String subquestion;
  List<Object> tags;
  String type;
  String updated_at;
  List files;
  String created_by;
  List dashOptions;
  List explanationFiles;
  int index;
  int marks;
  String post;
  int seqno;
  bool ansMultipal;

  Question(
      {this.v,
      this.id,
      this.category,
      this.created_at,
      this.explanation,
      this.explanationIsCode,
      this.isCode,
      this.level,
      this.options,
      this.question,
      this.school,
      this.subquestion,
      this.tags,
      this.type,
      this.updated_at,
      this.files,
      this.created_by,
      this.dashOptions,
      this.explanationFiles,
      this.index,
      this.marks,
      this.post,
      this.seqno,
      this.ansMultipal});

  factory Question.fromJson(Map<String, dynamic> json) {
    var ansMultiple = json['options'].where((item) {
      return item['answer'] == true;
    });
    return Question(
        v: json['__v'],
        id: json['_id'],
        category: json['category'],
        created_at: json['created_at'],
        created_by: json['created_by'],
        explanationFiles: json['explanationFiles'],
        dashOptions: json['dashOptions'],
        explanation: json['explanation'],
        isCode: json['isCode'],
        index: json['index'],
        level: json['level'],
        marks: json['marks'],
        post: json['post'],
        seqno: json['seqno'],
        options: json['options'] == null || json['options'] == []
            ? []
            : Option.fromJsonArray(json['options']),
        question: json['question'],
        school: json['school'],
        type: json['type'],
        updated_at: json['updated_at'],
        files: json['files'],
        ansMultipal: ansMultiple.length > 1 ? true : false);
  }

  Question.map(dynamic obj) {
    this.v = obj['__v'];
    this.id = obj['_id'];
    this.category = obj['category'];
    this.created_at = obj['created_at'];
    this.created_by = obj['created_by'];
    this.dashOptions = obj['dashOptions'];
    this.explanation = obj['explanation'];
    this.explanationFiles = obj['explanationFiles'];
    this.explanationIsCode = obj['explanationIsCode'];
    this.isCode = obj['isCode'];
    this.index = obj['index'];
    this.marks = obj['marks'];
    this.post = obj['post'];
    this.seqno = obj['seqno'];
    this.options = obj['options'] == null || obj['options'] == []
        ? []
        : Option.fromJsonArray(obj['options']);
    this.level = obj['level'];
    this.question = obj['question'];
    this.school = obj['school'];
    this.subquestion = obj['subquestion'];
    this.type = obj['type'];
    this.updated_at = obj['updated_at'];
    this.files = obj['files'];
    this.ansMultipal = obj['ansMultipal'];
  }
  static List<Question> fromJsonArray(List<dynamic> json) {
    List<Question> list =
        json.map<Question>((json) => Question.fromJson(json)).toList();
    print("This is List: ======>>>> $list");
    return list;
  }
}
