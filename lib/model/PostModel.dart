/// _id : "5e203f5dd6ffaf005e603d4f"
/// minified : true
/// type : "Material"
/// school : {"_id":"5b371b6c6b994000216d1457","name":"Modern University"}
/// class : {"_id":"5bef311b53b74d006742bca8","name":"Project ML","users":{"student":7,"teacher":2}}
/// text : "<p>This is a material</p>"
/// submittable : false
/// created_at : "2020-01-16T10:47:57.574Z"
/// updated_at : "2020-01-16T10:47:57.574Z"
/// author : {"name":{"firstname":"pranav","lastname":"singhania"},"avatar":{"src":"https://s3.amazonaws.com/uifaces/faces/twitter/magoo04/128.jpg","_id":"5b371b6c6b994000216d1452","created_at":"2018-06-30T05:55:56.523Z","gdrive":false},"_id":"5b371b6c6b994000216d144e"}
/// submissionDetails : {"results":[]}
/// users : []
/// poll : {"options":[]}
/// commentsDisabled : false
/// assessment : {"passageView":false,"random":true,"retake":false,"attempts":1}
/// task : {"additionalMarks":0,"additionalMaxMarks":0}
/// isCode : false
/// pinned : false
/// locked : false
/// replies : []
/// graded : 0
/// solutions : 0
/// comments : 0
/// submissions : 0
/// views : 2
/// likes : 0
/// files : []
/// anonymous : false
/// __v : 0
/// starred : false
/// liked : false
/// displayTime : "11 days ago"

class PostModel {
  String id;
  bool minified;
  String type;
  SchoolBean school;
  ClassBean classBean;
  String text;
  bool submittable;
  String createdAt;
  String updatedAt;
  AuthorBean author;
  SubmissionDetailsBean submissionDetails;
  List<dynamic> users;
  PollBean poll;
  bool commentsDisabled;
  AssessmentBean assessment;
  TaskBean task;
  bool isCode;
  bool pinned;
  bool locked;
  List<dynamic> replies;
  int graded;
  int solutions;
  int comments;
  int submissions;
  int views;
  int likes;
  List<dynamic> files;
  bool anonymous;
  int V;
  bool starred;
  bool liked;
  String displayTime;

  PostModel(
      this.id,
      this.minified,
      this.type,
      this.school,
      this.classBean,
      this.text,
      this.submittable,
      this.createdAt,
      this.updatedAt,
      this.author,
      this.submissionDetails,
      this.users,
      this.poll,
      this.commentsDisabled,
      this.assessment,
      this.task,
      this.isCode,
      this.pinned,
      this.locked,
      this.replies,
      this.graded,
      this.solutions,
      this.comments,
      this.submissions,
      this.views,
      this.likes,
      this.files,
      this.anonymous,
      this.V,
      this.starred,
      this.liked,
      this.displayTime);

  PostModel.map(dynamic obj) {
    this.id = obj["_id"];
    this.minified = obj["minified"];
    this.type = obj["type"];
    this.school = SchoolBean.map(obj["school"]);
    this.classBean = ClassBean.map(obj["class"]);
    this.text = obj["text"];
    this.submittable = obj["submittable"];
    this.createdAt = obj["createdAt"];
    this.updatedAt = obj["updatedAt"];
    this.author = AuthorBean.map(obj["author"]);
    this.submissionDetails =
        SubmissionDetailsBean.map(obj["submissionDetails"]);
    this.users = obj["users"];
    this.poll = PollBean.map(obj["poll"]);
    this.commentsDisabled = obj["commentsDisabled"];
    this.assessment = AssessmentBean.map(obj["assessment"]);
    this.task = TaskBean.map(obj["task"]);
    this.isCode = obj["isCode"];
    this.pinned = obj["pinned"];
    this.locked = obj["locked"];
    this.replies = obj["replies"];
    this.graded = obj["graded"];
    this.solutions = obj["solutions"];
    this.comments = obj["comments"];
    this.submissions = obj["submissions"];
    this.views = obj["views"];
    this.likes = obj["likes"];
    this.files = obj["files"];
    this.anonymous = obj["anonymous"];
    this.V = obj["V"];
    this.starred = obj["starred"];
    this.liked = obj["liked"];
    this.displayTime = obj["displayTime"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["Id"] = id;
    map["minified"] = minified;
    map["type"] = type;
    map["school"] = school;
    map["class"] = classBean;
    map["text"] = text;
    map["submittable"] = submittable;
    map["createdAt"] = createdAt;
    map["updatedAt"] = updatedAt;
    map["author"] = author;
    map["submissionDetails"] = submissionDetails;
    map["users"] = users;
    map["poll"] = poll;
    map["commentsDisabled"] = commentsDisabled;
    map["assessment"] = assessment;
    map["task"] = task;
    map["isCode"] = isCode;
    map["pinned"] = pinned;
    map["locked"] = locked;
    map["replies"] = replies;
    map["graded"] = graded;
    map["solutions"] = solutions;
    map["comments"] = comments;
    map["submissions"] = submissions;
    map["views"] = views;
    map["likes"] = likes;
    map["files"] = files;
    map["anonymous"] = anonymous;
    map["V"] = V;
    map["starred"] = starred;
    map["liked"] = liked;
    map["displayTime"] = displayTime;
    return map;
  }

  static List<PostModel> fromJsonArray(List<dynamic> json) {
    List<PostModel> bannerLists =
        json.map<PostModel>((json) => PostModel.map(json)).toList();
    return bannerLists;
  }
}

/// additionalMarks : 0
/// additionalMaxMarks : 0

class TaskBean {
  int additionalMarks;
  int additionalMaxMarks;

  TaskBean(this.additionalMarks, this.additionalMaxMarks);

  TaskBean.map(dynamic obj) {
    this.additionalMarks = obj["additionalMarks"];
    this.additionalMaxMarks = obj["additionalMaxMarks"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["additionalMarks"] = additionalMarks;
    map["additionalMaxMarks"] = additionalMaxMarks;
    return map;
  }
}

/// passageView : false
/// random : true
/// retake : false
/// attempts : 1

class AssessmentBean {
  bool passageView;
  bool random;
  bool retake;
  int attempts;

  AssessmentBean(this.passageView, this.random, this.retake, this.attempts);

  AssessmentBean.map(dynamic obj) {
    this.passageView = obj["passageView"];
    this.random = obj["random"];
    this.retake = obj["retake"];
    this.attempts = obj["attempts"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["passageView"] = passageView;
    map["random"] = random;
    map["retake"] = retake;
    map["attempts"] = attempts;
    return map;
  }
}

/// options : []

class PollBean {
  List<dynamic> options;

  PollBean(this.options);

  PollBean.map(dynamic obj) {
    this.options = obj["options"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["options"] = options;
    return map;
  }
}

/// results : []

class SubmissionDetailsBean {
  List<dynamic> results;

  SubmissionDetailsBean(this.results);

  SubmissionDetailsBean.map(dynamic obj) {
    this.results = obj["results"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["results"] = results;
    return map;
  }
}

/// name : {"firstname":"pranav","lastname":"singhania"}
/// avatar : {"src":"https://s3.amazonaws.com/uifaces/faces/twitter/magoo04/128.jpg","id":"5b371b6c6b994000216d1452","createdat":"2018-06-30T05:55:56.523Z","gdrive":false}
/// id : "5b371b6c6b994000216d144e"

class AuthorBean {
  NameBean name;
  AvatarBean avatar;
  String Id;

  AuthorBean(this.name, this.avatar, this.Id);

  AuthorBean.map(dynamic obj) {
    this.name = NameBean.map(obj["name"]);
    this.avatar = AvatarBean.map(obj["avatar"]);
    this.Id = obj["Id"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["name"] = name;
    map["avatar"] = avatar;
    map["Id"] = Id;
    return map;
  }
}

/// src : "https://s3.amazonaws.com/uifaces/faces/twitter/magoo04/128.jpg"
/// id : "5b371b6c6b994000216d1452"
/// createdat : "2018-06-30T05:55:56.523Z"
/// gdrive : false

class AvatarBean {
  String src;
  String Id;
  String createdAt;
  bool gdrive;

  AvatarBean(this.src, this.Id, this.createdAt, this.gdrive);

  AvatarBean.map(dynamic obj) {
    this.src = obj["src"];
    this.Id = obj["Id"];
    this.createdAt = obj["createdAt"];
    this.gdrive = obj["gdrive"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["src"] = src;
    map["Id"] = Id;
    map["createdAt"] = createdAt;
    map["gdrive"] = gdrive;
    return map;
  }
}

/// firstname : "pranav"
/// lastname : "singhania"

class NameBean {
  String firstname;
  String lastname;

  NameBean(this.firstname, this.lastname);

  NameBean.map(dynamic obj) {
    this.firstname = obj["firstname"];
    this.lastname = obj["lastname"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["firstname"] = firstname;
    map["lastname"] = lastname;
    return map;
  }
}

/// id : "5bef311b53b74d006742bca8"
/// name : "Project ML"
/// users : {"student":7,"teacher":2}

class ClassBean {
  String Id;
  String name;
  UsersBean users;

  ClassBean(this.Id, this.name, this.users);

  ClassBean.map(dynamic obj) {
    this.Id = obj["Id"];
    this.name = obj["name"];
    this.users = UsersBean.map(obj["users"]);
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["Id"] = Id;
    map["name"] = name;
    map["users"] = users;
    return map;
  }
}

/// student : 7
/// teacher : 2

class UsersBean {
  int student;
  int teacher;

  UsersBean(this.student, this.teacher);

  UsersBean.map(dynamic obj) {
    this.student = obj["student"];
    this.teacher = obj["teacher"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["student"] = student;
    map["teacher"] = teacher;
    return map;
  }
}

/// id : "5b371b6c6b994000216d1457"
/// name : "Modern University"

class SchoolBean {
  String Id;
  String name;

  SchoolBean(this.Id, this.name);

  SchoolBean.map(dynamic obj) {
    this.Id = obj["Id"];
    this.name = obj["name"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["Id"] = Id;
    map["name"] = name;
    return map;
  }
}
