import 'package:trainmoo/model/age_between.dart';

class Service {
  String title;
  String location = "Location";
  String cities = "City";
  String gender = "Male";
  List<AgeBetween> ageBetween;
  List<String> interests;

  Service(
      {this.title,
      this.location,
      this.cities,
      this.gender,
      this.ageBetween,
      this.interests});

  static getAgeBetween() {
    List<AgeBetween> ageBetweenList = [];
    ageBetweenList.add(AgeBetween(to: 18, from: 8));
    ageBetweenList.add(AgeBetween(to: 30, from: 19));
    ageBetweenList.add(AgeBetween(to: 50, from: 30));
    return ageBetweenList;
  }

  static getServiceList() {
    List<Service> services = [];
    services.add(Service(
        title: "For Services",
        location: "Location1",
        cities: "City",
        gender: "Male",
        ageBetween: getAgeBetween(),
        interests: getInterests()));
    services.add(Service(
        title: "Under 13, San Francisco",
        location: "Location2",
        cities: "City2",
        gender: "Male",
        ageBetween: getAgeBetween(),
        interests: getInterests()));
    services.add(Service(
        title: "For Male",
        location: "Location3",
        cities: "City3",
        gender: "Male",
        ageBetween: getAgeBetween(),
        interests: getInterests()));
    services.add(Service(
        title: "Online",
        location: "Location4",
        cities: "City4",
        gender: "Male",
        ageBetween: getAgeBetween(),
        interests: getInterests()));
    services.add(Service(
        title: "For Female",
        location: "Location5",
        cities: "City5",
        gender: "Male",
        ageBetween: getAgeBetween(),
        interests: getInterests()));
    services.add(Service(
        title: "For USA",
        location: "Location6",
        cities: "City6",
        gender: "Male",
        ageBetween: getAgeBetween(),
        interests: getInterests()));
    return services;
  }

  static List<String> getInterests() {
    return ["Interest1", "Interest2", "Interest3"];
  }
}
