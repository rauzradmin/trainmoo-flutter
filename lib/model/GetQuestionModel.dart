import 'Question.dart';

class GetQuestionModel {
    List<Question> questions;
    int total;

    GetQuestionModel({this.questions, this.total});

    factory GetQuestionModel.fromJson(Map<String, dynamic> json) {
        return GetQuestionModel(
            questions: json['questions'] != null ? (json['questions'] as List).map((i) => Question.fromJson(i)).toList() : null, 
            total: json['total'], 
        );
    }

    static List<Question> fromJsonArray(List<dynamic> json) {
        List<Question> list = json
            .map<Question>((json) => Question.map(json))
            .toList();
        return list;
    }

//    Map<String, dynamic> toJson() {
//        final Map<String, dynamic> data = new Map<String, dynamic>();
//        data['total'] = this.total;
//        if (this.questions != null) {
//            data['questions'] = this.questions.map((v) => ()).toList();
//        }
//        return data;
//    }
}