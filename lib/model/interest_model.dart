class InterestModel {
  String sId;
  int interestsId;
  String name;
  int iV;

  InterestModel({this.sId, this.interestsId, this.name, this.iV});

  InterestModel.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    interestsId = json['interestsId'];
    name = json['name'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['interestsId'] = this.interestsId;
    data['name'] = this.name;
    data['__v'] = this.iV;
    return data;
  }
}
