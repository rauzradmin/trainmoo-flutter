class ReviewAssignmentListModel {
  String title;
  String date;
  String desc;

  ReviewAssignmentListModel({this.title, this.date, this.desc});
}
