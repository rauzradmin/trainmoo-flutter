class AgeBetween {
  int from;
  int to;

  AgeBetween({this.to, this.from});

  static Map<String, dynamic> toMap(AgeBetween ageBetween) {
    Map<String, dynamic> assetMap = Map();
    assetMap['from'] = ageBetween.from;
    assetMap['to'] = ageBetween.to;

    return assetMap;
  }

  static List<Map<String, dynamic>> mapList(List<AgeBetween> agegroup) {
    List<Map<String, dynamic>> listOfOffer = agegroup
        .map((offer) => {
              "from": offer.from,
              "to": offer.to,
            })
        .toList();
    return listOfOffer;
  }
}
