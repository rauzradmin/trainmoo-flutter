class CommonModel {
  String title;
  String image;
  String desc;

  CommonModel({this.title, this.image, this.desc});
}
