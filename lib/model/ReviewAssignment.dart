import 'package:trainmoo/model/GetCommentModel.dart';
import 'package:trainmoo/model/RepliesModel.dart';
import 'package:trainmoo/model/StudentModel.dart';

class ReviewAssignemt {
  String postId;
  String type;
  String text;
  List<StudentModel> pendingUsers;
  List<StudentModel> submittedUsers;
  List<Replies> repliesData;
  List<RepliesModel> replies;

  ReviewAssignemt(
      {this.postId,
      this.type,
      this.text,
      this.pendingUsers,
      this.submittedUsers,
      this.replies});

  ReviewAssignemt.map(dynamic obj) {
    this.postId = obj["_id"];
    this.type = obj['type'];
    this.text = obj["text"];
    this.pendingUsers = StudentModel.fromJsonArray(obj["pendingUsers"]);
    this.submittedUsers = StudentModel.fromJsonArray(obj["submittedUsers"]);
    this.replies = obj['replies'] != null
        ? RepliesModel.fromJsonArray(obj['replies'])
        : null;
    if (obj['replies'] != null) {
      repliesData = new List<Replies>();
      obj['replies'].forEach((v) {
        repliesData.add(new Replies.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["_id"] = postId;
    map["text"] = text;
    map['type'] = type;
    map["pendingUsers"] = pendingUsers;
    map["submittedUsers"] = submittedUsers;
    map['replies'] = replies;
    return map;
  }

  static List<ReviewAssignemt> fromJsonArray(List<dynamic> json) {
    if (json == null || json.length <= 0) return [];
    List<ReviewAssignemt> list =
        json.map<ReviewAssignemt>((json) => ReviewAssignemt.map(json)).toList();
    return list;
  }
}
