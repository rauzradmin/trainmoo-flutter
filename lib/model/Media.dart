class Media {
    String mediaId;
    String createdAt;
    bool gDrive;
    int h;
    String name;
    int size;
    String src;
    String thumbnail;
    String title;
    String type;
    int w;

    Media({this.mediaId, this.createdAt, this.gDrive, this.h, this.name, this.size, this.src, this.thumbnail, this.title, this.type, this.w});

    factory Media.fromJson(Map<String, dynamic> json) {
        return Media(
            mediaId: json['_id'],
            createdAt: json['created_at'],
            gDrive: json['gdrive'],
            h: json['h'], 
            name: json['name'], 
            size: json['size'], 
            src: json['src'], 
            thumbnail: json['thumbnail'], 
            title: json['title'], 
            type: json['type'], 
            w: json['w'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['_id'] = this.mediaId;
        data['created_at'] = this.createdAt;
        data['gdrive'] = this.gDrive;
        data['h'] = this.h;
        data['name'] = this.name;
        data['size'] = this.size;
        data['src'] = this.src;
        data['thumbnail'] = this.thumbnail;
        data['title'] = this.title;
        data['type'] = this.type;
        data['w'] = this.w;
        return data;
    }
}