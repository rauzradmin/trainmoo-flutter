import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';

import 'Country.dart';

//export 'Country.dart';

const _platform = const MethodChannel('smart_landlord');
Future<List<CountryCode>> _fetchLocalizedCountryCodeNames() async {
  List<CountryCode> renamed = new List();
  Map result;
  try {
    var isoCodes = <String>[];
    CountryCode.ALL.forEach((CountryCode country) {
      isoCodes.add(country.isoCode);
    });
    result = await _platform.invokeMethod(
        'getCountryCodeNames', <String, dynamic>{'isoCodes': isoCodes});
  } on PlatformException catch (e) {
    return CountryCode.ALL;
  }

  for (var country in CountryCode.ALL) {
    renamed.add(country.copyWith(name: result[country.isoCode]));
  }
  renamed.sort((CountryCode a, CountryCode b) =>
      removeDiacritics(a.name).compareTo(b.name));

  return renamed;
}

removeDiacritics(String name) {}

/// The country picker widget exposes an dialog to select a country from a
/// pre defined list, see [CountryCode.ALL]
class CountryCodePicker extends StatelessWidget {
  const CountryCodePicker({
    Key key,
    this.selectedCountryCode,
    @required this.onChanged,
    this.dense = false,
    this.showFlag = true,
    this.showDialingCode = false,
    this.showName = true,
  }) : super(key: key);

  final String selectedCountryCode;
  final ValueChanged<CountryCode> onChanged;
  final bool dense;
  final bool showFlag;
  final bool showDialingCode;
  final bool showName;

  @override
  Widget build(BuildContext context) {
    assert(debugCheckHasMaterial(context));
    CountryCode displayCountryCode =
        getSelectedCountryCode(selectedCountryCode);

    if (displayCountryCode == null) {
      displayCountryCode = CountryCode.findByIsoCode(
          Localizations.localeOf(context).countryCode);
    }

    return dense
        ? _renderDenseDisplay(context, displayCountryCode)
        : _renderDefaultDisplay(context, displayCountryCode);
  }

  _renderDefaultDisplay(BuildContext context, CountryCode displayCountryCode) {
    return InkWell(
      child: Container(
          height: 48,
          child: Row(
            children: <Widget>[
              Container(
                  child: showFlag
                      ? displayCountryCode.dialingCode.compareTo('91') == 0
                          ? Image.asset(
                              'assets/flags/in_flag.png',
//                      package: "smart_landlord",
                              height: 26,
                              width: 15.7,
                              fit: BoxFit.fitWidth,
                            )
                          : Image.asset(
                              displayCountryCode.asset,
                              height: 26,
                              width: 15.7,
                              fit: BoxFit.fitWidth,
                            )
                      : Container()),
              SizedBox(
                width: 8,
              ),
              Container(
                  child: showDialingCode
                      ? Text(
                          " +${displayCountryCode.dialingCode}",
                          style: AppTextStyle.getDynamicFontStyle(
                              Palette.secondaryTextColor, 16, FontType.Regular),
                        )
                      : Container()),
              Container(
                  child: showName
                      ? Text(
                          " ${displayCountryCode.name}",
                          style: TextStyle(fontSize: 22.0),
                        )
                      : Container()),
              UIHelper.horizontalSpaceSmall,
              Image(
                image: AssetImage(
                  '',
                ),
                height: 6,
                width: 12,
              ),

//          Container(
//            height: double.infinity,
//            width: 1,
//            decoration: BoxDecoration(color: Palette.dividerColor),
//          ),
//          UIHelper.horizontalSpaceSmall,
            ],
          ),
          decoration: new BoxDecoration(
              border: Border(
                  top: BorderSide.none,
                  left: BorderSide.none,
                  right: BorderSide.none,
                  bottom: BorderSide(color: Palette.dividerColor, width: 1)))),
      onTap: () {
        _selectCountryCode(context, displayCountryCode);
      },
    );
  }

  _renderDenseDisplay(BuildContext context, CountryCode displayCountryCode) {
    return InkWell(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Image.asset(
            displayCountryCode.asset,
//            package: "smart_landlord",
            height: 24.0,
            fit: BoxFit.fitWidth,
          ),
          Icon(Icons.arrow_drop_down,
              color: Theme.of(context).brightness == Brightness.light
                  ? Colors.grey.shade700
                  : Colors.white70),
        ],
      ),
      onTap: () {
        _selectCountryCode(context, displayCountryCode);
      },
    );
  }

  Future<Null> _selectCountryCode(
      BuildContext context, CountryCode defaultCountryCode) async {
    final CountryCode picked = await showCountryCodePicker(
      context: context,
      defaultCountryCode: defaultCountryCode,
    );

    if (picked != null && picked != selectedCountryCode) onChanged(picked);
  }

  CountryCode getSelectedCountryCode(String selectedCountryCode) {
    if (selectedCountryCode == null) selectedCountryCode = '1';
    for (CountryCode code in CountryCode.ALL) {
      if (code.dialingCode == selectedCountryCode) {
        return code;
      }
    }
  }
}

/// Display an [Dialog] with the country list to selection
/// you can pass and [defaultCountryCode], see [CountryCode.findByIsoCode]
Future<CountryCode> showCountryCodePicker({
  BuildContext context,
  CountryCode defaultCountryCode,
}) async {
  assert(CountryCode.findByIsoCode(defaultCountryCode.isoCode) != null);

  return await showDialog<CountryCode>(
    context: context,
    builder: (BuildContext context) => _CountryCodePickerDialog(
      defaultCountryCode: defaultCountryCode,
    ),
  );
}

class _CountryCodePickerDialog extends StatefulWidget {
  const _CountryCodePickerDialog({
    Key key,
    CountryCode defaultCountryCode,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CountryCodePickerDialogState();
}

class _CountryCodePickerDialogState extends State<_CountryCodePickerDialog> {
  TextEditingController controller = new TextEditingController();
  String filter;
  List<CountryCode> countries;

  @override
  void initState() {
    super.initState();

    countries = CountryCode.ALL;

    _fetchLocalizedCountryCodeNames().then((renamed) {
      setState(() {
        countries = renamed;
      });
    });

    controller.addListener(() {
      setState(() {
        filter = controller.text;
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Column(
        children: <Widget>[
          new TextField(
            style: AppTextStyle.getDynamicFontStyle(
                Palette.primaryTextColor, 20, FontType.Bold),
            decoration: new InputDecoration(
              hintText: MaterialLocalizations.of(context).searchFieldLabel,
              prefixIcon: Icon(Icons.search),
              suffixIcon: filter == null || filter == ""
                  ? Container(
                      height: 0.0,
                      width: 0.0,
                    )
                  : InkWell(
                      child: Icon(Icons.clear),
                      onTap: () {
                        controller.clear();
                      },
                    ),
            ),
            controller: controller,
          ),
          Expanded(
            child: Scrollbar(
              child: ListView.builder(
                itemCount: countries.length,
                itemBuilder: (BuildContext context, int index) {
                  CountryCode country = countries[index];
                  if (filter == null ||
                      filter == "" ||
                      country.name
                          .toLowerCase()
                          .contains(filter.toLowerCase()) ||
                      country.isoCode.contains(filter)) {
                    return InkWell(
                      child: ListTile(
                        trailing: Text("+ ${country.dialingCode}"),
                        title: Row(
                          children: <Widget>[
                            country.dialingCode.compareTo('1') == 0
                                ? Image.asset(
                                    'assets/flags/us_flag.png',
                                    height: 30,
//                                package: "smart_landlord",
                                  )
                                : Image.asset(
                                    country.asset,
                                    height: 30,
//                                package: "smart_landlord",
                                  ),
                            UIHelper.horizontalSpaceSmall,
                            Expanded(
                              child: Container(
                                child: Text(
                                  country.name,
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: AppTextStyle.getDynamicFontStyle(
                                      Palette.primaryTextColor,
                                      16,
                                      FontType.Regular),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        Navigator.pop(context, country);
                      },
                    );
                  }
                  return Container();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
