import 'package:flutter/foundation.dart';

/// A country definition with image asset, dialing code and localized name.
class CountryCode {
  /// the flag image asset name
  final String asset;

  /// the dialing code
  final String dialingCode;

  /// the 2-letter ISO code
  final String isoCode;

  /// the localized / English country name
  final String name;

  /// Instantiates an [CountryCode] with the specified [asset], [dialingCode] and [isoCode]
  const CountryCode({
    @required this.asset,
    @required this.dialingCode,
    @required this.isoCode,
    this.name = "",
  });

  @override
  bool operator ==(o) =>
      o is CountryCode &&
      o.dialingCode == this.dialingCode &&
      o.isoCode == this.isoCode &&
      o.asset == this.asset &&
      o.name == this.name;

  int get hashCode {
    int hash = 7;
    hash = 31 * hash + this.dialingCode.hashCode;
    hash = 31 * hash + this.asset.hashCode;
    hash = 31 * hash + this.name.hashCode;
    hash = 31 * hash + this.isoCode.hashCode;
    return hash;
  }

  static const CountryCode AD = CountryCode(
    asset: "assets/flags/ad_flag.png",
    dialingCode: "376",
    isoCode: "AD",
    name: "Andorra",
  );
  static const CountryCode AE = CountryCode(
    asset: "assets/flags/ae_flag.png",
    dialingCode: "971",
    isoCode: "AE",
    name: "United Arab Emirates",
  );
  static const CountryCode AF = CountryCode(
    asset: "assets/flags/af_flag.png",
    dialingCode: "93",
    isoCode: "AF",
    name: "Afghanistan",
  );
  static const CountryCode AG = CountryCode(
    asset: "assets/flags/ag_flag.png",
    dialingCode: "1",
    isoCode: "AG",
    name: "Antigua and Barbuda",
  );
  static const CountryCode AI = CountryCode(
    asset: "assets/flags/ai_flag.png",
    dialingCode: "1",
    isoCode: "AI",
    name: "Anguilla",
  );
  static const CountryCode AL = CountryCode(
    asset: "assets/flags/al_flag.png",
    dialingCode: "355",
    isoCode: "AL",
    name: "Albania",
  );
  static const CountryCode AM = CountryCode(
    asset: "assets/flags/am_flag.png",
    dialingCode: "374",
    isoCode: "AM",
    name: "Armenia",
  );
  static const CountryCode AO = CountryCode(
    asset: "assets/flags/ao_flag.png",
    dialingCode: "244",
    isoCode: "AO",
    name: "Angola",
  );
  static const CountryCode AQ = CountryCode(
    asset: "assets/flags/aq_flag.png",
    dialingCode: "672",
    isoCode: "AQ",
    name: "Antarctica",
  );
  static const CountryCode AR = CountryCode(
    asset: "assets/flags/ar_flag.png",
    dialingCode: "54",
    isoCode: "AR",
    name: "Argentina",
  );
  static const CountryCode AS = CountryCode(
    asset: "assets/flags/as_flag.png",
    dialingCode: "1",
    isoCode: "AS",
    name: "American Samoa",
  );
  static const CountryCode AT = CountryCode(
    asset: "assets/flags/at_flag.png",
    dialingCode: "43",
    isoCode: "AT",
    name: "Austria",
  );
  static const CountryCode AU = CountryCode(
    asset: "assets/flags/au_flag.png",
    dialingCode: "61",
    isoCode: "AU",
    name: "Australia",
  );
  static const CountryCode AW = CountryCode(
    asset: "assets/flags/aw_flag.png",
    dialingCode: "297",
    isoCode: "AW",
    name: "Aruba",
  );
  static const CountryCode AX = CountryCode(
    asset: "assets/flags/ax_flag.png",
    dialingCode: "358",
    isoCode: "AX",
    name: "Aaland Islands",
  );
  static const CountryCode AZ = CountryCode(
    asset: "assets/flags/az_flag.png",
    dialingCode: "994",
    isoCode: "AZ",
    name: "Azerbaijan",
  );
  static const CountryCode BA = CountryCode(
    asset: "assets/flags/ba_flag.png",
    dialingCode: "387",
    isoCode: "BA",
    name: "Bosnia and Herzegowina",
  );
  static const CountryCode BB = CountryCode(
    asset: "assets/flags/bb_flag.png",
    dialingCode: "1",
    isoCode: "BB",
    name: "Barbados",
  );
  static const CountryCode BD = CountryCode(
    asset: "assets/flags/bd_flag.png",
    dialingCode: "880",
    isoCode: "BD",
    name: "Bangladesh",
  );
  static const CountryCode BE = CountryCode(
    asset: "assets/flags/be_flag.png",
    dialingCode: "32",
    isoCode: "BE",
    name: "Belgium",
  );
  static const CountryCode BF = CountryCode(
    asset: "assets/flags/bf_flag.png",
    dialingCode: "226",
    isoCode: "BF",
    name: "Burkina Faso",
  );
  static const CountryCode BG = CountryCode(
    asset: "assets/flags/bg_flag.png",
    dialingCode: "359",
    isoCode: "BG",
    name: "Bulgaria",
  );
  static const CountryCode BH = CountryCode(
    asset: "assets/flags/bh_flag.png",
    dialingCode: "973",
    isoCode: "BH",
    name: "Bahrain",
  );
  static const CountryCode BI = CountryCode(
    asset: "assets/flags/bi_flag.png",
    dialingCode: "257",
    isoCode: "BI",
    name: "Burundi",
  );
  static const CountryCode BJ = CountryCode(
    asset: "assets/flags/bj_flag.png",
    dialingCode: "229",
    isoCode: "BJ",
    name: "Benin",
  );
  static const CountryCode BL = CountryCode(
    asset: "assets/flags/bl_flag.png",
    dialingCode: "590",
    isoCode: "BL",
    name: "Saint-Barthélemy",
  );
  static const CountryCode BM = CountryCode(
    asset: "assets/flags/bm_flag.png",
    dialingCode: "1",
    isoCode: "BM",
    name: "Bermuda",
  );
  static const CountryCode BN = CountryCode(
    asset: "assets/flags/bn_flag.png",
    dialingCode: "673",
    isoCode: "BN",
    name: "Brunei Darussalam",
  );
  static const CountryCode BO = CountryCode(
    asset: "assets/flags/bo_flag.png",
    dialingCode: "591",
    isoCode: "BO",
    name: "Bolivia",
  );
  static const CountryCode BQ = CountryCode(
    asset: "assets/flags/bq_flag.png",
    dialingCode: "599",
    isoCode: "BQ",
    name: "Bonaire, Sint Eustatius and Saba",
  );
  static const CountryCode BR = CountryCode(
    asset: "assets/flags/br_flag.png",
    dialingCode: "55",
    isoCode: "BR",
    name: "Brazil",
  );
  static const CountryCode BS = CountryCode(
    asset: "assets/flags/bs_flag.png",
    dialingCode: "1",
    isoCode: "BS",
    name: "Bahamas",
  );
  static const CountryCode BT = CountryCode(
    asset: "assets/flags/bt_flag.png",
    dialingCode: "975",
    isoCode: "BT",
    name: "Bhutan",
  );
  static const CountryCode BV = CountryCode(
    asset: "assets/flags/bv_flag.png",
    dialingCode: "55",
    isoCode: "BV",
    name: "Bouvet Island",
  );
  static const CountryCode BW = CountryCode(
    asset: "assets/flags/bw_flag.png",
    dialingCode: "267",
    isoCode: "BW",
    name: "Botswana",
  );
  static const CountryCode BY = CountryCode(
    asset: "assets/flags/by_flag.png",
    dialingCode: "375",
    isoCode: "BY",
    name: "Belarus",
  );
  static const CountryCode BZ = CountryCode(
    asset: "assets/flags/bz_flag.png",
    dialingCode: "501",
    isoCode: "BZ",
    name: "Belize",
  );
  static const CountryCode CA = CountryCode(
    asset: "assets/flags/ca_flag.png",
    dialingCode: "1",
    isoCode: "CA",
    name: "Canada",
  );
  static const CountryCode CC = CountryCode(
    asset: "assets/flags/cc_flag.png",
    dialingCode: "891",
    isoCode: "CC",
    name: "Cocos (Keeling) Islands",
  );
  static const CountryCode CD = CountryCode(
    asset: "assets/flags/cd_flag.png",
    dialingCode: "243",
    isoCode: "CD",
    name: "Congo, Democratic Republic Of (Was Zaire)",
  );
  static const CountryCode CF = CountryCode(
    asset: "assets/flags/cf_flag.png",
    dialingCode: "236",
    isoCode: "CF",
    name: "Central African Republic",
  );
  static const CountryCode CG = CountryCode(
    asset: "assets/flags/cg_flag.png",
    dialingCode: "242",
    isoCode: "CG",
    name: "Congo, Republic Of",
  );
  static const CountryCode CH = CountryCode(
    asset: "assets/flags/ch_flag.png",
    dialingCode: "41",
    isoCode: "CH",
    name: "Switzerland",
  );
  static const CountryCode CI = CountryCode(
    asset: "assets/flags/ci_flag.png",
    dialingCode: "225",
    isoCode: "CI",
    name: "Cote D'ivoire",
  );
  static const CountryCode CK = CountryCode(
    asset: "assets/flags/ck_flag.png",
    dialingCode: "682",
    isoCode: "CK",
    name: "Cook Islands",
  );
  static const CountryCode CL = CountryCode(
    asset: "assets/flags/cl_flag.png",
    dialingCode: "56",
    isoCode: "CL",
    name: "Chile",
  );
  static const CountryCode CM = CountryCode(
    asset: "assets/flags/cm_flag.png",
    dialingCode: "237",
    isoCode: "CM",
    name: "Cameroon",
  );
  static const CountryCode CN = CountryCode(
    asset: "assets/flags/cn_flag.png",
    dialingCode: "86",
    isoCode: "CN",
    name: "China",
  );
  static const CountryCode CO = CountryCode(
    asset: "assets/flags/co_flag.png",
    dialingCode: "57",
    isoCode: "CO",
    name: "Colombia",
  );
  static const CountryCode CR = CountryCode(
    asset: "assets/flags/cr_flag.png",
    dialingCode: "506",
    isoCode: "CR",
    name: "Costa Rica",
  );
  static const CountryCode CU = CountryCode(
    asset: "assets/flags/cu_flag.png",
    dialingCode: "53",
    isoCode: "CU",
    name: "Cuba",
  );
  static const CountryCode CV = CountryCode(
    asset: "assets/flags/cv_flag.png",
    dialingCode: "238",
    isoCode: "CV",
    name: "Cape Verde",
  );
  static const CountryCode CW = CountryCode(
    asset: "assets/flags/cw_flag.png",
    dialingCode: "599",
    isoCode: "CW",
    name: "Curaçao",
  );
  static const CountryCode CX = CountryCode(
    asset: "assets/flags/cx_flag.png",
    dialingCode: "61",
    isoCode: "CX",
    name: "Christmas Island",
  );
  static const CountryCode CY = CountryCode(
    asset: "assets/flags/cy_flag.png",
    dialingCode: "357",
    isoCode: "CY",
    name: "Cyprus",
  );
  static const CountryCode CZ = CountryCode(
    asset: "assets/flags/cz_flag.png",
    dialingCode: "420",
    isoCode: "CZ",
    name: "Czech Republic",
  );
  static const CountryCode DE = CountryCode(
    asset: "assets/flags/de_flag.png",
    dialingCode: "49",
    isoCode: "DE",
    name: "Germany",
  );
  static const CountryCode DJ = CountryCode(
    asset: "assets/flags/dj_flag.png",
    dialingCode: "253",
    isoCode: "DJ",
    name: "Djibouti",
  );
  static const CountryCode DK = CountryCode(
    asset: "assets/flags/dk_flag.png",
    dialingCode: "45",
    isoCode: "DK",
    name: "Denmark",
  );
  static const CountryCode DM = CountryCode(
    asset: "assets/flags/dm_flag.png",
    dialingCode: "1",
    isoCode: "DM",
    name: "Dominica",
  );
  static const CountryCode DO = CountryCode(
    asset: "assets/flags/do_flag.png",
    dialingCode: "1",
    isoCode: "DO",
    name: "Dominican Republic",
  );
  static const CountryCode DZ = CountryCode(
    asset: "assets/flags/dz_flag.png",
    dialingCode: "213",
    isoCode: "DZ",
    name: "Algeria",
  );
  static const CountryCode EC = CountryCode(
    asset: "assets/flags/ec_flag.png",
    dialingCode: "593",
    isoCode: "EC",
    name: "Ecuador",
  );
  static const CountryCode EE = CountryCode(
    asset: "assets/flags/ee_flag.png",
    dialingCode: "372",
    isoCode: "EE",
    name: "Estonia",
  );
  static const CountryCode EG = CountryCode(
    asset: "assets/flags/eg_flag.png",
    dialingCode: "20",
    isoCode: "EG",
    name: "Egypt",
  );
  static const CountryCode EH = CountryCode(
    asset: "assets/flags/eh_flag.png",
    dialingCode: "212",
    isoCode: "EH",
    name: "Western Sahara",
  );
  static const CountryCode ER = CountryCode(
    asset: "assets/flags/er_flag.png",
    dialingCode: "291",
    isoCode: "ER",
    name: "Eritrea",
  );
  static const CountryCode ES = CountryCode(
    asset: "assets/flags/es_flag.png",
    dialingCode: "34",
    isoCode: "ES",
    name: "Spain",
  );
  static const CountryCode ET = CountryCode(
    asset: "assets/flags/et_flag.png",
    dialingCode: "251",
    isoCode: "ET",
    name: "Ethiopia",
  );
  static const CountryCode FI = CountryCode(
    asset: "assets/flags/fi_flag.png",
    dialingCode: "358",
    isoCode: "FI",
    name: "Finland",
  );
  static const CountryCode FJ = CountryCode(
    asset: "assets/flags/fj_flag.png",
    dialingCode: "679",
    isoCode: "FJ",
    name: "Fiji",
  );
  static const CountryCode FK = CountryCode(
    asset: "assets/flags/fk_flag.png",
    dialingCode: "500",
    isoCode: "FK",
    name: "Falkland Islands (Malvinas)",
  );
  static const CountryCode FM = CountryCode(
    asset: "assets/flags/fm_flag.png",
    dialingCode: "691",
    isoCode: "FM",
    name: "Micronesia, Federated States Of",
  );
  static const CountryCode FO = CountryCode(
    asset: "assets/flags/fo_flag.png",
    dialingCode: "298",
    isoCode: "FO",
    name: "Faroe Islands",
  );
  static const CountryCode FR = CountryCode(
    asset: "assets/flags/fr_flag.png",
    dialingCode: "33",
    isoCode: "FR",
    name: "France",
  );
  static const CountryCode GA = CountryCode(
    asset: "assets/flags/ga_flag.png",
    dialingCode: "241",
    isoCode: "GA",
    name: "Gabon",
  );
  static const CountryCode GB = CountryCode(
    asset: "assets/flags/gb_flag.png",
    dialingCode: "44",
    isoCode: "GB",
    name: "United Kingdom",
  );
  static const CountryCode GD = CountryCode(
    asset: "assets/flags/gd_flag.png",
    dialingCode: "1",
    isoCode: "GD",
    name: "Grenada",
  );
  static const CountryCode GE = CountryCode(
    asset: "assets/flags/ge_flag.png",
    dialingCode: "995",
    isoCode: "GE",
    name: "Georgia",
  );
  static const CountryCode GF = CountryCode(
    asset: "assets/flags/gf_flag.png",
    dialingCode: "594",
    isoCode: "GF",
    name: "French Guiana",
  );
  static const CountryCode GG = CountryCode(
    asset: "assets/flags/gg_flag.png",
    dialingCode: "44",
    isoCode: "GG",
    name: "Guernsey",
  );
  static const CountryCode GH = CountryCode(
    asset: "assets/flags/gh_flag.png",
    dialingCode: "233",
    isoCode: "GH",
    name: "Ghana",
  );
  static const CountryCode GI = CountryCode(
    asset: "assets/flags/gi_flag.png",
    dialingCode: "350",
    isoCode: "GI",
    name: "Gibraltar",
  );
  static const CountryCode GL = CountryCode(
    asset: "assets/flags/gl_flag.png",
    dialingCode: "299",
    isoCode: "GL",
    name: "Greenland",
  );
  static const CountryCode GM = CountryCode(
    asset: "assets/flags/gm_flag.png",
    dialingCode: "220",
    isoCode: "GM",
    name: "Gambia",
  );
  static const CountryCode GN = CountryCode(
    asset: "assets/flags/gn_flag.png",
    dialingCode: "224",
    isoCode: "GN",
    name: "Guinea",
  );
  static const CountryCode GP = CountryCode(
    asset: "assets/flags/gp_flag.png",
    dialingCode: "590",
    isoCode: "GP",
    name: "Guadeloupe",
  );
  static const CountryCode GQ = CountryCode(
    asset: "assets/flags/gq_flag.png",
    dialingCode: "240",
    isoCode: "GQ",
    name: "Equatorial Guinea",
  );
  static const CountryCode GR = CountryCode(
    asset: "assets/flags/gr_flag.png",
    dialingCode: "30",
    isoCode: "GR",
    name: "Greece",
  );
  static const CountryCode GS = CountryCode(
    asset: "assets/flags/gs_flag.png",
    dialingCode: "500",
    isoCode: "GS",
    name: "South Georgia and The South Sandwich Islands",
  );
  static const CountryCode GT = CountryCode(
    asset: "assets/flags/gt_flag.png",
    dialingCode: "502",
    isoCode: "GT",
    name: "Guatemala",
  );
  static const CountryCode GU = CountryCode(
    asset: "assets/flags/gu_flag.png",
    dialingCode: "1",
    isoCode: "GU",
    name: "Guam",
  );
  static const CountryCode GW = CountryCode(
    asset: "assets/flags/gw_flag.png",
    dialingCode: "245",
    isoCode: "GW",
    name: "Guinea-bissau",
  );
  static const CountryCode GY = CountryCode(
    asset: "assets/flags/gy_flag.png",
    dialingCode: "592",
    isoCode: "GY",
    name: "Guyana",
  );
  static const CountryCode HK = CountryCode(
    asset: "assets/flags/hk_flag.png",
    dialingCode: "852",
    isoCode: "HK",
    name: "Hong Kong",
  );
  static const CountryCode HM = CountryCode(
    asset: "assets/flags/hm_flag.png",
    dialingCode: "61",
    isoCode: "HM",
    name: "Heard and Mc Donald Islands",
  );
  static const CountryCode HN = CountryCode(
    asset: "assets/flags/hn_flag.png",
    dialingCode: "504",
    isoCode: "HN",
    name: "Honduras",
  );
  static const CountryCode HR = CountryCode(
    asset: "assets/flags/hr_flag.png",
    dialingCode: "385",
    isoCode: "HR",
    name: "Croatia (Local Name: Hrvatska)",
  );
  static const CountryCode HT = CountryCode(
    asset: "assets/flags/ht_flag.png",
    dialingCode: "509",
    isoCode: "HT",
    name: "Haiti",
  );
  static const CountryCode HU = CountryCode(
    asset: "assets/flags/hu_flag.png",
    dialingCode: "36",
    isoCode: "HU",
    name: "Hungary",
  );
  static const CountryCode ID = CountryCode(
    asset: "assets/flags/id_flag.png",
    dialingCode: "62",
    isoCode: "ID",
    name: "Indonesia",
  );
  static const CountryCode IE = CountryCode(
    asset: "assets/flags/ie_flag.png",
    dialingCode: "353",
    isoCode: "IE",
    name: "Ireland",
  );
  static const CountryCode IL = CountryCode(
    asset: "assets/flags/il_flag.png",
    dialingCode: "972",
    isoCode: "IL",
    name: "Israel",
  );
  static const CountryCode IM = CountryCode(
    asset: "assets/flags/im_flag.png",
    dialingCode: "44",
    isoCode: "IM",
    name: "Isle of Man",
  );
  static const CountryCode IN = CountryCode(
    asset: "assets/flags/in_flag.png",
    dialingCode: "91",
    isoCode: "IN",
    name: "India",
  );
  static const CountryCode IO = CountryCode(
    asset: "assets/flags/io_flag.png",
    dialingCode: "246",
    isoCode: "IO",
    name: "British Indian Ocean Territory",
  );
  static const CountryCode IQ = CountryCode(
    asset: "assets/flags/iq_flag.png",
    dialingCode: "964",
    isoCode: "IQ",
    name: "Iraq",
  );
  static const CountryCode IR = CountryCode(
    asset: "assets/flags/ir_flag.png",
    dialingCode: "98",
    isoCode: "IR",
    name: "Iran (Islamic Republic Of)",
  );
  static const CountryCode IS = CountryCode(
    asset: "assets/flags/is_flag.png",
    dialingCode: "354",
    isoCode: "IS",
    name: "Iceland",
  );
  static const CountryCode IT = CountryCode(
    asset: "assets/flags/it_flag.png",
    dialingCode: "39",
    isoCode: "IT",
    name: "Italy",
  );
  static const CountryCode JE = CountryCode(
    asset: "assets/flags/je_flag.png",
    dialingCode: "44",
    isoCode: "JE",
    name: "Jersey",
  );
  static const CountryCode JM = CountryCode(
    asset: "assets/flags/jm_flag.png",
    dialingCode: "1",
    isoCode: "JM",
    name: "Jamaica",
  );
  static const CountryCode JO = CountryCode(
    asset: "assets/flags/jo_flag.png",
    dialingCode: "962",
    isoCode: "JO",
    name: "Jordan",
  );
  static const CountryCode JP = CountryCode(
    asset: "assets/flags/jp_flag.png",
    dialingCode: "81",
    isoCode: "JP",
    name: "Japan",
  );
  static const CountryCode KE = CountryCode(
    asset: "assets/flags/ke_flag.png",
    dialingCode: "254",
    isoCode: "KE",
    name: "Kenya",
  );
  static const CountryCode KG = CountryCode(
    asset: "assets/flags/kg_flag.png",
    dialingCode: "996",
    isoCode: "KG",
    name: "Kyrgyzstan",
  );
  static const CountryCode KH = CountryCode(
    asset: "assets/flags/kh_flag.png",
    dialingCode: "855",
    isoCode: "KH",
    name: "Cambodia",
  );
  static const CountryCode KI = CountryCode(
    asset: "assets/flags/ki_flag.png",
    dialingCode: "686",
    isoCode: "KI",
    name: "Kiribati",
  );
  static const CountryCode KM = CountryCode(
    asset: "assets/flags/km_flag.png",
    dialingCode: "269",
    isoCode: "KM",
    name: "Comoros",
  );
  static const CountryCode KN = CountryCode(
    asset: "assets/flags/kn_flag.png",
    dialingCode: "1",
    isoCode: "KN",
    name: "Saint Kitts and Nevis",
  );
  static const CountryCode KP = CountryCode(
    asset: "assets/flags/kp_flag.png",
    dialingCode: "850",
    isoCode: "KP",
    name: "Korea, Democratic People's Republic Of",
  );
  static const CountryCode KR = CountryCode(
    asset: "assets/flags/kr_flag.png",
    dialingCode: "82",
    isoCode: "KR",
    name: "Korea, Republic Of",
  );
  static const CountryCode KW = CountryCode(
    asset: "assets/flags/kw_flag.png",
    dialingCode: "965",
    isoCode: "KW",
    name: "Kuwait",
  );
  static const CountryCode KY = CountryCode(
    asset: "assets/flags/ky_flag.png",
    dialingCode: "965",
    isoCode: "KY",
    name: "Cayman Islands",
  );
  static const CountryCode KZ = CountryCode(
    asset: "assets/flags/kz_flag.png",
    dialingCode: "7",
    isoCode: "KZ",
    name: "Kazakhstan",
  );
  static const CountryCode LA = CountryCode(
    asset: "assets/flags/la_flag.png",
    dialingCode: "856",
    isoCode: "LA",
    name: "Lao People's Democratic Republic",
  );
  static const CountryCode LB = CountryCode(
    asset: "assets/flags/lb_flag.png",
    dialingCode: "961",
    isoCode: "LB",
    name: "Lebanon",
  );
  static const CountryCode LC = CountryCode(
    asset: "assets/flags/lc_flag.png",
    dialingCode: "1",
    isoCode: "LC",
    name: "Saint Lucia",
  );
  static const CountryCode LI = CountryCode(
    asset: "assets/flags/li_flag.png",
    dialingCode: "423",
    isoCode: "LI",
    name: "Liechtenstein",
  );
  static const CountryCode LK = CountryCode(
    asset: "assets/flags/lk_flag.png",
    dialingCode: "94",
    isoCode: "LK",
    name: "Sri Lanka",
  );
  static const CountryCode LR = CountryCode(
    asset: "assets/flags/lr_flag.png",
    dialingCode: "231",
    isoCode: "LR",
    name: "Liberia",
  );
  static const CountryCode LS = CountryCode(
    asset: "assets/flags/ls_flag.png",
    dialingCode: "266",
    isoCode: "LS",
    name: "Lesotho",
  );
  static const CountryCode LT = CountryCode(
    asset: "assets/flags/lt_flag.png",
    dialingCode: "370",
    isoCode: "LT",
    name: "Lithuania",
  );
  static const CountryCode LU = CountryCode(
    asset: "assets/flags/lu_flag.png",
    dialingCode: "352",
    isoCode: "LU",
    name: "Luxembourg",
  );
  static const CountryCode LV = CountryCode(
    asset: "assets/flags/lv_flag.png",
    dialingCode: "371",
    isoCode: "LV",
    name: "Latvia",
  );
  static const CountryCode LY = CountryCode(
    asset: "assets/flags/ly_flag.png",
    dialingCode: "218",
    isoCode: "LY",
    name: "Libyan Arab Jamahiriya",
  );
  static const CountryCode MA = CountryCode(
    asset: "assets/flags/ma_flag.png",
    dialingCode: "212",
    isoCode: "MA",
    name: "Morocco",
  );
  static const CountryCode MC = CountryCode(
    asset: "assets/flags/mc_flag.png",
    dialingCode: "377",
    isoCode: "MC",
    name: "Monaco",
  );
  static const CountryCode MD = CountryCode(
    asset: "assets/flags/md_flag.png",
    dialingCode: "373",
    isoCode: "MD",
    name: "Moldova, Republic Of",
  );
  static const CountryCode ME = CountryCode(
    asset: "assets/flags/me_flag.png",
    dialingCode: "382",
    isoCode: "ME",
    name: "Montenegro",
  );
  static const CountryCode MF = CountryCode(
    asset: "assets/flags/mf_flag.png",
    dialingCode: "1",
    isoCode: "MF",
    name: "Saint-Martin",
  );
  static const CountryCode MG = CountryCode(
    asset: "assets/flags/mg_flag.png",
    dialingCode: "261",
    isoCode: "MG",
    name: "Madagascar",
  );
  static const CountryCode MH = CountryCode(
    asset: "assets/flags/mh_flag.png",
    dialingCode: "692",
    isoCode: "MH",
    name: "Marshall Islands",
  );
  static const CountryCode MK = CountryCode(
    asset: "assets/flags/mk_flag.png",
    dialingCode: "389",
    isoCode: "MK",
    name: "Macedonia, The Former Yugoslav Republic Of",
  );
  static const CountryCode ML = CountryCode(
    asset: "assets/flags/ml_flag.png",
    dialingCode: "223",
    isoCode: "ML",
    name: "Mali",
  );
  static const CountryCode MM = CountryCode(
    asset: "assets/flags/mm_flag.png",
    dialingCode: "95",
    isoCode: "MM",
    name: "Myanmar",
  );
  static const CountryCode MN = CountryCode(
    asset: "assets/flags/mn_flag.png",
    dialingCode: "976",
    isoCode: "MN",
    name: "Mongolia",
  );
  static const CountryCode MO = CountryCode(
    asset: "assets/flags/mo_flag.png",
    dialingCode: "853",
    isoCode: "MO",
    name: "Macau",
  );
  static const CountryCode MP = CountryCode(
    asset: "assets/flags/mp_flag.png",
    dialingCode: "1",
    isoCode: "MP",
    name: "Northern Mariana Islands",
  );
  static const CountryCode MQ = CountryCode(
    asset: "assets/flags/mq_flag.png",
    dialingCode: "596",
    isoCode: "MQ",
    name: "Martinique",
  );
  static const CountryCode MR = CountryCode(
    asset: "assets/flags/mr_flag.png",
    dialingCode: "222",
    isoCode: "MR",
    name: "Mauritania",
  );
  static const CountryCode MS = CountryCode(
    asset: "assets/flags/ms_flag.png",
    dialingCode: "1",
    isoCode: "MS",
    name: "Montserrat",
  );
  static const CountryCode MT = CountryCode(
    asset: "assets/flags/mt_flag.png",
    dialingCode: "356",
    isoCode: "MT",
    name: "Malta",
  );
  static const CountryCode MU = CountryCode(
    asset: "assets/flags/mu_flag.png",
    dialingCode: "230",
    isoCode: "MU",
    name: "Mauritius",
  );
  static const CountryCode MV = CountryCode(
    asset: "assets/flags/mv_flag.png",
    dialingCode: "960",
    isoCode: "MV",
    name: "Maldives",
  );
  static const CountryCode MW = CountryCode(
    asset: "assets/flags/mw_flag.png",
    dialingCode: "265",
    isoCode: "MW",
    name: "Malawi",
  );
  static const CountryCode MX = CountryCode(
    asset: "assets/flags/mx_flag.png",
    dialingCode: "52",
    isoCode: "MX",
    name: "Mexico",
  );
  static const CountryCode MY = CountryCode(
    asset: "assets/flags/my_flag.png",
    dialingCode: "60",
    isoCode: "MY",
    name: "Malaysia",
  );
  static const CountryCode MZ = CountryCode(
    asset: "assets/flags/mz_flag.png",
    dialingCode: "258",
    isoCode: "MZ",
    name: "Mozambique",
  );
  static const CountryCode NA = CountryCode(
    asset: "assets/flags/na_flag.png",
    dialingCode: "264",
    isoCode: "NA",
    name: "Namibia",
  );
  static const CountryCode NC = CountryCode(
    asset: "assets/flags/nc_flag.png",
    dialingCode: "687",
    isoCode: "NC",
    name: "New Caledonia",
  );
  static const CountryCode NE = CountryCode(
    asset: "assets/flags/ne_flag.png",
    dialingCode: "227",
    isoCode: "NE",
    name: "Niger",
  );
  static const CountryCode NF = CountryCode(
    asset: "assets/flags/nf_flag.png",
    dialingCode: "672",
    isoCode: "NF",
    name: "Norfolk Island",
  );
  static const CountryCode NG = CountryCode(
    asset: "assets/flags/ng_flag.png",
    dialingCode: "234",
    isoCode: "NG",
    name: "Nigeria",
  );
  static const CountryCode NI = CountryCode(
    asset: "assets/flags/ni_flag.png",
    dialingCode: "505",
    isoCode: "NI",
    name: "Nicaragua",
  );
  static const CountryCode NL = CountryCode(
    asset: "assets/flags/nl_flag.png",
    dialingCode: "31",
    isoCode: "NL",
    name: "Netherlands",
  );
  static const CountryCode NO = CountryCode(
    asset: "assets/flags/no_flag.png",
    dialingCode: "47",
    isoCode: "NO",
    name: "Norway",
  );
  static const CountryCode NP = CountryCode(
    asset: "assets/flags/np_flag.png",
    dialingCode: "977",
    isoCode: "NP",
    name: "Nepal",
  );
  static const CountryCode NR = CountryCode(
    asset: "assets/flags/nr_flag.png",
    dialingCode: "674",
    isoCode: "NR",
    name: "Nauru",
  );
  static const CountryCode NU = CountryCode(
    asset: "assets/flags/nu_flag.png",
    dialingCode: "683",
    isoCode: "NU",
    name: "Niue",
  );
  static const CountryCode NZ = CountryCode(
    asset: "assets/flags/nz_flag.png",
    dialingCode: "64",
    isoCode: "NZ",
    name: "New Zealand",
  );
  static const CountryCode OM = CountryCode(
    asset: "assets/flags/om_flag.png",
    dialingCode: "968",
    isoCode: "OM",
    name: "Oman",
  );
  static const CountryCode PA = CountryCode(
    asset: "assets/flags/pa_flag.png",
    dialingCode: "507",
    isoCode: "PA",
    name: "Panama",
  );
  static const CountryCode PE = CountryCode(
    asset: "assets/flags/pe_flag.png",
    dialingCode: "51",
    isoCode: "PE",
    name: "Peru",
  );
  static const CountryCode PF = CountryCode(
    asset: "assets/flags/pf_flag.png",
    dialingCode: "689",
    isoCode: "PF",
    name: "French Polynesia",
  );
  static const CountryCode PG = CountryCode(
    asset: "assets/flags/pg_flag.png",
    dialingCode: "675",
    isoCode: "PG",
    name: "Papua New Guinea",
  );
  static const CountryCode PH = CountryCode(
    asset: "assets/flags/ph_flag.png",
    dialingCode: "63",
    isoCode: "PH",
    name: "Philippines",
  );
  static const CountryCode PK = CountryCode(
    asset: "assets/flags/pk_flag.png",
    dialingCode: "92",
    isoCode: "PK",
    name: "Pakistan",
  );
  static const CountryCode PL = CountryCode(
    asset: "assets/flags/pl_flag.png",
    dialingCode: "48",
    isoCode: "PL",
    name: "Poland",
  );
  static const CountryCode PM = CountryCode(
    asset: "assets/flags/pm_flag.png",
    dialingCode: "508",
    isoCode: "PM",
    name: "Saint Pierre and Miquelon",
  );
  static const CountryCode PN = CountryCode(
    asset: "assets/flags/pn_flag.png",
    dialingCode: "64",
    isoCode: "PN",
    name: "Pitcairn",
  );
  static const CountryCode PR = CountryCode(
    asset: "assets/flags/pr_flag.png",
    dialingCode: "1",
    isoCode: "PR",
    name: "Puerto Rico",
  );
  static const CountryCode PS = CountryCode(
    asset: "assets/flags/ps_flag.png",
    dialingCode: "970",
    isoCode: "PS",
    name: "Palestinian Territory, Occupied",
  );
  static const CountryCode PT = CountryCode(
    asset: "assets/flags/pt_flag.png",
    dialingCode: "351",
    isoCode: "PT",
    name: "Portugal",
  );
  static const CountryCode PW = CountryCode(
    asset: "assets/flags/pw_flag.png",
    dialingCode: "680",
    isoCode: "PW",
    name: "Palau",
  );
  static const CountryCode PY = CountryCode(
    asset: "assets/flags/py_flag.png",
    dialingCode: "595",
    isoCode: "PY",
    name: "Paraguay",
  );
  static const CountryCode QA = CountryCode(
    asset: "assets/flags/qa_flag.png",
    dialingCode: "974",
    isoCode: "QA",
    name: "Qatar",
  );
  static const CountryCode RE = CountryCode(
    asset: "assets/flags/re_flag.png",
    dialingCode: "262",
    isoCode: "RE",
    name: "Reunion",
  );
  static const CountryCode RO = CountryCode(
    asset: "assets/flags/ro_flag.png",
    dialingCode: "40",
    isoCode: "RO",
    name: "Romania",
  );
  static const CountryCode RS = CountryCode(
    asset: "assets/flags/rs_flag.png",
    dialingCode: "381",
    isoCode: "RS",
    name: "Serbia",
  );
  static const CountryCode RU = CountryCode(
    asset: "assets/flags/ru_flag.png",
    dialingCode: "7",
    isoCode: "RU",
    name: "Russian Federation",
  );
  static const CountryCode RW = CountryCode(
    asset: "assets/flags/rw_flag.png",
    dialingCode: "250",
    isoCode: "RW",
    name: "Rwanda",
  );
  static const CountryCode SA = CountryCode(
    asset: "assets/flags/sa_flag.png",
    dialingCode: "966",
    isoCode: "SA",
    name: "Saudi Arabia",
  );
  static const CountryCode SB = CountryCode(
    asset: "assets/flags/sb_flag.png",
    dialingCode: "677",
    isoCode: "SB",
    name: "Solomon Islands",
  );
  static const CountryCode SC = CountryCode(
    asset: "assets/flags/sc_flag.png",
    dialingCode: "248",
    isoCode: "SC",
    name: "Seychelles",
  );
  static const CountryCode SD = CountryCode(
    asset: "assets/flags/sd_flag.png",
    dialingCode: "249",
    isoCode: "SD",
    name: "Sudan",
  );
  static const CountryCode SE = CountryCode(
    asset: "assets/flags/se_flag.png",
    dialingCode: "46",
    isoCode: "SE",
    name: "Sweden",
  );
  static const CountryCode SG = CountryCode(
    asset: "assets/flags/sg_flag.png",
    dialingCode: "65",
    isoCode: "SG",
    name: "Singapore",
  );
  static const CountryCode SH = CountryCode(
    asset: "assets/flags/sh_flag.png",
    dialingCode: "290",
    isoCode: "SH",
    name: "Saint Helena",
  );
  static const CountryCode SI = CountryCode(
    asset: "assets/flags/si_flag.png",
    dialingCode: "386",
    isoCode: "SI",
    name: "Slovenia",
  );
  static const CountryCode SJ = CountryCode(
    asset: "assets/flags/sj_flag.png",
    dialingCode: "47",
    isoCode: "SJ",
    name: "Svalbard and Jan Mayen Islands",
  );
  static const CountryCode SK = CountryCode(
    asset: "assets/flags/sk_flag.png",
    dialingCode: "421",
    isoCode: "SK",
    name: "Slovakia",
  );
  static const CountryCode SL = CountryCode(
    asset: "assets/flags/sl_flag.png",
    dialingCode: "232",
    isoCode: "SL",
    name: "Sierra Leone",
  );
  static const CountryCode SM = CountryCode(
    asset: "assets/flags/sm_flag.png",
    dialingCode: "378",
    isoCode: "SM",
    name: "San Marino",
  );
  static const CountryCode SN = CountryCode(
    asset: "assets/flags/sn_flag.png",
    dialingCode: "221",
    isoCode: "SN",
    name: "Senegal",
  );
  static const CountryCode SO = CountryCode(
    asset: "assets/flags/so_flag.png",
    dialingCode: "252",
    isoCode: "SO",
    name: "Somalia",
  );
  static const CountryCode SR = CountryCode(
    asset: "assets/flags/sr_flag.png",
    dialingCode: "597",
    isoCode: "SR",
    name: "Suriname",
  );
  static const CountryCode SS = CountryCode(
    asset: "assets/flags/ss_flag.png",
    dialingCode: "211",
    isoCode: "SS",
    name: "South Sudan",
  );
  static const CountryCode ST = CountryCode(
    asset: "assets/flags/st_flag.png",
    dialingCode: "239",
    isoCode: "ST",
    name: "Sao Tome and Principe",
  );
  static const CountryCode SV = CountryCode(
    asset: "assets/flags/sv_flag.png",
    dialingCode: "503",
    isoCode: "SV",
    name: "El Salvador",
  );
  static const CountryCode SX = CountryCode(
    asset: "assets/flags/sx_flag.png",
    dialingCode: "1",
    isoCode: "SX",
    name: "Sint Maarten",
  );
  static const CountryCode SY = CountryCode(
    asset: "assets/flags/sy_flag.png",
    dialingCode: "963",
    isoCode: "SY",
    name: "Syrian Arab Republic",
  );
  static const CountryCode SZ = CountryCode(
    asset: "assets/flags/sz_flag.png",
    dialingCode: "268",
    isoCode: "SZ",
    name: "Swaziland",
  );
  static const CountryCode TC = CountryCode(
    asset: "assets/flags/tc_flag.png",
    dialingCode: "1",
    isoCode: "TC",
    name: "Turks and Caicos Islands",
  );
  static const CountryCode TD = CountryCode(
    asset: "assets/flags/td_flag.png",
    dialingCode: "235",
    isoCode: "TD",
    name: "Chad",
  );
  static const CountryCode TF = CountryCode(
    asset: "assets/flags/tf_flag.png",
    dialingCode: "262",
    isoCode: "TF",
    name: "French Southern Territories",
  );
  static const CountryCode TG = CountryCode(
    asset: "assets/flags/tg_flag.png",
    dialingCode: "228",
    isoCode: "TG",
    name: "Togo",
  );
  static const CountryCode TH = CountryCode(
    asset: "assets/flags/th_flag.png",
    dialingCode: "66",
    isoCode: "TH",
    name: "Thailand",
  );
  static const CountryCode TJ = CountryCode(
    asset: "assets/flags/tj_flag.png",
    dialingCode: "992",
    isoCode: "TJ",
    name: "Tajikistan",
  );
  static const CountryCode TK = CountryCode(
    asset: "assets/flags/tk_flag.png",
    dialingCode: "690",
    isoCode: "TK",
    name: "Tokelau",
  );
  static const CountryCode TL = CountryCode(
    asset: "assets/flags/tl_flag.png",
    dialingCode: "670",
    isoCode: "TL",
    name: "Timor-leste",
  );
  static const CountryCode TM = CountryCode(
    asset: "assets/flags/tm_flag.png",
    dialingCode: "993",
    isoCode: "TM",
    name: "Turkmenistan",
  );
  static const CountryCode TN = CountryCode(
    asset: "assets/flags/tn_flag.png",
    dialingCode: "216",
    isoCode: "TN",
    name: "Tunisia",
  );
  static const CountryCode TO = CountryCode(
    asset: "assets/flags/to_flag.png",
    dialingCode: "676",
    isoCode: "TO",
    name: "Tonga",
  );
  static const CountryCode TR = CountryCode(
    asset: "assets/flags/tr_flag.png",
    dialingCode: "90",
    isoCode: "TR",
    name: "Turkey",
  );
  static const CountryCode TT = CountryCode(
    asset: "assets/flags/tt_flag.png",
    dialingCode: "1",
    isoCode: "TT",
    name: "Trinidad and Tobago",
  );
  static const CountryCode TV = CountryCode(
    asset: "assets/flags/tv_flag.png",
    dialingCode: "688",
    isoCode: "TV",
    name: "Tuvalu",
  );
  static const CountryCode TW = CountryCode(
    asset: "assets/flags/tw_flag.png",
    dialingCode: "886",
    isoCode: "TW",
    name: "Taiwan",
  );
  static const CountryCode TZ = CountryCode(
    asset: "assets/flags/tz_flag.png",
    dialingCode: "255",
    isoCode: "TZ",
    name: "Tanzania, United Republic Of",
  );
  static const CountryCode UA = CountryCode(
    asset: "assets/flags/ua_flag.png",
    dialingCode: "380",
    isoCode: "UA",
    name: "Ukraine",
  );
  static const CountryCode UG = CountryCode(
    asset: "assets/flags/ug_flag.png",
    dialingCode: "256",
    isoCode: "UG",
    name: "Uganda",
  );
  static const CountryCode UM = CountryCode(
    asset: "assets/flags/um_flag.png",
    dialingCode: "1",
    isoCode: "UM",
    name: "United States Minor Outlying Islands",
  );
  static const CountryCode US = CountryCode(
    asset: "assets/flags/us_flag.png",
    dialingCode: "1",
    isoCode: "US",
    name: "United States",
  );
  static const CountryCode UY = CountryCode(
    asset: "assets/flags/uy_flag.png",
    dialingCode: "598",
    isoCode: "UY",
    name: "Uruguay",
  );
  static const CountryCode UZ = CountryCode(
    asset: "assets/flags/uz_flag.png",
    dialingCode: "998",
    isoCode: "UZ",
    name: "Uzbekistan",
  );
  static const CountryCode VA = CountryCode(
    asset: "assets/flags/va_flag.png",
    dialingCode: "379",
    isoCode: "VA",
    name: "Vatican City State (Holy See)",
  );
  static const CountryCode VC = CountryCode(
    asset: "assets/flags/vc_flag.png",
    dialingCode: "1",
    isoCode: "VC",
    name: "Saint Vincent and The Grenadines",
  );
  static const CountryCode VE = CountryCode(
    asset: "assets/flags/ve_flag.png",
    dialingCode: "58",
    isoCode: "VE",
    name: "Venezuela",
  );
  static const CountryCode VG = CountryCode(
    asset: "assets/flags/vg_flag.png",
    dialingCode: "1",
    isoCode: "VG",
    name: "Virgin Islands (British)",
  );
  static const CountryCode VI = CountryCode(
    asset: "assets/flags/vi_flag.png",
    dialingCode: "1",
    isoCode: "VI",
    name: "Virgin Islands (U.S.)",
  );
  static const CountryCode VN = CountryCode(
    asset: "assets/flags/vn_flag.png",
    dialingCode: "84",
    isoCode: "VN",
    name: "Viet Nam",
  );
  static const CountryCode VU = CountryCode(
    asset: "assets/flags/vu_flag.png",
    dialingCode: "678",
    isoCode: "VU",
    name: "Vanuatu",
  );
  static const CountryCode WF = CountryCode(
    asset: "assets/flags/wf_flag.png",
    dialingCode: "681",
    isoCode: "WF",
    name: "Wallis and Futuna Islands",
  );
  static const CountryCode WS = CountryCode(
    asset: "assets/flags/ws_flag.png",
    dialingCode: "685",
    isoCode: "WS",
    name: "Samoa",
  );
  static const CountryCode YE = CountryCode(
    asset: "assets/flags/ye_flag.png",
    dialingCode: "967",
    isoCode: "YE",
    name: "Yemen",
  );
  static const CountryCode YT = CountryCode(
    asset: "assets/flags/yt_flag.png",
    dialingCode: "262",
    isoCode: "YT",
    name: "Mayotte",
  );
  static const CountryCode ZA = CountryCode(
    asset: "assets/flags/za_flag.png",
    dialingCode: "27",
    isoCode: "ZA",
    name: "South Africa",
  );
  static const CountryCode ZM = CountryCode(
    asset: "assets/flags/zm_flag.png",
    dialingCode: "260",
    isoCode: "ZM",
    name: "Zambia",
  );
  static const CountryCode ZW = CountryCode(
    asset: "assets/flags/zw_flag.png",
    dialingCode: "263",
    isoCode: "ZW",
    name: "Zimbabwe",
  );

  /// All the countries in the picker list
  static const ALL = <CountryCode>[
    AD,
    AE,
    AF,
    AG,
    AI,
    AL,
    AM,
    AO,
    AQ,
    AR,
    AS,
    AT,
    AU,
    AW,
    AX,
    AZ,
    BA,
    BB,
    BD,
    BE,
    BF,
    BG,
    BH,
    BI,
    BJ,
    BL,
    BM,
    BN,
    BO,
    BQ,
    BR,
    BS,
    BT,
    BV,
    BW,
    BY,
    BZ,
    CA,
    CC,
    CD,
    CF,
    CG,
    CH,
    CI,
    CK,
    CL,
    CM,
    CN,
    CO,
    CR,
    CU,
    CV,
    CW,
    CX,
    CY,
    CZ,
    DE,
    DJ,
    DK,
    DM,
    DO,
    DZ,
    EC,
    EE,
    EG,
    EH,
    ER,
    ES,
    ET,
    FI,
    FJ,
    FK,
    FM,
    FO,
    FR,
    GA,
    GB,
    GD,
    GE,
    GF,
    GG,
    GH,
    GI,
    GL,
    GM,
    GN,
    GP,
    GQ,
    GR,
    GS,
    GT,
    GU,
    GW,
    GY,
    HK,
    HM,
    HN,
    HR,
    HT,
    HU,
    ID,
    IE,
    IL,
    IM,
    IN,
    IO,
    IQ,
    IR,
    IS,
    IT,
    JE,
    JM,
    JO,
    JP,
    KE,
    KG,
    KH,
    KI,
    KM,
    KN,
    KP,
    KR,
    KW,
    KY,
    KZ,
    LA,
    LB,
    LC,
    LI,
    LK,
    LR,
    LS,
    LT,
    LU,
    LV,
    LY,
    MA,
    MC,
    MD,
    ME,
    MF,
    MG,
    MH,
    MK,
    ML,
    MM,
    MN,
    MO,
    MP,
    MQ,
    MR,
    MS,
    MT,
    MU,
    MV,
    MW,
    MX,
    MY,
    MZ,
    NA,
    NC,
    NE,
    NF,
    NG,
    NI,
    NL,
    NO,
    NP,
    NR,
    NU,
    NZ,
    OM,
    PA,
    PE,
    PF,
    PG,
    PH,
    PK,
    PL,
    PM,
    PN,
    PR,
    PS,
    PT,
    PW,
    PY,
    QA,
    RE,
    RO,
    RS,
    RU,
    RW,
    SA,
    SB,
    SC,
    SD,
    SE,
    SG,
    SH,
    SI,
    SJ,
    SK,
    SL,
    SM,
    SN,
    SO,
    SR,
    SS,
    ST,
    SV,
    SX,
    SY,
    SZ,
    TC,
    TD,
    TF,
    TG,
    TH,
    TJ,
    TK,
    TL,
    TM,
    TN,
    TO,
    TR,
    TT,
    TV,
    TW,
    TZ,
    UA,
    UG,
    UM,
    US,
    UY,
    UZ,
    VA,
    VC,
    VE,
    VG,
    VI,
    VN,
    VU,
    WF,
    WS,
    YE,
    YT,
    ZA,
    ZM,
    ZW,
  ];

  /// returns an country with the specified [isoCode] or ```null``` if
  /// none or more than 1 are found
  static findByIsoCode(String isoCode) {
    return ALL.singleWhere(
      (item) => item.isoCode == isoCode,
    );
  }

  /// Creates a copy with modified values
  CountryCode copyWith({
    String name,
    String isoCode,
    String dialingCode,
  }) {
    return CountryCode(
      name: name ?? this.name,
      isoCode: isoCode ?? this.isoCode,
      dialingCode: dialingCode ?? this.dialingCode,
      asset: asset ?? this.asset,
    );
  }
}
