class ImagePath {
  static const ICON_PATH = "assets/icons/";
  static const IMAGE_PATH = "assets/images/";
  static const ICON_APP = ICON_PATH + "app_icon.png";
  static const ICON_FACEBOOK = ICON_PATH + "facebook.png";
  static const ICON_GOOGLE = ICON_PATH + "google.png";
  static const SWIPE_ICON = ICON_PATH + "swipe_icon.png";
  static const CLASS_IMG = ICON_PATH + "ic_class.png";
  static const PENCIL_IMG = ICON_PATH + "ic_pencil.png";
  static const CLOCK_IMG = ICON_PATH + "ic_clock.png";
  static const NOTIFICATION_IMG = ICON_PATH + "ic_notification.png";
  static const FAB_BTN = ICON_PATH + "ic_floating.png";
  static const PLUS_IMG = ICON_PATH + "ic_plus.png";
  static const ADD_IMG = ICON_PATH + "ic_add.png";
  static const CLOSE_IMG = ICON_PATH + "ic_close.png";
  static const ASSIGNMENT_IMG = ICON_PATH + "ic_assignment.png";
  static const ATTENDANCE_IMG = ICON_PATH + "ic_attendance.png";
  static const GRADE_IMG = ICON_PATH + "ic_grade.png";
  static const MATERIAL_IMG = ICON_PATH + "ic_material.png";
  static const QUIZ_IMG = ICON_PATH + "ic_quiz.png";
  static const SYLLABUS_IMAGE = ICON_PATH + "ic_syllabus.png";
  static const STUDENT_IMAGE = ICON_PATH + "ic_student.png";
  static const CALENDAR_IMG = ICON_PATH + "ic_calendar.png";

  static const SELECTED_TRUE = ICON_PATH + "ic_selected_true.png";
  static const SELECTED_FALSE = ICON_PATH + "ic_selected_false.png";
  static const UNSELECTED_TRUE = ICON_PATH + "ic_unSelected_true.png";
  static const UNSELECTED_FALSE = ICON_PATH + "ic_unSelected_false.png";

  static const ICON_BackArrow = ICON_PATH + "backarrow.png";
  static const ICON_StudentPhoto = ICON_PATH + "student_photo.png";
  static const ICON_CALL = ICON_PATH + "call.png";
  static const ICON_MESSAGE = ICON_PATH + "message.png";
  static const ICON_CALENDAR = ICON_PATH + "calendar.png";
  static const ICON_ADD = ICON_PATH + "plus.png";
  static const ICON_rightArrow = ICON_PATH + "right_arrow.png";
  static const ICON_PDF = ICON_PATH + "pdf.png";
  static const ICON_WORD = ICON_PATH + "word.png";
  static const ICON_EXCEL = ICON_PATH + "excel.png";
  static const ICON_CAMERA = ICON_PATH + "camera.png";
  static const ICON_GALLERY = ICON_PATH + "photo.png";

  static const ICON_Fourth_Tab = ICON_PATH + "ic_fourth_tab.png";
  static const ICON_Second_Tab = ICON_PATH + "ic_second_tab.png";
  static const ICON_Third_Tab = ICON_PATH + "ic_third_tab.png";
  static const ICON_Tab_Home = ICON_PATH + "tab_home.png";
  static const ICON_FOURTH_TAB_SELECTED =
      ICON_PATH + "ic_fourth_tab_selected.png";
  static const ICON_THIRD_TAB_SELECTED =
      ICON_PATH + "ic_third_tab_selected.png";
  static const ICON_Second_TAB_UNSELECTED =
      ICON_PATH + "ic_second_tab_unselect.png";
  static const ICON_HOME_UNSELECTED = ICON_PATH + "ic_home_tab_unselected.png";
  static const USER = ICON_PATH + "user.png";
  static const CANCEL = ICON_PATH + "cancel.png";
  static const IC_UNSELECTED_GRADE = ICON_PATH + "ic_unselected_grade.png";
  static const IC_UNSELECTED_ATTENDENCE =
      ICON_PATH + "ic_unselected_attendence.png";
  static const IC_OVAL = ICON_PATH + "Oval.png";
  static const IC_PROFILE_CHAT = ICON_PATH + "ic_profile_chat.png";
  static const IC_PROFILE_EDIT = ICON_PATH + "profile_edit.png";
  static const IC_CALL = ICON_PATH + "ic_call.png";
  static const IC_DOWNLOAD = ICON_PATH + "download.png";
  static const ALL_DOCUMENTS = ICON_PATH + "document.png";
  static const PPT_ICON = ICON_PATH + "ppt.png";
  static const ICON_TXT = ICON_PATH + "text.png";
  static const ICON_RAR = ICON_PATH + "rar.png";
  static const ICON_VIDEO = ICON_PATH + "video.png";
  static const ICON_ZIP = ICON_PATH + "zip.png";
  static const ICON_AUDIO = ICON_PATH + "audio.png";
  static const ICON_FILE = ICON_PATH + "file.png";
  static const ICON_SCHOOL = ICON_PATH + "school.png";
  static const ICON_ENROLLNOW = ICON_PATH + "enrollNow.jpg";
}
