import 'package:trainmoo/model/GradeModel.dart';
import 'package:trainmoo/model/MaterialModel.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/SessionModel.dart';
import 'package:trainmoo/model/StudentModel.dart';

// home page action
class SetPostListAction {
  final List<MaterialModel> latestPost;
  SetPostListAction(this.latestPost);
}

class SetLoadFirstTime {
  final bool loadFirstTime;
  SetLoadFirstTime(this.loadFirstTime);
}

// session page action
class SetTodaySessionList {
  final List<SessionModel> todaySessionList;
  SetTodaySessionList(this.todaySessionList);
}

class SetWeekSessionList {
  final List weekSessionList;
  SetWeekSessionList(this.weekSessionList);
}

class SetMonthSessionList {
  final List monthSessionList;
  SetMonthSessionList(this.monthSessionList);
}

class SetLoadFirstTimeSessionList {
  final bool loadFirstTimeSessionList;
  SetLoadFirstTimeSessionList(this.loadFirstTimeSessionList);
}

// mywork page action
class SetSubmitedMaterialLists {
  final List<MaterialModel> submitedMaterialLists;
  SetSubmitedMaterialLists(this.submitedMaterialLists);
}

class SetPaddingMaterialLists {
  final List<MaterialModel> paddingMaterialLists;
  SetPaddingMaterialLists(this.paddingMaterialLists);
}

class SetLoadFirstTimeMyWork {
  final bool loadFirstTimeMyWork;
  SetLoadFirstTimeMyWork(this.loadFirstTimeMyWork);
}

// Assignment grade
class SetPaddingAssignmentMaterialLists {
  final List<MaterialModel> paddingAssignmentMaterialLists;
  SetPaddingAssignmentMaterialLists(this.paddingAssignmentMaterialLists);
}

class SetSubmitedAssignmenMaterialLists {
  final List<MaterialModel> submitedAssignmenMaterialLists;
  SetSubmitedAssignmenMaterialLists(this.submitedAssignmenMaterialLists);
}

class SetLoadFirstTimeGradAssignment {
  final bool loadFirstTimeGradAssignment;
  SetLoadFirstTimeGradAssignment(this.loadFirstTimeGradAssignment);
}

// review Quiz
class SetSubmitedQuizMaterialLists {
  final List<MaterialModel> submitedQuizMaterialLists;
  SetSubmitedQuizMaterialLists(this.submitedQuizMaterialLists);
}

class SetPaddingQuizMaterialLists {
  final List<MaterialModel> paddingQuizMaterialLists;
  SetPaddingQuizMaterialLists(this.paddingQuizMaterialLists);
}

class SetLoadFirstTimeReviewQuiz {
  final bool loadFirstTimeReviewQuiz;
  SetLoadFirstTimeReviewQuiz(this.loadFirstTimeReviewQuiz);
}

// class
class SetClassList {
  final List<ProgramClassesModel> classList;
  SetClassList(this.classList);
}

class SetLoadFirstTimeClass {
  final bool loadFirstTimeClass;
  SetLoadFirstTimeClass(this.loadFirstTimeClass);
}

// assignmet
class SetAssignmentList {
  final List<MaterialModel> assignmentList;
  SetAssignmentList(this.assignmentList);
}

class SetLoadFirstTimeAssignment {
  final bool loadFirstTimeAssignment;
  SetLoadFirstTimeAssignment(this.loadFirstTimeAssignment);
}

//quiz
class SetQuizList {
  final List<MaterialModel> quizList;
  SetQuizList(this.quizList);
}

class SetLoadFirstTimeQuiz {
  final bool loadFirstTimeQuiz;
  SetLoadFirstTimeQuiz(this.loadFirstTimeQuiz);
}

//matrial
class SetMaterialList {
  final List<MaterialModel> materialList;
  SetMaterialList(this.materialList);
}

class SetLoadFirstTimeMaterial {
  final bool loadFirstTimeMaterial;
  SetLoadFirstTimeMaterial(this.loadFirstTimeMaterial);
}

// grade
class SetGradeList {
  final GradeModel gradeList;
  SetGradeList(this.gradeList);
}

class SetLoadFirstTimeGrade {
  final bool loadFirstTimeGrade;
  SetLoadFirstTimeGrade(this.loadFirstTimeGrade);
}

// student

class SetStudentsList {
  final List<StudentModel> studentsList;
  SetStudentsList(this.studentsList);
}

class SetLoadFirstTimeGradeStudents {
  final bool loadFirstTimeGradeStudents;
  SetLoadFirstTimeGradeStudents(this.loadFirstTimeGradeStudents);
}
