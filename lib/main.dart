import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide Router;
import 'package:trainmoo/model/Database/DatabaseHelper.dart';
import 'package:trainmoo/model/User.dart';
import 'package:trainmoo/reducers/home_reducer.dart';
import 'package:trainmoo/shared_preference/user_preference.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/router.dart';
import 'package:trainmoo/ui/service_locator.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'app_state.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'model/Database/DatabaseHelper.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  final _initialState = AppState(latestPost: []);
  final store = new Store<AppState>(appReducers,
      initialState: _initialState, middleware: [thunkMiddleware]);
  API.init();
  setupLocator();
  print('Called main');
  checkUserLoggedinOrNot(store);
//  runApp(MyApp(Screen.Login.toString()));
}

void checkUserLoggedinOrNot(store) async {
  new DatabaseHelper();
  User user = await UserPreference.getUserDetail();
  if (user != null &&
      user.email != null &&
      await UserPreference.getUserDetail() != null) {
    runApp(MyApp(Screen.TabManagerView.toString(), store));
  } else {
    runApp(MyApp(Screen.Login.toString(), store));
  }
}

//
class MyApp extends StatelessWidget {
  final String initalRoute;
  final Store<AppState> store;
  MyApp(this.initalRoute, this.store);
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Trainmoo',
          theme: ThemeData(
            primaryColor: Palette.primaryColor,
            accentColor: Palette.accentColor,
            hintColor: Palette.secondaryTextColor,
            backgroundColor: Palette.screenBgColor,
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            canvasColor: Colors.white,
//          textTheme: TextTheme(
//              title: AppTextStyle.getDynamicFontStyle(
//                  Palette.primaryColor, 16, FontType.Regular)),
            primarySwatch: Colors.blue,
          ),
          initialRoute: initalRoute,
          onGenerateRoute: RouterSceen.generateRoute),
    );
  }
}
