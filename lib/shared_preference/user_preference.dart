import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:trainmoo/model/Avatar.dart';
import 'package:trainmoo/model/Education.dart';
import 'package:trainmoo/model/Experience.dart';
import 'package:trainmoo/model/Name.dart';
import 'package:trainmoo/model/User.dart';
import 'package:trainmoo/model/user_login_model.dart';

import '../model/Avatar.dart';

class UserPreference {
  static void logOut() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('_id');
    prefs.remove('userType');
    prefs.remove('email');
    prefs.remove('shortname');
    prefs.remove('fullname');
    prefs.remove('firstname');
    prefs.remove('lastname');
  }

  static Future<bool> saveUserDetails(User user) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('_id', user.id);
    prefs.setString('email', user.email);
    prefs.setString('userType', user.userType);
    prefs.setString('shortname', user.shortName);
    prefs.setString('fullname', user.fullname);
    prefs.setBool('isActive', user.isActive);
    return true;
  }

  static Future<bool> saveUser(String user) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('UserProfileData', user);
    return true;
  }

  static Future<bool> saveLoadHomeScreen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('LoadHomeScreen', true);
    return true;
  }

  static Future<bool> getLoadHomeScreen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('LoadHomeScreen');
  }

  static Future<bool> saveDate(String date) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('updatedDateOfEventData', date);
    return true;
  }

  static Future<String> getDate() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('updatedDateOfEventData');
  }

  static Future<bool> saveSchoolUserId(String schoolUserId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('SchoolUserId', schoolUserId);
    return true;
  }

  static Future<String> getSchoolUserId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('SchoolUserId');
  }

  Future<UserLoginModel> getSavedInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map userMap = jsonDecode(prefs.getString('UserProfileData'));
    UserLoginModel userData = UserLoginModel.fromJson(userMap);
    return userData;
  }

  static Future<dynamic> getUserInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return json.decode(prefs.getString('UserProfileData'));
  }

  static Future<User> getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return json.decode(prefs.getString('User'));
  }

  static Future<User> getUserDetail() async {
    print("---------jit id ----------");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print("---------jit id ----------$prefs");
    User user = new User();
    user.id = prefs.getString('_id');
    print("user.id===>${user.id}");
    user.email = prefs.getString('email');
    print("user.email===>${user.email}");
    user.userType = prefs.getString('userType');
    print("user.userType===>${user.userType}");
    user.shortName = prefs.getString('shortname');
    print("user.shortName===>${user.shortName}");
    user.fullname = prefs.getString('fullname');
    print("user.fullname===>${user.fullname}");
    user.isActive = prefs.getBool('isActive');
    print("user.isActive===>${user.isActive}");
    // print('user image ==> ${prefs.getString('userImage')}');
    user.userImage = prefs.getString('userImage');
    print("t====>>>his is data of user ${user.userImage}");
    return user;
  }

  static Future<String> getUserId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userID = prefs.getString('_id');
    return userID;
  }

  static Future<bool> saveUserName(UserNameObject user) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('firstname', user.firstname);
    prefs.setString('lastname', user.lastname);
    return true;
  }

  static Future<bool> saveUserType(String userType) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("userType", userType);
    return true;
  }

  static Future<String> getUserType() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString('userType') == null) return "admin";
    String userType = prefs.getString('userType');
    return userType;
  }

  static Future<bool> saveUserImage(Avatar avatar) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('userImage', avatar.src);
    return true;
  }

  static Future<UserNameObject> getUserName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    UserNameObject user = new UserNameObject();
    user.firstname = prefs.getString('firstname');
    user.lastname = prefs.getString('lastname');

    return user;
  }

  static Future<bool> saveUserEducation(Education education) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('school', education.school);
    prefs.setString('from', education.from);
    prefs.setString('to', education.to);
    prefs.setString('summary', education.summary);
    prefs.setString('degree', education.degree);
    prefs.setString(
        'fiel'
        'dOfStudy',
        education.fieldOfStudy);
    prefs.setString('_id', education.educationId);
    return true;
  }

  static Future<Education> getUserEducation() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Education education = new Education();
    education.educationId = prefs.getString('_id');
    education.school = prefs.getString('school');
    education.from = prefs.getString('from');
    education.to = prefs.getString('to');
    education.summary = prefs.getString('summary');
    education.degree = prefs.getString('degree');
    education.fieldOfStudy = prefs.getString('fieldOfStudy');
    return education;
  }

  static Future<bool> saveUserExperience(Experience experience) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('company', experience.company);
    prefs.setString('from', experience.from);
    prefs.setString('to', experience.to);
    prefs.setString('title', experience.title);
    prefs.setString('summary', experience.summary);
    prefs.setString('_id', experience.experienceId);
    return true;
  }

  static Future<Experience> getUserExperience() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Experience experience = new Experience();
    experience.company = prefs.getString('company');
    experience.from = prefs.getString('from');
    experience.to = prefs.getString('to');
    experience.title = prefs.getString('title');
    experience.summary = prefs.getString('summary');
    experience.experienceId = prefs.getString('_id');
    return experience;
  }

  static Future<bool> saveCookie(String cookie) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('Cookies ==> ${cookie.toString()}');
    prefs.setString('cookie', cookie);
    return true;
  }

  static Future<String> getCookie() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String cookie = prefs.getString('cookie');
    //cookie = cookie.replaceAll('; Path=/; HttpOnly', '');
    print('Cookies ==> $cookie');
//    cookie =
//        'connect.sid=s%3AgR3ZcuioKRkpxZSqkPhIPzmfn_nfGI0u.hd7iE%2BWiEPYp0CHrbUYaXlE2ZOvNwRRJzcU60%2FM6X6Y';
    print('Cookies ==> $cookie');

    return cookie;
  }

  static void saveSchoolDetails(String schoolDetails, String schoolId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('school', schoolDetails);
    prefs.setString('school_id', schoolId);
  }

  static Future<String> getSchoolDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('school');
  }

  static Future<String> getSchoolId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('school_id');
  }

  static void saveCurrentClass(String currentClass, String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('current_class', currentClass);
    prefs.setString('current_class_id', id);
  }

  static Future<String> getCurrentClass() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('current_class');
  }

  static Future<String> getCurrentClassID() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('current_class_id');
  }

  static void saveCurrentAssignmentTitle(
      String currentAssignmentTitle, String parentID) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('current_assignment_title', currentAssignmentTitle);
    prefs.setString('current_assignment_id', parentID);
  }

  static Future<String> getCurrentAssignmentID() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('current_assignment_id');
  }

  static Future<String> getCurrentAssignmentTitle() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('current_assignment_title');
  }

  static void saveClass(classData) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('classData', classData);
  }

  static Future<dynamic> getClass() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return json.decode(prefs.getString('classData'));
  }
}
