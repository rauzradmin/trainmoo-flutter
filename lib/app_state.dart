import 'package:flutter/material.dart';
import 'package:trainmoo/model/GradeModel.dart';
import 'package:trainmoo/model/MaterialModel.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/SessionModel.dart';
import 'package:trainmoo/model/StudentModel.dart';

class AppState {
  bool loadFirstTime;
  bool loadFirstTimeSessionList;
  bool loadFirstTimeMyWork;
  bool loadFirstTimeReviewQuiz;
  bool loadFirstTimeGradAssignment;
  bool loadFirstTimeClass;
  bool loadFirstTimeAssignment;
  bool loadFirstTimeQuiz;
  bool loadFirstTimeMaterial;
  bool loadFirstTimeGrade;
  bool loadFirstTimeGradeStudents;

  List weekSessionList;
  List monthSessionList;
  List<MaterialModel> latestPost;
  List<SessionModel> todaySessionList;
  List<MaterialModel> paddingQuizMaterialLists;
  List<MaterialModel> submitedQuizMaterialLists;
  List<MaterialModel> paddingAssignmentMaterialLists;
  List<MaterialModel> submitedAssignmenMaterialLists;
  List<MaterialModel> submitedMaterialLists;
  List<MaterialModel> paddingMaterialLists;
  List<ProgramClassesModel> classList;
  List<MaterialModel> assignmentList;
  List<MaterialModel> quizList;
  List<MaterialModel> materialList;
  GradeModel gradeList;
  List<StudentModel> studentsList;

  AppState({
    @required this.latestPost,
    this.loadFirstTime: false,
    this.loadFirstTimeSessionList: false,
    this.loadFirstTimeMyWork: false,
    this.loadFirstTimeGradAssignment: false,
    this.loadFirstTimeReviewQuiz: false,
    this.loadFirstTimeClass: false,
    this.loadFirstTimeAssignment: false,
    this.loadFirstTimeMaterial: false,
    this.loadFirstTimeQuiz: false,
    this.loadFirstTimeGrade: false,
    this.loadFirstTimeGradeStudents: false,
    this.todaySessionList,
    this.monthSessionList,
    this.weekSessionList,
    this.submitedMaterialLists,
    this.paddingMaterialLists,
    this.paddingAssignmentMaterialLists,
    this.submitedAssignmenMaterialLists,
    this.paddingQuizMaterialLists,
    this.submitedQuizMaterialLists,
    this.classList,
    this.assignmentList,
    this.materialList,
    this.quizList,
    this.gradeList,
    this.studentsList,
  });

  AppState.fromAppState(AppState another) {
    latestPost = another.latestPost;
    loadFirstTime = another.loadFirstTime;
    todaySessionList = another.todaySessionList;
    loadFirstTimeSessionList = another.loadFirstTimeSessionList;
    weekSessionList = another.weekSessionList;
    monthSessionList = another.monthSessionList;
    loadFirstTimeMyWork = another.loadFirstTimeMyWork;
    submitedMaterialLists = another.submitedMaterialLists;
    paddingMaterialLists = another.paddingMaterialLists;
    loadFirstTimeGradAssignment = another.loadFirstTimeGradAssignment;
    loadFirstTimeReviewQuiz = another.loadFirstTimeReviewQuiz;
    paddingAssignmentMaterialLists = another.paddingAssignmentMaterialLists;
    submitedAssignmenMaterialLists = another.submitedAssignmenMaterialLists;
    paddingQuizMaterialLists = another.paddingQuizMaterialLists;
    submitedQuizMaterialLists = another.submitedQuizMaterialLists;
    loadFirstTimeClass = another.loadFirstTimeClass;
    classList = another.classList;
    loadFirstTimeAssignment = another.loadFirstTimeAssignment;
    loadFirstTimeQuiz = another.loadFirstTimeQuiz;
    loadFirstTimeMaterial = another.loadFirstTimeMaterial;
    assignmentList = another.assignmentList;
    quizList = another.quizList;
    materialList = another.materialList;
    gradeList = another.gradeList;
    loadFirstTimeGrade = another.loadFirstTimeGrade;
    loadFirstTimeGradeStudents = another.loadFirstTimeGradeStudents;
    studentsList = another.studentsList;
  }

  // List<MaterialModel> get getLatestPost => latestPost;
  //double get viewFontSize => sliderFontSize * 30;
}
