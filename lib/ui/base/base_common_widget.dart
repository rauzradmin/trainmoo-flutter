import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:path/path.dart' as path;
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/loaders/color_loader_5.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/router.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/util/image_path.dart';

class BaseCommonWidget {
  void showErrorMessage(BuildContext context, String message, bool isError) {
    try {
      if (message != null) {
//        Flushbar(
//          message: message,
//          flushbarStyle: FlushbarStyle.GROUNDED,
//          flushbarPosition: FlushbarPosition.TOP,
//          duration: Duration(seconds: SnackDuration.Short.toInt()),
//          backgroundColor: Palette.snackBarColor,
//        )..show(context);
      }
    } catch (e) {
      print(e);
    }
  }

  commonAppBar(String title, BuildContext context) {
    var width = MediaQuery.of(context).size.width * 0.65;
    return Container(
      margin: EdgeInsets.only(left: 12, right: 14, top: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          IconButton(
              icon: Image.asset(
                ImagePath.ICON_BackArrow,
                width: 16,
                height: 12,
              ),
              onPressed: () {
                backPress(context);
              }),
          Container(
            //decoration: BoxDecoration(border: Border.all(width: 1)),
            width: width,
            child: Text(
              title,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: AppTextStyle.getDynamicFontStyle(
                  Colors.black, 24, FontType.SemiBol),
            ),
          ),

          // Flexible(
          //   child: Container(),
          // ),
          // GestureDetector(
          //   onTap: () {
          //     Navigator.pushNamed(
          //       context,
          //       Screen.NotificationView.toString(),
          //     );
          //   },
          //   child: Container(
          //       height: 30,
          //       child: Stack(
          //         children: [
          //           Container(
          //             height: 30,
          //             child: Image.asset(ImagePath.NOTIFICATION_IMG,
          //                 width: 19, height: 20),
          //           ),
          //           Positioned(
          //             right: 1.0,
          //             top: 3,
          //             child: Container(
          //               decoration: BoxDecoration(
          //                 borderRadius: BorderRadius.circular(50),
          //                 color: Colors.red,
          //               ),
          //               child: Center(
          //                 child: Text(
          //                   "",
          //                   style: TextStyle(fontSize: 8, color: Colors.white),
          //                 ),
          //               ),
          //               height: 8.0,
          //               width: 8.0,
          //             ),
          //           )
          //         ],
          //       )),
          // ),
          IconButton(
              icon: Image.asset(ImagePath.NOTIFICATION_IMG,
                  width: 19, height: 20),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  Screen.NotificationView.toString(),
                );
              })
        ],
      ),
    );
  }

  Widget commonAppBarWithIcon(String title, BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 12),
      child: Row(
        children: <Widget>[
          IconButton(
              icon: Image.asset(
                ImagePath.ICON_BackArrow,
                width: 16,
                height: 12,
              ),
              onPressed: () {
                backPress(context);
              }),
          Text(
            title,
            overflow: TextOverflow.clip,
            style: AppTextStyle.getDynamicFontStyle(
                Colors.black, 24, FontType.SemiBol),
          ),
          Flexible(
            child: Container(),
          ),
          IconButton(
              icon:
                  Image.asset(ImagePath.IC_PROFILE_EDIT, width: 19, height: 19),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  Screen.NotificationView.toString(),
                );
              })
        ],
      ),
    );
  }

//  Widget commonAppBarWithDownloadIcon(String title, BuildContext context) {
//    return Container(
//      margin: EdgeInsets.only(top: 12),
//      child: Row(
//        children: <Widget>[
//          IconButton(
//              icon: Image.asset(
//                ImagePath.ICON_BackArrow,
//                width: 16,
//                height: 12,
//              ),
//              onPressed: () {
//                backPress(context);
//              }),
//          Text(
//            title,
//            overflow: TextOverflow.clip,
//            style: AppTextStyle.getDynamicFontStyle(
//                Colors.black, 24, FontType.SemiBol),
//          ),
//          Flexible(
//            child: Container(),
//          ),
//          IconButton(
//              icon:
//              Image.asset(ImagePath.IC_DOWNLOAD, width: 19, height: 19),
//              onPressed: () {})
//        ],
//      ),
//    );
//  }

  Widget commonAppBarWithCancelIcon(String title, BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 25, right: 14, top: 8),
      child: Row(
        children: <Widget>[
          Text(
            title,
            overflow: TextOverflow.clip,
            style: AppTextStyle.getDynamicFontStyle(
                Colors.black, 24, FontType.SemiBol),
          ),
          Flexible(
            child: Container(),
          ),
          IconButton(
              icon: Image.asset(
                ImagePath.CANCEL,
                width: 13.93,
                height: 15,
              ),
              onPressed: () {
                Navigator.pop(context);
              })
        ],
      ),
    );
  }

  Widget commonAppBarWithoutArrow(String title, BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 24, right: 14, top: 30),
      child: Row(
        children: <Widget>[
          Text(
            title,
            overflow: TextOverflow.clip,
            style: AppTextStyle.getDynamicFontStyle(
                Colors.black, 24, FontType.SemiBol),
          ),
          Flexible(
            child: Container(),
          ),
          IconButton(
              icon: Image.asset(ImagePath.NOTIFICATION_IMG,
                  width: 19, height: 20),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  Screen.NotificationView.toString(),
                );
              })
        ],
      ),
    );
  }

  Widget commonTabBar(BuildContext context, TabController tabController,
      String tabOneText, String tabSecondText) {
    return Container(
      child: TabBar(
        tabs: [
          Tab(
              child: Container(
            width: 240,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 19),
                  child: Text(tabOneText,
                      style: AppTextStyle.getDynamicFontStyleWithoutColor(
                        18,
                        FontType.Medium,
                      )),
                ),
                Container(
                    padding: EdgeInsets.only(left: 14),
                    height: 20,
                    child: VerticalDivider(
                      color: Colors.black,
                      thickness: 2.0,
                    ))
              ],
            ),
          )),
          Tab(
              child: Container(
            width: 160,
            transform: Matrix4.translationValues(-12, 0, 0),
            child: Text(tabSecondText,
                style: AppTextStyle.getDynamicFontStyleWithoutColor(
                  18,
                  FontType.Medium,
                )),
          )),
        ],
        indicatorColor: Colors.transparent,
        labelColor: Colors.black,
        unselectedLabelColor: Palette.secondaryTextColor,
        controller: tabController,
      ),
    );
  }

  Widget reviewButton(String title, String subTitle) {
    return Container(
      width: 327,
      height: 75,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Palette.transparentBlue,
      ),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(title,
              textAlign: TextAlign.center,
              style: AppTextStyle.getDynamicFontStyle(
                  Palette.white, 16, FontType.SemiBol)),
          subTitle != ""
              ? Padding(
                  padding: const EdgeInsets.only(top: 3),
                  child: Text(subTitle,
                      textAlign: TextAlign.center,
                      style: AppTextStyle.getDynamicFontStyleWithWeight(
                          false,
                          Palette.transparentWhiteColor,
                          13,
                          FontType.RobotoMedium,
                          FontWeight.normal)),
                )
              : Container()
        ],
      ),
    );
  }

  Widget dropDownSelectClassList(String selectClass, BuildContext context) {
    return Container();
  }

  Widget getProgressBar(ViewState viewState) {
    if (viewState == ViewState.Busy) {
      return Container(
        color: Colors.white.withAlpha(204),
        child: Center(
          child: ColorLoader5(
            dotOneColor: Palette.accentColor,
            dotTwoColor: Palette.primaryColor,
            dotThreeColor: Palette.primaryDarkColor,
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  addMediaFileIconWidget(double width) {
    return Container(
      height: 54,
      width: width,
      decoration: rectangleDecoration(),
      padding: EdgeInsets.all(5),
      child: Image.asset(
        '',
        height: 36.77,
        width: 37.77,
      ),
    );
  }

  showMediaFileWidget(double width, File image) {
    return Container(
      height: 54,
      width: width,
      decoration: rectangleDecoration(),
      // padding: EdgeInsets.all(5),
      margin: EdgeInsets.only(left: 5, right: 5),
      child: Image.file(
        image,
        fit: BoxFit.contain,
        // height: 36.77,
        // width: 37.77,
      ),
    );
  }

  showMediaFileIconWidget(double width, Icon image) {
    return Container(
      height: 54,
      width: width,
      decoration: rectangleDecoration(),
      // padding: EdgeInsets.all(5),
      margin: EdgeInsets.only(left: 5, right: 5),
      child: image,
    );
  }

  bottomBorder() {
    return BoxDecoration(
        border: Border(bottom: BorderSide(color: Palette.dividerColor)));
  }

  rectangleDecoration() {
    return BoxDecoration(
        border: Border.all(color: Palette.dividerColor), color: Colors.white);
  }

  rectangleDecorationWithDynamicColor(Color webColor) {
    return BoxDecoration(
        border: Border.all(color: Palette.dividerColor), color: webColor);
  }

  colorIcon(String image, double height, double width) {
    return Image.asset(
      image,
      width: height,
      height: width,
    );
  }

  mediaLabelText(String label) {
    return Text(
      label,
      style: AppTextStyle.getDynamicFontStyle(
          Palette.secondaryTextColor, 14, FontType.Regular),
    );
  }

  checkBoxLabelText(String label) {
    return Expanded(
      child: Text(
        label,
        style: AppTextStyle.getDynamicFontStyle(
            Palette.primaryTextColor, 14, FontType.Regular),
      ),
    );
  }

  customCheckBox(bool isChecked) {
    return Image.asset(
      isChecked ? '' : '',
      height: 20,
      width: 20,
      color: Palette.primaryTextColor,
    );
  }

  customRadioButton(bool isChecked) {
    return Image.asset(isChecked ? '' : '', height: 20, width: 20);
  }

  activeInactiveSwitch(bool isChecked) {
    return Image.asset(
      isChecked ? '' : '',
      height: 24,
      width: 42,
    );
  }

  introTitleTest(String text) {
    return Text(
      text,
      style: AppTextStyle.getDynamicFontStyle(
          Palette.primaryTextColor, 16, FontType.Regular),
    );
  }

  introSubTitleTest(String text) {
    return Text(
      text,
      style: AppTextStyle.getDynamicFontStyle(
          Palette.secondaryTextColor, 14, FontType.Regular),
    );
  }

  introBottomText(String text) {
    return Center(
      child: Text(
        text.toUpperCase(),
        style: AppTextStyle.getDynamicFontStyle(
            Palette.accentColor, 16, FontType.Regular),
      ),
    );
  }

  showCircularProgressDialog(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  showToastMessage(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        // timeInSecForIos: 1,
        backgroundColor: Colors.black,
        textColor: Palette.white,
        fontSize: 16.0);
  }

  backPress(BuildContext context) {
    Navigator.of(context).pop();
  }

  tabBarWidget(String title, String subTitle) {
    return Tab(
        child: Container(
      child: Row(
        children: <Widget>[
          Column(
            children: <Widget>[
              Text(title,
                  style: AppTextStyle.getDynamicFontStyleWithoutColor(
                      21, FontType.Medium)),
              Text(subTitle,
                  style: AppTextStyle.getDynamicFontStyle(
                      Palette.listColor, 13, FontType.RobotoMedium))
            ],
          ),
          Container(
              padding: EdgeInsets.only(left: 12),
              height: 40,
              child: VerticalDivider(
                color: Palette.dividerColor,
              ))
        ],
      ),
    ));
  }

  Widget showImage(Future<File> imageFile) {
    return FutureBuilder<File>(
      future: imageFile,
      builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data != null) {
          return Padding(
            padding: const EdgeInsets.only(top: 10, left: 10),
            child: Image.file(
              snapshot.data,
              width: 91,
              height: 84,
              fit: BoxFit.cover,
            ),
          );
        } else if (snapshot.error != null) {
          return Padding(
            padding: const EdgeInsets.only(top: 10, left: 10),
            child: const Text(
              'Error Picking Image',
              textAlign: TextAlign.center,
            ),
          );
        } else {
          return Padding(
            padding: const EdgeInsets.only(top: 10, left: 10),
            child: const Text(
              '',
              textAlign: TextAlign.center,
            ),
          );
        }
      },
    );
  }

//  Widget showImageOrFilePath(List<File> file)
//  {
//    return file!=null?
//    Container(
//      height: 200,
//      margin: EdgeInsets.only(top: 10,bottom: 92),
//      child: ListView.builder(
//        physics: ClampingScrollPhysics(),
//        scrollDirection: Axis.horizontal,
//        shrinkWrap: true,
//        itemCount: file.length,
//        itemBuilder: (BuildContext cont, int ind) {
//          return uploadFileExtensionImage(ind,file[ind]);
//        },
//      ),
//    ):Container();
//  }

  Widget uploadFileExtensionImage(int ind, File file, List<File> fileList) {
    String filePath = file.path;
    String fileExtension = path.extension(filePath);
    String fileName = path.basename(filePath);
    print('fileName========${fileName}');
    print('fileExtensions========${fileExtension}');

    return Container(
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                height: 80,
                width: 100,
                child: Card(
                  color: Colors.white,
                  elevation: 0,
                  child: fileExtension == '.pdf'
                      ? Container(
                          width: 90,
                          child: Center(
                            child: Image.asset(
                              ImagePath.ICON_PDF,
                              width: 20,
                              height: 20,
                            ),
                          ),
                        )
                      : fileExtension == '.doc'
                          ? Container(
                              width: 90,
                              child: Center(
                                child: Image.asset(
                                  ImagePath.ICON_WORD,
                                  width: 20,
                                  height: 20,
                                ),
                              ),
                            )
                          : fileExtension == '.jpg'
                              ? FutureBuilder<File>(
                                  builder: (BuildContext context,
                                      AsyncSnapshot<File> snapshot) {
                                    return Container(
                                      child: Image.file(
                                        file,
                                        fit: BoxFit.cover,
                                      ),
                                    );
                                  },
                                )
                              : Container(),
                ),
              ),
              Center(
                  child: Text(
                fileName,
                style: TextStyle(fontSize: 10),
              ))
            ],
          ),
          Container(
            alignment: Alignment.topRight,
            child: IconButton(
              alignment: Alignment.topRight,
              icon: Icon(
                Icons.close,
                size: 15,
                color: Palette.dividerColor,
              ),
              onPressed: () {
                fileList.remove(file);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget loadCircleNetworkImage(
      String imageUrl, double height, Map<String, String> header) {
    print("imageUrl : $imageUrl");
    if (imageUrl == null) imageUrl = '';
    if (!imageUrl.contains(API.imageURL)) {
      if (imageUrl[0] == "/")
        imageUrl = API.imageURL + imageUrl;
      else
        imageUrl = imageUrl;
    }
    return new ClipOval(
      child: CachedNetworkImage(
        httpHeaders: header,
        imageUrl: imageUrl,
        imageBuilder: (context, imageProvider) => Container(
          height: height,
          width: height,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,
                colorFilter: ColorFilter.mode(Colors.red, BlendMode.colorBurn)),
          ),
        ),
        placeholder: (context, url) => CircularProgressIndicator(),
        //errorWidget: (context, url, error) => Icon(Icons.error),
        errorWidget: (context, url, error) {
          print("error----: $error");
        },
      ),
    );
  }

  Widget loadNetworkImage(
      String imageUrl, double height, Map<String, String> header) {
    print('file Url ==> $imageUrl');
    if (imageUrl == null) imageUrl = '';
    if (!imageUrl.contains(API.imageURL)) {
      if (imageUrl[0] == "/")
        imageUrl = API.imageURL + imageUrl;
      else
        imageUrl = imageUrl;
    }
    return CachedNetworkImage(
      imageUrl: imageUrl,
      httpHeaders: header,
      imageBuilder: (context, imageProvider) => Container(
        height: height,
        width: height,
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          image: DecorationImage(
            image: imageProvider,
            fit: BoxFit.cover,
          ),
        ),
      ),
      placeholder: (context, url) => Container(
          height: height, width: height, child: CircularProgressIndicator()),
      errorWidget: (context, url, error) => Icon(
        Icons.broken_image,
        size: height,
      ),
    );
  }

  Widget loadFileImage(
      String imageUrl, double height, Map<String, String> header) {
    return CachedNetworkImage(
      imageUrl: imageUrl,
      httpHeaders: header,
      imageBuilder: (context, imageProvider) => Container(
        height: height,
        width: height,
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          image: DecorationImage(
            image: imageProvider,
            fit: BoxFit.cover,
          ),
        ),
      ),
      placeholder: (context, url) => Container(
          height: height, width: height, child: CircularProgressIndicator()),
      errorWidget: (context, url, error) => Icon(
        Icons.insert_drive_file,
        size: height,
      ),
    );
  }

  Widget loadCommentImage(String imageUrl, double height, double width,
      Map<String, String> header) {
    if (imageUrl == null) imageUrl = '';
    if (!imageUrl.contains(API.imageURL)) {
      if (imageUrl[0] == "/")
        imageUrl = API.imageURL + imageUrl;
      else
        imageUrl = imageUrl;
    }
    return CachedNetworkImage(
      httpHeaders: header,
      imageUrl: imageUrl,
      imageBuilder: (context, imageProvider) => Container(
        height: height,
        width: height,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
            image: imageProvider,
            fit: BoxFit.cover,
          ),
        ),
      ),
      placeholder: (context, url) => Container(
          height: height, width: height, child: CircularProgressIndicator()),
      errorWidget: (context, url, error) => Icon(
        Icons.error,
        size: height,
      ),
    );
  }

  Widget loadNetworkImageInFullView(
      String imageUrl, Map<String, String> header) {
    if (imageUrl != null && imageUrl.trim().isNotEmpty) {
//      if (imageUrl.contains('https://') ||
//          imageUrl.contains('http://'))

      if (!imageUrl.contains('http') && !imageUrl.contains(API.baseURL)) {
        imageUrl = API.baseURL + imageUrl.replaceAll('/api/', '');
      }
    }

    return CachedNetworkImage(
      imageUrl: imageUrl,
      httpHeaders: header,
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          image: DecorationImage(
            image: imageProvider,
            fit: BoxFit.fitWidth,
          ),
        ),
      ),
      placeholder: (context, url) => Container(
          height: 100, width: 100, child: CircularProgressIndicator()),
      errorWidget: (context, url, error) => Icon(
        Icons.error,
        size: 100,
      ),
    );
  }

  Widget showMessageInCenter(String title) {
    return Center(
      child: Text(
        title,
        overflow: TextOverflow.clip,
        textAlign: TextAlign.center,
        style: AppTextStyle.getDynamicFontStyle(
            Colors.blueGrey, 20, FontType.Regular),
      ),
    );
  }
}
