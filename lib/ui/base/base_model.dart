// base_model.dart

import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/ui/theme/palette.dart';

/// Represents the state of the view
/// this is trypically used to show/hide progress indicator
enum ViewState { Idle, Busy }

class BaseModel extends ChangeNotifier {
  static const String success = "Success";

  ViewState _state = ViewState.Idle;

  ViewState get state => _state;

  bool _shouldShowMessage = false;
  bool _isError = false;
  String _message;

  get message => _message;

  get isError => _isError;

  get shouldShowMessage => _shouldShowMessage;

  BuildContext context;

  void setState(ViewState viewState) {
    if (_state != viewState) _state = viewState;
    notifyListeners();
  }

  void messageIsShown() {
    _shouldShowMessage = false;
  }

  getState() {
    return _state;
  }

  void showMessage(String message, bool isError) {
    _shouldShowMessage = true;
    _isError = isError;
    _message = message;
    notifyListeners();
  }

  void showToast(String message, BuildContext context) {
    try {
      if (message != null) {
        Flushbar(
          message: message,
          flushbarStyle: FlushbarStyle.GROUNDED,
          flushbarPosition: FlushbarPosition.TOP,
          duration: Duration(seconds: 5),
          backgroundColor: Palette.snackBarColor,
        )..show(context);
      }
    } catch (e) {
      print(e);
    }
  }

  bool isUserSessionValid(int responseCode) {
//    if (responseCode == ApiConfig.invalidUserCodeOne ||
//        responseCode == ApiConfig.invalidUserCodeTwo) {
//      UserPreferences.logOut();
//      Navigator.pushAndRemoveUntil(
//        context,
//        MaterialPageRoute(builder: (context) => LoginView()),
//        (Route<dynamic> route) => false,
//      );
//    }
    return true;
  }
}
