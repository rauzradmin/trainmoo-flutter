import 'package:get_it/get_it.dart';
import 'package:trainmoo/model/studentModuel/grade/studentGrdeListViewModel.dart';
import 'package:trainmoo/ui/view/Assignment/CreateAssignment/create_assignment_view_model.dart';
import 'package:trainmoo/ui/view/Assignment/Reminder/review_submitted_assignment_view_model.dart';
import 'package:trainmoo/ui/view/Assignment/ReviewAssignment/review_assignment_view_model.dart';
import 'package:trainmoo/ui/view/Attendance/TakeAttendance/take_attendance_view_model.dart';
import 'package:trainmoo/ui/view/Attendance/attendance_view_model.dart';
import 'package:trainmoo/ui/view/Attendance/student/student_attendance_view_model.dart';
import 'package:trainmoo/ui/view/Event/create_event_view_model.dart';
import 'package:trainmoo/ui/view/Grade/GradeAssignment/grade_assignment_view_model.dart';
import 'package:trainmoo/ui/view/Grade/GradeList/GradeListViewModel.dart';
import 'package:trainmoo/ui/view/Material/CreateMaterial/create_material_view_model.dart';
import 'package:trainmoo/ui/view/Material/MaterialDetail/material_detail_view_model.dart';
import 'package:trainmoo/ui/view/Material/material_view_model.dart';
import 'package:trainmoo/ui/view/MySession/my_session_view_model.dart';
import 'package:trainmoo/ui/view/MyWork/my_work_teacher_view_model.dart';
import 'package:trainmoo/ui/view/MyWork/my_work_view_model.dart';
import 'package:trainmoo/ui/view/Notification/notification_view_model.dart';
import 'package:trainmoo/ui/view/Quiz/CreateQuiz/AddQuestion/add_question_view_model.dart';
import 'package:trainmoo/ui/view/Quiz/CreateQuiz/create_quiz_view_model.dart';
import 'package:trainmoo/ui/view/Registration/EnrollNowModel.dart';
import 'package:trainmoo/ui/view/Registration/registration_view_model.dart';
import 'package:trainmoo/ui/view/StudentList/student_listview_model.dart';
import 'package:trainmoo/ui/view/Syllabus/SyllabusDetail/syllabus_detail_model.dart';
import 'package:trainmoo/ui/view/Syllabus/syllabus_view_model.dart';
import 'package:trainmoo/ui/view/class/class_item_view_model.dart';
import 'package:trainmoo/ui/view/class/class_view_model.dart';
import 'package:trainmoo/ui/view/home/home_view_model.dart';
import 'package:trainmoo/ui/view/login/login_view_model.dart';
import 'package:trainmoo/ui/view/studentUserModuel/student_quiz_view_model.dart';

GetIt locator = GetIt();

void setupLocator() {
  locator.registerFactory(() {
    return HomeViewModel();
  });

  locator.registerFactory(() {
    return RegistrationViewModel();
  });

  locator.registerFactory(() {
    return StudentListViewModel();
  });

  locator.registerFactory(() {
    return LoginViewModel();
  });

  locator.registerFactory(() {
    return ClassItemViewModel();
  });

  locator.registerFactory(() {
    return MaterialViewModel();
  });
  locator.registerFactory(() {
    return MaterialDetailViewModel();
  });

  locator.registerFactory(() {
    return ReviewAssignmentViewModel();
  });
  locator.registerFactory(() {
    return GradeAssignmentViewModel();
  });

  locator.registerFactory(() {
    return ClassViewModel();
  });

  locator.registerFactory(() {
    return ReviewSubmittedAssignmentViewModel();
  });

  locator.registerFactory(() {
    return SyllabusViewModel();
  });

  locator.registerFactory(() {
    return SyllabusDetailViewModel();
  });

  locator.registerFactory(() {
    return GradeListViewModel();
  });

  locator.registerFactory(() {
    return AttendanceViewModel();
  });

  locator.registerFactory(() {
    return CreateAssignmentViewModel();
  });

  locator.registerFactory(() {
    return CreateMaterialViewModel();
  });

  locator.registerFactory(() {
    return CreateQuizViewModel();
  });

  locator.registerFactory(() {
    return TakeAttendanceViewModel();
  });

  locator.registerFactory(() {
    return AddQuestionViewModel();
  });

  locator.registerFactory(() {
    return MyWorkViewModel();
  });

  locator.registerFactory(() {
    return MyWorkTeacherViewModel();
  });

  locator.registerFactory(() {
    return MySessionViewModel();
  });

  locator.registerFactory(() {
    return StudentGradeListViewModel();
  });

  locator.registerFactory(() {
    return StudentQuizModel();
  });

  locator.registerFactory(() {
    return StudentAttendanceViewModel();
  });
  locator.registerFactory(() {
    return NotificationViewModel();
  });
  locator.registerFactory(() {
    return CreateCreateEventViewModel();
  });
  locator.registerFactory(() {
    return EnrollNowModel();
  });
}
