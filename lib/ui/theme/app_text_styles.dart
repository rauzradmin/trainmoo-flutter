import 'package:flutter/material.dart';
import 'package:trainmoo/ui/theme/palette.dart';

enum FontType {
  Bold,
  Regular,
  Light,
  Medium,
  SemiBol,
  RobotoBold,
  RobotoLight,
  RobotoMedium,
  RobotoRegular
}

class AppTextStyle {
  static const String fontBold = "PoppinsBold";
  static const String fontRegular = "PoppinsRegular";
  static const String fontLight = "PoppinsLight";
  static const String fontMedium = "PoppinsMedium";
  static const String fontSemiBold = "PoppinsSemiBold";
  static const String robotoBold = "Roboto-Bold";
  static const String robotoLight = "Roboto-Light";
  static const String robotoMedium = "Roboto-Medium";
  static const String robotoRegular = "Roboto-Regular";

  static String getFont(FontType fontType) {
    if (fontType == FontType.Bold) {
      return fontBold;
    } else if (fontType == FontType.Regular) {
      return fontRegular;
    } else if (fontType == FontType.Light) {
      return fontLight;
    } else if (fontType == FontType.Medium) {
      return fontMedium;
    } else if (fontType == FontType.SemiBol) {
      return fontSemiBold;
    } else if (fontType == FontType.RobotoBold) {
      return robotoBold;
    } else if (fontType == FontType.RobotoLight) {
      return robotoLight;
    } else if (fontType == FontType.RobotoMedium) {
      return robotoMedium;
    } else if (fontType == FontType.RobotoRegular) {
      return robotoRegular;
    }
    return fontRegular;
  }

  static TextStyle getDynamicFontStyle(
      Color color, double fontSize, FontType fontType) {
    return TextStyle(
      fontStyle: FontStyle.normal,
      color: color,
      fontSize: fontSize,
      fontWeight: fontType == FontType.Bold ? FontWeight.w700 : FontWeight.w400,
      fontFamily: getFont(fontType),
    );
  }

  static TextStyle getDynamicFontStyleWithoutColor(
      double fontSize, FontType fontType) {
    return TextStyle(
      fontStyle: FontStyle.normal,
      fontSize: fontSize,
      fontWeight: fontType == FontType.Bold ? FontWeight.w700 : FontWeight.w400,
      fontFamily: getFont(fontType),
    );
  }

  static TextStyle getDynamicFontStyleWithWeight(bool isBold, Color color,
      double fontSize, FontType fontType, FontWeight fontWeight) {
    return TextStyle(
      fontWeight: fontWeight,
      fontStyle: FontStyle.normal,
      color: color,
      fontSize: fontSize,
      fontFamily: getFont(fontType),
    );
  }

  static TextStyle titleStyle() {
    return getDynamicFontStyle(Colors.black, 20, FontType.SemiBol);
  }

  static TextStyle subTitleStyle() {
    return getDynamicFontStyle(Palette.listColor, 13, FontType.RobotoRegular);
  }

  static TextStyle subTitleStyleWithTextSize(double fontSize) {
    return TextStyle(
      fontStyle: FontStyle.normal,
      color: Palette.listColor,
      fontSize: fontSize,
      fontFamily: getFont(FontType.RobotoRegular),
    );
  }

  static TextStyle descriptionStyle() {
    return getDynamicFontStyle(Palette.lightBlackColor, 13, FontType.RobotoRegular);
  }

  static TextStyle commonTitleStyle() {
    return getDynamicFontStyle(Colors.black, 28, FontType.SemiBol);
  }

  static TextStyle commonSubTitleStyle() {
    return getDynamicFontStyle(Palette.listColor, 13, FontType.RobotoRegular);
  }

  static TextStyle detailDescriptionStyle() {
    return getDynamicFontStyle(Colors.black, 16, FontType.RobotoRegular);
  }

  static TextStyle commentTextStyle() {
    return getDynamicFontStyle(Colors.black, 17, FontType.SemiBol);
  }

  static TextStyle hintStyle() {
    return getDynamicFontStyle(Palette.hintTextColor, 14, FontType.Regular);
  }
}
