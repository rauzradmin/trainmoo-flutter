import 'package:flutter/material.dart';

class Palette {
  static const primaryDarkColor = Color(0xFF1E4EA0);
  static const primaryColor = Color(0xFF2D67CA);
  static const accentColor = Color(0xFF2FB1FE);
  static const titleColor = Color(0xFFC1E3FC);
  static const hintTextColor = Color(0xFFA7B6BC);
  static const textIconColor = Color(0xFFFFFFFF);
  static const snackBarColor = Color.fromARGB(255, 85, 144, 207);
  static const primaryTextColor = Color(0xFF151515);
  static const dividerColor = Color(0xFFE8EDF0);
  static const webColor = Color(0xFFF7F7F7);
  static const headerColor = Color(0xFFF0F0F0);
  static const screenBgColor = Color(0xFFF8F9Fa);
  static const titleBg = Color(0xFFDEDEDE);
  static const secondaryTextColor = Color(0xFFBDC6CA);
  static const indicatorDotColor = Color(0xFFC4C4C4);
  static const redColor = Color(0xFFFD0000);
  static const blackColor = Color(0x0000000);
  static const cancelButtonColor = Color(0xFF9A9C9C);
  static const listColor = Color(0xFFBDC6CA);
  static const blue = Color(0xFF2FB1FE);
  static const transparentBlue = Color(0xF22FB1FE);
  static const lightBlackColor = Color(0xFF0E2A2F);
  static const transparentWhiteColor = Color(0x80FFFFFF);
  static const lightWhiteColor = Color(0xFFF9FBFD);
  static const assignmentColorItem = Color(0xFFBA4BFE);
  static const syllabusColor = Color(0xFFEC9D21);

  static const material = Color(0xFF4ebcd5);
  static const assignment = Color(0xFFf3983e);
  static const discussion = Color(0xFF50af50);
  static const message = Color(0xFF3f51b5);
  static const question = Color(0xFF01579B);

  // added by parth for using in personal_info_view.dart file
  static const transparent = Colors.transparent;
  static const white = Colors.white;
}

Map<String, Color> colorMap = new Map();
Color getColorFromType(String type) {
  if (colorMap.isEmpty) {
    colorMap['Question'] = Palette.question;
    colorMap['Poll'] = Palette.assignment;
    colorMap['Notification'] = Palette.assignment;
    colorMap['Assessment'] = Palette.assignment;
    colorMap['Assignment'] = Palette.assignment;
    colorMap['Discussion'] = Palette.discussion;
    colorMap['Material'] = Palette.material;
    colorMap['Message'] = Palette.message;
  }
  return colorMap[type];
}
