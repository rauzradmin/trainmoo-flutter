class SnackDuration {
  static const Short = 5;
  static const Medium = 7;
  static const Long = 10;
}
