import 'package:flutter/material.dart';

///https://proandroiddev.com/flutter-localization-step-by-step-30f95d06018d
///This class is there to ,merely keep values of english right now.
///later if we have to support internationalization, it will get easy
///as we are already structuring app im similar way

class AppLocalizations {
  static final _localization = AppLocalizations();

  get takeAttendance => "Take Attendance";

  get startDiscussion => "Start Discussion";
  get askQuestion => "Ask Question";

  get createAssignment => "Create Assignment";

  get createSyllabus => "Create Syllabus";

  get createQuiz => "Create Quiz";

  get createQuestion => "Create Question";
  get grade => "Grade";

  get createMaterial => "Create Material";

  String get classes => "Classes";

  String get quiz => "Quiz";

  String get students => "Students";

  String get attendance => "Attendance";

  String get reply => '+ reply';

  String get className => "Class Name";

  String get teacherName => "Teacher Name";

  String get all => "All";

  String get present => "Present";

  String get absent => "Absent";

  String get leave => "Leave";

  String get submitEd => "Submited";
  String get submit => "Submit";

  String get pending => "Pending";
  String get internetMessage => "Please Check Your Internet Connection";

  String get sendReminder => "Send Reminder";

  String get selectFromTemplate => 'Select From Template(Optional)';

  String get selectCategory => 'Select Question Category';

  String get selectQuestionLevel => 'Select Question Level';

  get submitAssignment => 'Submit Assignment';
  get retake => 'Retake';
  get review => 'Review';

  get takeQuiz => 'Take Quiz';
  get retakeQuiz => 'Retake Quiz';

  String get myGrades => 'My Grades';

  String get myDetails => 'My Details';

  String get profilePicture => 'Profile Picture';

  String get gender => 'Gender';

  String get dateOfBirth => 'Date of Birth';

  String get address => 'Address';

  String get skills => 'Skills';

  String get gurdianName => 'Guardian Name';

  get logout => 'Logout';

  String get areSureYouWantToLogout => "Are you sure you want to logout?";

  String get close => 'Close';

  String get showAdvanceSetting => 'Show Advance Settings';

  String get allowRetake => "Allow Retake?";
  String get allowRetakeDesc =>
      "Allow students to retake quiz till it is locked";

  String get changeToHorizonView => "Change to horizontal view?";
  String get changeToHorizonDesc => "Show questions and options side by side";
  String get randomizeQuestions => "Randomize Questions?";
  String get randomizeQuestionsDesc =>
      "Show questions in random order for each student";

  String get hideAdvanceSetting => 'Hide Advance Settings';

  static AppLocalizations of(BuildContext context) {
    return _localization;
  }

  static AppLocalizations getLocalization() {
    return _localization;
  }

  String get welcome => 'Welcome,';

  String get student => 'Students';

  String get trainMoo => 'trainmoo';
  String get or => 'Or';

  String get name => 'Name';
  String get firstName => 'First Name';
  String get lastName => 'Last Name';

  String get phone => 'Phone';

  String get email => 'Email';
  String get emailOrMobile => 'Email or Mobile Number';

  String get password => 'Password';
  String get detail =>
      'By clicking Sign Up, you hereby agree to the Trainmoo Website Membership Agreement, Privacy Policy, Product Terms and Terms of Use , under which I am contracting with Rauzr Technologies Private Limited';

  String get confirmPassword => 'Confirm Password';

  String get signUp => 'SignUp';

  String get alreadyHaveAccount => 'Already have account?';

  String get loginHere => ' Login here';

  String get material => 'Materials';

  String get grades => 'Grades';

  String get syllabus => 'Syllabus';

  String get materialTitle => 'Material Title';
  String get dicussionTitle => 'Discussion Title';
  String get questionTitle => 'Question Title';

  String get quizTitle => 'Quiz Title';

  String get syllabusTitle => 'Syllabus Title';

  String get assignmentTitle => 'Assignment Title';

  String get question => 'Question 1';

  String get title => '   Assignment Title';

  String get date => '29 Sep 2019';

  String get comment => 'Comments';

  String get writeSomethingHere => 'Write Something Here....';

  String get filenamePdf => 'FileName \n .pdf ';

  String get assignment => 'Assignments';

  String get gradeAssignment => 'Grade Assignment';

  String get marks => 'Marks';

  String get description => 'Description';

  String get comments => 'Comments';

  String get reviewAssignment => 'Review Assignments';

  String get reviewQuiz => 'Review Quiz';

  String get assignments => 'Assignments';

  String get internals => 'Internals';

  String get total => 'Total';

  String get mark => 'Marks (Optional)';
  String get duration => 'Duration (mins)';

  String get selectClass => 'Select Class';
  String get selectEvent => 'Select Event';
  String get dueDate => 'Due Date';

  String get startDateTime => 'Start Date Time';
  String get dueDateTime => 'Due Date Time';

  String get addQuestion => 'Add Question';
  String get submitAttendance => 'Submit Attendance';

  String get quizQuestion => 'Quiz Question';
  String get answers => 'Answers';
  String get answer => 'Answer';
  String get myWork => 'My Works';
  String get completed => 'Completed';
  String get today => 'Today';
  String get thisWeek => 'This Week';
  String get thisMonth => 'This Month';
  String get mySession => 'My Sessions';
  String get myAssignment => 'My Assignment';
  String get myAttendance => 'My Attendance';
  String get createNotification => "Create notification";
  String get notification => "notification";
  String get sendReminderToAll => "Send Reminder To All";
  String get createEvent => "Create Event";
  String get eventDescription => "Event Discription";
  String get endDateTime => "End Date Time";
  String get create => "Create";
  String get onlineClass => "Online Class";
}
