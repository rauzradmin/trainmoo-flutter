class ValidationChecks {
  static validateFirstName(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'First Name required';
  }

  static validateLastName(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Last Name required';
  }

  static validatePhone(String value) {
    if (value.isNotEmpty && value.trim().length == 10)
      return null;
    else
      return 'Enter Valid Phone ';
  }

  static validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  static validateCompanyName(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Company Name required';
  }

  static validateCompanyAddress(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Company Address required';
  }

  static validateWebUrl(String value) {
    if (Uri.parse(value).isAbsolute)
      return null;
    else
      return 'Enter Valid Website';
  }

  static validateDocURL(String value) {
    if (Uri.parse(value).isAbsolute)
      return null;
    else
      return 'Enter Valid Doc URL';
  }

  static validateAboutCompany(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Enter something about your company';
  }

  static validateJobTitle(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Job Title required';
  }

  static validateJobType(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Job Type required';
  }

  static validateJobLocation(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Job Location required';
  }

  static validateRole(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Role required';
  }

  static validateDuration(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Duration required';
  }

  static validateStatus(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Status required';
  }

  static validateTitle(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Title required';
  }

  static validateTextToDisplay(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Text To Display Required';
  }

  static validateAgeFrom(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Age From required';
  }

  static validateAgeTo(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Age To required';
  }

  static validateInterest(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Interest required';
  }

  static validateDateFrom(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Date From required';
  }

  static validateDateTo(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Date To required';
  }

  static validateGroupName(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Group Name required';
  }

  static validateAboutGroup(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'About Group required';
  }

  static validateGroupOtherInfo(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Other Information required';
  }

  static validateExpiryDate(String value) {
    if (value.isNotEmpty)
      return null;
    else
      return 'Expiry Date required';
  }
}
