import 'package:flutter/material.dart';

import '../theme/palette.dart';

class UIHelper {
  // Vertical spacing constants. Adjust to your liking.
  static const double _verticalSpaceExtraSmall = 3.0;
  static const double _verticalSpaceSmall = 10.0;
  static const double _verticalSpaceSmall1 = 15.0;
  static const double _verticalSpaceMedium = 20.0;
  static const double _verticalSpaceMedium1 = 30.0;
  static const double _verticalSpaceLarge = 70.0;

  static const double marginSpaceSmall = 10.0;
  static const double marginSpaceSmall1 = 15.0;
  static const double marginSpaceMedium = 20.0;
  static const double marginSpaceMedium1 = 30.0;
  static const double marginSpaceLarge = 60.0;

  // Vertical spacing constants. Adjust to your liking.
  static const double _horizontalSpaceExtraSmall = 5.0;
  static const double _horizontalSpaceSmall = 10.0;
  static const double _horizontalSpaceSmall1 = 15.0;
  static const double _horizontalSpaceMedium = 20.0;
  static const double _horizontalSpaceMedium1 = 30.0;
  static const double _horizontalSpaceMedium2 = 50.0;
  static const double _horizontalSpaceLarge = 60.0;

  static const Widget verticalSpaceExtraSmall =
      SizedBox(height: _verticalSpaceExtraSmall);
  static const Widget verticalSpaceSmall =
      SizedBox(height: _verticalSpaceSmall);
  static const Widget verticalSpaceSmall1 =
      SizedBox(height: _verticalSpaceSmall1);
  static const Widget verticalSpaceMedium =
      SizedBox(height: _verticalSpaceMedium);
  static const Widget verticalSpaceLarge =
      SizedBox(height: _verticalSpaceLarge);
  static const Widget verticalSpaceMedium1 =
      SizedBox(height: _verticalSpaceMedium1);

  static const Widget verticalGapBetweenBox = SizedBox(height: 13);
  static const Widget horizontalGapBetweenBox = SizedBox(width: 13);

  static const Widget horizontalSpaceExtraSmall =
      SizedBox(width: _horizontalSpaceExtraSmall);
  static const Widget horizontalSpaceSmall =
      SizedBox(width: _horizontalSpaceSmall);
  static const Widget horizontalSpaceSmall1 =
      SizedBox(width: _horizontalSpaceSmall1);
  static const Widget horizontalSpaceMedium =
      SizedBox(width: _horizontalSpaceMedium);
  static const Widget horizontalSpaceMedium1 =
      SizedBox(width: _horizontalSpaceMedium1);
  static const Widget horizontalSpaceLarge =
      SizedBox(width: _horizontalSpaceLarge);
  static const Widget horizontalSpaceMedium2 =
      SizedBox(width: _horizontalSpaceMedium2);

  static Widget verticalGap(double heightOfBox) {
    return SizedBox(height: heightOfBox);
  }

  static Widget horizontalGap(double widthOfBox) {
    return SizedBox(width: widthOfBox);
  }

  static Widget divider = Container(
    height: 2,
    color: Palette.dividerColor,
  );

  static Widget dividerVertical = Container(
    height: double.infinity,
    width: 2,
    color: Palette.dividerColor,
  );

  static double loginContentSidePadding = 30.0;
  //height of input boxes
  static double inputBoxHeight90 = 90.0;

  static double inputBoxHeight80 = 80.0;
  static double inputBoxHeight120 = 120.0;

  //Card elevation
  static double cardElevation = 3;
  static double cardRadius = 8;
  static double contentPaddingFromCard = 20;
  static double contentPadding = 30;
  static double cardPaddingFromAll = 10;

  static double appBarSize = 80;

  static double imageSize = 140;

  static double imageCenterProgressSize = 55;
  static double cancelImageSize = 10;

  static double buttonHeight = 10;
  static double profileContentWidgetLeftPadding = 30;
  static double profileContentWidgetRightPadding = 20;

  //Subscription
  static double widthOfTitle = 70;
  static double heightOfTitle = 150;
  static double titleTopBottomPadding = 150;
  static double amountLeftRightPadding = 10;

  static double loanMonthIncreseDecreaseIconSize = 60;

  static double dialogWidth = 470;

  static double closeIconSize = 32;

  static double headerHeight = 200;
  static double circleTabSize = 37;
  static double circleNextPreviousSize = 47;

  static double screenPadding = 13;

  static getVerticalSpaceOf(double heightOfGap) {
    return SizedBox(
      height: heightOfGap,
    );
  }

  static getHorizontalSpaceOf(double heightOfGap) {
    return SizedBox(
      width: heightOfGap,
    );
  }
}
