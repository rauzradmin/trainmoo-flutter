import 'dart:convert';
import 'dart:io';

import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:path_provider/path_provider.dart';
import 'package:trainmoo/model/AttendanceModel.dart';
import 'package:trainmoo/model/Database/DatabaseHelper.dart';
import 'package:trainmoo/model/GetCommentModel.dart';
import 'package:trainmoo/model/GetQuestionModel.dart';
import 'package:trainmoo/model/GradeModel.dart';
import 'package:trainmoo/model/HomeModelList.dart';
import 'package:trainmoo/model/MaterialModel.dart';
import 'package:trainmoo/model/Notifications.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/Question.dart';
import 'package:trainmoo/model/QuestionCategory.dart';
import 'package:trainmoo/model/QuizUsersModel.dart';
import 'package:trainmoo/model/RepliesModel.dart';
import 'package:trainmoo/model/ReviewAssignment.dart';
import 'package:trainmoo/model/SchoolList/SchoolListModel.dart';
import 'package:trainmoo/model/SessionModel.dart';
import 'package:trainmoo/model/StudentModel.dart';
import 'package:trainmoo/model/SyllabusModel.dart';
import 'package:trainmoo/model/User.dart';
import 'package:trainmoo/model/studentModuel/grade/StudentGradeModel.dart';
import 'package:trainmoo/model/swipe_home_model.dart';
import 'package:trainmoo/model/takeAttendenceModel.dart';
import 'package:trainmoo/model/user_login_model.dart';
import 'package:trainmoo/model/user_signup_model.dart';
import 'package:trainmoo/shared_preference/user_preference.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:trainmoo/util/image_path.dart';

class API {
//  static String baseURL = 'https://alpha.trainmoo.in/api/';

  static String baseURL = 'https://www.trainmoo.in/api/';
  static String imageURL = 'https://www.trainmoo.in';

//  static String imageURL = 'https://alpha.trainmoo.in';
  static String generateSasTokenUrl = baseURL + 'login';
  static String registration = baseURL + 'signup';
  static String getUserDetailUrl = baseURL + 'user';
  static String getSchools = baseURL + 'schools';
  static String getSchool = baseURL + 'school';
  static int successStatusCode = 200;
  static Map<String, String> cookies = {};
  static Map<String, String> headers = {"content-type": "application/json"};
  static Dio dio = new Dio();

  static PersistCookieJar persistCookieJar;

  static const String materialScreenType = 'Material';
  static const String assignmentScreenType = 'Assignment';
  static const String quizScreenType = 'Assessment';
  static const String postScreenType = 'Post';

  static const String StudentUser = "student";
  static const String TeacherUser = "admin";
  static List<MaterialModel> latestPost = [];

  static Future init() async {
    dio.options.followRedirects = false;
    dio.options.validateStatus = (status) {
      return status < 500;
    };
    dio.options.headers = await getAuthHeader();
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    persistCookieJar = new PersistCookieJar(dir: tempPath);
    dio.interceptors.add(CookieManager(persistCookieJar));
    return;
  }

  static getAuthHeader() async {
    return {
      "Cookie": await UserPreference.getCookie(),
      // "Host": "www.trainmoo.in",
      // "Accept": "application/json, text/plain, */*",
      // "Accept-Encoding": "gzip, deflate, br",
      // "Connection": "keep-alive",
      // "Content-Length": 0,
      // "Pragma": "no-cache",
      // "Cache-Control": "no-cache",
      // "Referer": "https://www.trainmoo.in/app/apph/grades",
      // "Origin": "https://alpha.trainmoo.in",
      // "Content-Type": "application/json;charset=UTF-8",
      // "Sec-Fetch-Site": "same-origin",
      // "Sec-Fetch-Mode": "cors",
      // "Accept-Language": "en-US,en;q=0.9"
    };
  }

  static void getCookie(cookies) {
    String rawCookie = cookies;
    print("rawCookie : $headers");
    if (rawCookie != null) {
      int index = rawCookie.indexOf(';');
      rawCookie = (index == -1) ? rawCookie : rawCookie.substring(0, index);
    }

    UserPreference.saveCookie(rawCookie);
  }

  static Future<List<HomeModelList>> getCategories() async {
    List<HomeModelList> userList = [];
    for (int i = 0; i < 3; i++) {
      userList.add(new HomeModelList(
          name: "UserName",
          className: "ClassName",
          days: "3 days ago",
          desc:
              "Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor facilisis luctus, metus.",
          image: ImagePath.USER));
    }
    return userList;
  }

  static Future<UserLoginModel> loginUser(String email, String password) async {
    print('Login API calling : $generateSasTokenUrl');
    // getCookie(response.headers);,connect.sid=s%3AcSYS2eDQvux8tpvzA-C6N1aa5qcktZjQ.i%2BaMT056qBkjtlDc49YXC17D9h2tfvte6Ga13STJdV4; Path=/; Domain=www.trainmoo.in; HttpOnly;
    var response = await dio.post(generateSasTokenUrl,
        data: {"emailOrMobileNumber": email, "password": password});
    var responseData;
    // print('response ==>  ${response.data}');

    if (response.statusCode == successStatusCode) {
      responseData = response.data;
      print("response : ::: >>>> ${response.headers.value('set-cookie')}");
      String user = json.encode(UserLoginModel.fromJson(responseData));
      // print('user--$user');
      UserPreference.saveUser(user);
      //updateCookie(response);
      print("coookies ==> ");
      var cookies =
          persistCookieJar.loadForRequest(Uri.parse("https://www.trainmoo.in"));
      getCookie(cookies[0].toString());

      return UserLoginModel.fromJson(responseData);
    } else {
      responseData = response.data;
      // print('Login API calling done');

      return UserLoginModel.fromJson(responseData);
    }
  }

  static Future<UserLoginModel> googleloginUser(String email, String familyName,
      String fullName, String givenName, String image, String token) async {
    String url = baseURL + "glogin";
    print("url : $url");
    dio.options.headers = await getAuthHeader();
    var response = await dio.post(url, data: {
      "email": email,
      "familyName": familyName,
      "fullName": fullName,
      "givenName": givenName,
      "image": image,
      "token": token,
    });
    var responseData;
    print("response : ::: >>>> ${response.statusCode}");
    if (response.statusCode == successStatusCode) {
      responseData = response.data;
      print("response : ::: >>>> ${response.headers.value('set-cookie')}");
      String user = json.encode(UserLoginModel.fromJson(responseData));
      UserPreference.saveUser(user);
      var cookies =
          persistCookieJar.loadForRequest(Uri.parse("https://www.trainmoo.in"));
      getCookie(cookies[0].toString());
      return UserLoginModel.fromJson(responseData);
    } else {
      responseData = response.data;
      // print('Login API calling done');
      return UserLoginModel.fromJson(responseData);
    }
  }

  static Future<UserSignUpModel> registerUser(String firstName, String lastName,
      String emailOrMobileNumber, String password) async {
    var response = await dio.post(
      registration,
      data: {
        "name": {"firstname": firstName, "lastname": lastName},
        "emailOrMobileNumber": emailOrMobileNumber,
        "password": password
      },
    );

    var responseData;
    if (response.statusCode == successStatusCode) {
      responseData = response.data;
      String user = json.encode(UserLoginModel.fromJson(responseData));
      print('user--$user');
      UserPreference.saveUser(user);
      return UserSignUpModel.fromJson(responseData);
    } else {
      responseData = response.data;
      return UserSignUpModel.fromJson(responseData);
    }
  }

  static Future<List<CommonModel>> getWorkList() async {
    List<CommonModel> workLists = [];

    workLists.add(new CommonModel(
        title: 'Take Attendance',
        desc:
            'Suspendisse urna nibh, viverra non, semper suscipit posuere a, pede.',
        image: ImagePath.ASSIGNMENT_IMG));
    workLists.add(new CommonModel(
        title: 'Grade Assignment',
        desc:
            'Suspendisse urna nibh, viverra non, semper suscipit posuere a, pede.',
        image: ImagePath.GRADE_IMG));
    workLists.add(new CommonModel(
        title: 'Review Quiz',
        desc:
            'Suspendisse urna nibh, viverra non, semper suscipit posuere a, pede.',
        image: ImagePath.QUIZ_IMG));
    return workLists;
  }

  static Future<List<CommonModel>> getClassItems(
      BuildContext context, ProgramClassesModel classObject) async {
    List<CommonModel> classItemList = [];
    print("classObject : ${classObject.postCount.assignment}");
    classItemList.add(new CommonModel(
        title: AppLocalizations.of(context).students,
        desc: classObject.users.student.toString(),
        image: ImagePath.STUDENT_IMAGE));

    classItemList.add(new CommonModel(
        title: AppLocalizations.of(context).material,
        desc: classObject.postCount == null
            ? "0"
            : classObject.postCount.material.toString(),
        image: ImagePath.MATERIAL_IMG));

    classItemList.add(new CommonModel(
        title: "Assignments",
        desc: classObject.postCount == null
            ? "0"
            : classObject.postCount.assignment.toString(),
        image: ImagePath.ASSIGNMENT_IMG));

    classItemList.add(new CommonModel(
        title: "Quiz",
        desc: classObject.postCount == null
            ? "0"
            : classObject.postCount.assessment.toString(),
        image: ImagePath.QUIZ_IMG));

    classItemList.add(new CommonModel(
        title: "Attendance", desc: "72%", image: ImagePath.ATTENDANCE_IMG));

    classItemList.add(new CommonModel(
        title: "Grade", desc: "1 New", image: ImagePath.GRADE_IMG));
    classItemList.add(new CommonModel(
        title: "Syllabus", desc: "1 New", image: ImagePath.SYLLABUS_IMAGE));

    return classItemList;
  }

  static Future<List<TakeAttendanceModel>> getAttendance() async {
    List<TakeAttendanceModel> _attendanceList = [];

    for (int i = 0; i < 6; i++) {
      _attendanceList.add(new TakeAttendanceModel(
          title: "Bellie Curtis",
          image: ImagePath.ICON_StudentPhoto,
          isFalse: true,
          isTrue: false));
    }

    return _attendanceList;
  }

  static getUserDetail() async {
    dio.options.headers = await getAuthHeader();

    var response = await dio.get(getUserDetailUrl);

    if (response.statusCode == 200) {
      print('response ==>  ${response.data}');
      var responseData = response.data;
      return response.data;
    } else {
      return response.data;
    }
  }

  static getActiveSesssionAttendance(schoolId, userId) async {
    String url =
        getSchool + '/$schoolId/attendance/activeSessions?userid=$userId';
    var response = await dio.get(url);
    print("getActiveSesssionAttendance ------>>>  ${response.data}");
  }

  static getClassAttendance(classId) async {
    String schoolId = await UserPreference.getSchoolId();
    String schoolUserId = await UserPreference.getSchoolUserId();
    print("user is : $schoolUserId");
    print("class id : $classId");
    print("schoolid: $schoolId");
    //getActiveSesssionAttendance(schoolId, user.id);
    String url = getSchool +
        '/$schoolId/user/$schoolUserId/attendanceagg?classid=$classId';
    var response = await dio.get(url);
    print("getClassAttendance ------>>>  ${response.data}");
    if (response.statusCode == 200) {
      return response.data;
    } else {
      return null;
    }
  }

  static getStudentAttendance(classId) async {
    String schoolId = await UserPreference.getSchoolId();
    String schoolUserId = await UserPreference.getSchoolUserId();
    String url =
        getSchool + '/$schoolId/user/$schoolUserId/attendance?classid=$classId';
    print("url=====> $url");
    var response = await dio.get(url);
    print("getStudentAttendance ------>>>  ${response.data}");
    if (response.statusCode == 200) {
      return response.data;
    } else {
      return null;
    }
  }

  static getEventList(date, classId) async {
    String schoolId = await UserPreference.getSchoolId();
    User user = await UserPreference.getUserDetail();
    print("user is : ${user.id}");
    print("date: $date");
    print("class id : $classId");
    print("schoolid: $schoolId");
    String url = getSchool +
        '/$schoolId/calendarEvents?start=$date&end=$date&schoolid=$schoolId&classid=$classId&userid=${user.id}';
    var response = await dio.get(url);
    print("getEventList ------>>>  ${response.data}");
    if (response.statusCode == 200) {
      return response.data;
    } else {
      return [];
    }
  }

  static createAssignment(
      String classID,
      String title,
      String marks,
      String desc,
      DateTime startDate,
      DateTime dueDate,
      List<Map<String, dynamic>> filePath) async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/posts';
    dio.options.headers = await getAuthHeader();
    print('Params => ${{
      "files": filePath,
      "type": "Assignment",
      "class": classID,
      "likes": 0,
      "replies": [],
      "school": schoolId,
      "task": {
        "title": title,
        "maxmarks": marks,
        "start": Utility.serverDateTimeFormat(startDate),
        "startTime": Utility.serverTimeFormate(startDate),
        "dueby": Utility.serverDateTimeFormat(dueDate),
        "duebyTime": Utility.serverTimeFormate(dueDate),
        "graded": true
      },
      "text": desc,
      "submittable": true,
      "submissionOptions": {"type": "Any"}
    }}');
    var response = await dio.post(url, data: {
      "files": filePath,
      "type": "Assignment",
      "class": classID,
      "likes": 0,
      "replies": [],
      "school": schoolId,
      "task": {
        "title": title,
        "maxmarks": marks,
        "start": Utility.serverDateTimeFormat(startDate),
        "startTime": Utility.serverTimeFormate(startDate),
        "dueby": Utility.serverDateTimeFormat(dueDate),
        "duebyTime": Utility.serverTimeFormate(dueDate),
        "graded": true
      },
      "text": desc,
      "submittable": true,
      "submissionOptions": {"type": "Any"}
    });
    print('response ==>  ${response.data}');
    if (response.statusCode == 200) {
      return null;
    } else {
      return response.data;
    }
  }

  static submitAssignment(
      String title,
      String desc,
      DateTime startDate,
      DateTime dueDate,
      List<Map<String, dynamic>> filePath,
      MaterialModel materialObject) async {
    // print("--------->>> filePath <<<<----------- $filePath");
    String schoolId = await UserPreference.getSchoolId();
    var parentID = await UserPreference.getCurrentAssignmentID();
    // String userID = await UserPreference.getUserId();
    var classData = await UserPreference.getClass();
    // UserLoginModel userData = new UserLoginModel();
    var userData = await UserPreference.getUserInfo();
    print("classData======>>>>>jit: $classData");
    // print("userdata======>>>>>jit: $userData.");
    String url = getSchool + '/$schoolId/posts';
    // print("url ========>>>>>>jit $url");
    dio.options.headers = await getAuthHeader();
    print('print => ${{
      "title": title,
      "start": materialObject.task.start,
      "startTime": materialObject.task.startTime,
      "dueby": materialObject.task.dueby,
      "duebyTime": materialObject.task.duebyTime
    }}');
    var response = await dio.post(url, data: {
      "parent": parentID,
      "files": filePath,
      "likes": 0,
      "replies": [],
      "type": "Assignment",
      "responseType": "submission",
      "user": userData,
      "school": schoolId,
      "class": classData,
      "task": {
        "title": title,
        "start": materialObject.task.start,
        "startTime": materialObject.task.startTime,
        "dueby": materialObject.task.dueby,
        "duebyTime": materialObject.task.duebyTime
      },
      "text": desc,
      "submittable": true,
    });

    print("response : ======>   ${response.data}");

    if (response.statusCode == 200) {
      return null;
    } else {
      return response.data;
    }
  }

  static Future<String> createMaterial(String selectClass, String title,
      String desc, String type, List<Map<String, dynamic>> filePath) async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/posts';
    dio.options.headers = await getAuthHeader();
    print('params ==>  ${{
      "files": filePath,
      "likes": 0,
      "replies": [],
      "type": type,
      "title": title,
      "text": desc,
      "class": selectClass,
      "school": schoolId,
      "submittable": false
    }}');

    var response = await dio.post(url, data: {
      "files": filePath,
      "likes": 0,
      "replies": [],
      "type": type,
      "title": title,
      "text": desc,
      "class": selectClass,
      "school": schoolId,
      "submittable": false
    });
    print('response ==>  ${response.data}');

    if (response.statusCode == 200) {
      return null;
    } else {
      return response.data['errors'].toString();
    }
  }

  static Future<String> createQuiz(
      String selectClass,
      String title,
      String desc,
      String mark,
      String startTime,
      String dueTime,
      String startDate,
      String dueDate,
      bool reTake,
      bool random,
      bool changeToView,
      List<String> questionIdList,
      List<Map<String, dynamic>> attendanceData,
      String duration) async {
    String schoolId = await UserPreference.getSchoolId();
    User user = await UserPreference.getUserDetail();
    String url = getSchool + '/$schoolId/quiz';

    var params = {
      "files": [],
      "replies": [],
      "type": "Assessment",
      "user": user.id,
      "school": schoolId,
      "assessment": {
        "retake": reTake,
        "random": random,
        "passageView": false,
        "perItemMarks": 1,
        "duration": duration,
        "allocationType": "spread",
        "count": 3
      },
      "task": {
        "start": startDate,
        "startTime": startTime,
        "dueby": dueDate,
        "duebyTime": dueTime,
        "graded": true,
        "title": title,
        "maxmarks": int.parse(mark)
      },
      "quizids": questionIdList,
      "text": title,
      "class": selectClass,
      "marks": [null, null, null]
    };
    print('params ==> $params');
    dio.options.headers = await getAuthHeader();
    var response = await dio.post(url, data: params);
    if (response.statusCode == 200) {
      print('response ==>  ${response.data}');
      return response.data;
    } else {
      return null;
    }
  }

  static Future<Question> saveQuestion(
    String question,
    String desc,
    String option1,
    String option2,
    String option3,
    String option4,
    String answer,
    BuildContext context,
    String selectedQuestionLevel,
    String category,
  ) async {
    String schoolId = await UserPreference.getSchoolId();

    String saveQuestionUrl = getSchool + '/$schoolId/questions';
    dio.options.headers = await getAuthHeader();
    var data = {
      "type": 'MC',
      "files": [],
      "question": question,
      "options": [
        {
          "text": "$option1",
          "seqno": "A",
          "answer": answer.compareTo("answer1") == 0 ? true : false,
          "files": []
        },
        {
          "text": "$option2",
          "seqno": "B",
          "answer": answer.compareTo("answer2") == 0 ? true : false,
          "files": []
        },
        {
          "text": "$option3",
          "seqno": "C",
          "answer": answer.compareTo("answer3") == 0 ? true : false,
          "files": []
        },
        {
          "text": "$option4",
          "seqno": "D",
          "answer": answer.compareTo("answer4") == 0 ? true : false,
          "files": []
        }
      ],
      "explanation": desc,
      "explanationFiles": [],
      "tags": [],
      "category": category,
      "level": selectedQuestionLevel,
      "subquestion": "",
      "school": schoolId
    };

    var response = await dio.post(saveQuestionUrl, data: data);
    if (response.statusCode == successStatusCode) {
      print('response ==>  ${response.data}');

      Question question = Question.map(response.data);
      return question;
//      List<dynamic> questions = response.data['questions'];
//      return GetQuestionModel.fromJsonArray(questions);
    } else {
      return null;
    }

//    return ;
  }

  static addNewQuestion(String question, String desc, String answerA) async {
    dio.options.headers = await getAuthHeader();

    var response = await dio.post(generateSasTokenUrl,
        data: {"question": question, "description": desc, "answer": answerA});

    var responseData;
    if (response.statusCode == successStatusCode) {
      print('response ==>  ${response.data}');
      responseData = response.data;
      return responseData;
    } else {
      responseData = response.data;
      return responseData;
    }
  }

  static Future<SchoolListModel> getSchoolData() async {
    dio.options.headers = await getAuthHeader();
    var response = await dio.get(getSchools);
    print(response.data);
    if (response.statusCode == successStatusCode) {
      List<dynamic> responseData = response.data;
      List<SchoolListModel> schoolList =
          SchoolListModel.fromJsonArray(responseData);
      if (schoolList.length > 0)
        UserPreference.saveSchoolDetails(
            json.encode(responseData[0]), schoolList[0].id);
      // UserPreference.setSchoolId(schoolList[0].id);
      return schoolList.length > 0 ? schoolList[0] : null;
    } else {
      return null;
    }
  }

  static Future<List<MaterialModel>> getPosts(String classId, String postType,
      String sortBy, String section, int timeStamp) async {
//    if (latestPost.length > 0) return latestPost;
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool +
        '/$schoolId/posts?schoolid=$schoolId&classid=${classId == null ? '' : classId}&postType=${postType == null ? '' : postType}&timestamp=$timeStamp&sortBy=${sortBy == null ? '' : sortBy}&section=${section == null ? '' : section}';
    print(" getPosts url : $url");

    dio.options.headers = await getAuthHeader();
    var response = await dio.get(url);

    if (response.statusCode == successStatusCode) {
      print('response ==>  ${response.data}');
      List<dynamic> posts = response.data;
      print('response length ==>  ${posts.length}');
      latestPost = MaterialModel.fromJsonArray(posts);
      return latestPost;
    } else {
      return [];
    }
  }

  static Future<List<ProgramClassesModel>> getProgramClass(offline) async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/programsclasses?archived=false';
    print('URL for the get post ===> $url');

    dio.options.headers = await getAuthHeader();
    var response = await dio.get(url);

    if (response.statusCode == successStatusCode) {
      print('response  get program class==>  ${response.data}');
      List<dynamic> posts = response.data;
      if (offline) {
        await DatabaseHelper().saveClassData(posts);
      }

      print('response length ==>  ${posts.length}');

      return ProgramClassesModel.fromJsonArray(posts);
    } else {
      return [];
    }
  }

  static Future<GetCommonModel> getPostComments(String id) async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/post/$id';
    print('URL for the get post ===> $url');

    dio.options.headers = await getAuthHeader();
    var response = await dio.get(url);

    if (response.statusCode == successStatusCode) {
      print('getPostComments jitnedra==>  ${response.data}');
      UserPreference.saveClass(json.encode(response.data['class']));
      return GetCommonModel.fromJson(response.data);
//      return (response.data as List)
//          .map((p) => GetCommentModel.fromJson(p))
//          .toList();
    } else {
      return null;
    }
  }

  static Future<bool> deletePostComments(String id) async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/reply/$id';
    print('URL for the get post ===> $url');
    dio.options.headers = await getAuthHeader();
    var response = await dio.delete(url);
    if (response.statusCode == successStatusCode) {
      print('getPostComments jitnedra==>  ${response.data}');
      return true;
    } else {
      return false;
    }
  }

  static Future<bool> like(String like, String postId) async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/post/$postId/$like';
    print('URL for the get post ===> $url');

    dio.options.headers = await getAuthHeader();
    var response = await dio.post(url);

    if (response.statusCode == successStatusCode) {
      print('response ==>  ${response.data}');

      return true;
    } else {
      return false;
    }
  }

  static Future<List<MaterialModel>> getMaterials(
      String classID, String postType, String sortBy, String section) async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool +
        '/$schoolId/posts?schoolid=$schoolId&classid=$classID&postType=$postType&timestamp=${new DateTime.now().millisecondsSinceEpoch}&sortBy=$sortBy&section=${section == null ? '' : section}';
    print('URL for the get post ===> $url');

    dio.options.headers = await getAuthHeader();
    var response = await dio.get(url);

    if (response.statusCode == successStatusCode) {
      // print('response ==>  ${response.data}');
      List<dynamic> posts = response.data;
      //print('response length ==>  ${posts.length}');

      return MaterialModel.fromJsonArray(posts);
    } else {
      return [];
    }
  }

  static Future<List<Question>> getQuestionsFromApi() async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool +
        '/$schoolId/questions?category=&level=&timestamp=${new DateTime.now().millisecondsSinceEpoch}&marketschoolid=';
    print('URL for the get post ===> $url');

    dio.options.headers = await getAuthHeader();
    var response = await dio.get(url);

    if (response.statusCode == successStatusCode) {
      return (response.data['questions'] as List)
          .map((p) => Question.fromJson(p))
          .toList();
    } else {
      return [];
    }
  }

  static Future<String> postComment(
      String parentId,
      String type,
      String classId,
      String comment,
      List<Map<String, dynamic>> filePath) async {
    String schoolId = await UserPreference.getSchoolId();
    User user = await UserPreference.getUserDetail();
    String url = getSchool + '/$schoolId/posts';

    dio.options.headers = await getAuthHeader();
    print('Files Path API CAll======> ${filePath.length}');
    var response = await dio.post(url, data: {
      "parent": parentId,
      "files": filePath,
      "likes": 0,
      "replies": [],
      "type": type,
      "responseType": "reply",
      "user": {"_id": user.id},
      "school": schoolId,
      "class": {"_id": classId},
      "text": comment,
      "submittable": false
    });
    if (response.statusCode == successStatusCode) {
      print('response post comment  ==>  ${response.data}');
      return 'Comment Added SuccessFully';
    } else {
      return null;
    }
  }

  static Future<String> gradeAssignment(data) async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/post/${data["_id"]}';
    print("url :  $url");
    dio.options.headers = await getAuthHeader();
    var response = await dio.put(url, data: data);
    if (response.statusCode == successStatusCode) {
      print('response ==>  ${response.data}');
      return 'grade added successfully..';
    } else {
      return null;
    }
  }

  static Future<List<StudentModel>> getStudents(String classId) async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/class/$classId/users';

    dio.options.headers = await getAuthHeader();
    var response = await dio.get(url);

    if (response.statusCode == successStatusCode) {
      print('response ==>  ${response.data}');
      List<dynamic> posts = response.data;
      await DatabaseHelper().saveStudentData(response.data, classId);
      await DatabaseHelper().getStudents(classId);
      print('response length ==>  ${posts.length}');

      return StudentModel.fromJsonArray(posts);
    } else {
      return [];
    }
  }

  static Future<List<StudentGradeModel>> getStudentGrade(String classId) async {
    String schoolId = await UserPreference.getSchoolId();
    String schoolUserId = await UserPreference.getSchoolUserId();
    print('schoolUserId==>  ${schoolUserId}');
    String url =
        getSchool + '/$schoolId/gradebook/student?schooluserid=${schoolUserId}';
    print('url ==>  ${url}');
    dio.options.headers = await getAuthHeader();
    var response = await dio.get(url);
    print('getStudentGrade response ==>  ${response.data}');
    if (response.statusCode == successStatusCode) {
      print('response ==>  ${response.data}');
      return (response.data as List)
          .map((p) => StudentGradeModel.fromJson(p))
          .toList();
    } else {
      return [];
    }
  }

  static Future<List<AttendanceModel>> getUserAttendance(
      String classID, String date, String eventId) async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool +
        '/$schoolId/class/5b3c65c0ebfc8000353ed91d/date/$date/attendance?eventid=$eventId';
    print('attendance API ==> $url');
    dio.options.headers = await getAuthHeader();
    var response = await dio.get(url);
    print('response for the get post ===> $response');

    if (response.statusCode == successStatusCode) {
      return AttendanceModel.fromJsonArray(response.data);
    } else {
      return null;
    }
  }

  static Future<GradeModel> getGradeList(String classID) async {
    String schoolId = await UserPreference.getSchoolId();
    print('class id ==> $classID');
    String url = getSchool +
        '/$schoolId/class/$classID/gradebook?items=15&skip=0&sortBy=roll-no';

    dio.options.headers = await getAuthHeader();
    var response = await dio.get(url);
    print('response for the get post ===> $response');

    if (response.statusCode == successStatusCode) {
      return GradeModel.map(response.data);
    } else {
      return null;
    }
  }

  static Future<SyllabusModel> getSyllabus(String classID) async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/class/$classID';
    print('SyllabusModel url ==> $url');

    dio.options.headers = await getAuthHeader();
    var response = await dio.get(url);
    print('response for the get post ===> $response');

    if (response.statusCode == successStatusCode) {
      return SyllabusModel.map(response.data);
    } else {
      return null;
    }
  }

  static Future<ReviewAssignemt> getQuizSubmittedUsers(String quizId) async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/post/$quizId';
    print('SyllabusModel url ==> $url');

    dio.options.headers = await getAuthHeader();
    var response = await dio.get(url);
    print('response for the get post ===> $response');

    if (response.statusCode == successStatusCode) {
      // return QuizUserModel.map(response.data);
      return ReviewAssignemt.map(response.data);
    } else {
      return null;
    }
  }

  static Future<String> notifyPendingUser(
      StudentModel studentModel, String assignmentId) async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/post/$assignmentId/notifyPendingUsers';
    print('notifyPendingUser url ==> $url');

    dio.options.headers = await getAuthHeader();
    var response = await dio.post(url, data: studentModel.toJson());
    print('response for the get post ===> $response');

    if (response.statusCode == successStatusCode) {
      print('Success-->');
      return response.data;
    } else {
      print('Failure-->');
      return null;
    }
  }

  static Future<void> takeAttendance(List<Map<String, dynamic>> attendanceArray,
      String classId, String eventId, String date, bool isOffline) async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool +
        '/$schoolId/class/$classId/event/$eventId/eventdate/$date/saveattendance';

    if (isOffline)
      url = getSchool +
          '/$schoolId/class/$classId/event/$eventId/eventdate/$date/saveattendance?offline=true';

    //new line add(trainmoo.in)
//    String url = getSchool +
//        '/$schoolId/class/$classId/event/$eventId/eventdate/$date/saveattendance?offline=false';
//
//    if (isOffline)
//      url = getSchool +
//          '/$schoolId/class/$classId/event/$eventId/eventdate/$date/saveattendance?offline=true';

    print('SyllabusModel url ==> $url');
    print("attendanceArray :: $attendanceArray");
    print('params url ==> ${jsonEncode(attendanceArray)}');

    dio.options.headers = await getAuthHeader();
    var response = await dio.post(url, data: attendanceArray);
    print('response for the get post ===> $response');

    if (response.statusCode == successStatusCode) {
      return true;
    } else {
      return false;
    }
  }

  static Future<List<SessionModel>> getSessionData(
      String startDate, String endDate) async {
    String schoolId = await UserPreference.getSchoolId();
    User user = await UserPreference.getUserDetail();
//    String url = getSchool +
//        '/$schoolId/calendarEvents?start=$startDate&end=$endDate&schoolid=$schoolId&userid=${user.id}';

    //new url for (trainmoo.in)
    String url = getSchool +
        '/$schoolId/calendarEvents?start=$startDate&end=$endDate&schoolid=$schoolId&classid=&userid=';
    print('URL===> $url');
    dio.options.headers = await getAuthHeader();
    var response = await dio.get(url);
    print('response===>  ${response.data}');

    if (response.statusCode == successStatusCode) {
      return SessionModel.fromJsonArray(response.data);
    } else {
      return [];
    }
  }

  static Future<List<QuestionCategory>> getQuestionCategories() async {
    String schoolId = await UserPreference.getSchoolId();
    User user = await UserPreference.getUserDetail();
    String url = getSchool + '/$schoolId/quizbank/categories';
    print('URL===>  $url');
    dio.options.headers = await getAuthHeader();
    var response = await dio.get(url);
    print('response===>  ${response.data}');

    if (response.statusCode == successStatusCode) {
      return QuestionCategory.fromJsonArray(response.data);
    } else {
      return [];
    }
  }

  static Future<List<Question>> getTempQuestion(
      String category, String level) async {
    String schoolId = await UserPreference.getSchoolId();
    if (category == null) category = '';
    if (level == null) level = '';
    int timeStamp = new DateTime.now().millisecondsSinceEpoch;
    String url = getSchool +
        '/$schoolId//questions?category=$category&level=$level&timestamp=$timeStamp&marketschoolid=$schoolId';
    print('API url for get Question ==> $url');
    dio.options.headers = await getAuthHeader();
    var response = await dio.get(url);
    if (response.statusCode == successStatusCode) {
      print('response question selections ==>  ${response.data}');
      List<dynamic> questions = response.data['questions'];
      return GetQuestionModel.fromJsonArray(questions);
    } else {
      return [];
    }
  }

  static Future<String> getuserType() async {
    String useId = await UserPreference.getUserId();
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/user/$useId';
    dio.options.headers = await getAuthHeader();
    var response = await dio.get(url);
    if (response.statusCode == successStatusCode) {
      var responseData = response.data;
      var type = responseData['type'];
      var schoolUserId = responseData['_id'];
      print("user id : $schoolUserId");
      UserPreference.saveSchoolUserId(schoolUserId);
      return type;
    } else {
      print("Failure to call api");
      return null;
    }
  }

  static Future<dynamic> getQuizQuestion(String assessmentsID) async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/assessments/$assessmentsID';
    print('notifyPendingUser url ==> $url');

    dio.options.headers = await getAuthHeader();
    var response = await dio.get(url);
    // print("response for the get Quiz Question jit ===> ${response.data}");

    if (response.statusCode == successStatusCode) {
      print('Success--> ${response.data}');
      return response.data;
    } else {
      print('Failure-->');
      return null;
    }
  }

  static Future<dynamic> submitQuiz(data, bool list) async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/assessmentSubmit';
    String url2 = getSchool + '/$schoolId/assessmentSubmits';

    print('submitQuiz url ==> ${list ? url2 : url}');
    print("data : $data");
    dio.options.headers = await getAuthHeader();
    var response = await dio.post(list ? url2 : url, data: data);
    // print("response for the get Quiz Question jit ===> ${response.data}");

    if (response.statusCode == successStatusCode) {
      print('Success--> ${response.data}');
      return response.data;
    } else {
      print('Failure-->');
      return null;
    }
  }

  static Future<dynamic> getAssesment(postId) async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/post/$postId';
    print('getAssesment url ==> $url');

    dio.options.headers = await getAuthHeader();
    var response = await dio.get(url);
    // print("response for the get Quiz Question jit ===> ${response.data}");

    if (response.statusCode == successStatusCode) {
      print('Success--> ${response.data}');
      return response.data;
    } else {
      print('Failure-->');
      return null;
    }
  }

  static Future<dynamic> submitAssesment(data) async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/assessments/${data['_id']}';
    print('notifyPendingUser url ==> $url');

    dio.options.headers = await getAuthHeader();
    var response = await dio.put(url, data: data);
    // print("response for the get Quiz Question jit ===> ${response.data}");

    if (response.statusCode == successStatusCode) {
      print('submitAssesment -------> Success--> ${response.data}');
      return response.data;
    } else {
      print('Failure-->');
      return null;
    }
  }

  //notifications
  static Future<dynamic> getNotifications() async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/notifications';
    print('getNotifications url ==> $url');

    dio.options.headers = await getAuthHeader();
    var response = await dio.get(url);

    if (response.statusCode == successStatusCode) {
      print('notification -------> Success--> ${response.data}');
      var data = NotificationModel.fromJsonArray(response.data);
      print('notification ------->  ${data.length}');
      return data;
    } else {
      print('Failure-->');
      return null;
    }
  }

  static Future<dynamic> dismissNotifications(notificationId) async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/notification/$notificationId/dismiss';
    print('getNotifications url ==> $url');
    dio.options.headers = await getAuthHeader();
    print("cockies : ${dio.options.headers}");
    var response = await dio.put(url);
    if (response.statusCode == successStatusCode) {
      print(
          'notification -------> $successStatusCode Success--> ${response.data}');
      return true;
    } else {
      print('Failure-->');
      return false;
    }
  }

  static Future<dynamic> dismissAllNotifications() async {
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/notification/dismissall';
    print('dismissAllNotifications url ==> $url');

    dio.options.headers = await getAuthHeader();
    print("cockies : ${dio.options.headers}");
    var response = await dio.put(url);

    if (response.statusCode == successStatusCode) {
      print(
          'dismissAllNotifications -------> $successStatusCode Success--> ${response.data}');
      return true;
    } else {
      print('Failure-->');
      return false;
    }
  }

  //events
  static Future<dynamic> createEvent(
      classobject,
      String desc,
      DateTime selectedStartDate,
      DateTime selectedEndDate,
      String startDate,
      String startTime,
      String endDate,
      String endTime,
      String program,
      String tutorName) async {
    //String school = await UserPreference.getSchoolDetails();
    String schoolId = await UserPreference.getSchoolId();
    String url = getSchool + '/$schoolId/events';
    print("url of createEvent : $url");
    dio.options.headers = await getAuthHeader();
    print(
        "value : ${tutorName != null ? program + " : " + classobject['name'] + " - " + tutorName : program + " : " + classobject['name']}");
    print(
        "selectedEndDate.toUtc().toString() ${selectedEndDate.add(new Duration(days: 365)).toUtc().toString()}");
    var response = await dio.post(url, data: {
      "type": "event",
      "class": classobject,
      "details": desc,
      "end": selectedEndDate.toUtc().toString(),
      "start": selectedStartDate.toUtc().toString(),
      "endDate": endDate,
      "endOn":
          selectedStartDate.add(new Duration(days: 365)).toUtc().toString(),
      "endTime": endTime,
      "files": [],
      "isCustom": true,
      "repeat": "none",
      "startDate": startDate,
      "startTime": startTime,
      "title": tutorName != null
          ? program + " : " + classobject['name'] + " - " + tutorName
          : program + " : " + classobject['name'],
    });
    print('createEvent response ==>  ${response.data}');
    if (response.statusCode == 200) {
      return null;
    } else {
      return response.data;
    }
  }

  static Future<dynamic> creatMeeting(url) async {
    print('creatMeeting url ==> $url');
    var response = await dio.put(url);
    if (response.statusCode == successStatusCode) {
      print('creatMeeting -------> Success--> ${response.data}');
      return true;
    } else {
      print('Failure-->');
      return false;
    }
  }

  static Future<dynamic> setEnrollNow(enrollNo) async {
    var url = baseURL + "class/enroll/$enrollNo";
    print('setEnrollNow==> $url');
    var response = await dio.post(url);
    if (response.statusCode == successStatusCode) {
      print('setEnrollNow -------> Success--> ${response.data}');
      return true;
    } else {
      print('Failure-->');
      return false;
    }
  }
}
