import 'package:flutter/material.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/SessionModel.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';

class EventDropDownList extends StatelessWidget {
  final List<SessionModel> eventList;
  final Function onTap;
  final String selectedEventId;

  EventDropDownList({this.eventList, this.onTap, this.selectedEventId});

  @override
  Widget build(BuildContext context) {
    print("eventList------>  $eventList");
    return Container(
      height: 50,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: new BorderRadius.all(Radius.circular(3)),
      ),
      padding: EdgeInsets.only(left: 18, right: 10),
      child: DropdownButton(
        underline: Container(
          decoration: BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.transparent))),
        ),
        isExpanded: true,
        icon: Icon(
          Icons.arrow_drop_down,
          color: Palette.accentColor,
          size: 25,
        ),
        hint: new Text(AppLocalizations.of(context).selectEvent,
            style: AppTextStyle.hintStyle()),
        value: selectedEventId,
        onChanged: onTap,
        items: eventList.map((event) {
          print("event =====>: ${event.event}");
          return new DropdownMenuItem(
            value: event.event,
            child: new Text(
              event.title,
              style: TextStyle(color: Colors.black),
            ),
          );
        }).toList(),
      ),
    );
  }
}
