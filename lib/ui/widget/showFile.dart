import 'dart:core';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:permission_handler/permission_handler.dart';
import 'package:trainmoo/model/UploadFileModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/util/image_path.dart';
import 'package:mime_type/mime_type.dart';

class ShowFileView extends StatefulWidget {
  final Function function;
  ShowFileView({this.function});
  @override
  _ShowFileViewState createState() => _ShowFileViewState();
}

class _ShowFileViewState extends State<ShowFileView> with BaseCommonWidget {
  List<UploadFileModel> selectedFiles = [];

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Align(
          alignment: Alignment.topLeft,
          child: Padding(
            padding: const EdgeInsets.only(top: 10),
            child: GestureDetector(
              onTap: () {
//                           pickImageFromGallery(
//                           ImageSource.gallery, model);
                _openBottomSheet();
              },
              child: Container(
                width: 91,
                height: 85,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: new BorderRadius.all(Radius.circular(3)),
                ),
                child: Center(
                  child: Image.asset(
                    ImagePath.ICON_ADD,
                    width: 20,
                    height: 20,
                  ),
                ),
              ),
            ),
          ),
        ),
//                        showImage(model.imageFile),
        showImageOrFilePath()
      ],
    );
  }

  openOtherFile(String extension) async {
    print("extension === > $extension");
    try {
      bool permissionGranted = false;
      if (await Permission.storage.request().isGranted) {
        permissionGranted = true;
      } else {
// You can request multiple permissions at once.
        Map<Permission, PermissionStatus> statuses = await [
          Permission.storage,
        ].request();
        if (statuses.values.elementAt(0) == PermissionStatus.granted) {
          permissionGranted = true;
        } else {
          permissionGranted = false;
        }
      }

      if (!permissionGranted) {
        print('Permission Not granted');
        _showDialog();
      } else {
        FilePickerResult result =
            await FilePicker.platform.pickFiles(allowMultiple: true);

        if (result != null) {
          List<File> files = result.paths.map((path) {
            return File(path);
          }).toList();
          files.forEach((file) {
            addFileToArray(file);
          });
        }
        // List<File> files = await FilePicker.getMultiFile(
        //     // type: FileType.custom,
        //     // allowedExtensions: [
        //     //   'pdf',
        //     //   'doc',
        //     //   'docx',
        //     //   'xlsx',
        //     //   'xls',
        //     //   "ppt",
        //     //   "pptx",
        //     //   "txt"
        //     // ],
        //     );

        // files.forEach((file) {
        //   addFileToArray(file);
        // });
        // if (file != null) {
        //   String fileExtension = path.extension(file.path);
        //   // print('fileExtension-->$fileExtension');

        //   if (fileExtension.toLowerCase().compareTo(".pdf") == 0 ||
        //       fileExtension.toLowerCase().compareTo(".doc") == 0 ||
        //       fileExtension.toLowerCase().compareTo(".docx") == 0 ||
        //       fileExtension.toLowerCase().compareTo(".xlsx") == 0 ||
        //       fileExtension.toLowerCase().compareTo(".xls") == 0 ||
        //       fileExtension.toLowerCase().compareTo(".png") == 0 ||
        //       fileExtension.toLowerCase().compareTo(".jpg") == 0) {
        //     addFileToArray(file);
        //   }
        // }
      }
    } catch (e) {
      print('Exeption on checing location permission ==> $e');
    }
  }

  void _openBottomSheet() {
    showModalBottomSheet<void>(
      context: context,
      builder: (context) {
        return new Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Padding(
                padding: EdgeInsets.all(10),
                child: Text(
                  'Select Attachment',
                  style: AppTextStyle.getDynamicFontStyleWithWeight(
                      false, Colors.black, 14, FontType.Light, FontWeight.w500),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      _getImage(true);
                    },
                    child: mediaPickerWidget(
                        ImagePath.ICON_CAMERA, Colors.black, 'Camera'),
                  ),
                  GestureDetector(
                    onTap: () {
                      _getImage(false);
                    },
                    child: mediaPickerWidget(
                        ImagePath.ICON_GALLERY, Colors.orange, 'Gallery'),
                  ),
                  GestureDetector(
                    onTap: () {
                      openOtherFile('all');
                      Navigator.of(context).pop();
                    },
                    child: mediaPickerWidget(
                        ImagePath.ALL_DOCUMENTS, Colors.blue, 'Documents'),
                  ),
                  // GestureDetector(
                  //   onTap: () {
                  //     openOtherFile('docx');
                  //     Navigator.of(context).pop();
                  //   },
                  //   child: mediaPickerWidget(
                  //       ImagePath.ICON_WORD, Colors.blue, 'Word'),
                  // ),
                  // GestureDetector(
                  //   onTap: () {
                  //     openOtherFile(
                  //       'pdf',
                  //     );
                  //     Navigator.of(context).pop();
                  //   },
                  //   child: mediaPickerWidget(
                  //       ImagePath.ICON_PDF, Colors.red, 'PDF'),
                  // ),
                  // GestureDetector(
                  //   onTap: () {
                  //     openOtherFile(
                  //       'xlsx',
                  //     );
                  //     Navigator.of(context).pop();
                  //   },
                  //   child: mediaPickerWidget(
                  //       ImagePath.ICON_EXCEL, Colors.red, 'Excel'),
                  // ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  mediaPickerWidget(String path, Color color, String name) {
    return Column(
      children: <Widget>[
        name == "Documents"
            ? Image.asset(
                path,
                height: 40,
                color: color,
              )
            : Image.asset(
                path,
                height: 40,
              ),
        SizedBox(
          height: 5,
        ),
        Text(name,
            style: AppTextStyle.getDynamicFontStyleWithWeight(
                false, Colors.black, 12, FontType.Regular, FontWeight.w500)),
        SizedBox(
          height: 5,
        ),
      ],
    );
  }

  void _getImage(bool isCamera) async {
    Navigator.pop(context);

    File image;
    if (isCamera) {
      image = await ImagePicker.pickImage(source: ImageSource.camera);
      if (image != null) {
        String filePathName = image.path;
        String fileExtension = path.extension(filePathName);
        if (fileExtension.compareTo(".png") == 0 ||
            fileExtension.compareTo(".jpeg") == 0 ||
            fileExtension.compareTo(".jpg") == 0) {
          addFileToArray(image);
        }
      }
    } else {
      image = await ImagePicker.pickImage(source: ImageSource.gallery);
      if (image != null) {
        String filePathName = image.path;
        String fileExtension = path.extension(filePathName);
        if (fileExtension.compareTo(".png") == 0 ||
            fileExtension.compareTo(".jpeg") == 0 ||
            fileExtension.compareTo(".jpg") == 0) {
          addFileToArray(image);
        }
      }
    }
  }

  void _showDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Permission"),
          content: new Text("Please Allow Storage Permission"),
          actions: <Widget>[
            new FlatButton(
              child: new Text("CLOSE"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("ENABLE"),
              onPressed: () async {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  showImageOrFilePath() {
    setState(() {});
    if (selectedFiles != null && selectedFiles.length > 0) {
      return Container(
        margin: EdgeInsets.only(top: 5),
        child: GridView.builder(
          physics: NeverScrollableScrollPhysics(),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 4,
            childAspectRatio: MediaQuery.of(context).size.width /
                (MediaQuery.of(context).size.height / 2 + 300),
          ),
          shrinkWrap: true,
          itemCount: selectedFiles.length,
          itemBuilder: (BuildContext cont, int ind) {
            return uploadFileImage(ind, selectedFiles[ind]);
          },
        ),
      );
    } else {
      return Container();
    }
  }

  Widget uploadFileImage(int ind, UploadFileModel showFile) {
    if (showFile != null) {
      String fileExtension = path.extension(showFile.localFileUrl.path);
      print('File extension at show time ==> $fileExtension');
      String fileName = path.basename(showFile.localFileUrl.path);
      String mimeType = mime(fileName);

      return Container(
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  width: 91,
                  height: 85,
                  child: Card(
                    color: Colors.white,
                    elevation: 0,
                    child: fileExtension.toLowerCase() == '.jpg' ||
                            fileExtension.toLowerCase() == '.jpeg' ||
                            fileExtension.toLowerCase() == '.png'
                        ? FutureBuilder<File>(
                            builder: (BuildContext context,
                                AsyncSnapshot<File> snapshot) {
                              return Container(
                                child: Image.file(
                                  showFile.localFileUrl,
                                  fit: BoxFit.fitWidth,
                                ),
                              );
                            },
                          )
                        : Container(
                            width: 90,
                            child: Center(
                              child: Image.asset(
                                fileExtension.toLowerCase() == '.xls' ||
                                        fileExtension.toLowerCase() == '.xlsx'
                                    ? ImagePath.ICON_EXCEL
                                    : fileExtension.toLowerCase() == '.pdf'
                                        ? ImagePath.ICON_PDF
                                        : fileExtension.toLowerCase() == '.docx' ||
                                                fileExtension.toLowerCase() ==
                                                    '.word' ||
                                                fileExtension.toLowerCase() ==
                                                    '.doc'
                                            ? ImagePath.ICON_WORD
                                            : fileExtension.toLowerCase() == '.ppt' ||
                                                    fileExtension.toLowerCase() ==
                                                        '.pptx'
                                                ? ImagePath.PPT_ICON
                                                : fileExtension.toLowerCase() == '.txt'
                                                    ? ImagePath.ICON_TXT
                                                    : fileExtension.toLowerCase() ==
                                                            '.zip'
                                                        ? ImagePath.ICON_ZIP
                                                        : fileExtension.toLowerCase() ==
                                                                '.rar'
                                                            ? ImagePath.ICON_RAR
                                                            : mimeType.indexOf("video") != -1
                                                                ? ImagePath
                                                                    .ICON_VIDEO
                                                                : mimeType.indexOf("audio") != -1
                                                                    ? ImagePath
                                                                        .ICON_AUDIO
                                                                    : ImagePath
                                                                        .ICON_FILE,
                                width: 50,
                                height: 50,
                              ),
                            ),
                          ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 5, right: 4),
//                  height: 40,
                  width: 90,
                  child: Center(
                      child: Text(
                    fileName,
                    maxLines: 2,
                    overflow: TextOverflow.clip,
                    style: TextStyle(fontSize: 10),
                  )),
                )
              ],
            ),
            Container(
              alignment: Alignment.topRight,
              child: IconButton(
                alignment: Alignment.topRight,
                icon: Icon(
                  Icons.remove_circle,
                  size: 15,
                  color: Palette.redColor,
                ),
                onPressed: () {
                  setState(() {
                    selectedFiles.removeAt(ind);
                    setState(() {});
                  });
                },
              ),
            ),
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  addFileToArray(File file) {
    selectedFiles.add(UploadFileModel(file));
    widget.function(selectedFiles);
    setState(() {});
  }
}
