import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:trainmoo/model/FileModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:trainmoo/ui/widget/FileView.dart';

class ItemDetailView extends StatelessWidget with BaseCommonWidget {
  final String title, time, details;
  final List<FileModel> files;
  final bool isHtmlData;
  final Map<String, String> header;

  ItemDetailView(
      {Key key,
      this.title,
      this.time,
      this.details,
      this.files,
      this.isHtmlData,
      this.header})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        elevation: 1,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 3),

                child: Text(
                  title,
                  style: AppTextStyle.titleStyle(),
                  maxLines: 1,
                ),
              ),
              SizedBox(
                height: 4,
              ),
              Padding(
                  padding: const EdgeInsets.only(left: 3),

                child: Text(time, style: AppTextStyle.subTitleStyle()),
              ),
              details != null
                  ? isHtmlData
                      ? Html(
                          data: details,
                          showImages: true,
                          linkStyle: TextStyle(
                              color: Colors.blue,
                              decoration: TextDecoration.underline),
                          onLinkTap: (String link) {
                            Utility.launchURL(link);
                          },
                          defaultTextStyle: AppTextStyle.descriptionStyle())
                      : Container(
                          padding: EdgeInsets.only(top: 6),
                          child: Text(details,
                              style: AppTextStyle.descriptionStyle()))
                  : Container(),
              Padding(
                padding: const EdgeInsets.only(left: 3),
                child: new FileView(
                  files: files,
                  headers: header,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
