import 'package:flutter/material.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';

class DropDownList extends StatelessWidget {
  final List<ProgramClassesModel> classList;
  final Function onTap;
  final ProgramClassesModel selectedClass;

  DropDownList({this.classList, this.onTap, this.selectedClass});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: new BorderRadius.all(Radius.circular(3)),
      ),
      padding: EdgeInsets.only(left: 18, right: 10),
      child: DropdownButton<ProgramClassesModel>(
        underline: Container(
          decoration: BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.transparent))),
        ),
        isExpanded: true,
        icon: Icon(
          Icons.arrow_drop_down,
          color: Palette.accentColor,
          size: 25,
        ),
        hint: new Text(AppLocalizations.of(context).selectClass,
            style: AppTextStyle.hintStyle()),
        value: selectedClass,
        onChanged: onTap,
        items: classList.map((ProgramClassesModel role) {
          return new DropdownMenuItem<ProgramClassesModel>(
            value: role,
            child: new Text(
              role.name,
              style: TextStyle(color: Colors.black),
            ),
          );
        }).toList(),
      ),
    );
  }
}
