import 'package:flutter/material.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';

class RegularButton extends StatelessWidget {
  final String buttonLabel;
  final Function onTap;

  RegularButton({
    this.buttonLabel,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 0, right: 0, top: 10),
      child: InkWell(
        onTap: onTap,
        child: Container(
          height: 50,
          decoration: new BoxDecoration(
            color: Palette.accentColor,
            borderRadius: new BorderRadius.all(Radius.circular(3)),
          ),
          child: Center(
            child: Text(
              buttonLabel,
              style: AppTextStyle.getDynamicFontStyle(
                  Colors.white, 14, FontType.SemiBol),
            ),
          ),
        ),
      ),
    );
  }
}
