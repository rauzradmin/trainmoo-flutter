import 'package:flutter/material.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';

class LargeTextFormFieldWithPadding extends StatelessWidget {
  final Color backgroundColor;
  final String hintText;
  final Color textColor;
  final Color hintColor;
  final TextStyle hinTextStyle;
  final TextStyle textStyle;
  final Function onTextChanged;
  final TextInputType inputType;
  final Function onTextValidate;
  final Function onTextSaved;
  final TextEditingController textEditingController;
  final FocusNode focusNode;
  final bool obscureText;
  final TextAlign textAlign;
  final int maxLines;
  final bool enabled;

  LargeTextFormFieldWithPadding(
      {this.obscureText = false,
      this.focusNode,
      this.textEditingController,
      this.backgroundColor = Colors.white,
      this.hintText,
      this.hintColor = Palette.hintTextColor,
      this.textColor = Palette.primaryTextColor,
      this.hinTextStyle = const TextStyle(
          color: Palette.hintTextColor,
          fontSize: 14,
          fontFamily: AppTextStyle.fontRegular),
      this.textStyle = const TextStyle(
          color: Palette.primaryTextColor,
          fontSize: 14,
          fontFamily: AppTextStyle.fontRegular),
      this.onTextChanged,
      this.onTextSaved,
      this.onTextValidate,
      this.inputType = TextInputType.text,
      this.textAlign = TextAlign.left,
      this.maxLines = 5,
      this.enabled});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 162,
      margin: EdgeInsets.only(left: 0, right: 0, top: 10),
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: new BorderRadius.all(Radius.circular(3)),
      ),
      child: TextFormField(
        enabled: enabled,
        obscureText: obscureText,
        focusNode: focusNode,
        style: textStyle,
        keyboardType: inputType,
        onChanged: onTextChanged,
        validator: onTextValidate,
        textAlign: textAlign,
        maxLines: maxLines,
        onSaved: onTextSaved,
        cursorColor: Colors.grey,
        controller: textEditingController,
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
            border: InputBorder.none,
            hintText: hintText,
            contentPadding: EdgeInsets.only(left: 13, top: 10),
            hintStyle: hinTextStyle,
            errorStyle: const TextStyle(
                color: Colors.red,
                fontSize: 14,
                fontFamily: AppTextStyle.fontRegular)),
      ),
    );
  }
}
