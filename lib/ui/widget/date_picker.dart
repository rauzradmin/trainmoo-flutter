import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/util/image_path.dart';

class DatePicker extends StatelessWidget {
  final Function onChanged;
  final dateFormat = DateFormat("yyy-MM-dd hh:mm a");
  final String hint;
  DatePicker({this.onChanged, this.hint});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: DateTimeField(
        decoration: InputDecoration(
            fillColor: Colors.white,
            filled: true,
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent, width: 2.0),
                borderRadius: BorderRadius.circular(5)),
            suffixIcon: IconButton(
                icon: Image.asset(
                  ImagePath.ICON_CALENDAR,
                  width: 18,
                  height: 20,
                ),
                onPressed: null),
            contentPadding: EdgeInsets.only(top: 10, left: 18),
            hintText: hint,
            hintStyle: AppTextStyle.hintStyle(),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(9),
              borderSide: BorderSide(color: Colors.transparent, width: 2.0),
            )),
        format: dateFormat,
        readOnly: true,
        style: AppTextStyle.getDynamicFontStyle(
            Colors.black, 14, FontType.Regular),
        onShowPicker: (context, currentValue) async {
          final date = await showDatePicker(
              context: context,
              firstDate: DateTime(1900),
              initialDate: currentValue ?? DateTime.now(),
              lastDate: DateTime(2100));
          if (date != null) {
            final time = await showTimePicker(
              context: context,
              initialTime:
                  TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
            );
            return DateTimeField.combine(date, time);
          } else {
            return currentValue;
          }
        },
        onChanged: onChanged,
      ),
    );
  }
}
