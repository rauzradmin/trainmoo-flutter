import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:trainmoo/model/FileModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/widget/DownloadScreen.dart';
import 'package:trainmoo/util/image_path.dart';
import 'package:mime_type/mime_type.dart';

class ShowAttachments extends StatefulWidget {
  final List<FileModel> imgList;
  final Map<String, dynamic> headers;
  final int index;

  ShowAttachments({this.imgList, this.index, this.headers});

  @override
  _ImageMenuDetailState createState() => _ImageMenuDetailState();
}

class _ImageMenuDetailState extends State<ShowAttachments>
    with BaseCommonWidget {
  List<String> permissionValues;
  var progress = "";
  bool downloading = false;
  String url;
  int index = 0;

  @override
  void initState() {
    index = widget.index;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: _getImage(context),
      ),
    );
  }

  _showcontent(context, path, dirToSave, exist) {
    showDialog(
      context: context, barrierDismissible: false, // user must tap button!

      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('Attachments'),
          content: new SingleChildScrollView(
            child: new ListBody(
              children: [
                new Text(exist
                    ? '$path file is already downloaded.'
                    : '$path file is Downloaded.'),
              ],
            ),
          ),
          actions: [
            new FlatButton(
              child: new Text('Ok'),
              onPressed: () {
                OpenFile.open("${dirToSave.path}/${path}")
                    .then((value) => print("value :$value"));
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  _getImage(context) {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 25, right: 14, top: 8),
          child: Row(
            children: <Widget>[
              Text(
                'Attachments',
                overflow: TextOverflow.clip,
                style: AppTextStyle.getDynamicFontStyle(
                    Colors.black, 24, FontType.SemiBol),
              ),
              Flexible(
                child: Container(),
              ),
              IconButton(
                  icon: Image.asset(
                    ImagePath.IC_DOWNLOAD,
                    width: 13.93,
                    height: 15,
                  ),
                  onPressed: () {
                    _download(widget.imgList[index].src, context);
//                    downLoadImage(
//                        url != null ? url : widget.imgList[index].src);
                  }),
              IconButton(
                  icon: Image.asset(
                    ImagePath.CANCEL,
                    width: 13.93,
                    height: 15,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  })
            ],
          ),
        ),
        Expanded(
          child: Center(
            child: widget.imgList.length > 1
                ? Swiper(
                    itemBuilder: (BuildContext context, int index) {
                      return InkWell(
                        onTap: () {
                          url = widget.imgList[index].thumbnail;
                        },
                        child: widget.imgList[index].thumbnail != null &&
                                widget.imgList[index].thumbnail.isNotEmpty
                            ? loadNetworkImageInFullView(
                                widget.imgList[index].thumbnail, widget.headers)
                            : loadImageBasedOnFileType(
                                widget.imgList[index], 100),
                      );
                    },
                    onIndexChanged: (int index) {
                      this.index = index;
                      url = widget.imgList[index].src;
                      _getSwipControll();
                    },
                    autoplay: false,
                    index: index,
                    itemCount: widget.imgList.length,
                    pagination: new SwiperPagination(),
                    control:
                        widget.imgList.length > 1 ? _getSwipControll() : null)
                : widget.imgList[index].thumbnail != null &&
                        widget.imgList[index].thumbnail.isNotEmpty
                    ? loadNetworkImageInFullView(
                        widget.imgList[index].thumbnail, widget.headers)
                    : loadImageBasedOnFileType(widget.imgList[index], 100),
          ),
        ),
      ],
    );
  }

  _getSwipControll() {
    if (widget.imgList.length == 1) {
      return SwiperControl(
        iconNext: null,
        color: Palette.accentColor,
        iconPrevious: null,
      );
    } else {
      return SwiperControl(
        iconNext: Icons.arrow_forward_ios,
        color: Palette.accentColor,
        iconPrevious: Icons.arrow_back_ios,
      );
    }
  }

  Future<void> _download(String src, context) async {
    print('_download server-->$src');

    Dio dio = Dio();
    dio.options.headers = await API.getAuthHeader();
    var dirToSave = Platform.isIOS
        ? await getApplicationDocumentsDirectory()
        : await getExternalStorageDirectory();

    print('_download local-->${dirToSave.path}');
    List<String> imageList = [];

    if (src != null && src.contains("/")) {
      imageList = src.split("/");
    }
    print('path-->${dirToSave.path}/${imageList[imageList.length - 1]}');
    File downloadLocation =
        File("${dirToSave.path}/${imageList[imageList.length - 1]}");

    if (downloadLocation.existsSync()) {
      downloading = true;
      _showcontent(context, imageList[imageList.length - 1], dirToSave, true);
      //OpenFile.open("${dirToSave.path}/${imageList[imageList.length - 1]}");
      print('exist');
    } else {
      print('not exist');
      try {
        bool isGranted = false;
        if (await Permission.storage.request().isGranted) {
          isGranted = true;
        } else {
          Map<Permission, PermissionStatus> statuses = await [
            Permission.storage,
          ].request();
          if (statuses.values.elementAt(0) == PermissionStatus.granted) {
            isGranted = true;
          }
        }
        if (isGranted) {
          String path = await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => DownloadMedia(
                        mediaFileUrl: src,
                        downloadLocation: downloadLocation,
                      )));
          if (path != null) {
            _showcontent(
                context, imageList[imageList.length - 1], dirToSave, false);
            // OpenFile.open(
            //     "${dirToSave.path}/${imageList[imageList.length - 1]}");
          }
        } else {
          _showDialog();
        }
      } catch (e) {
        print('Exeption on checing location permission ==> $e');
      }
    }
  }

  void _showDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Permission"),
          content: new Text("Please Allow GrubDealz Location Permission"),
          actions: <Widget>[
            new FlatButton(
              child: new Text("CLOSE"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("Enable"),
              onPressed: () async {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  IconData getIconData(String type) {
    print('attachment type ==> $type');
    switch (type) {
      case 'pdf':
        return Icons.picture_as_pdf;
      case 'doc':
        return Icons.insert_drive_file;
      case 'word':
        return Icons.insert_drive_file;
      case 'excel':
        return Icons.insert_drive_file;
      case 'image':
        return Icons.image;
      default:
        return Icons.insert_drive_file;
    }
  }

  loadImageBasedOnFileType(FileModel file, double height) {
    print('file type ==> ${file.src}');
    String mimeType = mime(file.name);
    String fileUrl = 'assets/icons/file.png';
    if (file.src.contains('.xlsx') || file.src.contains('.xls')) {
      fileUrl = 'assets/icons/excel.png';
    }
    if (file.src.contains('.pdf')) {
      fileUrl = 'assets/icons/pdf.png';
    }
    if (file.src.contains('.word') || file.src.contains('.doc')) {
      fileUrl = 'assets/icons/word.png';
    }
    if (file.src.contains('.ppt') || file.src.contains('.pptx')) {
      fileUrl = 'assets/icons/ppt.png';
    }
    if (file.src.contains('.txt')) {
      fileUrl = 'assets/icons/text.png';
    }
    if (file.src.contains('.zip')) {
      fileUrl = 'assets/icons/zip.png';
    }
    if (file.src.contains('.rar')) {
      fileUrl = 'assets/icons/rar.png';
    }
    if (mimeType.indexOf("video") != -1) {
      fileUrl = 'assets/icons/video.png';
    }
    if (mimeType.indexOf("audio") != -1) {
      fileUrl = 'assets/icons/audio.png';
    }
    return Container(
      child: Image.asset(
        fileUrl,
        width: height,
        height: height,
        //fit: BoxFit.fill,
      ),
    );
  }
}
