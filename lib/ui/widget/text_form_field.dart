import 'package:flutter/material.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';

class RegularTextFormField extends StatelessWidget {
  final Color backgroundColor;
  final String hintText;
  final Color textColor;
  final Color hintColor;
  final TextStyle hinTextStyle;
  final TextStyle textStyle;
  final Function onTextChanged;
  final TextInputType inputType;
  final Function onTextValidate;
  final Function onTextSaved;
  final TextEditingController textEditingController;
  final FocusNode focusNode;
  final bool obscureText;
  final TextAlign textAlign;
  TextInputAction textInputAction;
  Function onTextSubmitted;

  RegularTextFormField(
      {this.obscureText = false,
      this.focusNode,
      this.textEditingController,
      this.backgroundColor = Colors.white,
      this.hintText,
      this.hintColor = Palette.hintTextColor,
      this.textInputAction,
      this.textColor = Palette.primaryTextColor,
      this.hinTextStyle = const TextStyle(
          color: Palette.hintTextColor,
          fontSize: 14,
          fontFamily: AppTextStyle.fontRegular),
      this.textStyle = const TextStyle(
          color: Palette.primaryTextColor,
          fontSize: 14,
          fontFamily: AppTextStyle.fontRegular),
      this.onTextChanged,
      this.onTextSaved,
      this.onTextSubmitted,
      this.onTextValidate,
      this.inputType = TextInputType.text,
      this.textAlign = TextAlign.left});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: new BorderRadius.all(Radius.circular(3)),
      ),
      child: Center(
        child: TextFormField(
          obscureText: obscureText,
          focusNode: focusNode,
          maxLines: null,
          style: textStyle,
          keyboardType: inputType,
          onChanged: onTextChanged,
          validator: onTextValidate,
          textAlign: textAlign,
          onFieldSubmitted: onTextSubmitted,
          onSaved: onTextSaved,
          textCapitalization: TextCapitalization.sentences,
          textInputAction: textInputAction,
          cursorColor: Colors.grey,
          controller: textEditingController,
          decoration: InputDecoration(
              border: InputBorder.none,
              hintText: hintText,
              hintStyle: hinTextStyle,
              contentPadding: EdgeInsets.only(left: 13, top: 0),
              errorStyle: const TextStyle(
                  color: Colors.red,
                  fontSize: 14,
                  fontFamily: AppTextStyle.fontRegular)),
        ),
      ),
    );
  }
}
