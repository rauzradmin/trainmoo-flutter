import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:trainmoo/model/FileModel.dart';
import 'package:trainmoo/model/LinkPreview.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/util.dart';

import '../router.dart';
import 'FileView.dart';

class DetailView extends StatelessWidget with BaseCommonWidget {
  final String title, time, details, type, screenType, className;
  final List<FileModel> files;
  final LinkPreview linkPreview;
  final Map<String, String> header;
  final Color color;

  DetailView(
      {Key key,
      this.title,
      this.time,
      this.details,
      this.files,
      this.header,
      this.type,
      this.screenType,
      this.color,
      this.className,
      this.linkPreview})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          child: Text(
              screenType != null && screenType.compareTo("Post") == 0
                  ? className
                  : Utility.removeAllHtmlTags(title),
              maxLines: 1,
              overflow: TextOverflow.clip,
              style: AppTextStyle.commonTitleStyle()),
        ),
        screenType != null && screenType.compareTo("Post") == 0
            ? Row(
                children: <Widget>[
                  Text(time,
                      style: AppTextStyle.getDynamicFontStyle(
                          Palette.listColor, 13, FontType.RobotoRegular)),
                  SizedBox(
                    width: 5,
                  ),
                  Text(type,
                      style: AppTextStyle.getDynamicFontStyle(
                          color, 13, FontType.RobotoRegular)),
                ],
              )
            : Container(
                margin: EdgeInsets.only(top: 4),
                child: Text(time, style: AppTextStyle.commonSubTitleStyle()),
              ),
        Container(
          margin: EdgeInsets.only(top: 3),
          child: title != null &&
                  screenType != null &&
                  screenType.compareTo("Post") == 0
              ? Text(Utility.removeAllHtmlTags(title),
                  style: AppTextStyle.commentTextStyle())
              : Container(),
        ),
        Container(
            margin: EdgeInsets.only(top: 5),
            child: Html(
                data: details.toString(),
                linkStyle: new TextStyle(color: Colors.blue),
                defaultTextStyle: AppTextStyle.detailDescriptionStyle())
//                    child: Text(
//                        'Praesent dapibus, neque id cursus faucibus, tortor neque egestas auguae, eu vulputate magna eros eu erat.',
//                        style: AppTextStyle.detailDescriptionStyle()),
            ),

        linkPreview == null
            ? Container()
            : Container(
                padding: EdgeInsets.only(top: 5, bottom: 5),
                child: Card(
                  elevation: 0,
                  child: ListTile(
                    onTap: () async {
                      await Utility.launchURL(linkPreview.url);
                    },
                    contentPadding: EdgeInsets.all(5),
                    leading: loadFileImage(linkPreview.image, 50, header),
                    title: Text(linkPreview.title),
                    subtitle: Text(linkPreview.description),
                  ),
                ),
              ),
        files == null || files.length <= 0
            ? Container()
            : new FileView(
                files: files,
                headers: header,
                showName: true,
              )
//        Container(
//                height: 150,
//                child: ListView.builder(
//                  physics: ClampingScrollPhysics(),
//                  shrinkWrap: true,
//                  scrollDirection: Axis.horizontal,
//                  itemCount: files.length,
//                  itemBuilder: (BuildContext cont, int ind) {
//                    return showFile(ind, context);
//                  },
//                ),
//              ),
      ],
    );
  }

  Widget showFile(int index, BuildContext context) {
    return InkWell(
      child: Container(
        width: 100,
        child: Column(
          children: <Widget>[
            loadFileImage(files[index].thumbnail, 94, header),
            Text(
              files[index].name,
              textAlign: TextAlign.center,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: AppTextStyle.getDynamicFontStyle(
                  Palette.listColor, 13, FontType.RobotoMedium),
            )
          ],
        ),
      ),
      onTap: () async {
        Navigator.pushNamed(context, Screen.ShowAttachments.toString(),
            arguments: {
              'index': index,
              'header': await API.getAuthHeader(),
              'files': files
            });
      },
    );
  }
}
