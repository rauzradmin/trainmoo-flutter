import 'dart:io';

import 'package:flutter/material.dart';
import 'package:trainmoo/ui/network/API.dart';

class DownloadMedia extends StatefulWidget {
  final String mediaFileUrl;
  final File downloadLocation;

  DownloadMedia({this.mediaFileUrl, this.downloadLocation});

  @override
  _DownloadMediaState createState() => _DownloadMediaState();
}

class _DownloadMediaState extends State<DownloadMedia> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  @override
  void initState() {
    downloadMediaFile(widget.mediaFileUrl);
    super.initState();
  }

  void downloadMediaFile(String mediaFileUrl) async {
    print('mediaFileUrl $mediaFileUrl');
    if (mediaFileUrl != null && mediaFileUrl.trim().isNotEmpty) {
      if (!mediaFileUrl.contains('http') &&
          !mediaFileUrl.contains(API.baseURL)) {
        mediaFileUrl = API.baseURL + mediaFileUrl.replaceAll('/api/', '');
      }
    }
    print('mediaFileUrl $mediaFileUrl');

    API.init();
    await API.dio
        .download(mediaFileUrl, widget.downloadLocation.path)
        .whenComplete(() {
      print('onComplete download');
    }).catchError((onError) {
      print('on Error called ==>  $onError');
    }).then((value) {
      print('then download');

      Navigator.pop(context, 'done');
    });
  }
}
