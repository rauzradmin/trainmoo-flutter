import 'package:flutter/material.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';

class SocialSignInButton extends StatelessWidget {
  final String buttonLabel;
  final String image;
  final double leftMargin;
  final double height;
  final double width;
  final Function onTap;

  SocialSignInButton(
      {this.buttonLabel,
      this.onTap,
      this.image,
      this.leftMargin,
      this.width,
      this.height});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 0, right: 0, top: 10),
      child: InkWell(
        onTap: onTap,
        child: Container(
          height: 50,
          decoration: new BoxDecoration(
            color: Palette.white,
            borderRadius: new BorderRadius.all(Radius.circular(3)),
          ),
          child: Row(
            children: <Widget>[
              Container(
                  padding: EdgeInsets.only(left: leftMargin),
                  child: Image.asset(
                    image,
                    height: height,
                    width: width,
                  )),
              Expanded(
                child: Center(
                  child: Text(
                    buttonLabel,
                    textAlign: TextAlign.center,
                    style: AppTextStyle.getDynamicFontStyle(
                        Palette.primaryTextColor, 14, FontType.SemiBol),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
