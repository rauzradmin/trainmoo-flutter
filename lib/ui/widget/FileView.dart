import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/model/FileModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/util/image_path.dart';
import 'package:mime_type/mime_type.dart';

import '../router.dart';
import '../theme/app_text_styles.dart';
import '../theme/palette.dart';

class FileView extends StatefulWidget {
  final List<FileModel> files;
  final Map<String, dynamic> headers;
  final bool showName;
  FileView({this.files, this.headers, this.showName});
  @override
  _FileViewState createState() => _FileViewState();
}

class _FileViewState extends State<FileView> with BaseCommonWidget {
  double height = 0;
  double c_width = 0;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    c_width = MediaQuery.of(context).size.width * 0.6;
    return widget.files != null && widget.files.length > 0
        ? widget.showName == null || !widget.showName
            ? Container(height: 50, child: documentWidget())
            : Container(height: 150, child: documentWidget())
        : Container();
  }

  Widget documentWidget() {
    return ListView.builder(
      physics: ClampingScrollPhysics(),
      shrinkWrap: true,
      scrollDirection: Axis.horizontal,
      itemCount: widget.files.length,
      itemBuilder: (BuildContext cont, int ind) {
        return widget.showName == null || !widget.showName
            ? showFile(widget.files[ind], ind)
            : showFileWithName(widget.files[ind], ind);
      },
    );
  }

  Widget showFile(FileModel file, int index) {
    return InkWell(
      onTap: () async {
        Navigator.pushNamed(context, Screen.ShowAttachments.toString(),
            arguments: {
              'index': index,
              'header': await API.getAuthHeader(),
              'files': widget.files,
              //"fileName": file.name
            });
      },
      child: Container(
        padding: EdgeInsets.only(top: 10),
        child: Row(children: <Widget>[
//          file.thumbnail != null
//              ? loadNetworkImage(file.thumbnail, 32, widget.headers)
//              :
          loadImageBasedOnFileType(file, 32, 32, false),
          // Container(
          //   padding: EdgeInsets.only(left: 10),
          //   width: c_width,
          //   child: Text(
          //     file.name,
          //     textAlign: TextAlign.left,
          //     maxLines: 2,
          //     overflow: TextOverflow.ellipsis,
          //     style: AppTextStyle.getDynamicFontStyle(
          //         Palette.listColor, 13, FontType.RobotoMedium),
          //   ),
          // )
        ]),
      ),
    );
  }

  Widget showFileWithName(FileModel file, int index) {
    return InkWell(
      onTap: () async {
        Navigator.pushNamed(context, Screen.ShowAttachments.toString(),
            arguments: {
              'index': index,
              'header': await API.getAuthHeader(),
              'files': widget.files
            });
      },
      child: Container(
        width: 90,
        height: height,
        child: Column(
          children: <Widget>[
//            file.thumbnail != null
//                ? loadNetworkImage(file.thumbnail, 94, widget.headers)
//                :
            loadImageBasedOnFileType(file, 94, 94, true),
            Text(
              file.name,
              textAlign: TextAlign.center,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: AppTextStyle.getDynamicFontStyle(
                  Palette.listColor, 13, FontType.RobotoMedium),
            )
          ],
        ),
      ),
    );
  }

  Widget loadImageBasedOnFileType(
      FileModel file, double height, width, showThumbnail) {
    print('thumbnail url ==> ${file.thumbnail}');
    // if (file.thumbnail != null &&
    //     file.thumbnail.trim().isNotEmpty &&
    //     showThumbnail) {
    //   if (!file.thumbnail.contains('http') &&
    //       !file.thumbnail.contains(API.baseURL)) {
    //     file.thumbnail = API.baseURL + file.thumbnail.replaceAll('/api/', '');
    //     return loadFileImage(file.thumbnail, height, widget.headers);
    //   }
    //   return loadFileImage(file.thumbnail, height, widget.headers);
    // }
    String fileUrl = ImagePath.ICON_FILE;
    String mimeType = mime(file.name);
    if (file.src.contains('.xlsx') || file.src.contains('.xls')) {
      fileUrl = ImagePath.ICON_EXCEL;
    }
    if (file.src.contains('.pdf')) {
      fileUrl = ImagePath.ICON_PDF;
    }
    if (file.src.contains('.ppt') || file.src.contains('.pptx')) {
      fileUrl = ImagePath.PPT_ICON;
    }
    if (file.src.contains('.txt')) {
      fileUrl = ImagePath.ICON_TXT;
    }
    if (file.src.contains('.word') ||
        file.src.contains('.doc') ||
        file.src.contains('docx')) {
      fileUrl = ImagePath.ICON_WORD;
    }
    if (file.src.contains('.png') ||
        file.src.contains('.jpeg') ||
        file.src.contains('.jpg')) {
      fileUrl = ImagePath.ICON_GALLERY;
    }
    if (file.src.contains('.zip')) {
      fileUrl = ImagePath.ICON_ZIP;
    }
    if (file.src.contains('.rar')) {
      fileUrl = ImagePath.ICON_RAR;
    }
    if (mimeType.indexOf("video") != -1) {
      fileUrl = ImagePath.ICON_VIDEO;
    }
    if (mimeType.indexOf("audio") != -1) {
      fileUrl = ImagePath.ICON_AUDIO;
    }
    return Container(
      margin:
          EdgeInsets.all(widget.showName == null || !widget.showName ? 5 : 10),
      child: Image.asset(
        fileUrl,
        height: height,
        //fit: BoxFit.cover,
        // width: width,
      ),
    );
  }
}
