class Validation
{

  static String validatePassword(String value) {
     if (value.isEmpty) {
    return "Please Enter Password";
    }
     else if (!(value.length > 4) ) {
      return "Incorrect password";
    }

    return null;
  }



  static String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
     if (value.isEmpty) {
    return "Please Enter EmailId";
    }
    else if (!regex.hasMatch(value))
      return 'Incorrect Email';

    else
      return null;
  }

  static String validateNameField(String value) {
    if (value.isEmpty) {
      return 'Please Enter Name';
    }
    return null;
  }

  static String validatePhoneField(String value) {
    if (value.isEmpty) {
      return 'Please Enter Phone Number';
    }
    else if(value.length>10)
      {
        return 'Incorrect PhoneNumber';

      }
    return null;
  }

}