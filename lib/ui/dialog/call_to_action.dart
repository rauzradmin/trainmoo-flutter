import 'package:flutter/material.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';

class CallToActionDialog extends StatefulWidget {
  CallToActionDialog({
    this.title,
    this.message,
  });

  final String title, message;

  @override
  CallToActionDialogState createState() => CallToActionDialogState();
}

class CallToActionDialogState extends State<CallToActionDialog>
    with BaseCommonWidget {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Palette.transparent,
      body: Container(
        alignment: AlignmentDirectional.bottomCenter,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
              width: 348,
              height: 145,
              padding: EdgeInsets.only(left: 25, right: 25),
              decoration: rectangleDecoration(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    widget.title,
                    softWrap: true,
                    textAlign: TextAlign.center,
                  ),
                  // Text(widget.message),
                ],
              ),
            ),
            UIHelper.verticalSpaceSmall,
            Container(
              width: 348,
              height: 51,
              decoration: rectangleDecoration(),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FlatButton(
                    padding: EdgeInsets.only(left: 25),
                    onPressed: () {
                      Navigator.of(context).pop(false);
                    },
                    child: Text('Cancel',
                        style: AppTextStyle.getDynamicFontStyle(
                            Palette.cancelButtonColor, 16, FontType.Regular)),
                  ),
                  FlatButton(
                    padding: EdgeInsets.only(right: 25),
                    onPressed: () {
                      Navigator.of(context).pop(true);
                    },
                    child: Text('Remove',
                        style: AppTextStyle.getDynamicFontStyle(
                            Palette.accentColor, 16, FontType.Regular)),
                  ),
                ],
              ),
            ),
            UIHelper.verticalSpaceSmall,
          ],
        ),
      ),
    );
  }
}
