import 'package:flutter/material.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';

class MyDialog extends StatefulWidget {
  MyDialog({
    this.cities,
    this.selectedCities,
    this.onSelectedCitiesListChanged,
  });

  final List<String> cities;
  final List<String> selectedCities;
  final ValueChanged<List<String>> onSelectedCitiesListChanged;

  @override
  MyDialogState createState() => MyDialogState();
}

class MyDialogState extends State<MyDialog> with BaseCommonWidget {
  List<String> _tempSelectedCities = [];

  @override
  void initState() {
    _tempSelectedCities = widget.selectedCities;
    super.initState();
  }

  


  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Column(
        children: <Widget>[
          Container(
            color: Palette.titleBg,
            padding: EdgeInsets.all(UIHelper.screenPadding),
            child: Text(
              'Select all the interests you feel to get maximum audience interaction on your opportunity.',
              style: AppTextStyle.getDynamicFontStyle(
                  Palette.primaryTextColor, 14, FontType.Regular),
              textAlign: TextAlign.left,
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(
                left: UIHelper.screenPadding,
                right: UIHelper.screenPadding,
              ),
              child: Scrollbar(
                child: ListView.builder(
                    itemCount: widget.cities.length,
                    itemBuilder: (BuildContext context, int index) {
                      final interestName = widget.cities[index];
                      return Container(
                        padding: EdgeInsets.only(top: 10),
                        child: InkWell(
                          child: Row(
                            children: <Widget>[
                              customCheckBox(
                                  _tempSelectedCities.contains(interestName)),
                              UIHelper.horizontalGapBetweenBox,
                              Text(
                                interestName,
                                style: AppTextStyle.getDynamicFontStyle(
                                    Palette.primaryTextColor,
                                    index == 0 ? 14 : 15,
                                    index == 0
                                        ? FontType.Bold
                                        : FontType.Regular),
                              ),
                            ],
                          ),
                          onTap: () {
                            if (!_tempSelectedCities.contains(interestName)) {
                              _tempSelectedCities.add(interestName);
                              if (index == 0) {
                                _tempSelectedCities.clear();
                                _tempSelectedCities.addAll(widget.cities);
                              }
                            } else {
                              if (_tempSelectedCities.contains(interestName)) {
                                _tempSelectedCities.removeWhere(
                                    (String interest) =>
                                        interest == interestName);
                              }
                            }
                            if (_tempSelectedCities.length !=
                                widget.cities.length) {
                              _tempSelectedCities.remove(widget.cities[0]);
                            }

                            setState(() {});
                          },
                        ),
                      );
                    }),
              ),
            ),
          ),
          UIHelper.verticalGapBetweenBox,
          Container(
            padding: EdgeInsets.all(UIHelper.screenPadding),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  child: Text(
                    'Cancel',
                    style: AppTextStyle.getDynamicFontStyle(
                        Palette.secondaryTextColor, 16, FontType.Regular),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                InkWell(
                  onTap: () {
                    widget.onSelectedCitiesListChanged(_tempSelectedCities);
                    Navigator.pop(context);
                  },
                  child: Text(
                    'Done',
                    style: AppTextStyle.getDynamicFontStyle(
                        Palette.accentColor, 16, FontType.Regular),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
