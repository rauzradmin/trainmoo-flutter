import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/shared_preference/user_preference.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/view/home/HomeTab.dart';
import 'package:trainmoo/ui/view/home/MyWorkTab.dart';
import 'package:trainmoo/ui/view/home/StudentGradeListTab.dart';
import 'package:trainmoo/ui/view/home/TakeAttendanceTab.dart';
import 'package:trainmoo/ui/view/more/MoreListView.dart';
import 'package:trainmoo/ui/widget/FixedCenterDockedFabLocation.dart';
import 'package:trainmoo/util/image_path.dart';

import '../router.dart';
import 'home/ClassTab.dart';
import 'home/FabListTab.dart';
import 'home/MoreListTab.dart';
import 'home/MySessionTab.dart';

class TabManagerView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => TabManagerViewState();
}

class TabManagerViewState extends State<TabManagerView> with BaseCommonWidget {
  int currentIndex = 0;
  int lastIndex = 0;

  String userType;

  List<Widget> _children() => [
        HomeTab(Screen.HomeView.toString()),
        ClassTab(Screen.ClassListView.toString()),
        // userType == API.StudentUser
        //     ? TakeAttendanceTab(Screen.StudentAttendanceView.toString())
        FabListTab(Screen.FabListView.toString()),
        // userType == API.StudentUser
        //     ? StudentGradeListTab(Screen.StudentGradeListView.toString())
        MySessionTab(Screen.MySession.toString()),
        userType == API.StudentUser
            ? MoreListTab(Screen.MoreListView.toString())
            : MyWorkTab(Screen.MyWorkTeacher.toString())
      ];

  @override
  void initState() {
    _getUserType();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> children = _children();
    return SafeArea(
      bottom: false,
      top: false,
      maintainBottomViewPadding: false,
      child: new Scaffold(
        backgroundColor: Palette.screenBgColor,
        resizeToAvoidBottomInset: true,

        floatingActionButton:
            //userType == API.StudentUser
            //     ? Container()
            FloatingActionButton(
                onPressed: () {
                  setState(() {});
                  onTabTapped(2);
                },
                child: Image.asset(
                  currentIndex == 2 ? ImagePath.CLOSE_IMG : ImagePath.ICON_ADD,
                  color: Palette.white,
                  width: 28,
                  height: 28,
                )),
//        Container(
//          color: Colors.transparent,
//          child: UnicornDialer(
//            //hasBackground: false,
// //            hasNotch: true,

//            backgroundColor: Colors.white70,
//            parentHeroTag: "parent",
//            onMainButtonPressed: () {
//              onTabTapped(2);
//            },
//            orientation: UnicornOrientation.VERTICAL,
//            parentButtonBackground: Colors.blue,
//            hasBackground: true,
//            parentButton: Icon(Icons.add, color: Colors.white, size: 20),
//          ),
//        ),
        floatingActionButtonLocation:
            FixedCenterDockedFabLocation(context: context),
        body: children[currentIndex],
        bottomNavigationBar: BottomAppBar(
//          notchMargin: -10.0,
//          shape: CircularNotchedRectangle(),
          child: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            selectedItemColor: Colors.transparent,
            selectedIconTheme: IconThemeData(color: Colors.transparent),
            unselectedIconTheme: IconThemeData(color: Colors.transparent),
            unselectedItemColor: Colors.transparent,
            onTap: (index) => onTabTapped(index),

            // new

            currentIndex: currentIndex,
            items: [
              BottomNavigationBarItem(
                  activeIcon: Image.asset(
                    (ImagePath.ICON_Tab_Home),
                    height: 25,
                    width: 24,
                  ),
                  backgroundColor: Colors.transparent,
                  icon: Image.asset(
                    (ImagePath.ICON_HOME_UNSELECTED),
                    height: 25,
                    width: 24,
                  ),
                  title: Text("Home")),
              BottomNavigationBarItem(
                  activeIcon: Image.asset(
                    (ImagePath.ICON_Second_Tab),
                    height: 25,
                    width: 24,
                  ),
                  icon: Image.asset(
                    (ImagePath.ICON_Second_TAB_UNSELECTED),
                    height: 25,
                    width: 24,
                  ),
                  title: Text(
                    "Classes",
                    style: TextStyle(color: Palette.white),
                  )),
              BottomNavigationBarItem(
                  activeIcon: Image.asset(
                    ImagePath.ATTENDANCE_IMG,
                    color: Colors.transparent,
                    height: 25,
                    width: 24,
                  ),
                  icon: Image.asset(
                    ImagePath.IC_UNSELECTED_ATTENDENCE,
                    color: Colors.transparent,
                    height: 25,
                    width: 24,
                  ),
                  title: Text(
                    "Create",
                    style: TextStyle(color: Palette.white),
                  )),
              BottomNavigationBarItem(
                  activeIcon: Image.asset(
                    ImagePath.ICON_THIRD_TAB_SELECTED,
                    height: 25,
                    width: 24,
                  ),
                  icon: Image.asset(
                    ImagePath.ICON_Third_Tab,
                    height: 25,
                    width: 24,
                  ),
                  title: Text("Session")),
              BottomNavigationBarItem(
                  activeIcon: userType == API.StudentUser
                      ? Icon(
                          Icons.more_vert,
                          color: Colors.black,
                          size: 25,
                        )
                      : Image.asset(
                          (ImagePath.ICON_FOURTH_TAB_SELECTED),
                          height: 25,
                          width: 24,
                        ),
                  icon: userType == API.StudentUser
                      ? Icon(
                          Icons.more_vert,
                          color: Colors.grey,
                          size: 25,
                        )
                      : Image.asset(
                          (ImagePath.ICON_Fourth_Tab),
                          height: 25,
                          width: 24,
                        ),
                  title: Text("Work")),
            ],
          ),
        ),
      ),
    );
  }

  void onTabTapped(int index) async {
    if (currentIndex == index && index == 2) {
      setState(() {
        currentIndex = lastIndex;
      });
    } else {
      if (index == 2) {
        setState(() {
          currentIndex = index;
        });
      } else {
        setState(() {
          currentIndex = index;
          lastIndex = index;
        });
      }
    }
  }

  void _getUserType() async {
    userType = await UserPreference.getUserType();
    setState(() {
      userType = userType;
    });
    print('krima-->');
    print("userType: $userType");
  }
}
