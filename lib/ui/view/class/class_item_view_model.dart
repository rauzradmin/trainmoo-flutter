import 'package:flutter/cupertino.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/User.dart';
import 'package:trainmoo/model/swipe_home_model.dart';
import 'package:trainmoo/shared_preference/user_preference.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';

import '../../router.dart';

class ClassItemViewModel extends BaseModel {
  bool isCalledFirstTime = false;
  List<CommonModel> categoriesList = [];
  User user;
  bool isOpen = false;

  ProgramClassesModel classObject;

  init(ProgramClassesModel classObject) async {
    if (!isCalledFirstTime) {
      isCalledFirstTime = true;
      this.classObject = classObject;
//      UserPreference.saveUserType(1);
      getClassItemList();
    }
  }

  void getClassItemList() {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        categoriesList.addAll(await API.getClassItems(context, classObject));
        notifyListeners();

        //setState(ViewState.Busy);
        //        await API.getClassItemList('');
        //        setState(ViewState.Idle);
      } else {
        showToast(AppLocalizations.of(context).internetMessage, context);
      }
    });
  }

  Future<void> onItemTap(int index) async {
    if (index == 0) {
      Navigator.pushNamed(context, Screen.StudentList.toString(),
          arguments: {"class_id": classObject.id});
    } else if (index == 1) {
      Navigator.pushNamed(context, Screen.MaterialView.toString(),
          arguments: {"class": classObject});
    } else if (index == 2) {
      Navigator.pushNamed(context, Screen.AssignmentView.toString(),
          arguments: {"class": classObject});
    } else if (index == 3) {
      Navigator.pushNamed(context, Screen.QuizView.toString(),
          arguments: {"class": classObject});
    } else if (index == 4) {
      var user = await getUserDetail();
      print("user.userType $user");
      user.userType == API.StudentUser
          ? Navigator.pushNamed(
              context, Screen.StudentAttendanceView.toString())
          : Navigator.pushNamed(context, Screen.Attendance.toString(),
              arguments: {"class_id": classObject.id});
    } else if (index == 5) {
      Navigator.pushNamed(context, Screen.GradeView.toString(),
          arguments: {"class_id": classObject.id});
    } else if (index == 6) {
      Navigator.pushNamed(context, Screen.SyllabusView.toString(),
          arguments: {"class_id": classObject.id});
    }
  }

  Future getUserDetail() async {
    user = await UserPreference.getUserDetail();
    print(user);
    return user;
  }
}
