import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:trainmoo/app_state.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/shared_preference/user_preference.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';

import '../../router.dart';
import 'class_view_model.dart';

class ClassListView extends StatefulWidget {
  @override
  _ClassListViewState createState() => _ClassListViewState();
}

class _ClassListViewState extends State<ClassListView> with BaseCommonWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          print("state :  ${state.loadFirstTimeClass}");
          return BaseView<ClassViewModel>(builder: (context, model, child) {
            model.init(state, context);
            return SafeArea(child: _getBody(model));
          });
        });
  }

  Widget _getBody(ClassViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(ClassViewModel model) {
    return Container(
      child: Column(
        children: <Widget>[
          commonAppBarWithoutArrow(
              AppLocalizations.of(context).classes, context),
          Expanded(
              child: Container(
                  margin:
                      EdgeInsets.only(left: 24, right: 24, top: 15, bottom: 5),
                  child: SingleChildScrollView(child: _classList(model))))
        ],
      ),
    );
  }

  Widget _classList(ClassViewModel model) {
    return GridView.builder(
      semanticChildCount: 2,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      gridDelegate:
          SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      itemCount: model.classList.length,
      itemBuilder: (context, index) {
        return InkWell(
            onTap: () {
              Navigator.pushNamed(context, Screen.ClassItemView.toString(),
                  arguments: {"class": model.classList[index]});
              ProgramClassesModel programClassesModel = model.classList[index];
              UserPreference.saveCurrentClass(
                  programClassesModel.name, programClassesModel.id);
            },
            child: _getClassItem(index, model));
      },
    );
  }

  Widget _getClassItem(
    int index,
    ClassViewModel model,
  ) {
    ProgramClassesModel programClassesModel = model.classList[index];

    if (model.colorIndex == 5) {
      model.colorIndex = 0;
    } else {
      model.colorIndex += 1;
    }
    return Card(
      elevation: 1,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7)),
      child: Container(
        height: 200,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
//            Container(
//              margin: EdgeInsets.only(left: 18, top: 40),
//              child: Text(
//                'Class',
//                style: AppTextStyle.getDynamicFontStyleWithWeight(
//                    false,
//                    Palette.primaryTextColor,
//                    16,
//                    FontType.SemiBol,
//                    FontWeight.w500),
//              ),
//            ),
            Container(
              transform: Matrix4.translationValues(0, -4, 0),
              margin: EdgeInsets.only(left: 18, top: 40),
              padding: EdgeInsets.only(left: 10),
              decoration: BoxDecoration(
                  border: Border(
                      left: BorderSide(
                          width: 4, color: model.typeColor[model.colorIndex]))),
              child: Text(
                programClassesModel.name,
                style: AppTextStyle.getDynamicFontStyleWithWeight(
                    false,
                    Palette.primaryTextColor,
                    16,
                    FontType.SemiBol,
                    FontWeight.w500),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 18, top: 12),
                    child: Text(
                      AppLocalizations.of(context).students,
                      style: AppTextStyle.getDynamicFontStyleWithWeight(
                          false,
                          Palette.secondaryTextColor,
                          13,
                          FontType.RobotoRegular,
                          FontWeight.normal),
                    )),
                Container(
                    margin: EdgeInsets.only(right: 18, top: 12),
                    child: Text(
                      programClassesModel.users.student.toString(),
                      style: AppTextStyle.getDynamicFontStyleWithWeight(
                          false,
                          Palette.lightBlackColor,
                          13,
                          FontType.RobotoBold,
                          FontWeight.normal),
                    )),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 18, top: 4),
                    child: Text(
                      AppLocalizations.of(context).attendance,
                      style: AppTextStyle.getDynamicFontStyleWithWeight(
                          false,
                          Palette.secondaryTextColor,
                          13,
                          FontType.RobotoRegular,
                          FontWeight.normal),
                    )),
                Container(
                    margin: EdgeInsets.only(right: 9, top: 3),
                    child: Text(
                      "85%",
                      style: AppTextStyle.getDynamicFontStyleWithWeight(
                          false,
                          Palette.lightBlackColor,
                          13,
                          FontType.RobotoBold,
                          FontWeight.normal),
                    )),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
