import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/swipe_home_model.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';
import 'package:trainmoo/ui/util/util.dart';

import 'class_item_view_model.dart';

class ClassItemView extends StatefulWidget {
  final ProgramClassesModel classObject;

  const ClassItemView({Key key, this.classObject}) : super(key: key);

  @override
  _ClassItemViewState createState() => _ClassItemViewState();
}

class _ClassItemViewState extends State<ClassItemView> with BaseCommonWidget {
  int listPosition = 0;

  @override
  Widget build(BuildContext context) {
    return BaseView<ClassItemViewModel>(builder: (context, model, child) {
      model.init(widget.classObject);
//    showMessage(model);
      return SafeArea(
        child: Scaffold(
          backgroundColor: Palette.screenBgColor,
          body: _getBody(model),
        ),
      );
    });
  }

  Widget _getBody(ClassItemViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
      ],
    );
  }

  Widget _getBaseContainer(ClassItemViewModel model) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          commonAppBar(widget.classObject.name, context),
          Container(
            margin: EdgeInsets.only(left: 24, right: 24),
            height: 93,
            width: double.infinity,
            child: Card(
              elevation: 1,
              color: Palette.lightWhiteColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(3)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    widget.classObject.tutor != null
                        ? widget.classObject.tutor.name
                        : "",
                    style: AppTextStyle.getDynamicFontStyleWithWeight(
                        false,
                        Palette.primaryTextColor,
                        18,
                        FontType.SemiBol,
                        FontWeight.w700),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 2),
                    child: Text(
                      Utility.getDisplayDate(widget.classObject.from) +
                          ' - ' +
                          Utility.getDisplayDate(widget.classObject.to),
                      style: AppTextStyle.subTitleStyleWithTextSize(14),
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  model.categoriesList.length > 0
                      ? _classItemsView(model)
                      : Container(),
                  UIHelper.verticalSpaceSmall,
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _classItemsView(ClassItemViewModel model) {
    return Container(
      margin: EdgeInsets.only(top: 11, left: 24, right: 24),
      child: GridView.builder(
        semanticChildCount: 2,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        padding: EdgeInsets.only(top: 0.0),
        gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemCount: model.categoriesList.length,
        itemBuilder: (context, index) {
          return _classItemView(model.categoriesList[index], index, model);
        },
      ),
    );
  }

  Widget _classItemView(
      CommonModel categoriesList, int index, ClassItemViewModel model) {
    return Card(
      elevation: 1,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7)),
      child: InkWell(
        onTap: () {
          model.onItemTap(index);
          setState(() {});
        },
        child: Container(
          height: 200,
          width: 158,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                categoriesList.image,
                fit: BoxFit.fill,
                height: 50,
                width: 50,
              ),
              Container(
                margin: EdgeInsets.only(top: 8),
                child: Text(
                  categoriesList.title,
                  style: AppTextStyle.getDynamicFontStyleWithWeight(
                      false,
                      Palette.primaryTextColor,
                      16,
                      FontType.SemiBol,
                      FontWeight.w500),
                ),
              ),
              Container(
                transform: Matrix4.translationValues(0, -1, 0),
                child: Text(categoriesList.desc,
                    style: AppTextStyle.commonSubTitleStyle()),
              )
            ],
          ),
        ),
      ),
    );
  }
}
