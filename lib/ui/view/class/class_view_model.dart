import 'dart:math';
import 'dart:ui';

import 'package:flutter_redux/flutter_redux.dart';
import 'package:trainmoo/actions/home_actions.dart';
import 'package:trainmoo/app_state.dart';
import 'package:trainmoo/model/Database/DatabaseHelper.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/util.dart';

class ClassViewModel extends BaseModel {
  bool isCalledFirstTime = false;
  List<ProgramClassesModel> classList = [];
  List<Color> typeColor = [
    Palette.blue,
    Palette.primaryColor,
    Palette.assignmentColorItem,
    Palette.redColor,
    Palette.discussion,
    Palette.syllabusColor
  ];
  int colorIndex = 0;
  bool isOpen = false;

  init(state, context) async {
    if (!isCalledFirstTime) {
      isCalledFirstTime = true;
      if (state.loadFirstTimeClass == false) {
        await getDataFromLocal();
        getClassList(context);
      } else {
        classList = state.classList;
      }
    }
  }

  void getClassList(context) async {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        if (this.classList.length <= 0) setState(ViewState.Busy);

        List<ProgramClassesModel> list = await API.getProgramClass(false);
        this.classList.clear();
        print("class : ${list.length}");
        for (ProgramClassesModel classesModel in list) {
          // if (classesModel.users.student > 0) {
          print("classesModel.type ---> : ${classesModel.type}");
          if (classesModel.type == "class") {
            this.classList.add(classesModel);
          }
          //}
        }
        StoreProvider.of<AppState>(context)
            .dispatch(SetLoadFirstTimeClass(true));
        StoreProvider.of<AppState>(context)
            .dispatch(SetClassList(this.classList));
        setState(ViewState.Idle);
      } else {
        if (classList.length <= 0)
          showToast(AppLocalizations.of(context).internetMessage, context);
      }
    });
  }

  Future<void> getDataFromLocal() async {
    this.classList = await DatabaseHelper().getClasses();

    notifyListeners();
  }

  Color getRandomColor() {
    final _random = new Random();
    return typeColor[_random.nextInt(typeColor.length)];
  }
}
