import 'package:flutter/material.dart';
import 'package:trainmoo/model/StudentModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:trainmoo/util/image_path.dart';

class StudentView extends StatelessWidget with BaseCommonWidget {
  final StudentModel studentModel;

  const StudentView({Key key, this.studentModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return studentItem(studentModel);
  }

  studentItem(StudentModel student) {
    return Card(
      elevation: 1,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(UIHelper.cardRadius)),
      child: Padding(
        padding: const EdgeInsets.only(left: 16, top: 12, bottom: 12),
        child: Row(
          children: <Widget>[
            CircleAvatar(
                child: student.avatarThumbnail != null
                    ? loadCircleNetworkImage(
                        student.avatarThumbnail, 40, new Map())
                    : Image.asset(
                        ImagePath.USER,
                        height: 40,
                        width: 40,
                      )),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      student.name,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: AppTextStyle.getDynamicFontStyle(
                          Colors.black, 16, FontType.SemiBol),
                    ),
                    Text(student.email != null ? student.email : "",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: AppTextStyle.getDynamicFontStyle(
                            Palette.listColor, 13, FontType.RobotoRegular)),
                  ],
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                IconButton(
                    icon: Image.asset(
                      ImagePath.ICON_CALL,
                      width: 17.9,
                      height: 17.94,
                    ),
                    onPressed: () {
                      Utility.launchURL("tel://${student.mobile}");
                    }),
                IconButton(
                    icon: Image.asset(
                      ImagePath.ICON_MESSAGE,
                      width: 18,
                      height: 18,
                    ),
                    onPressed: () {
                      Utility.launchURL("tel://${student.mobile}");
                    })
              ],
            )
          ],
        ),
      ),
    );
  }
}
