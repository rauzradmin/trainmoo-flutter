import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:trainmoo/app_state.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/view/StudentList/student_listview_model.dart';
import 'package:provider/provider.dart';

import 'StudentView.dart';

class StudentListView extends StatefulWidget {
  final String classId;

  const StudentListView({Key key, this.classId}) : super(key: key);
  @override
  _RegistrationViewState createState() => _RegistrationViewState();
}

class _RegistrationViewState extends State<StudentListView>
    with BaseCommonWidget {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          print("state : ${state.loadFirstTimeGradeStudents}");
          return BaseView<StudentListViewModel>(
              builder: (context, model, child) {
            model.init(widget.classId, state, context);
            return SafeArea(child: _getBody(model));
          });
        });
  }

  Widget _getBody(StudentListViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(StudentListViewModel model) {
    return Column(
      children: <Widget>[
        commonAppBar(AppLocalizations.of(context).student, context),
        Expanded(
            child: model.studentsList.length > 0 && model.studentsList != null
                ? getStudentList(model)
                : Container())
      ],
    );
  }

  getStudentList(StudentListViewModel model) {
    return Container(
      margin: const EdgeInsets.only(left: 24, right: 24),
      padding: EdgeInsets.only(top: 20),
      child: SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        header: WaterDropMaterialHeader(
          backgroundColor: Palette.accentColor,
        ),
        controller: _refreshController,
        onRefresh: () async {
          model.getStudentList(context);
          _refreshController.refreshCompleted();
        },
        child: ListView.builder(
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          itemCount: model.studentsList.length,
          itemBuilder: (context, int ind) {
            return StudentView(studentModel: model.studentsList[ind]);
          },
        ),
      ),
    );
  }
}
