import 'package:flutter/foundation.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:trainmoo/actions/home_actions.dart';
import 'package:trainmoo/app_state.dart';
import 'package:trainmoo/model/Database/DatabaseHelper.dart';
import 'package:trainmoo/model/StudentModel.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';

class StudentListViewModel extends BaseModel {
  bool isFirstTime = false;
  List<StudentModel> studentsList = [];

  String classId;

  init(String classId, state, context) async {
    if (!isFirstTime) {
      isFirstTime = true;
      if (state.loadFirstTimeGradeStudents == false) {
        this.classId = classId;
        getDataFromLocal(context);
      } else {
        this.studentsList = state.studentsList;
      }
    }
  }

  Future<void> getStudentList(context) async {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        if (studentsList.length <= 0) setState(ViewState.Busy);
        studentsList = await API.getStudents(classId);
        StoreProvider.of<AppState>(context)
            .dispatch(SetStudentsList(studentsList));
        StoreProvider.of<AppState>(context)
            .dispatch(SetLoadFirstTimeGrade(true));
      } else {
        await getDataFromLocal(context);
        if (studentsList.length <= 0)
          showToast(AppLocalizations.of(context).internetMessage, context);
      }
      setState(ViewState.Idle);
    });
    notifyListeners();
  }

  Future<void> getDataFromLocal(context) async {
    studentsList = await DatabaseHelper().getStudents(classId);
    if (studentsList.length > 0) {
      notifyListeners();
    }
    getStudentList(context);
  }
}
