import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/shared_preference/user_preference.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';
import 'package:trainmoo/util/image_path.dart';

import '../../router.dart';

class FabListView extends StatefulWidget {
  @override
  _FabListViewState createState() => _FabListViewState();
}

class _FabListViewState extends State<FabListView> {
  String userType;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUsertype();
  }

  void getUsertype() async {
    var type = await UserPreference.getUserType();
    setState(() {
      userType = type;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
      child: Container(
        margin: EdgeInsets.only(left: 10, bottom: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _getChildFabIcon(
                ImagePath.ATTENDANCE_IMG,
                // ignore: unrelated_type_equality_checks
                userType == API.StudentUser
                    ? AppLocalizations.of(context).startDiscussion
                    : AppLocalizations.of(context).takeAttendance,
                userType == API.StudentUser
                    ? Screen.CreateMaterial.toString()
                    : Screen.TakeAttendanceView.toString(),
                null),
            _getChildFabIcon(
                ImagePath.ASSIGNMENT_IMG,
                // ignore: unrelated_type_equality_checks
                userType == API.StudentUser
                    ? AppLocalizations.of(context).askQuestion
                    : AppLocalizations.of(context).createAssignment,
                userType == API.StudentUser
                    ? Screen.CreateMaterial.toString()
                    : Screen.CreateAssignment.toString(),
                null),
//            _getChildFabIcon(ImagePath.SYLLABUS_IMAGE,
//                AppLocalizations.of(context).createSyllabus, ""),
            userType == API.TeacherUser
                ? _getChildFabIcon(
                    ImagePath.QUIZ_IMG,
                    AppLocalizations.of(context).createQuiz,
                    userType == API.StudentUser
                        ? Screen.CreateMaterial.toString()
                        : Screen.CreateQuiz.toString(),
                    null)
                : Container(),
//            _getChildFabIcon(
//                ImagePath.GRADE_IMG,
//                AppLocalizations.of(context).grade,
//                Screen.GradeView.toString()),
            _getChildFabIcon(
                ImagePath.MATERIAL_IMG,
                AppLocalizations.of(context).createMaterial,
                Screen.CreateMaterial.toString(),
                null),
            userType == API.TeacherUser
                ? _getChildFabIcon(
                    ImagePath.NOTIFICATION_IMG,
                    AppLocalizations.of(context).createNotification,
                    Screen.CreateMaterial.toString(),
                    Colors.blueAccent)
                : Container(),
            userType == API.TeacherUser
                ? _getChildFabIcon(
                    ImagePath.CALENDAR_IMG,
                    AppLocalizations.of(context).createEvent,
                    Screen.CreateEventView.toString(),
                    null)
                : Container(),
          ],
        ),
      ),
//        visible: widget.isFab,
    );
  }

  _getChildFabIcon(
      String imageName, String fabName, String screenName, Color color) {
    return GestureDetector(
      onTap: () {
        if (screenName != null) {
          Navigator.of(context).pushNamed(screenName,
              arguments: {"materialObject": null, "materialType": fabName});
        }
      },
      child: Row(
        children: <Widget>[
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
            child: color == null
                ? IconButton(
                    icon: Image.asset(
                    imageName,
                    height: 24,
                    width: 20,
                  ))
                : IconButton(
                    icon: Image.asset(
                      imageName,
                      height: 24,
                      width: 20,
                      color: color,
                    ),
                    // onPressed: () {
                    //   if (screenName != null) {
                    //     userType == API.TeacherUser
                    //         ? Navigator.of(context).pushNamed(screenName, arguments: {
                    //             "materialObject": null,
                    //             "materialType": fabName
                    //           })
                    //         : Navigator.pushNamed(context, screenName);
                    //   }
                    //},
                  ),
          ),
          UIHelper.horizontalSpaceExtraSmall,
          Text(fabName,
              style: AppTextStyle.getDynamicFontStyleWithWeight(
                  false,
                  Palette.primaryTextColor,
                  15,
                  FontType.Medium,
                  FontWeight.normal))
        ],
      ),
    );
  }
}
