import 'package:trainmoo/model/swipe_home_model.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:trainmoo/util/image_path.dart';

class MyWorkTeacherViewModel extends BaseModel {
  bool isFirstTime = false;
  List<CommonModel> workLists = [];

  init() async {
    if (!isFirstTime) {
      isFirstTime = true;
      if (workLists != null && workLists.length > 0) {
        workLists.clear();
      }
      getWorkList();
    }
  }

  void getWorkList() {
    setState(ViewState.Busy);
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        workLists.addAll(await API.getWorkList());
        setState(ViewState.Idle);
        notifyListeners();
        print('lenghth of-->${workLists.length}');
      } else {
        setState(ViewState.Idle);
        showToast(AppLocalizations.of(context).internetMessage, context);
      }
    });
  }
}
