import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:trainmoo/actions/home_actions.dart';
import 'package:trainmoo/app_state.dart';
import 'package:trainmoo/model/MaterialModel.dart';
import 'package:trainmoo/model/swipe_home_model.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:trainmoo/ui/view/MyWork/my_work_view_model.dart';
import 'package:trainmoo/ui/widget/FileView.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:intl/intl.dart';
import '../../router.dart';

class MyWorkView extends StatefulWidget {
  final String postType;
  const MyWorkView(this.postType);

  @override
  _MyWorkViewState createState() => _MyWorkViewState();
}

class _MyWorkViewState extends State<MyWorkView>
    with BaseCommonWidget, SingleTickerProviderStateMixin {
  TabController tabController;
  int tabIndex = 0;
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
    tabController = TabController(vsync: this, length: 2);
    tabController.addListener(toggleTab);
  }

  void toggleTab() {
    setState(() {
      tabIndex = tabController.index;
    });
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          return BaseView<MyWorkViewModel>(builder: (context, model, child) {
            print(
                "state : => ${widget.postType} ${state.loadFirstTimeGradAssignment} ${state.loadFirstTimeReviewQuiz}");
            model.init(widget.postType, state,
                (submitedMaterialLists, paddingMaterialLists) {
              if (widget.postType == "Grade Assignment") {
                StoreProvider.of<AppState>(context)
                    .dispatch(SetLoadFirstTimeGradAssignment(true));
                StoreProvider.of<AppState>(context).dispatch(
                    SetSubmitedAssignmenMaterialLists(submitedMaterialLists));
                StoreProvider.of<AppState>(context).dispatch(
                    SetPaddingAssignmentMaterialLists(paddingMaterialLists));
              } else if (widget.postType == "Review Quiz") {
                StoreProvider.of<AppState>(context)
                    .dispatch(SetLoadFirstTimeReviewQuiz(true));
                StoreProvider.of<AppState>(context).dispatch(
                    SetSubmitedQuizMaterialLists(submitedMaterialLists));
                StoreProvider.of<AppState>(context).dispatch(
                    SetPaddingQuizMaterialLists(paddingMaterialLists));
              } else {
                StoreProvider.of<AppState>(context)
                    .dispatch(SetLoadFirstTimeMyWork(true));
                StoreProvider.of<AppState>(context)
                    .dispatch(SetSubmitedMaterialLists(submitedMaterialLists));
                StoreProvider.of<AppState>(context)
                    .dispatch(SetPaddingMaterialLists(paddingMaterialLists));
              }
            });

            return SafeArea(
                child: Scaffold(
                    backgroundColor: Palette.screenBgColor,
                    body: _getBody(model)));
          });
        });
  }

  Widget _getBody(MyWorkViewModel model) {
    return Stack(
      children: <Widget>[_getBaseContainer(model), getProgressBar(model.state)],
    );
  }

  Widget _getBaseContainer(MyWorkViewModel model) {
    return Container(
        child: Column(children: <Widget>[
      commonAppBar(
          widget.postType == "Grade Assignment"
              ? AppLocalizations.of(context).gradeAssignment
              : widget.postType == "Review Quiz"
                  ? AppLocalizations.of(context).reviewQuiz
                  : AppLocalizations.of(context).myWork,
          context),
      //completedList(model),
      Expanded(
        child: Container(
          child: DefaultTabController(
            length: 2,
            child: Column(
              children: <Widget>[
                Container(
                  width: 400,
                  child: TabBar(
                    tabs: [
                      Tab(
                          child: Container(
                        //decoration: BoxDecoration(border: Border.all(width: 1)),
                        width: 120,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Text(AppLocalizations.of(context).pending,
                                  style: AppTextStyle
                                      .getDynamicFontStyleWithoutColor(
                                    18,
                                    FontType.Medium,
                                  )),
                            ),
                            Container(
                                //padding: EdgeInsets.only(right: 33),
                                height: 20,
                                child: VerticalDivider(
                                  color: Colors.black,
                                  thickness: 2.0,
                                ))
                          ],
                        ),
                      )),
                      Tab(
                          child: Container(
                        // decoration: BoxDecoration(border: Border.all(width: 1)),
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        width: 130,
                        margin: EdgeInsets.only(right: 10),
                        transform: Matrix4.translationValues(-50, 0, 0),
                        child: Text(AppLocalizations.of(context).completed,
                            style: AppTextStyle.getDynamicFontStyleWithoutColor(
                              18,
                              FontType.Medium,
                            )),
                      )),
                    ],
                    indicatorColor: Colors.transparent,
                    labelColor: Colors.black,
                    unselectedLabelColor: Palette.secondaryTextColor,
                    controller: tabController,
                  ),
                ),
                Expanded(
                  child: TabBarView(
                    children: [
                      model.paddingMaterialLists != null
                          ? _categoryView(model, model.paddingMaterialLists)
                          : Text('No Data'),
                      model.submitedMaterialLists.length != null
                          ? _categoryView(model, model.submitedMaterialLists)
                          : Center(
                              child: Text('No Data'),
                            ),
                    ],
                    controller: tabController,
                  ),
                ),
              ],
            ),
          ),
        ),
      )
    ]));
  }

  Widget _categoryView(
      MyWorkViewModel model, List<MaterialModel> materialLists) {
    void _onRefresh() async {
      await Future.delayed(Duration(milliseconds: 1500));
      model.getClassList(model.postType,
          (submitedMaterialLists, paddingMaterialLists) {
        if (widget.postType == "Grade Assignment") {
          StoreProvider.of<AppState>(context)
              .dispatch(SetLoadFirstTimeGradAssignment(true));
          StoreProvider.of<AppState>(context).dispatch(
              SetSubmitedAssignmenMaterialLists(submitedMaterialLists));
          StoreProvider.of<AppState>(context).dispatch(
              SetPaddingAssignmentMaterialLists(paddingMaterialLists));
        } else if (widget.postType == "Review Quiz") {
          StoreProvider.of<AppState>(context)
              .dispatch(SetLoadFirstTimeReviewQuiz(true));
          StoreProvider.of<AppState>(context)
              .dispatch(SetSubmitedQuizMaterialLists(submitedMaterialLists));
          StoreProvider.of<AppState>(context)
              .dispatch(SetPaddingQuizMaterialLists(paddingMaterialLists));
        } else {
          StoreProvider.of<AppState>(context)
              .dispatch(SetLoadFirstTimeMyWork(true));
          StoreProvider.of<AppState>(context)
              .dispatch(SetSubmitedMaterialLists(submitedMaterialLists));
          StoreProvider.of<AppState>(context)
              .dispatch(SetPaddingMaterialLists(paddingMaterialLists));
        }
      });
      _refreshController.refreshCompleted();
    }

    return SmartRefresher(
      enablePullDown: true,
      enablePullUp: false,
      header: WaterDropMaterialHeader(
        backgroundColor: Palette.accentColor,
      ),
      controller: _refreshController,
      child: ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: materialLists.length,
        itemBuilder: (BuildContext cont, int ind) {
          return assignmentItem(
              ind, model, materialLists, materialLists.length, ind);
        },
      ),
      onRefresh: _onRefresh,
    );

//     return Padding(
//       padding: const EdgeInsets.only(bottom: 5),
//       child: ListView.builder(
//         physics: ClampingScrollPhysics(),
//         shrinkWrap: true,
//         itemCount: materialLists.length,
//         itemBuilder: (BuildContext cont, int ind) {
// //              model.isCallingPage == false
// //                  ? model.isCallingPage = true
// //                  : model.isCallingPage = false
//           return assignmentItem(
//               ind, model, materialLists, materialLists.length, ind);
//         },
//       ),
//     );
  }

  assignmentItem(
      int index, MyWorkViewModel model, materialLists, int length, int ind) {
    MaterialModel postModel = materialLists[index];
    int colorPosition = index;
    if (index > 9) {
      String strIndex = index.toString();
      strIndex = strIndex.substring(strIndex.length - 1);
      print('last character ==> $strIndex');
      colorPosition = int.parse(strIndex);
    }

    return InkWell(
      onTap: () {
        tabIndex == 0
            ? widget.postType != null
                ? Navigator.pushNamed(
                    context, Screen.ReviewAssignment.toString(), arguments: {
                    "assignment_id": postModel.id,
                    "postType": widget.postType
                  })
                : Navigator.pushNamed(context, Screen.MaterialDetail.toString(),
                    arguments: {
                        "material": postModel,
                        'screen_type': postModel.type
                      })
            : widget.postType != null
                ? Navigator.pushNamed(
                    context, Screen.ReviewAssignment.toString(), arguments: {
                    "assignment_id": postModel.id,
                    "postType": widget.postType
                  })
                : Navigator.pushNamed(context, Screen.MaterialDetail.toString(),
                    arguments: {
                        "material": postModel,
                        'screen_type': postModel.type,
                        "color": getColorFromType(postModel.type)
                      });
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
        child: Card(
          elevation: 2,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(UIHelper.cardRadius)),
          child: Container(
            padding: EdgeInsets.all(22),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Text(
                    Utility.removeAllHtmlTags(postModel.title),
                    maxLines: 1,
                    overflow: TextOverflow.fade,
                    style: AppTextStyle.getDynamicFontStyleWithWeight(
                        false,
                        Palette.primaryTextColor,
                        18,
                        FontType.SemiBol,
                        FontWeight.normal),
                  ),
                ),
                Text(
                    new DateFormat("dd MMM yyyy")
                        .format(DateTime.parse(postModel.createdAt)),
                    style: AppTextStyle.getDynamicFontStyle(
                        Palette.listColor, 13, FontType.RobotoRegular)),
                SizedBox(
                  height: 10,
                ),
                postModel.type != 'Assessment'
                    ? Text(Utility.removeAllHtmlTags(postModel.text),
                        style: AppTextStyle.detailDescriptionStyle())
                    : Container(),
                postModel.files.length > 0
                    ? Padding(
                        padding: const EdgeInsets.only(left: 3),
                        child: new FileView(
                          files: postModel.files,
                          headers: model.header,
                        ),
                      )
                    : Container(),
                SizedBox(
                  height: 10,
                ),
                Container(
                    child: postModel.type == 'Assessment'
                        ? Text(
                            Utility.removeAllHtmlTags(postModel.text),
                            style: AppTextStyle.getDynamicFontStyleWithWeight(
                                false,
                                Palette.primaryTextColor,
                                14,
                                FontType.Medium,
                                FontWeight.normal),
                          )
                        : Text(
                            '${postModel.submissions} Submitted | ${postModel.pending} Pending')),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

//   completedList(MyWorkViewModel model) {
//     return Padding(
//       padding: const EdgeInsets.only(left: 24, right: 24, bottom: 5, top: 10),
//       child: ListView.builder(
//         physics: ClampingScrollPhysics(),
//         shrinkWrap: true,
//         itemCount: model.workLists.length,
//         itemBuilder: (BuildContext cont, int ind) {
//           return workItem(model.workLists[ind], ind);
//         },
//       ),
//     );
//   }

//   workItem(CommonModel workList, int ind) {
//     return GestureDetector(
//       child: Container(
//         width: 327,
//         height: 136,
//         child: Card(
//           elevation: 0,
//           shape:
//               RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
//           child: Padding(
//             padding: const EdgeInsets.only(left: 16),
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               crossAxisAlignment: CrossAxisAlignment.center,
//               children: <Widget>[
//                 Padding(
//                   padding: const EdgeInsets.only(bottom: 28),
//                   child: Image.asset(
//                     workList.image,
//                     height: 50,
//                     width: 41,
//                   ),
//                 ),
//                 Padding(
//                   padding: const EdgeInsets.only(left: 14, right: 10),
//                   child: Column(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: <Widget>[
//                       Text(workList.title,
//                           style: AppTextStyle.getDynamicFontStyle(
//                               Colors.black, 18, FontType.SemiBol)),
//                       Container(
//                         width: 195,
//                         padding: const EdgeInsets.only(top: 6),
//                         child: Text(workList.desc,
//                             style: AppTextStyle.getDynamicFontStyle(
//                                 Palette.listColor, 14, FontType.RobotoRegular)),
//                       ),
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ),
//       ),
//       onTap: () {
//         switch (ind) {
//           case 0:
//             Navigator.of(context)
//                 .pushNamed(Screen.TakeAttendanceView.toString());
//             break;

//           case 1:
//             Navigator.of(context).pushNamed(Screen.ClassListView.toString());

//             break;

//           case 2:
//             Navigator.of(context).pushNamed(Screen.ClassListView.toString());

//             break;
//         }
// //        Navigator.pushNamed(context, Screen.AssignmentDetail.toString());
//       },
//     );
//   }
}
