import 'package:flutter/material.dart';
import 'package:trainmoo/model/swipe_home_model.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/view/MyWork/my_work_teacher_view_model.dart';
import 'package:trainmoo/ui/view/MyWork/my_work_view_model.dart';

import '../../router.dart';

class MyWorkTeacherView extends StatefulWidget {
  @override
  _MyWorkTeacherViewState createState() => _MyWorkTeacherViewState();
}

class _MyWorkTeacherViewState extends State<MyWorkTeacherView>
    with BaseCommonWidget, SingleTickerProviderStateMixin {
  TabController tabController;
  int tabIndex = 0;

  @override
  void initState() {
    super.initState();
    tabController = TabController(vsync: this, length: 2);
    tabController.addListener(toggleTab);
  }

  void toggleTab() {
    setState(() {
      tabIndex = tabController.index;
    });
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<MyWorkTeacherViewModel>(builder: (context, model, child) {
      model.init();
      return SafeArea(
          child: Scaffold(
              backgroundColor: Palette.screenBgColor, body: _getBody(model)));
    });
  }

  Widget _getBody(MyWorkTeacherViewModel model) {
    return Stack(
      children: <Widget>[_getBaseContainer(model), getProgressBar(model.state)],
    );
  }

  Widget _getBaseContainer(MyWorkTeacherViewModel model) {
    return Container(
        child: Column(children: <Widget>[
      commonAppBarWithoutArrow(AppLocalizations.of(context).myWork, context),
      completedList(model)

      /*Expanded(
              child: Container(
                child: DefaultTabController(
                  length: 2,
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: TabBar(
                          tabs: [
                            Tab(
                                child: Container(
                                  width: 220,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(left: 10),
                                        child: Text(AppLocalizations.of(context).pending,
                                            style: AppTextStyle.getDynamicFontStyleWithoutColor(
                                              18,
                                              FontType.Medium,
                                            )),
                                      ),
                                      Container(
                                          padding: EdgeInsets.only(right: 33),
                                          height: 20,
                                          child: VerticalDivider(
                                            color: Colors.black,
                                            thickness: 2.0,
                                          ))
                                    ],
                                  ),
                                )),
                            Tab(
                                child: Container(
                                  width: 130,
                                  margin: EdgeInsets.only(right: 10),
                                  transform: Matrix4.translationValues(-55,0, 0),
                                  child: Text(AppLocalizations.of(context).completed,
                                      style: AppTextStyle.getDynamicFontStyleWithoutColor(
                                        18,
                                        FontType.Medium,
                                      )),
                                )),
                          ],
                          indicatorColor: Colors.transparent,
                          labelColor: Colors.black,
                          unselectedLabelColor: Palette.secondaryTextColor,
                          controller: tabController,
                        ),
                      ),
                      Expanded(
                        child: TabBarView(
                          children: [
                            model.workLists != null
                                ? completedList(model)
                                : Text('No Data'),
                            model.workLists == null
                                ? completedList(model)
                                : Center(
                              child: Text('No Data'),
                            ),
                          ],
                          controller: tabController,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )*/
    ]));
  }

  completedList(MyWorkTeacherViewModel model) {
    return Padding(
      padding: const EdgeInsets.only(left: 24, right: 24, bottom: 5, top: 10),
      child: ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: model.workLists.length,
        itemBuilder: (BuildContext cont, int ind) {
          return workItem(model.workLists[ind], ind);
        },
      ),
    );
  }

  workItem(CommonModel workList, int ind) {
    return GestureDetector(
      child: Container(
        width: 327,
        height: 136,
        child: Card(
          elevation: 0,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Padding(
            padding: const EdgeInsets.only(left: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 28),
                  child: Image.asset(
                    workList.image,
                    height: 50,
                    width: 41,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 14, right: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(workList.title,
                          style: AppTextStyle.getDynamicFontStyle(
                              Colors.black, 18, FontType.SemiBol)),
                      Container(
                        width: 195,
                        padding: const EdgeInsets.only(top: 6),
                        child: Text(workList.desc,
                            style: AppTextStyle.getDynamicFontStyle(
                                Palette.listColor, 14, FontType.RobotoRegular)),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      onTap: () {
        switch (ind) {
          case 0:
            Navigator.of(context)
                .pushNamed(Screen.TakeAttendanceView.toString());
            break;

          case 1:
            Navigator.of(context).pushNamed(Screen.MyWork.toString(),
                arguments: {"postType": "Grade Assignment"});

            break;

          case 2:
            // Navigator.of(context).pushNamed(Screen.ClassListView.toString());
            Navigator.of(context).pushNamed(Screen.MyWork.toString(),
                arguments: {"postType": "Review Quiz"});

            break;
        }
//        Navigator.pushNamed(context, Screen.AssignmentDetail.toString());
      },
    );
  }
}
