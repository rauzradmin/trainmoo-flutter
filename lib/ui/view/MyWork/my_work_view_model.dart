import 'dart:ui';

import 'package:trainmoo/model/MaterialModel.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/User.dart';
import 'package:trainmoo/model/swipe_home_model.dart';
import 'package:trainmoo/shared_preference/user_preference.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/util.dart';

class MyWorkViewModel extends BaseModel {
  String postType;
  bool isFirstTime = false;
  List<CommonModel> workLists = [];
  List<ProgramClassesModel> classList = [];
  List<MaterialModel> materialLists = [];
  List<MaterialModel> paddingMaterialLists = [];
  List<MaterialModel> submitedMaterialLists = [];
  List<MaterialModel> paddingQuizMaterialLists = [];
  List<MaterialModel> submitedQuizMaterialLists = [];

  Map<String, String> header;

  List<Color> typeColor = [
    Palette.assignmentColorItem,
    Palette.accentColor,
    Palette.syllabusColor,
    Palette.transparentBlue,
    Palette.primaryDarkColor,
    Palette.assignmentColorItem,
    Palette.accentColor,
    Palette.syllabusColor,
    Palette.transparentBlue,
    Palette.primaryDarkColor
  ];

  init(String postType, state, setMaterialList) async {
    print(
        "postType ==========>>>>>>>>> >>>>>> >>>>>: $postType ${state.loadFirstTimeMyWork}");

    if (!isFirstTime) {
      isFirstTime = true;
      // if (workLists != null && workLists.length > 0) {
      //   workLists.clear();
      // }
      //  header = await API.getAuthHeader();
      //getWorkList();

      if ((state.loadFirstTimeMyWork == false && postType == null) ||
          (state.loadFirstTimeGradAssignment == false &&
              postType == "Grade Assignment") ||
          (state.loadFirstTimeReviewQuiz == false &&
              postType == "Review Quiz")) {
        setState(ViewState.Busy);
        print("hello");
        getClassList(postType, setMaterialList);
      } else {
        if (postType == "Grade Assignment") {
          paddingMaterialLists = state.paddingAssignmentMaterialLists;
          submitedMaterialLists = state.submitedAssignmenMaterialLists;
        } else if (postType == "Review Quiz") {
          paddingMaterialLists = state.paddingQuizMaterialLists;
          submitedMaterialLists = state.submitedQuizMaterialLists;
        } else {
          paddingMaterialLists = state.paddingMaterialLists;
          submitedMaterialLists = state.submitedMaterialLists;
        }
      }
    }
  }

  void getWorkList() {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        workLists.addAll(await API.getWorkList());
        notifyListeners();
        print('lenghth of-->${workLists.length}');
      } else {
        showToast(AppLocalizations.of(context).internetMessage, context);
      }
    });
  }

  Future<void> getMaterialsAll(
      List<ProgramClassesModel> classList, int i, String postType,
      [setMaterialList]) async {
    print("hiiii");
    if (classList.length == i) {
      User user = await UserPreference.getUserDetail();
      materialLists.forEach((element) {
        print("user : $user");
        var data = element.replies.where((e) => e['author']['id'] == user.id);
        if (data.length == 0) {
          paddingMaterialLists.add(element);
          // print('peding ${element.pending} Pending ');
        } else {
          // print('========>>>>> >>>> >>> >>> ${element.type} type');
          submitedMaterialLists.add(element);
        }
      });
      setState(ViewState.Idle);
      notifyListeners();
      setMaterialList(submitedMaterialLists, paddingMaterialLists);
      print(
          'paddingMaterialLists ===>  ${materialLists.length} submitedMaterialLists : ${submitedMaterialLists.length}');
    }
    if (classList.length > i) {
      print("hello index : $i  type: $postType");
      if (postType == "Grade Assignment") {
        var mLists = await getMaterialList(classList[i].id);
        materialLists.addAll(mLists);
        print('paddingMaterialLists ===>  ${paddingMaterialLists.length}');
        getMaterialsAll(classList, i + 1, postType, setMaterialList);
      } else if (postType == "Review Quiz") {
        print("this is the Review Quiz ");
        var mLists = await getQuizMaterialList(classList[i].id);
        materialLists.addAll(mLists);
        print('paddingMaterialLists ===>  ${materialLists.length}');
        getMaterialsAll(classList, i + 1, postType, setMaterialList);
      } else {
        print("this is the else ");
        var mLists1 = await getMaterialList(classList[i].id);
        materialLists.addAll(mLists1);
        var mLists = await getQuizMaterialList(classList[i].id);
        materialLists.addAll(mLists);
        getMaterialsAll(classList, i + 1, postType, setMaterialList);
      }
    }
  }

  void getClassList(String postType, [setMaterialList]) async {
    setState(ViewState.Busy);
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        List<ProgramClassesModel> list = await API.getProgramClass(false);
        this.classList.clear();
        for (ProgramClassesModel classesModel in list) {
          if (classesModel.users.student > 0) {
            this.classList.add(classesModel);
          }
        }
        // for (ProgramClassesModel classesModel in classList) {
        //   // print('classesModel -->${classesModel.id}');
        //   if (postType == "Grade Assignment") {
        //     getMaterialList(classesModel.id);
        //   }
        //   if (postType == "Review Quiz") {
        //     getQuizMaterialList(classesModel.id);
        //   } else {
        //     print("this is the else ");
        //     getMaterialList(classesModel.id);
        //     getQuizMaterialList(classesModel.id);
        //   }
        // }

        getMaterialsAll(classList, 0, postType, setMaterialList);

        //print('lenghth of-->${classList.length}');
      } else {
        setState(ViewState.Idle);
        if (classList.length <= 0)
          showToast(AppLocalizations.of(context).internetMessage, context);
      }
    });
  }

  getMaterialList(classId) async {
    return await API.getMaterials(classId, "Assignment", null, null);
    //print('matrialLists ===>  ${materialLists[0]}');

    // setState(ViewState.Idle);
    // notifyListeners();
  }

  getQuizMaterialList(classId) async {
    return await API.getMaterials(classId, "Assessment", null, null);
    //print('matrialLists ===>  ${materialLists[0]}');
  }
}
