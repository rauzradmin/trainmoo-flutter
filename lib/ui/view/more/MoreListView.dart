import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/shared_preference/user_preference.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';
import 'package:trainmoo/util/image_path.dart';

import '../../router.dart';

class MoreListView extends StatefulWidget {
  @override
  _MoreListViewState createState() => _MoreListViewState();
}

class _MoreListViewState extends State<MoreListView> {
  String userType;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUsertype();
  }

  void getUsertype() async {
    var type = await UserPreference.getUserType();
    setState(() {
      userType = type;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
      child: Container(
        margin: EdgeInsets.only(left: 10, bottom: 10, right: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _getChildFabIcon(
                ImagePath.GRADE_IMG,
                AppLocalizations.of(context).myGrades,
                Screen.StudentGradeListView.toString()),
            _getChildFabIcon(
                ImagePath.ATTENDANCE_IMG,
                AppLocalizations.of(context).myAttendance,
                Screen.StudentAttendanceView.toString()),
            _getChildFabIcon(ImagePath.ICON_FOURTH_TAB_SELECTED,
                AppLocalizations.of(context).myWork, Screen.MyWork.toString()),
          ],
        ),
      ),
//        visible: widget.isFab,
    );
  }

  _getChildFabIcon(String imageName, String fabName, String screenName) {
    return InkWell(
      onTap: () {
        if (screenName != null) {
          if (AppLocalizations.of(context).myWork == fabName)
            Navigator.pushNamed(context, screenName,
                arguments: {"postType": null});
          else
            Navigator.pushNamed(context, screenName);
        }
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Text(fabName,
              style: AppTextStyle.getDynamicFontStyleWithWeight(
                  false,
                  Palette.primaryTextColor,
                  15,
                  FontType.Medium,
                  FontWeight.normal)),
          UIHelper.horizontalSpaceExtraSmall,
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
            child: IconButton(
              icon: Image.asset(
                imageName,
                height: 24,
                width: 20,
              ),
              onPressed: () {
                if (screenName != null) {
                  if (AppLocalizations.of(context).myWork == fabName)
                    Navigator.pushNamed(context, screenName,
                        arguments: {"postType": null});
                  else
                    Navigator.pushNamed(context, screenName);
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
