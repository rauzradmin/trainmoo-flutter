import 'package:flutter/cupertino.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';

import '../../router.dart';

class EnrollNowModel extends BaseModel {
  void enrollNow(String enrollNum) {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
        print("enrollNum : $enrollNum");
        if (enrollNum.isNotEmpty) {
          var status = await API.setEnrollNow(enrollNum);
          if (status == true) {
            Navigator.of(context).pushNamedAndRemoveUntil(
                Screen.TabManagerView.toString(),
                (Route<dynamic> route) => false);
          } else {
            showToast("Class Code is Invalid!", context);
          }
        } else {
          showToast("Please Enter Class Code!", context);
        }
        setState(ViewState.Idle);
      }
    });
  }
}
