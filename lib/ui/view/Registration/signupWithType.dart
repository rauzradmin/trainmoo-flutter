import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/ui/router.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';
import 'package:trainmoo/util/image_path.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';

class SignupWithTypeView extends StatefulWidget {
  @override
  _SignupWithTypeView createState() => _SignupWithTypeView();
}

class _SignupWithTypeView extends State<SignupWithTypeView>
    with BaseCommonWidget {
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: Palette.screenBgColor, body: _getBody(context)));
  }

  Widget _getBody(context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 14),
          child: GestureDetector(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Container(
              alignment: Alignment.topLeft,
              child: Icon(Icons.arrow_back_ios),
            ),
          ),
        ),
        _getBaseContainer(context),
      ],
    );
  }

  Widget _getBaseContainer(context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 14),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, Screen.NoticeView.toString());
            },
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 14),
              child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: Column(
                    children: [
                      UIHelper.getVerticalSpaceOf(10),
                      Center(
                        child: Text(
                          'I am a school administrator',
                          style: AppTextStyle.getDynamicFontStyle(
                              Colors.black, 14, FontType.SemiBol),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 15),
                        child: Image.asset(
                          ImagePath.ICON_SCHOOL,
                          height: 100,
                          width: 97,
                        ),
                      ),
                      UIHelper.getVerticalSpaceOf(10),
                      Center(
                        child: Text(
                          'Register School',
                          style: AppTextStyle.getDynamicFontStyle(
                              Colors.blue[300], 14, FontType.SemiBol),
                        ),
                      ),
                      UIHelper.getVerticalSpaceOf(15)
                    ],
                  )),
            ),
          ),
          UIHelper.getVerticalSpaceOf(25),
          GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, Screen.EnrollNowView.toString());
            },
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 14),
              child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: Column(
                    children: [
                      UIHelper.getVerticalSpaceOf(10),
                      Center(
                        child: Text(
                          'I am a student',
                          style: AppTextStyle.getDynamicFontStyle(
                              Colors.black, 14, FontType.SemiBol),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 15),
                        child: Image.asset(
                          ImagePath.ICON_ENROLLNOW,
                          height: 100,
                          width: 97,
                        ),
                      ),
                      UIHelper.getVerticalSpaceOf(10),
                      Center(
                        child: Text(
                          'Enroll Now',
                          style: AppTextStyle.getDynamicFontStyle(
                              Colors.blue[300], 14, FontType.SemiBol),
                        ),
                      ),
                      UIHelper.getVerticalSpaceOf(15)
                    ],
                  )),
            ),
          ),
        ],
      ),
    );
  }
}
