import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:trainmoo/model/user_signup_model.dart';
import 'package:trainmoo/shared_preference/user_preference.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';

import '../../router.dart';

class RegistrationViewModel extends BaseModel {
  UserSignUpModel userRegModel;

  validDate(String firstName, String lastName, String emailOrMobileNumber,
      String password, BuildContext context) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (firstName.isEmpty &&
        lastName.isEmpty &&
        emailOrMobileNumber.isEmpty &&
        password.isEmpty) {
      showToast("Please fill the details", context);
    } else if (firstName.isEmpty) {
      showToast("Please enter the FirstName", context);
    } else if (lastName.isEmpty) {
      showToast("Please enter the LastName", context);
    } else if (emailOrMobileNumber.isEmpty) {
      showToast("Please enter the emailOrMobileNumber", context);
    } else if (password.isEmpty) {
      showToast("Please enter the password", context);
    }
//    else if (!regex.hasMatch(emailOrMobileNumber) || emailOrMobileNumber.length >12 || emailOrMobileNumber.length<10 ) {
//      showToast("Incorrect EmailOrMobile", context);
//    }
    else if (password.length < 8) {
      showToast("Password length must be greater than 8", context);
    } else {
//      registerUser(firstName,lastName,emailOrMobileNumber,password);
      registerUser(firstName, lastName, emailOrMobileNumber, password);
    }
  }

  void registerUser(String firstName, String lastName,
      String emailOrMobileNumber, String password) {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
        userRegModel = await API.registerUser(
            firstName, lastName, emailOrMobileNumber, password);
        if (userRegModel != null && userRegModel.user != null) {
          UserPreference.saveUserDetails(userRegModel.user);
          UserPreference.saveUserName(userRegModel.user.name);

          Navigator.of(context).pushNamedAndRemoveUntil(
              Screen.TabManagerView.toString(), (Route<dynamic> route) => false);
        } else {
          if (userRegModel != null && userRegModel.message != null) {
            showToast(userRegModel.message.toString(), context);
          } else {
            showToast("Something Went Wrong Please try again later", context);
          }
        }
        setState(ViewState.Idle);
      }
    });
  }
}
