import 'package:flutter/material.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';
import 'package:trainmoo/ui/view/Registration/registration_view_model.dart';
import 'package:trainmoo/ui/widget/regular_button.dart';
import 'package:trainmoo/ui/widget/regular_text_form_field.dart';
import 'package:trainmoo/util/image_path.dart';

import '../../router.dart';

class RegistrationView extends StatefulWidget {
  @override
  _RegistrationViewState createState() => _RegistrationViewState();
}

class _RegistrationViewState extends State<RegistrationView>
    with BaseCommonWidget {
  @override
  TextEditingController firstNameController = new TextEditingController();
  TextEditingController lastNameController = new TextEditingController();
  TextEditingController emailOrMobileController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  Widget build(BuildContext context) {
    return BaseView<RegistrationViewModel>(builder: (context, model, child) {
      return SafeArea(
          child: Scaffold(
              backgroundColor: Palette.screenBgColor, body: _getBody(model)));
    });
  }

  Widget _getBody(RegistrationViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(RegistrationViewModel model) {
    return Padding(
      padding: EdgeInsets.only(left: 34, right: 34),
      child: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 45),
            child: Image.asset(
              ImagePath.ICON_APP,
              height: 100,
              width: 97,
            ),
          ),
          UIHelper.getVerticalSpaceOf(22),
          Center(
            child: Text(
              AppLocalizations.of(context).trainMoo,
              style: AppTextStyle.getDynamicFontStyle(
                  Colors.black, 34, FontType.SemiBol),
            ),
          ),
          UIHelper.getVerticalSpaceOf(35),
          Column(
            children: <Widget>[
              RegularTextFormField(
                hintText: AppLocalizations.of(context).firstName,
                textEditingController: firstNameController,
                textAlign: TextAlign.center,
                inputType: TextInputType.text,
              ),
              UIHelper.getVerticalSpaceOf(10),
              RegularTextFormField(
                hintText: AppLocalizations.of(context).lastName,
                textEditingController: lastNameController,
                textAlign: TextAlign.center,
                inputType: TextInputType.text,
              ),
              UIHelper.getVerticalSpaceOf(10),
              RegularTextFormField(
                hintText: AppLocalizations.of(context).emailOrMobile,
                textEditingController: emailOrMobileController,
                textAlign: TextAlign.center,
                inputType: TextInputType.text,
              ),
              UIHelper.getVerticalSpaceOf(10),
              RegularTextFormField(
                hintText: AppLocalizations.of(context).password,
                textEditingController: passwordController,
                textAlign: TextAlign.center,
                inputType: TextInputType.text,
                obscureText: true,
              ),
//              Padding(
//                padding: const EdgeInsets.only(top: 10,bottom: 5),
//                child: Center(
//                  child: Text(
//                    AppLocalizations.of(context).detail,
//                    style: AppTextStyle.getDynamicFontStyle(
//                        Palette.listColor, 13, FontType.RobotoRegular),
//                  ),
//                ),
//              ),
              RegularButton(
                buttonLabel: AppLocalizations.of(context).signUp,
                onTap: () {
                  model.validDate(
                      firstNameController.text,
                      lastNameController.text,
                      emailOrMobileController.text,
                      passwordController.text,
                      context);
                },
              ),
            ],
          ),
          UIHelper.getVerticalSpaceOf(87),
          Center(
              child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                AppLocalizations.of(context).alreadyHaveAccount,
                style: AppTextStyle.getDynamicFontStyle(
                    Palette.hintTextColor, 13, FontType.Medium),
              ),
              GestureDetector(
                child: Text(
                  AppLocalizations.of(context).loginHere,
                  style: AppTextStyle.getDynamicFontStyle(
                      Palette.accentColor, 13, FontType.Medium),
                ),
                onTap: () {
                  Navigator.pushNamed(context, Screen.Login.toString());
                },
              ),
            ],
          )),
          UIHelper.getVerticalSpaceOf(78),
        ],
      ),
    );
  }
}
