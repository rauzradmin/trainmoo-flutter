import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';
import 'package:trainmoo/ui/view/Registration/EnrollNowModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';

class EnrollNowView extends StatefulWidget {
  @override
  _EnrollNowViewState createState() => _EnrollNowViewState();
}

class _EnrollNowViewState extends State<EnrollNowView> with BaseCommonWidget {
  TextEditingController enrollnoController = new TextEditingController();
  Widget build(BuildContext context) {
    return BaseView<EnrollNowModel>(builder: (context, model, child) {
      return SafeArea(
          child: Scaffold(
              backgroundColor: Palette.screenBgColor,
              body: _getBody(context, model)));
    });
  }

  Widget _getBody(context, model) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 14, right: 14),
          //decoration: BoxDecoration(border: Border.all(width: 1)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                //  padding: EdgeInsets.only(left: 14),
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    child: Icon(Icons.arrow_back_ios),
                  ),
                ),
              ),
              UIHelper.getHorizontalSpaceOf(10),
              Text(
                'Enroll With Class Code',
                style: AppTextStyle.getDynamicFontStyle(
                    Colors.black, 20, FontType.SemiBol),
              ),
            ],
          ),
        ),
        _getBaseContainer(context, model),
      ],
    );
  }

  Widget _getBaseContainer(context, model) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 14),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          // Card(
          //   elevation: 3,
          //   shape:
          //       RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          //   child:
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                UIHelper.getVerticalSpaceOf(25),
                Text(
                  'Class Code',
                  style: AppTextStyle.getDynamicFontStyle(
                      Colors.black, 20, FontType.SemiBol),
                ),
                UIHelper.getVerticalSpaceOf(10),
                Container(
                  // padding: EdgeInsets.symmetric(horizontal: 15),
                  child: TextField(
                    controller: enrollnoController,
                    decoration: InputDecoration(
                        hintText: "EG:CSE001...",
                        border: new OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                            borderSide: new BorderSide(color: Colors.teal))),
                  ),
                ),
                UIHelper.getVerticalSpaceOf(25),
                Center(
                  child: MaterialButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5)),
                      onPressed: () {
                        model.enrollNow(enrollnoController.text);
                      },
                      elevation: 3,
                      height: 45,
                      color: Colors.blue[600],
                      child: Text(
                        "Enroll",
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      )),
                ),
                UIHelper.getVerticalSpaceOf(15),
                Text(
                  'Note: If your school has already enrolled you, you must have received an email or SMS. Check your account if you have received. If not, please check with your school administrator.',
                  style: AppTextStyle.getDynamicFontStyle(
                      Colors.black, 15, FontType.Medium),
                ),
              ],
            ),
          ),
          // ),
        ],
      ),
    );
  }
}
