import 'package:trainmoo/model/GetCommentModel.dart';
import 'package:trainmoo/model/MaterialModel.dart';
import 'package:trainmoo/model/Question.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';

class StudentQuizModel extends BaseModel {
  bool isFirstTime = false;
  List<Question> questionList = [];
  MaterialModel materialObject;
  List<Replies> getPostCommentList = [];
  int currentQuestion=0;

  GetCommonModel getAllCommentsList;
  Map<String, String> header;
  int userType;
  String screenType;

  init() async {
    if (!isFirstTime) {
      getAllQuestionsFromApi();
      isFirstTime = true;
    }
  }

  void getAllQuestionsFromApi() {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
        questionList = await API.getQuestionsFromApi();
        setState(ViewState.Idle);
      }
    });
  }
}
