import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/view/studentUserModuel/student_quiz_view_model.dart';

import '../../router.dart';

class StudentQuizResult extends StatefulWidget {
  final String percentage;
  StudentQuizResult(this.percentage);
  @override
  _StudentQuizResultState createState() => _StudentQuizResultState();
}

class _StudentQuizResultState extends State<StudentQuizResult>
    with BaseCommonWidget {
  int isBorderGreen;
  int selectItem;
  int counter = 0;
  var selectAnsList = new List();

  @override
  Widget build(BuildContext context) {
    return SafeArea(child: _getBody());
  }

  Widget _getBody() {
    return Stack(
      children: <Widget>[_getBaseContainer()],
    );
  }

  _getBaseContainer() {
    // print("question list ${model.questionList}");
    double width = MediaQuery.of(context).size.width;

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        commonAppBar('Quiz Result', context),
        Container(
            padding: EdgeInsets.only(left: 30, right: 30, bottom: 15, top: 50),
            child: Text("Praeset dapibus, neque id cursus faucibus!",
                textAlign: TextAlign.center,
                style: AppTextStyle.getDynamicFontStyle(
                    Palette.lightBlackColor, 25, FontType.Medium))),
        Expanded(
          child: Center(
            child: Card(
              elevation: 8,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(width * 0.20)),
              child: Container(
                  width: width * 0.40,
                  height: width * 0.40,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(widget.percentage,
                          style: AppTextStyle.getDynamicFontStyle(
                              Palette.lightBlackColor,
                              45,
                              FontType.RobotoBold)),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 10),
                        color: Colors.grey,
                        width: width * 0.25,
                        height: 0.5,
                      ),
                      Text("100",
                          style: AppTextStyle.getDynamicFontStyle(
                              Palette.lightBlackColor, 25, FontType.RobotoBold))
                    ],
                  )),
            ),
          ),
        ),
        Container(
            padding: EdgeInsets.only(left: 30, right: 30, bottom: 50, top: 15),
            child: Text(
                "Answer : Aliquam erat volupat. Nam dui mi. Tincidunt auis.",
                textAlign: TextAlign.center,
                style: AppTextStyle.getDynamicFontStyle(
                    Palette.secondaryTextColor, 15, FontType.RobotoMedium))),
        Container(
          height: 60,
          padding: EdgeInsets.only(left: 24, right: 24, bottom: 5, top: 10),
          width: MediaQuery.of(context).size.width,
          child: RaisedButton(
            child: Text(
              'Done',
              style: TextStyle(color: Colors.white),
            ),
            color: Color.fromRGBO(82, 178, 254, 1),
            padding: EdgeInsets.symmetric(horizontal: 10),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            //elevation: 5,
            onPressed: () {
              Navigator.pushNamed(context, Screen.ClassListView.toString());
            },
          ),
        ),
      ],
    );
  }
}
