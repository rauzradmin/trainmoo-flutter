import 'package:flutter/material.dart';
import 'package:trainmoo/model/StudentModel.dart';
import 'package:trainmoo/model/studentModuel/grade/StudentGradeModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:trainmoo/ui/view/studentUserModuel/student_grade_class_detail.dart';
import 'package:trainmoo/util/image_path.dart';

class StudentGradeView extends StatelessWidget with BaseCommonWidget {
  final StudentGradeModel studentModel;

  const StudentGradeView({Key key, this.studentModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return studentItem(studentModel, context);
  }

  studentItem(StudentGradeModel student, BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => StudentGradeClassListView(
                    mClassList: student.classes,
                    className: student.name,
                  )),
        );
      },
      child: Container(
        height: 90,
        child: Card(
          elevation: 1,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(UIHelper.cardRadius)),
          child: Padding(
            padding:
                const EdgeInsets.only(left: 16, top: 12, bottom: 12, right: 16),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          student.name,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: AppTextStyle.getDynamicFontStyle(
                              Colors.black, 16, FontType.SemiBol),
                        ),
                        Text('11 Dec 2019 - 11 Dec 2020',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: AppTextStyle.getDynamicFontStyle(
                                Palette.listColor, 13, FontType.RobotoRegular)),
                      ],
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      student.maxMarks.toString(),
                      //+ "%",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: AppTextStyle.getDynamicFontStyle(
                          Colors.black, 16, FontType.SemiBol),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
