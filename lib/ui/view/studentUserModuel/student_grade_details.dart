import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/model/studentModuel/grade/Classe.dart';
import 'package:trainmoo/model/studentModuel/grade/Group.dart';
import 'package:trainmoo/model/studentModuel/grade/studentGrdeListViewModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';

class StudentGradeDetailsView extends StatefulWidget {
  final Group groupDetails;
  const StudentGradeDetailsView({Key key, this.groupDetails}) : super(key: key);
  @override
  _StudentGradeDetailsViewState createState() =>
      _StudentGradeDetailsViewState();
}

class _StudentGradeDetailsViewState extends State<StudentGradeDetailsView>
    with BaseCommonWidget {
  @override
  Widget build(BuildContext context) {
    return BaseView<StudentGradeListViewModel>(
        builder: (context, model, child) {
      return SafeArea(child: _getBody(model));
    });
  }

  Widget _getBody(StudentGradeListViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(StudentGradeListViewModel model) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        commonAppBar(widget.groupDetails.name, context),
        getStudentList(model)
      ],
    );
  }

  getStudentList(StudentGradeListViewModel model) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      margin: const EdgeInsets.only(left: 24, right: 24),
      padding: EdgeInsets.only(top: 10, bottom: 10),
      child: Card(
        elevation: 1,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(UIHelper.cardRadius)),
        child: Padding(
          padding: const EdgeInsets.only(bottom: 15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(width: 1, color: Colors.grey))),
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 20),
                      width: width * 0.55,
                      child: Text(
                        "Name",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: AppTextStyle.getDynamicFontStyle(
                            Colors.black, 15, FontType.RobotoBold),
                      ),
                    ),
                    Container(
                      width: width * 0.15,
                      child: Center(
                        child: Text(
                          "score",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: AppTextStyle.getDynamicFontStyle(
                              Palette.lightBlackColor, 15, FontType.RobotoBold),
                        ),
                      ),
                    ),
                    Container(
                      width: width * 0.15,
                      child: Center(
                        child: Text(
                          "Out of",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: AppTextStyle.getDynamicFontStyle(
                              Palette.lightBlackColor, 15, FontType.RobotoBold),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              _getClassItem(widget.groupDetails),
            ],
          ),
        ),
      ),
    );
  }

  Widget _getClassItem(Group groups) {
    double width = MediaQuery.of(context).size.width;

    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: ListView.builder(
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          itemCount: groups.tests.length,
          itemBuilder: (BuildContext context, int i) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 20),
                  width: width * 0.55,
                  child: Text(
                    groups.tests[i].name,
                    style: AppTextStyle.getDynamicFontStyle(
                        Colors.black, 13, FontType.Regular),
                  ),
                ),
                Container(
                  width: width * 0.15,
                  child: Center(
                    child: Text(
                      groups.tests[i].score != "null"
                          ? groups.tests[i].score
                          : "0",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: AppTextStyle.getDynamicFontStyle(
                          Palette.lightBlackColor, 13, FontType.Regular),
                    ),
                  ),
                ),
                Container(
                  width: width * 0.15,
                  child: Center(
                    child: Text(
                      groups.tests[i].maxMarks.toString(),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: AppTextStyle.getDynamicFontStyle(
                          Palette.lightBlackColor, 13, FontType.Regular),
                    ),
                  ),
                ),
              ],
            );
          }),
    );
  }
}
