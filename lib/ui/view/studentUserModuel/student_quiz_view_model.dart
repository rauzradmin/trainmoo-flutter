import 'package:flutter/cupertino.dart';
import 'package:trainmoo/model/GetCommentModel.dart';
import 'package:trainmoo/model/MaterialModel.dart';
import 'package:trainmoo/model/Question.dart';
import 'package:trainmoo/shared_preference/user_preference.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:intl/intl.dart';

import '../../router.dart';

class StudentQuizModel extends BaseModel {
  bool isFirstTime = false;
  List<Question> questionList = [];
  MaterialModel materialObject;
  List<Replies> getPostCommentList = [];
  int currentQuestion = 0;
  double percentage = 0;
  double correctAns = 0;
  List assessmentList = [];

  GetCommonModel getAllCommentsList;
  Map<String, String> header;
  int userType;
  String screenType;
  String assessmentsID;

  init(String assessmentsID) async {
    if (!isFirstTime) {
      getQuizQuestion(assessmentsID);
      print("print  assessmentsid $assessmentsID");
      isFirstTime = true;
    }
  }

  void setCurrentQuestion() {
    if (currentQuestion != questionList.length - 1) {
      setState(ViewState.Busy);
      currentQuestion = currentQuestion + 1;
      var length = questionList.length - 1;
      percentage = length == currentQuestion ? 1.0 : currentQuestion / length;
      setState(ViewState.Idle);
    }
  }

  void getQuizQuestion(String assessmentsID) {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        print("assessmentsID : $assessmentsID");
        setState(ViewState.Busy);
        var questionData = await API.getQuizQuestion(assessmentsID);
        print("questionData : $questionData");
        questionList = Question.fromJsonArray(questionData);
        var length = questionList.length;
        print("length : $length");
        percentage = length == 0 ? 1.0 : 1 / length;
        setState(ViewState.Idle);
      }
    });
  }

  Future<void> saveQuiz(Question question, List selectItem,
      int currentQuestionIndex, String answer) async {
    var userId = await UserPreference.getUserId();
    var date = new DateTime.now();
    var answeredAt = DateFormat("yyyy-MM-ddTHH:mm:ss").format(date);
    var assessment = {
      "category": question.category,
      "created_at": question.created_at,
      "created_by": question.created_by,
      "dashOptions": question.dashOptions,
      "explanation": question.explanation,
      "explanationFiles": question.explanationFiles,
      "explanationIsCode": question.explanationIsCode,
      "files": question.files,
      "index": question.index,
      "isCode": question.isCode,
      "level": question.level,
      "marks": question.marks,
      "options": question.options,
      "post": question.post,
      "question": question.question,
      "seqno": question.seqno,
      "type": question.type,
      "updated_at": question.updated_at,
      "__v": question.v,
      "_id": question.id
    };

    var status = "not answered";
    var options = [];
    //var i;
    // for (i = 0; i < question.options.length; i++) {
    //   if (i == selectItem) {
    if (question.type == "MC") {
      print("length : ${question.options.length}");
      print("selectItem : $selectItem");

      selectItem.forEach((element) {
        var item = question.options[element];
        if (item.answer) {
          print(" answer    : ${item.answer}");
          status = "correct";
          if (question.ansMultipal) {
            correctAns += (1 / selectItem.length).truncateToDouble();
          } else {
            correctAns += 1;
          }
        }
        options.add(item.seqno);
      });

      print("status : $status");
    }

    // }
    // }
    print("answeredAt : $answeredAt");
    print("assessmentList : $assessmentList");

    var data = {
      "answered": true,
      "answeredAt": answeredAt,
      "assessment": assessment,
      "assessmentPost": question.post,
      "options": question.type == "SA" ? [] : options,
      "status": status,
      "type": question.type,
      "user": userId
    };
    if (question.type == "SA") {
      data['answer'] = answer;
    }
    assessmentList.add(data);

    var result;
    !(currentQuestionIndex != questionList.length - 1)
        ? result = await API.submitQuiz(assessmentList, true)
        : result = await API.submitQuiz(data, true);
    print("correct ans : $correctAns");

    print("response ===> ${result['result']}");
    print(
        "currentQuestionIndex != questionList.length - 1 ,${currentQuestionIndex == questionList.length - 1}");
    if (currentQuestionIndex == questionList.length - 1) {
      var percentage = correctAns == 0
          ? "0"
          : ((correctAns / questionList.length) * 100).round().toString();
      Navigator.pushNamed(context, Screen.StudentQuizResult.toString(),
          arguments: {"percentage": percentage.toString()});
    }
    // else {
    //   var percentage = correctAns == 0
    //       ? "0"
    //       : ((correctAns / questionList.length) * 100).round().toString();
    //   print("
    //   percentage ===> $percentage");
    //   var response = await API.getAssesment(question.post);
    //   print("response ===========>>>>: $response");
    //   var data = {
    //     "anonymous": response['anonymous'],
    //     "assessment": {
    //       "answered": response['assessment']['count'],
    //       //  "attempts": 12,
    //       "correct": correctAns,
    //       "retake": response['assessment']['retake'],
    //       "status": "fail",
    //       //  "timeSpent": 18,
    //     },
    //     "author": response['author'],
    //     "class": response['class']['_id'],
    //     "comments": 0,
    //     "commentsDisabled": false,
    //     "created_at": response['created_at'],
    //     "files": [],
    //     "graded": 0,
    //     "isCode": false,
    //     "likes": 0,
    //     "locked": false,
    //     "minified": true,
    //     "parent": response['replies'][0]['parent'],
    //     "pinned": false,
    //     "replies": [],
    //     "responseType": "submission",
    //     "school": response['school']['_id'],
    //     "solutions": 0,
    //     "submissionDetails": {"results": []},
    //     "submissions": 0,
    //     "submittable": true,
    //     "task": {"grade": 0, "additionalMarks": 0, "additionalMaxMarks": 0},
    //     "type": "Assessment",
    //     "updated_at": answeredAt,
    //     "users": [],
    //     "views": 0,
    //     "__v": 0,
    //     "_id": question.post
    //   };
    //   print("data ======>   $data");
    //   var response2 = await API.submitAssesment(data);
    //   Navigator.pushNamed(context, Screen.StudentQuizResult.toString(),
    //       arguments: {"percentage": percentage.toString()});
    // }
  }

  // void getAllQuestionsFromApi() {
  //   Utility.isInternetAvailable().then((isConnected) async {
  //     if (isConnected) {
  //       setState(ViewState.Busy);
  //       questionList = await API.getQuestionsFromApi();
  //       setState(ViewState.Idle);
  //     }
  //   });
  // }
}
