//import 'package:flutter/cupertino.dart';
//import 'package:flutter/material.dart';
//import 'package:trainmoo/model/FileModel.dart';
//import 'package:trainmoo/model/MaterialModel.dart';
//import 'package:trainmoo/shared_preference/user_preference.dart';
//import 'package:trainmoo/ui/base/base_common_widget.dart';
//import 'package:trainmoo/ui/base/base_view.dart';
//import 'package:trainmoo/ui/localization/app_localization.dart';
//import 'package:trainmoo/ui/network/API.dart';
//import 'package:trainmoo/ui/theme/app_text_styles.dart';
//import 'package:trainmoo/ui/theme/palette.dart';
//import 'package:trainmoo/ui/util/util.dart';
//import 'package:trainmoo/ui/view/Material/MaterialDetail/material_detail_view_model.dart';
//import 'package:trainmoo/ui/widget/DetailView.dart';
//import 'package:trainmoo/ui/widget/FileView.dart';
//import 'package:trainmoo/ui/widget/text_form_field_with_padding.dart';
//
//
//class MaterialDetailView extends StatefulWidget {
//  final MaterialModel materialObject;
//  final String screenType;
//
//  const MaterialDetailView({Key key, this.materialObject, this.screenType})
//      : super(key: key);
//
//  @override
//  _MaterialDetailViewState createState() => _MaterialDetailViewState();
//}
//
//class _MaterialDetailViewState extends State<MaterialDetailView>
//    with BaseCommonWidget {
//  int userType;
//
//  TextEditingController _commentController = new TextEditingController();
//
//  @override
//  Widget build(BuildContext context) {
//    return BaseView<MaterialDetailViewModel>(builder: (context, model, child) {
//      model.init(widget.materialObject, widget.screenType);
//      return SafeArea(
//          child: Scaffold(
//              backgroundColor: Palette.screenBgColor, body: _getBody(model)));
//    });
//  }
//
//  @override
//  void initState() {
//    _getUserType();
//  }
//
//  Widget _getBody(MaterialDetailViewModel model) {
//    return Stack(
//      children: <Widget>[_getBaseContainer(model), getProgressBar(model.state)],
//    );
//  }
//
//  Widget _getBaseContainer(MaterialDetailViewModel model) {
//    return Container(
//      child: Column(
//        mainAxisAlignment: MainAxisAlignment.start,
//        crossAxisAlignment: CrossAxisAlignment.start,
//        children: <Widget>[
//          commonAppBar('', context),
//          Expanded(
//              child: SingleChildScrollView(
//                child: Container(
//                  padding: const EdgeInsets.only(
//                      left: 24, right: 24, bottom: 5, top: 10),
//                  child: Column(
//                    mainAxisAlignment: MainAxisAlignment.start,
//                    crossAxisAlignment: CrossAxisAlignment.start,
//                    children: <Widget>[
//                      DetailView(
//                        title: widget.materialObject.text,
//                        time: widget.materialObject.displayTime,
//                        details: widget.materialObject.text,
//                        files: widget.screenType == API.quizScreenType
//                            ? null
//                            : widget.materialObject.files,
//                        header: model.header,
//                      ),
//                      widget.screenType == API.assignmentScreenType
//                          ? Container(
//                        padding: EdgeInsets.only(bottom: 16),
//                        child: GestureDetector(
//                          child: reviewButton(
//                              userType != null && userType == 1
//                                  ? AppLocalizations.of(context)
//                                  .submitAssignment
//                                  : AppLocalizations.of(context)
//                                  .reviewAssignment,
//                              '${widget.materialObject.submissions} Submitted | ${widget.materialObject.pending} Pending'),
//                          onTap: () {
//                            if (userType == 1) {
//                              Navigator.pushNamed(
//                                  context, Screen.CreateMaterial.toString());
//                            } else {
//                              Navigator.pushNamed(
//                                  context, Screen.ReviewAssignment.toString(),
//                                  arguments: {
//                                    "assignment_id": widget.materialObject.id
//                                  });
//                            }
//                          },
//                        ),
//                      )
//                          : Container(),
//                      widget.screenType == API.quizScreenType
//                          ? Container(
//                        margin: EdgeInsets.only(top: 16, bottom: 16),
//                        child: GestureDetector(
//                          child: reviewButton(
//                              userType != null && userType == 1
//                                  ? AppLocalizations.of(context).takeQuiz
//                                  : AppLocalizations.of(context)
//                                  .submitAssignment,
//                              '${widget.materialObject.submissions} Submitted | ${widget.materialObject.pending} Pending'),
//                          onTap: () {
//                            if(userType==1)
//                            {
//                              Navigator.pushNamed(
//                                  context, Screen.ReviewQuiz.toString(),
//                                  arguments: {
//                                    'quiz_id': widget.materialObject.id
//                                  });
//                            }
//
//                          },
//                        ),
//                      )
//                          : Container(),
//                      Text(AppLocalizations.of(context).comment,
//                          style: AppTextStyle.commentTextStyle()),
//                      Padding(
//                        padding: const EdgeInsets.only(top: 10.0),
//                        child: RegularTextFormFieldWithPadding(
//                          textInputAction: TextInputAction.done,
//                          onTextSubmitted: (value) {
//                            model.postComment(_commentController.text);
//                            _commentController.text = "";
//                          },
//                          textEditingController: _commentController,
//                          hintText: AppLocalizations.of(context).writeSomethingHere,
//                          inputType: TextInputType.text,
//                        ),
//                      ),
//                      model.getPostCommentList != null &&
//                          model.getPostCommentList.length > 0
//                          ? ListView.builder(
//                        physics: ClampingScrollPhysics(),
//                        shrinkWrap: true,
//                        itemCount: model.getPostCommentList.length,
//                        itemBuilder: (BuildContext context, int ind) {
//                          return commentItemWidget(
//                            model.getPostCommentList[ind].author.avatar !=
//                                null
//                                ? model
//                                .getPostCommentList[ind].author.avatar.src
//                                : '',
//                            model.getPostCommentList[ind].author.name
//                                .firstname +
//                                " " +
//                                model.getPostCommentList[ind].author.name
//                                    .lastname,
//                            model.getPostCommentList[ind].text,
//                            Utility.getDisplayFormatDate(
//                                model.getPostCommentList[ind].createdAt),
//                            model.getPostCommentList[ind].files,
//                            model.header,
//                          );
//                        },
//                      )
//                          : Container(),
//                    ],
//                  ),
//                ),
//              ))
//        ],
//      ),
//    );
//  }
//
//  commentItemWidget(String imagePath, String name, String comment, String date,
//      List<FileModel> files, Map<String, String> header) {
//    if (imagePath.contains('https')) {
//      imagePath = imagePath;
//    } else {
//      imagePath = API.imageURL + imagePath;
//    }
//    return Padding(
//      padding: const EdgeInsets.only(top: 20, bottom: 16),
//      child: Container(
//        child: Row(
//          mainAxisAlignment: MainAxisAlignment.start,
//          crossAxisAlignment: CrossAxisAlignment.start,
//          children: <Widget>[
//            loadCommentImage(imagePath, 50.0, 50.0),
//            Padding(
//              padding: const EdgeInsets.only(left: 15),
//              child: Column(
//                mainAxisAlignment: MainAxisAlignment.center,
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: <Widget>[
//                  Container(
//                    height: 29,
//                    child: Text(
//                      name,
//                      overflow: TextOverflow.ellipsis,
//                      style: AppTextStyle.getDynamicFontStyle(
//                          Colors.black, 16, FontType.Medium),
//                    ),
//                  ),
//                  Container(
//                    child: Text(
//                      comment,
//                      overflow: TextOverflow.ellipsis,
//                      style: AppTextStyle.getDynamicFontStyle(
//                        Colors.black,
//                        13,
//                        FontType.RobotoRegular,
//                      ),
//                    ),
//                  ),
//                  new FileView(
//                    files: files,
//                    headers: header,
//                  ),
//                  Padding(
//                    padding: const EdgeInsets.only(top: 15),
//                    child: Text(
//                      date,
//                      style: AppTextStyle.getDynamicFontStyle(
//                          Palette.listColor, 14, FontType.RobotoMedium),
//                    ),
//                  ),
//                ],
//              ),
//            )
//          ],
//        ),
//      ),
//    );
//  }
//
//  void _getUserType() async {
//    userType=await UserPreference.getUserType();
//    setState(() {
//
//    });
//  }
//}
