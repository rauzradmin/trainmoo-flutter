import 'package:flutter/material.dart';
import 'package:trainmoo/model/studentModuel/grade/studentGrdeListViewModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'grede_one_item_view.dart';

class StudentGradeListView extends StatefulWidget {
  final String classId;

  const StudentGradeListView({Key key, this.classId}) : super(key: key);

  @override
  _StudentGradeListViewState createState() => _StudentGradeListViewState();
}

class _StudentGradeListViewState extends State<StudentGradeListView>
    with BaseCommonWidget {
  @override
  Widget build(BuildContext context) {
    return BaseView<StudentGradeListViewModel>(
        builder: (context, model, child) {
      model.init(widget.classId);

      return SafeArea(child: _getBody(model));
    });
  }

  Widget _getBody(StudentGradeListViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(StudentGradeListViewModel model) {
    return Column(
      children: <Widget>[
        commonAppBar(AppLocalizations.of(context).myGrades, context),
        Expanded(
            child: model.studentsGradeList.length > 0 &&
                    model.studentsGradeList != null
                ? getStudentList(model)
                : Container())
      ],
    );
  }

  getStudentList(StudentGradeListViewModel model) {
    return Container(
      margin: const EdgeInsets.only(left: 24, right: 24),
      padding: EdgeInsets.only(top: 20),
      child: ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: model.studentsGradeList.length,
        itemBuilder: (BuildContext context, int ind) {
          return StudentGradeView(studentModel: model.studentsGradeList[ind]);
        },
      ),
    );
  }
}
