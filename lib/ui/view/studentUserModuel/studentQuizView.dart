import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:trainmoo/model/Question.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:trainmoo/ui/view/studentUserModuel/student_quiz_view_model.dart';

class StudentQuizView extends StatefulWidget {
  final String quizTitle;
  final String quizId;
  const StudentQuizView({this.quizTitle, this.quizId});
  @override
  _StudentQuizViewState createState() => _StudentQuizViewState();
}

class _StudentQuizViewState extends State<StudentQuizView>
    with BaseCommonWidget {
  int isBorderGreen;
  List selectItem = [];
  int counter = 0;
  String answer;
  var selectAnsList = new List();
  @override
  Widget build(BuildContext context) {
    return BaseView<StudentQuizModel>(builder: (context, model, child) {
      model.init(widget.quizId);
      return SafeArea(child: _getBody(model));
    });
  }

  Widget _getBody(StudentQuizModel model) {
    return Stack(
      children: <Widget>[_getBaseContainer(model), getProgressBar(model.state)],
    );
  }

  _getBaseContainer(StudentQuizModel model) {
    // print("question list ${model.questionList}");
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        commonAppBar('', context),
        Expanded(
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(left: 24, right: 24, bottom: 5, top: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: Text(Utility.removeAllHtmlTags(widget.quizTitle),
                        maxLines: 1,
                        overflow: TextOverflow.clip,
                        style: AppTextStyle.commonTitleStyle()),
                  ),
                  _getQuestion(model.questionList, model),
                ],
              ),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 24, right: 24, bottom: 15, top: 15),
          child: LinearPercentIndicator(
            // width: MediaQuery.of(context).size.width,
            lineHeight: 6.0,
            percent: model.percentage,
            backgroundColor: Colors.grey[300],
            progressColor: Color.fromRGBO(82, 178, 254, 1),
          ),
        ),
        Container(
          margin: EdgeInsets.only(bottom: Platform.isAndroid ? 15 : 0),
          height: 60,
          padding: EdgeInsets.only(left: 24, right: 24, bottom: 5, top: 10),
          width: MediaQuery.of(context).size.width,
          child: RaisedButton(
            child: Text(
              'Next',
              style: TextStyle(color: Colors.white),
            ),
            color: Color.fromRGBO(82, 178, 254, 1),
            padding: EdgeInsets.symmetric(horizontal: 10),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            //elevation: 5,
            onPressed: () {
              if (selectItem.length > 0 || answer != null) {
                model.saveQuiz(model.questionList[model.currentQuestion],
                    selectItem, model.currentQuestion, answer);
                model.setCurrentQuestion();
                setState(() {
                  //selectAnsList[counter] = selectItem;
                  counter = counter + 1;
                  selectItem = [];
                });
                print(
                    "select Ans list : ==>> ${selectAnsList[model.currentQuestion]}");
              }
            },
          ),
        ),
      ],
    );
  }

  Widget _getQuestion(List<Question> questionList, StudentQuizModel model) {
    if (questionList != null && questionList.length > 0) {
      int questionLength = questionList.length;
      if (model.currentQuestion == questionLength) {
        return Container();
      } else {
        print("file : ${questionList[model.currentQuestion].files}");
        double c_width = MediaQuery.of(context).size.width * 0.9;
        return Container(
          padding: const EdgeInsets.all(16.0),
          width: c_width,
          child: Column(
            children: <Widget>[
              Text(
                  'Question: ' +
                      Utility.removeAllHtmlTags(
                          questionList[model.currentQuestion].question),
                  style: AppTextStyle.getDynamicFontStyle(
                      Palette.lightBlackColor, 18, FontType.Medium)),
              questionList[model.currentQuestion].files.length > 0
                  ? Column(
                      children: List.generate(
                          questionList[model.currentQuestion].files.length,
                          (index) {
                      print(
                          "https://www.trainmoo.in${questionList[model.currentQuestion].files[index]['src']}");
                      return loadNetworkImage(
                          questionList[model.currentQuestion].files[index]
                              ['src'],
                          250,
                          Map());
                    }))
                  : Container(),
              questionList[model.currentQuestion].type == "SA"
                  ? TextField(
                      onChanged: (value) {
                        setState(() {
                          answer = value;
                        });
                      },
                      maxLines: null,
                      decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color.fromRGBO(82, 178, 254, 1),
                                  width: 2),
                              borderRadius: BorderRadius.circular(20)),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color.fromRGBO(82, 178, 254, 1)),
                              borderRadius: BorderRadius.circular(20))),
                    )
                  : questionList[model.currentQuestion].options.length > 0
                      ? _getOption(questionList, model)
                      : Container(),
            ],
          ),
        );
      }
    } else {
      return Container();
    }
  }

  _getOption(List<Question> questionList, StudentQuizModel model) {
    for (int i = 0;
        i < questionList[model.currentQuestion].options.length;
        i++) {
      return Column(
        children: List.generate(
            questionList[model.currentQuestion].options.length, (index) {
          return GestureDetector(
            onTap: () {
              // print("this is just");
              setState(() {
                print(
                    " this multiple ans : ${questionList[model.currentQuestion].ansMultipal}");
                if (questionList[model.currentQuestion].ansMultipal) {
                  var found = selectItem.where((element) => element == index);
                  if (found.length > 0) {
                    selectItem.remove(index);
                  } else {
                    selectItem.add(index);
                  }
                } else {
                  selectItem.clear();
                  selectItem.add(index);
                  print("value is selected : $selectItem}");
                }
              });
            },
            child: Container(
              margin: EdgeInsets.only(top: 5),
              child: Card(
                shape: RoundedRectangleBorder(
                    side: selectItem.indexOf(index) != -1
                        ? BorderSide(
                            width: 1.5, color: Color.fromRGBO(82, 178, 254, 1))
                        : BorderSide(width: 0, color: Colors.transparent),
                    borderRadius: BorderRadius.circular(5)),
                //elevation: 1,
                child: Container(
                  margin: EdgeInsets.only(top: 10, left: 20, bottom: 10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        questionList[model.currentQuestion]
                                .options[index]
                                .seqno
                                .toString() +
                            "     ",
                        overflow: TextOverflow.ellipsis,
                        style: AppTextStyle.getDynamicFontStyle(
                            selectItem == index
                                ? Color.fromRGBO(82, 178, 254, 1)
                                : Colors.black,
                            17,
                            FontType.SemiBol),
                      ),
                      Text(
                          Utility.removeAllHtmlTags(
                              questionList[model.currentQuestion]
                                  .options[index]
                                  .text),
                          overflow: TextOverflow.ellipsis,
                          style: AppTextStyle.getDynamicFontStyle(
                              Palette.lightBlackColor,
                              16,
                              FontType.RobotoRegular)),
                    ],
                  ),
                ),
              ),
            ),
          );
        }),
      );
    }
  }
}
