import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/model/studentModuel/grade/Classe.dart';
import 'package:trainmoo/model/studentModuel/grade/Group.dart';
import 'package:trainmoo/model/studentModuel/grade/studentGrdeListViewModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/router.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';

class StudentGradeClassListView extends StatefulWidget {
  final List<GradeClassModel> mClassList;
  final String className;

  const StudentGradeClassListView({Key key, this.mClassList, this.className})
      : super(key: key);

  @override
  _StudentGradeClassListViewState createState() =>
      _StudentGradeClassListViewState();
}

class _StudentGradeClassListViewState extends State<StudentGradeClassListView>
    with BaseCommonWidget {
  @override
  Widget build(BuildContext context) {
    return BaseView<StudentGradeListViewModel>(
        builder: (context, model, child) {
      return SafeArea(child: _getBody(model));
    });
  }

  Widget _getBody(StudentGradeListViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(StudentGradeListViewModel model) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        commonAppBar(widget.className, context),
        Expanded(
            child: widget.mClassList.length > 0 && widget.mClassList != null
                ? getStudentList(model)
                : Container())
      ],
    );
  }

  getStudentList(StudentGradeListViewModel model) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      margin: const EdgeInsets.only(left: 24, right: 24),
      padding: EdgeInsets.only(top: 10, bottom: 10),
      child: ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: widget.mClassList.length,
        itemBuilder: (BuildContext context, int ind) {
          return Container(
            child: Card(
              elevation: 1,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(UIHelper.cardRadius)),
              child: Padding(
                padding: const EdgeInsets.only(bottom: 15),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                      child: Text(
                        widget.mClassList[ind].name,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: AppTextStyle.getDynamicFontStyle(
                            Colors.black, 16, FontType.SemiBol),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          border: Border.symmetric(
                              vertical:
                                  BorderSide(width: 1, color: Colors.grey))),
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(left: 20),
                            width: width * 0.55,
                            child: Text(
                              "Name",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: AppTextStyle.getDynamicFontStyle(
                                  Colors.black, 14, FontType.RobotoBold),
                            ),
                          ),
                          Container(
                            width: width * 0.15,
                            child: Center(
                              child: Text(
                                "score",
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: AppTextStyle.getDynamicFontStyle(
                                    Palette.lightBlackColor,
                                    14,
                                    FontType.RobotoBold),
                              ),
                            ),
                          ),
                          Container(
                            width: width * 0.15,
                            child: Center(
                              child: Text(
                                "Out of",
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: AppTextStyle.getDynamicFontStyle(
                                    Palette.lightBlackColor,
                                    14,
                                    FontType.RobotoBold),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    _getClassItem(widget.mClassList[ind].groups, model),
                    // widget.mClassList[ind].groups != null &&
                    //         widget.mClassList[ind].groups.length > 0
                    //     ? Row(
                    //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //         children: <Widget>[
                    //           Text(
                    //             AppLocalizations.of(context).total,
                    //             maxLines: 1,
                    //             overflow: TextOverflow.ellipsis,
                    //             style: AppTextStyle.getDynamicFontStyle(
                    //                 Colors.black, 10, FontType.Bold),
                    //           ),
                    //           Text(
                    //             (widget.mClassList[ind].maxMarks).toString(),
                    //             maxLines: 1,
                    //             overflow: TextOverflow.ellipsis,
                    //             style: AppTextStyle.getDynamicFontStyle(
                    //                 Palette.lightBlackColor, 14, FontType.Bold),
                    //           ),
                    //         ],
                    //       )
                    //     : Container()
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _getClassItem(List<Group> groups, StudentGradeListViewModel model) {
    double width = MediaQuery.of(context).size.width;

    return ListView.builder(
      physics: ClampingScrollPhysics(),
      shrinkWrap: true,
      itemCount: groups.length,
      itemBuilder: (BuildContext context, int ind) {
        return Column(
          children: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.pushNamed(
                    context, Screen.StudentGradeDetailsView.toString(),
                    arguments: {"groupDetails": groups[ind]});
              },
              child: Container(
                padding: EdgeInsets.only(top: 10),
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    // Container(
                    //   // padding: EdgeInsets.only(left: 20),
                    //   width: width * 0.05,
                    //   child: Icon(model.selectDroupId == groups[ind].groupId
                    //       ? Icons.arrow_drop_down
                    //       : Icons.arrow_right),
                    // ),
                    Container(
                      padding: EdgeInsets.only(left: 10),
                      width: width * 0.55,
                      child: Text(
                        groups[ind].name,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: AppTextStyle.getDynamicFontStyle(
                            Palette.lightBlackColor, 12, FontType.SemiBol),
                      ),
                    ),
                    Container(
                      width: width * 0.15,
                      child: Center(
                        child: Text(
                          groups[ind].score != null
                              ? groups[ind].score.toString()
                              : "0",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: AppTextStyle.getDynamicFontStyle(
                              Palette.lightBlackColor, 12, FontType.SemiBol),
                        ),
                      ),
                    ),
                    Container(
                      width: width * 0.15,
                      child: Center(
                        child: Text(
                          groups[ind].maxMarks.toString(),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: AppTextStyle.getDynamicFontStyle(
                              Palette.lightBlackColor, 12, FontType.SemiBol),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            // model.selectDroupId == groups[ind].groupId
            //     ? Container(
            //         padding: EdgeInsets.symmetric(vertical: 10),
            //         child: ListView.builder(
            //             physics: ClampingScrollPhysics(),
            //             shrinkWrap: true,
            //             itemCount: groups[ind].tests.length,
            //             itemBuilder: (BuildContext context, int i) {
            //               return Row(
            //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //                 children: <Widget>[
            //                   Container(
            //                     padding: EdgeInsets.only(left: 20),
            //                     width: width * 0.55,
            //                     child: Text(
            //                       groups[ind].tests[i].name,
            //                       style: AppTextStyle.getDynamicFontStyle(
            //                           Colors.black, 11, FontType.Regular),
            //                     ),
            //                   ),
            //                   Container(
            //                     width: width * 0.15,
            //                     child: Center(
            //                       child: Text(
            //                         "-",
            //                         maxLines: 1,
            //                         overflow: TextOverflow.ellipsis,
            //                         style: AppTextStyle.getDynamicFontStyle(
            //                             Palette.lightBlackColor,
            //                             11,
            //                             FontType.Regular),
            //                       ),
            //                     ),
            //                   ),
            //                   Container(
            //                     width: width * 0.15,
            //                     child: Center(
            //                       child: Text(
            //                         groups[ind].tests[i].maxMarks.toString(),
            //                         maxLines: 1,
            //                         overflow: TextOverflow.ellipsis,
            //                         style: AppTextStyle.getDynamicFontStyle(
            //                             Palette.lightBlackColor,
            //                             11,
            //                             FontType.Regular),
            //                       ),
            //                     ),
            //                   ),
            //                 ],
            //               );
            //             }),
            //       )
            //     : Container()
          ],
        );
      },
    );
  }
}
