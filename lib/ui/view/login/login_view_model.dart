import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/StudentModel.dart';
import 'package:trainmoo/model/user_login_model.dart';
import 'package:trainmoo/shared_preference/user_preference.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/router.dart';
import 'package:trainmoo/ui/util/util.dart';

import '../../../shared_preference/user_preference.dart';

class LoginViewModel extends BaseModel {
  UserLoginModel _userLoginModel;

  loginUser(String email, String password) async {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
        _userLoginModel = await API.loginUser(email, password);
        if (_userLoginModel != null &&
            _userLoginModel.user != null &&
            _userLoginModel.user.email != null) {
          UserPreference.saveUserDetails(_userLoginModel.user);
          UserPreference.saveUserName(_userLoginModel.user.name);
          UserPreference.saveUserImage(_userLoginModel.user.avatar);

          await API.getSchoolData();
          var userType = await API.getuserType();
          print("loginType : $userType");
          if (userType == "parent") {
            UserPreference.saveUserType("student");
          } else if (userType == "student") {
            UserPreference.saveUserType("student");
          } else {
            UserPreference.saveUserType("admin");
          }

          List<ProgramClassesModel> classes = [];
          List<ProgramClassesModel> classList =
              await API.getProgramClass(false);
          if (classList.length > 0) {
            for (ProgramClassesModel classesModel in classList) {
              if (classesModel.users.student > 0) {
                classes.add(classesModel);
              }
            }
          }
          print("this is affter getProgramClass: ------>>>>>");
          for (ProgramClassesModel programClassesModel in classes) {
            await API.getStudents(programClassesModel.id);
          }
          print("this is affter getStudents: ------>>>>>");
          Navigator.of(context).pushNamedAndRemoveUntil(
              Screen.TabManagerView.toString(),
              (Route<dynamic> route) => false);
        } else {
          if (_userLoginModel != null && _userLoginModel.loginMessage != null) {
            showToast(_userLoginModel.loginMessage.toString(), context);
          } else {
            showToast("Something Went Wrong Please try again later", context);
          }
        }
        setState(ViewState.Idle);
      }
    });
  }

  void validateLogin(String email, String password, BuildContext context) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (email.isEmpty && password.isEmpty) {
      showToast("Please fill the login details", context);
    } else if (email.isEmpty) {
      showToast("Please enter email", context);
    } else if (password.isEmpty) {
      showToast("Please enter password", context);
    } else if (!regex.hasMatch(email)) {
      showToast("Incorrect Email", context);
    } else {
      loginUser(email, password);
    }
  }

  Future<void> googleLogin(
      String email, String image, String token, String name) async {
    var dname = name.split(" ");
    var familyName = dname[1];
    var givenName = dname[0];
    _userLoginModel = await API.googleloginUser(
        email, familyName, name, givenName, image, token);

    if (_userLoginModel != null) {
      var schooldata = await API.getSchoolData();
      print("schoool : $schooldata");
      if (_userLoginModel.user.userType == "admin" && schooldata == null) {
        Navigator.pushNamed(context, Screen.NoticeView.toString());
      } else if (schooldata == null) {
        Navigator.pushNamed(context, Screen.SignupWithTypeView.toString());
      } else {
        UserPreference.saveUserDetails(_userLoginModel.user);
        UserPreference.saveUserName(_userLoginModel.user.name);
        UserPreference.saveUserImage(_userLoginModel.user.avatar);
        var userType = await API.getuserType();
        print("loginType : $userType");
        if (userType == "parent") {
          UserPreference.saveUserType("student");
        } else if (userType == "student") {
          UserPreference.saveUserType("student");
        } else {
          UserPreference.saveUserType("admin");
        }

        List<ProgramClassesModel> classes = [];
        List<ProgramClassesModel> classList = await API.getProgramClass(false);
        if (classList.length > 0) {
          for (ProgramClassesModel classesModel in classList) {
            if (classesModel.users.student > 0) {
              classes.add(classesModel);
            }
          }
        }
        print("this is affter getProgramClass: ------>>>>>");
        for (ProgramClassesModel programClassesModel in classes) {
          await API.getStudents(programClassesModel.id);
        }
        print("this is affter getStudents: ------>>>>>");
        Navigator.of(context).pushNamedAndRemoveUntil(
            Screen.TabManagerView.toString(), (Route<dynamic> route) => false);
      }
    } else {
      showToast("Something went to wrong!", context);
    }
  }
}

mixin Var {}
