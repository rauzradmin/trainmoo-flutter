import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/router.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';
import 'package:trainmoo/ui/widget/regular_button.dart';
import 'package:trainmoo/ui/widget/regular_text_form_field.dart';
import 'package:trainmoo/ui/widget/soical_signin_button.dart';
import 'package:trainmoo/util/image_path.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:dio/dio.dart';

import 'login_view_model.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> with BaseCommonWidget {
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: <String>[
      'email',
    ],
  );
  GoogleSignInAccount _currentUser;

  @override
  void initState() {
    super.initState();
    // _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) {
    //   setState(() {
    //     _currentUser = account;
    //   });
    //   if (_currentUser != null) {
    //     print("data : ${_currentUser}");

    //   }
    // });
    _googleSignIn.signInSilently();
  }

  Future<void> _handleSignIn(model) async {
    try {
      _googleSignIn.signOut();
      _googleSignIn.signIn().then((result) {
        result.authentication.then((googleKey) {
          print(googleKey.accessToken);
          print(googleKey.idToken);
          print(_googleSignIn.currentUser.displayName);
          model.googleLogin(
              _googleSignIn.currentUser.email,
              _googleSignIn.currentUser.photoUrl,
              googleKey.idToken,
              _googleSignIn.currentUser.displayName);
          _googleSignIn.signInSilently();
        }).catchError((err) {
          print('inner error');
        });
      }).catchError((err) {
        print('error occured');
      });
    } catch (error) {
      print(error);
    }
  }

  Future<void> _handleSignOut() => _googleSignIn.disconnect();

  Widget build(BuildContext context) {
    return BaseView<LoginViewModel>(builder: (context, model, child) {
      return SafeArea(
          child: Scaffold(
              backgroundColor: Palette.screenBgColor, body: _getBody(model)));
    });
  }

  Widget _getBody(LoginViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(LoginViewModel model) {
    return Padding(
      padding: EdgeInsets.only(left: 34, right: 34),
      child: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 45),
            child: Image.asset(
              ImagePath.ICON_APP,
              height: 100,
              width: 97,
            ),
          ),
          UIHelper.getVerticalSpaceOf(22),
          Center(
            child: Text(
              'trainmoo',
              style: AppTextStyle.getDynamicFontStyle(
                  Colors.black, 34, FontType.SemiBol),
            ),
          ),
          UIHelper.getVerticalSpaceOf(35),
          RegularTextFormField(
            textEditingController: emailController,
            hintText: 'Email',
            textAlign: TextAlign.center,
            inputType: TextInputType.emailAddress,
          ),
          UIHelper.getVerticalSpaceOf(10),
          RegularTextFormField(
            textEditingController: passwordController,
            hintText: 'Password',
            textAlign: TextAlign.center,
            obscureText: true,
          ),
          RegularButton(
            buttonLabel: 'Login',
            onTap: () {
              print('Button pressed');
              model.validateLogin(
                emailController.text,
                passwordController.text,
                context,
              );
//              Navigator.pushNamed(context, Screen.HomeView.toString());
//              model.loginUser();
            },
          ),
          UIHelper.getVerticalSpaceOf(25),
          Center(
            child: Text(
              'Forgot Password?',
              style: AppTextStyle.getDynamicFontStyle(
                  Palette.hintTextColor, 13, FontType.Medium),
            ),
          ),
          UIHelper.getVerticalSpaceOf(30),
          Container(
            height: 36,
            child: Stack(
              children: <Widget>[
                Center(
                  child: Container(
                    height: 1,
                    alignment: Alignment.center,
                    color: Palette.dividerColor,
                  ),
                ),
                Center(
                  child: Container(
                    height: 36,
                    width: 36,
                    alignment: Alignment.center,
                    decoration: new BoxDecoration(
                      color: Palette.dividerColor,
                      borderRadius: new BorderRadius.all(Radius.circular(18)),
                    ),
                    child: Center(
                      child: Text(
                        'Or',
                        style: AppTextStyle.getDynamicFontStyle(
                            Palette.primaryTextColor, 13, FontType.Medium),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          UIHelper.getVerticalSpaceOf(15),
          // SocialSignInButton(
          //   buttonLabel: 'Sign In With Facebook',
          //   image: ImagePath.ICON_FACEBOOK,
          //   leftMargin: 33,
          //   width: 9,
          //   height: 20,
          // ),
          SocialSignInButton(
            onTap: () => _handleSignIn(model),
            buttonLabel: 'Sign In With Google',
            image: ImagePath.ICON_GOOGLE,
            leftMargin: 27,
            width: 20,
            height: 20,
          ),
          UIHelper.getVerticalSpaceOf(25),
          Center(
              child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                'Dont have an account? ',
                style: AppTextStyle.getDynamicFontStyle(
                    Palette.hintTextColor, 13, FontType.Medium),
              ),
              GestureDetector(
                child: Text(
                  'Sign Up.',
                  style: AppTextStyle.getDynamicFontStyle(
                      Palette.accentColor, 13, FontType.Medium),
                ),
                onTap: () {
                  Navigator.pushNamed(context, Screen.Registration.toString());
                },
              ),
            ],
          )),
          UIHelper.getVerticalSpaceOf(102),
        ],
      ),
    );
  }
}
