import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';
import 'package:trainmoo/util/image_path.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';

class Notice extends StatelessWidget with BaseCommonWidget {
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: Palette.screenBgColor, body: _getBody(context)));
  }

  Widget _getBody(context) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(context),
      ],
    );
  }

  Widget _getBaseContainer(context) {
    return Padding(
      padding: EdgeInsets.only(left: 14, right: 34),
      child: ListView(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Container(
              alignment: Alignment.topLeft,
              child: Icon(Icons.arrow_back_ios),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 34, top: 45),
            child: Image.asset(
              ImagePath.ICON_APP,
              height: 100,
              width: 97,
            ),
          ),
          UIHelper.getVerticalSpaceOf(22),
          Center(
            child: Text(
              'trainmoo',
              style: AppTextStyle.getDynamicFontStyle(
                  Colors.black, 34, FontType.SemiBol),
            ),
          ),
          UIHelper.getVerticalSpaceOf(35),
          Container(
              child: Column(
            children: [
              Text(
                "Enroll you school and class setup at https://www.trainmoo.in. Please come back here once your setup is done.",
                textAlign: TextAlign.center,
                style: AppTextStyle.getDynamicFontStyle(
                    Colors.black, 14, FontType.Medium),
              )
            ],
          ))
        ],
      ),
    );
  }
}
