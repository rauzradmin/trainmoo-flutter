import 'package:flutter/material.dart';
import 'package:trainmoo/model/SyllabusModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/view/Syllabus/syllabus_view_model.dart';
import 'package:trainmoo/ui/widget/ItemDetailView.dart';

import '../../router.dart';

class SyllabusView extends StatefulWidget {
  final String classId;

  const SyllabusView({Key key, this.classId}) : super(key: key);
  @override
  _SyllabusViewState createState() => _SyllabusViewState();
}

class _SyllabusViewState extends State<SyllabusView> with BaseCommonWidget {
  @override
  Widget build(BuildContext context) {
    return BaseView<SyllabusViewModel>(builder: (context, model, child) {
      model.init(widget.classId);
      return RefreshIndicator(
          onRefresh: () async {
            model.getSyllabusList();
            return;
          },
          child: SafeArea(child: _getBody(model)));
    });
  }

  Widget _getBody(SyllabusViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(SyllabusViewModel model) {
    return Container(
      child: Column(
        children: <Widget>[
          commonAppBar(AppLocalizations.of(context).syllabus, context),
          Expanded(
              child: model.sections.length > 0
                  ? getSyllabusList(model)
                  : showMessageInCenter('No Syllabus found'))
        ],
      ),
    );
  }

  getSyllabusList(SyllabusViewModel model) {
    return Container(
      padding: const EdgeInsets.only(left: 24, right: 24, bottom: 5, top: 10),
      child: ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: model.sections.length,
        itemBuilder: (BuildContext cont, int ind) {
          return syllabusItem(model.sections[ind], model);
        },
      ),
    );
  }

  syllabusItem(SectionsBean syllabus, SyllabusViewModel model) {
    return GestureDetector(
      child: ItemDetailView(
        title: syllabus.title,
        time: '29 Feb 2020',
        details: syllabus.summary,
        isHtmlData: true,
        files: syllabus.files,
        header: model.header,
      ),
      onTap: () {
        Navigator.pushNamed(context, Screen.SyllabusDetail.toString(),
            arguments: {
              "syllabus": syllabus,
            });
      },
    );
  }
}
