import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/network/API.dart';

class SyllabusDetailViewModel extends BaseModel {
  String classId;
  bool isFirstTime = true;
  Map<String, String> header;

  void init() async {
    if (isFirstTime) {
      isFirstTime = false;
      header = await API.getAuthHeader();
      notifyListeners();
    }
  }
}
