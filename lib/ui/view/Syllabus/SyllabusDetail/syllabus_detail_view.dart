import 'package:flutter/material.dart';
import 'package:trainmoo/model/SyllabusModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/view/Syllabus/SyllabusDetail/syllabus_detail_model.dart';
import 'package:trainmoo/ui/widget/DetailView.dart';

class SyllabusDetailView extends StatefulWidget {
  final SectionsBean syllabus;

  const SyllabusDetailView({Key key, this.syllabus}) : super(key: key);

  @override
  _SyllabusDetailViewState createState() => _SyllabusDetailViewState();
}

class _SyllabusDetailViewState extends State<SyllabusDetailView>
    with BaseCommonWidget {
  @override
  Widget build(BuildContext context) {
    return BaseView<SyllabusDetailViewModel>(builder: (context, model, child) {
      model.init();
      print('tapped on item');

      return SafeArea(child: _getBody(model));
    });
  }

  Widget _getBody(SyllabusDetailViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(SyllabusDetailViewModel model) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          commonAppBar('', context),
          Expanded(
              child: SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.only(
                  left: 24, right: 24, bottom: 5, top: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  DetailView(
                    title: widget.syllabus.title,
                    time: '29 Feb 2020',
                    details: widget.syllabus.summary,
                    files: widget.syllabus.files,
                    header: model.header,
                  ),
                ],
              ),
            ),
          ))
        ],
      ),
    );
  }
}
