import 'package:trainmoo/model/SyllabusModel.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';

class SyllabusViewModel extends BaseModel {
  bool isFirstTime = false;
  String classId;
  List<SectionsBean> sections = [];
  Map<String, String> header;

  init(String classID) async {
    if (!isFirstTime) {
      isFirstTime = true;
      this.classId = classID;
      header = await API.getAuthHeader();
      getSyllabusList();
    }
  }

  void getSyllabusList() {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
        SyllabusModel syllabusModel = await API.getSyllabus(classId);
        sections = syllabusModel.sections;
      } else {
        showToast(AppLocalizations.of(context).internetMessage, context);
      }
      setState(ViewState.Idle);
    });
  }
}
