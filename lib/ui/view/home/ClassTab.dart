import 'package:flutter/material.dart';
import 'package:flutter/material.dart' hide Router;
import 'package:trainmoo/ui/theme/palette.dart';

import '../../router.dart';

class ClassTab extends StatelessWidget {
  final String initalRoute;

  ClassTab(this.initalRoute);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Trainmoo',
        debugShowCheckedModeBanner: false,
        initialRoute: initalRoute,
        theme: ThemeData(
          primaryColor: Palette.primaryColor,
          accentColor: Palette.accentColor,
          hintColor: Palette.secondaryTextColor,
          backgroundColor: Palette.screenBgColor,
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          canvasColor: Colors.white,

          primarySwatch: Colors.blue,
        ),
        onGenerateRoute: RouterSceen.generateRoute);
  }
}
