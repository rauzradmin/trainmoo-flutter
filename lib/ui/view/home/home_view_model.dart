import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:trainmoo/app_state.dart';
import 'package:trainmoo/model/Database/AttendanceTable.dart';
import 'package:trainmoo/model/Database/DatabaseHelper.dart';
import 'package:trainmoo/model/MaterialModel.dart';
import 'package:trainmoo/model/Name.dart';
import 'package:trainmoo/model/Notifications.dart';
import 'package:trainmoo/model/User.dart';
import 'package:trainmoo/model/swipe_home_model.dart';
import 'package:trainmoo/shared_preference/user_preference.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:trainmoo/util/image_path.dart';

import '../../router.dart';
import 'home_view.dart';

class HomeViewModel extends BaseModel {
  bool isCalledFirstTime = false;
  int timestamp = 0;
  List<CommonModel> swipeList = [];
  List<AttendanceTable> pendingAttendanceList = [];
  Map<String, String> header;
  RefreshController refreshController =
      RefreshController(initialRefresh: false);
  bool hasNextPage = true;
  bool isLoading = false;

  bool isCallingPage = false;
  void onRefresh() async {
//    latestPost = await API.getPosts(null, null, 'Latest%20Posts', null);
  }

  void onLoading() async {
//    latestPost =
//        await API.getPosts(null, null, 'Latest%20Posts', null, timestamp);
//    refreshController.loadComplete();
//    return true;
  }

//  List<HomeModelList> userDetailList = [];
  bool isOpen = false;
  bool firstTime = true;
  User user;
  String userType;
  List<MaterialModel> latestPost = [];
  List postList = [];
  List<Color> typeColor = [
    Palette.assignmentColorItem,
    Palette.accentColor,
    Palette.syllabusColor,
    Palette.transparentBlue,
    Palette.primaryDarkColor,
    Palette.assignmentColorItem,
    Palette.accentColor,
    Palette.syllabusColor,
    Palette.transparentBlue,
    Palette.primaryDarkColor
  ];
  List<NotificationModel> notificationList = [];
  bool newNotification;
  UserNameObject userNameObject;
  Random rnd;

  int r;

  init(Function callback, loadFirstTime, latestPost) async {
    if (!isCalledFirstTime) {
      isCalledFirstTime = true;
      if (swipeList != null && swipeList.length > 0) {
        swipeList.clear();
      }
      header = await API.getAuthHeader();

      int min = 0;
      int max = 4;
      rnd = new Random();
      r = min + rnd.nextInt(max - min);
      print("$r is in the range of $min and $max");
      userType = await UserPreference.getUserType();
      swipeList.addAll(await getSwipeList());
      getUserDetail();

      if (loadFirstTime == false) {
        //print("value of firstTimes : $firstTime");
        // UserPreference.saveLoadHomeScreen();
        getNotificationList();
        getLatestPosts(callback);
        pendingAttendanceList = await DatabaseHelper().getAttendanceData();
        await API.getProgramClass(true);
        print("userType : $userType");
        if (userType == API.TeacherUser) {
          String todayDate = Utility.getSessionFormatDate(new DateTime.now());
          var currentDateTime = DateTime.now();
          DateTime weekLastDate = DateTime(currentDateTime.year,
              currentDateTime.month + 1, currentDateTime.day + 6);
          String lastdate = await UserPreference.getDate();
          print("=todayDate : $todayDate , lastdate: $lastdate ");
          if (lastdate == null) {
            print("api is called");
            var eventList = await API.getSessionData(
                Utility.getSessionFormatDate(currentDateTime),
                Utility.getSessionFormatDate(weekLastDate));
            // var eventList = await API.getSessionData(todayDate, todayDate);
            print("eventListofhome : $eventList");
            DatabaseHelper().saveEventData(eventList);
            UserPreference.saveDate(todayDate);
          } else if (todayDate.compareTo(lastdate) != 0) {
            print("api is called not compair");
            API.getProgramClass(true);
            var eventList = await API.getSessionData(
                Utility.getSessionFormatDate(currentDateTime),
                Utility.getSessionFormatDate(weekLastDate));
            print("eventListofhome : $eventList");
            DatabaseHelper().saveEventData(eventList);
            UserPreference.saveDate(todayDate);
          }
        }
      } else {
        this.latestPost = latestPost;
        notifyListeners();
        print("set this value in latestPost ${this.latestPost}");
      }
    }
  }

  setvalueFromSate(AppState state) {
    this.latestPost = state.latestPost;
    notifyListeners();
    print("set this value in latestPost ${this.latestPost}");
  }

  void getNotificationList() {
    setState(ViewState.Busy);
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        notificationList.addAll(await API.getNotifications());
        setState(ViewState.Idle);
        notifyListeners();
        print('lenghth of-->${notificationList.length}');
        newNotification = notificationList.length > 0 ? true : false;
      } else {
        setState(ViewState.Idle);
        showToast(AppLocalizations.of(context).internetMessage, context);
      }
    });
  }

  void getLatestPosts([callback]) {
    print('getLatestPosts =============> called');

    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        // if (timestamp == 0) {
        //   setState(ViewState.Busy);
        timestamp = new DateTime.now().millisecondsSinceEpoch;
        //}
        latestPost = [];
        List<MaterialModel> posts =
            await API.getPosts(null, null, 'Latest%20Posts', null, timestamp);
        posts.sort((a, b) => a.createTimeStamp.compareTo(b.createTimeStamp));
        posts = posts.reversed.toList();
        print(
            '-------------->>> getLatestPosts =============> called : ${posts[0].createTimeStamp}');
        if (posts.length > 0) {
          // timestamp = 0;
          posts.reversed.toList();
          if (posts.length >= 20)
            hasNextPage = true;
          else
            hasNextPage = false;
        }
        callback(posts);
        latestPost.addAll(posts);

        setState(ViewState.Idle);
      } else {
        showToast(AppLocalizations.of(context).internetMessage, context);
      }
    });
  }

  Future<bool> loadMore() async {
//    isCallingPage = false;
    print("Load New Page===============> ");

    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        if (timestamp == 0) {
          setState(ViewState.Busy);
          timestamp = new DateTime.now().millisecondsSinceEpoch;
        }
        if (!isCallingPage) {
          isCallingPage = true;
          print("Load New Page===============> 5555555 ");
          List<MaterialModel> postList =
              await API.getPosts(null, null, 'Latest%20Posts', null, timestamp);
          postList
              .sort((a, b) => a.createTimeStamp.compareTo(b.createTimeStamp));
          postList = postList.reversed.toList();
          timestamp = postList[postList.length - 1].createTimeStamp;
          latestPost.addAll(postList);
          print("loadmore List : $postList");
        } else {
          isCallingPage = false;
        }
        setState(ViewState.Idle);
      }
    });
    return isCallingPage;
  }

  Future<List<CommonModel>> getSwipeList() async {
    List<CommonModel> swipeItems = [];
    //print("userType=================>>>>>>>>>>>>> : $userType");
    if (userType == API.StudentUser) {
      swipeItems.add(new CommonModel(
          title: AppLocalizations.of(context).askQuestion,
          desc: "create Question",
          image: ImagePath.QUIZ_IMG));
      swipeItems.add(new CommonModel(
          title: AppLocalizations.of(context).startDiscussion,
          desc: "Create Discussion",
          image: ImagePath.MATERIAL_IMG));
      swipeItems.add(new CommonModel(
          title: "Submit Assignment",
          desc: "Submit Assignment",
          image: ImagePath.SWIPE_ICON));
    } else {
      swipeItems.add(new CommonModel(
          title: "Take Attendance",
          desc: "Attendence pending for classmate",
          image: ImagePath.SWIPE_ICON));
      swipeItems.add(new CommonModel(
          title: "Craete Material",
          desc: "Create Material for classmate",
          image: ImagePath.MATERIAL_IMG));
      swipeItems.add(new CommonModel(
          title: "Create Quiz",
          desc: "Create Quiz for classmate",
          image: ImagePath.QUIZ_IMG));
    }
    notifyListeners();
    return swipeItems;
  }

  void onItemTap(int index) {
    if (index == 0) {
      Navigator.pushNamed(context, Screen.ClassListView.toString());
    } else if (index == 2) {
      Navigator.pushNamed(context, Screen.MySession.toString());
    } else if (index == 3) {
      Navigator.pushNamed(context, Screen.MyWork.toString());
    }
  }

  Future getUserDetail() async {
    print("this is user details =====>>>> user");
    user = await UserPreference.getUserDetail();
    print("this is user details =====>>>> $user");
//    await API.getAttachment(user.userImage);
    userNameObject = await UserPreference.getUserName();
  }

  void like(bool isLiked, String id, int index) {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
//        userDetailList.addAll(await API.getCategories());
        bool isliked = await API.like(isLiked ? 'unlike' : 'like', id);
        latestPost[index].liked = !latestPost[index].liked;
        if (latestPost[index].liked)
          latestPost[index].likes = latestPost[index].likes + 1;
        else
          latestPost[index].likes = latestPost[index].likes - 1;
        setState(ViewState.Idle);
      } else {
        showToast(AppLocalizations.of(context).internetMessage, context);
      }
    });
  }

  void submitAttendance() async {
    if (pendingAttendanceList.length == 0) {
      setState(ViewState.Idle);
      return;
    }
    AttendanceTable attendanceTable = pendingAttendanceList[0];
    String attendanceData = attendanceTable.data;
    print('decode attendace data ==> ${jsonDecode(attendanceData)}');
    List<dynamic> attendanceDataList = jsonDecode(attendanceData);
    print('decode attendace data List==> $attendanceDataList}');
    List<Map<String, dynamic>> attendanceMapData = [];
    for (int i = 0; i < attendanceDataList.length; i++) {
      Map<String, dynamic> studentAttendance = Map();
      studentAttendance['id'] = attendanceDataList[i]['id'];
      studentAttendance['present'] = attendanceDataList[i]['present'];
      attendanceMapData.add(studentAttendance);
    }
    print('decode attendace map data ==> $attendanceMapData}');

    String displayDate = Utility.getyearMonthDate(attendanceTable.id);

    if (await Utility.isInternetAvailable()) {
      setState(ViewState.Busy);
      await API.takeAttendance(attendanceMapData, attendanceTable.classID,
          attendanceTable.eventId, displayDate, true);
      print("attendanceTable.id : ${attendanceTable.id}");
      await DatabaseHelper().deleteAttendanceFromTable(attendanceTable.id);
      pendingAttendanceList.removeAt(0);
      submitAttendance();
    } else {
      showToast(AppLocalizations.of(context).internetMessage, context);
    }
  }
}
