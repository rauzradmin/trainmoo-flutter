import 'package:flutter/material.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';

import '../../router.dart';

class FabListTab extends StatelessWidget {
  final String initalRoute;

  FabListTab(this.initalRoute);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Trainmoo',
        initialRoute: initalRoute,
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Palette.primaryColor,
          accentColor: Palette.accentColor,
          hintColor: Palette.secondaryTextColor,
          backgroundColor: Palette.screenBgColor,
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          canvasColor: Colors.white,
          textTheme: TextTheme(
              title: AppTextStyle.getDynamicFontStyle(
                  Palette.primaryColor, 16, FontType.Regular)),
          primarySwatch: Colors.blue,
        ),
        onGenerateRoute: RouterSceen.generateRoute);
  }
}
