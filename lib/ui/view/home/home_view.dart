import 'dart:async';

import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:trainmoo/actions/home_actions.dart';
import 'package:trainmoo/app_state.dart';
import 'package:trainmoo/model/MaterialModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:trainmoo/util/image_path.dart';
import 'package:loadmore/loadmore.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../router.dart';
import '../../widget/FileView.dart';
import 'home_view_model.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_redux/flutter_redux.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> with BaseCommonWidget {
  int listPosition = 0;
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  NotificationAppLaunchDetails notificationAppLaunchDetails;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  _setupNotification() async {
    notificationAppLaunchDetails =
        await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();

    var initializationSettingsAndroid =
        AndroidInitializationSettings('mipmap/ic_launcher');
    var initializationSettingsIOS = IOSInitializationSettings();
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings);
  }

  _displayNotification(Map<String, dynamic> notifymessage) async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'chat-channel', 'chat channel', 'chat channel description',
        importance: Importance.Max, priority: Priority.High, ticker: 'ticker');
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        0,
        notifymessage['notification']['title'],
        notifymessage['notification']['body'],
        platformChannelSpecifics);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      // setState(() {
      print("Push Messaging token: $token");
      // });
    });
    _setupNotification();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> notifymessage) {
        print('onMessage called: $notifymessage');

        _displayNotification(notifymessage);
      },
      onResume: (Map<String, dynamic> notifymessage) {
        print('onResume called: $notifymessage');
      },
      onLaunch: (Map<String, dynamic> notifymessage) {
        print('onLaunch called: $notifymessage');
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          print("the state is 1 : ${state.latestPost}");

          return BaseView<HomeViewModel>(builder: (context, model, child) {
            print("the state is 2 : ${state.latestPost}");
            model.init((newValue) {
              print("newValue : $newValue");
              try {
                StoreProvider.of<AppState>(context)
                    .dispatch(SetPostListAction(newValue));
                StoreProvider.of<AppState>(context)
                    .dispatch(SetLoadFirstTime(true));
                model.setvalueFromSate(state);
              } catch (e) {
                print("error is => $e");
              }
            }, state.loadFirstTime, state.latestPost);

//    showMessage(model);
            return SafeArea(child: _getBody(model, state));
          });
        });
  }

  Widget _getBody(HomeViewModel model, state) {
    return Stack(
      children: <Widget>[
        model.user != null ? _getBaseContainer(model, state) : Container(),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(HomeViewModel model, state) {
    return Container(
      margin: EdgeInsets.only(left: 24, right: 24, top: 12),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            onTap: () {
              Navigator.pushNamed(context, Screen.UserDetailView.toString());
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                loadCircleNetworkImage(model.user.userImage, 44, model.header),
                SizedBox(
                  width: 17,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      AppLocalizations.of(context).welcome,
                      style: AppTextStyle.getDynamicFontStyleWithWeight(
                          false,
                          Palette.primaryTextColor,
                          18,
                          FontType.Medium,
                          FontWeight.w500),
                    ),
                    Container(
                      transform: Matrix4.translationValues(0, -6, 0),
                      child: Text(
                        model.user != null && model.user.fullname != null
                            ? model.user.fullname
                            : '',
                        style: AppTextStyle.getDynamicFontStyleWithWeight(
                            false,
                            Palette.primaryTextColor,
                            26,
                            FontType.SemiBol,
                            FontWeight.normal),
                      ),
                    ),
                  ],
                ),
                Flexible(
                  child: Container(),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      Screen.NotificationView.toString(),
                    );
                  },
                  child: Container(
                      height: 30,
                      child: Stack(
                        children: [
                          Container(
                            height: 30,
                            child: Image.asset(
                              ImagePath.NOTIFICATION_IMG,
                              height: 20,
                              width: 19,
                              color: Palette.transparentBlue,
                            ),
                          ),
                          model.newNotification == true
                              ? Positioned(
                                  right: 1.0,
                                  top: 1,
                                  child: Icon(
                                    Icons.brightness_1,
                                    size: 10,
                                    color: Colors.red,
                                  ))
                              : Container()
                        ],
                      )),
                ),
                // GestureDetector(
                //     onTap: () {
                //       Navigator.pushNamed(
                //         context,
                //         Screen.NotificationView.toString(),
                //       );
                //     },
                //     child: Image.asset(
                //       ImagePath.NOTIFICATION_IMG,
                //       height: 20,
                //       width: 19,
                //       color: Palette.transparentBlue,
                //     ))
              ],
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              model.swipeList != null && model.swipeList.length > 0
                  ? _swipeImage(model)
                  : Container(),
              model.swipeList != null && model.swipeList.length > 0
                  ? _dotsIndicator(model)
                  : Container(),
            ],
          ),
          UIHelper.verticalSpaceSmall,
          model.pendingAttendanceList.length > 0
              ? Card(
                  child: Container(
                    padding: EdgeInsets.only(left: 10, top: 10, bottom: 10),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            'Offline attendance are not synced with server',
                            style: AppTextStyle.getDynamicFontStyleWithWeight(
                                false,
                                Palette.redColor,
                                18,
                                FontType.Medium,
                                FontWeight.w500),
                          ),
                        ),
                        FlatButton(
                          onPressed: () {
                            model.submitAttendance();
                          },
                          child: Text(
                            'Sync\nNow',
                            textAlign: TextAlign.center,
                            style: AppTextStyle.getDynamicFontStyleWithWeight(
                                false,
                                Colors.green,
                                18,
                                FontType.Medium,
                                FontWeight.w500),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              : Container(),
          UIHelper.verticalSpaceSmall,
          Expanded(
            child: _categoryView(model, state),
          ),
          UIHelper.verticalSpaceSmall,
        ],
      ),
    );
  }

  Widget _swipeImage(HomeViewModel model) {
    return Container(
      margin: EdgeInsets.only(left: 3),
      height: 136,
      width: 327,
      child: new Swiper(
        itemBuilder: (BuildContext context, int index) {
          return swipeItem(model, index);
        },
        itemCount: model.swipeList.length,
        autoplay: false,
        loop: false,
        onIndexChanged: (index) {
          setState(() {
            listPosition = index;
          });
        },
      ),
    );
  }

  Widget swipeItem(HomeViewModel model, int index) {
    return InkWell(
      onTap: () {
        if (index == 0) {
          model.userType == API.StudentUser
              ? Navigator.of(context)
                  .pushNamed(Screen.CreateMaterial.toString(), arguments: {
                  "materialObject": null,
                  "materialType": model.swipeList[index].title
                })
              : Navigator.of(context)
                  .pushNamed(Screen.TakeAttendanceView.toString());
        } else if (index == 1) {
          model.userType == API.StudentUser
              ? Navigator.of(context)
                  .pushNamed(Screen.CreateMaterial.toString(), arguments: {
                  "materialObject": null,
                  "materialType": model.swipeList[index].title
                })
              : Navigator.of(context)
                  .pushNamed(Screen.CreateMaterial.toString(), arguments: {
                  "materialObject": null,
                  "materialType": model.swipeList[index].title
                });
        } else if (index == 2) {
          model.userType == API.StudentUser
              ? Navigator.of(context).pushNamed(
                  Screen.CreateMaterial.toString(),
                  arguments: {"materialObject": null})
              : Navigator.of(context).pushNamed(Screen.CreateQuiz.toString());
        } else {
          model.userType == API.StudentUser
              ? Navigator.of(context)
                  .pushNamed(Screen.CreateMaterial.toString(), arguments: {
                  "materialObject": null,
                  "materialType": AppLocalizations.of(context).askQuestion
                })
              : Navigator.of(context)
                  .pushNamed(Screen.TakeAttendanceView.toString());
        }
      },
      child: Container(
        child: Card(
          elevation: 1,
          child: Row(
            children: <Widget>[
              SizedBox(width: 20),
              Image.asset(
                model.swipeList[index].image,
                height: 40,
                width: 41,
              ),
              Container(
                margin: EdgeInsets.only(top: 36, left: 14),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        model.swipeList[index].title,
                        style: AppTextStyle.getDynamicFontStyleWithWeight(
                            false,
                            Palette.primaryTextColor,
                            18,
                            FontType.SemiBol,
                            FontWeight.w500),
                      ),
                    ),
                    Container(
                      width: 202,
                      child: Text(
                        model.swipeList[index].desc,
                        style: AppTextStyle.getDynamicFontStyleWithWeight(
                            false,
                            Palette.secondaryTextColor,
                            14,
                            FontType.RobotoRegular,
                            FontWeight.normal),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _dotsIndicator(HomeViewModel model) {
    return Container(
      margin: EdgeInsets.only(top: 8, bottom: 10),
      child: Center(
        child: DotsIndicator(
          dotsCount: model.swipeList.length,
          position: listPosition,
          decorator: DotsDecorator(
            color: Palette.indicatorDotColor,
            activeColor: Palette.primaryTextColor,
          ),
        ),
      ),
    );
  }

  Widget _categoryView(HomeViewModel model, state) {
    void _onRefresh() async {
      //await Future.delayed(Duration(milliseconds: 1000));
      model.getLatestPosts((newValue) {
        print("newValue : $newValue");
        try {
          StoreProvider.of<AppState>(context)
              .dispatch(SetPostListAction(newValue));
          StoreProvider.of<AppState>(context).dispatch(SetLoadFirstTime(true));
          model.setvalueFromSate(state);
        } catch (e) {
          print("error is => $e");
        }
      });
      _refreshController.refreshCompleted();
    }

    void _onLoading() async {
      // monitor network fetch
      model.loadMore();
      await Future.delayed(Duration(milliseconds: 1500));
      _refreshController.loadComplete();
    }

    return Padding(
        padding: const EdgeInsets.only(bottom: 5),
//       child: NotificationListener<ScrollNotification>(
//           onNotification: (ScrollNotification scrollInfo) {
//             if (!model.isLoading &&
//                 scrollInfo.metrics.pixels ==
//                     scrollInfo.metrics.maxScrollExtent) {
//               // start loading data
//               setState(() {
//                 model.isCallingPage = true;
//               });
// //            model.isCallingPage = true;
//               model.getLatestPosts();
// //            model.isCallingPage = true;

//               //_loadData();
//             }
//             return false;
//           },
        child: SmartRefresher(
          enablePullDown: true,
          enablePullUp: true,
          header: WaterDropMaterialHeader(
            backgroundColor: Palette.accentColor,
          ),
          controller: _refreshController,
          child: ListView.builder(
            physics: ClampingScrollPhysics(),
            shrinkWrap: true,
            itemCount: model.latestPost.length,
            itemBuilder: (BuildContext cont, int ind) {
//              model.isCallingPage == false
//                  ? model.isCallingPage = true
//                  : model.isCallingPage = false;
              return assignmentItem(ind, model, model.latestPost.length, ind);
            },
          ),
          onRefresh: _onRefresh,
          onLoading: _onLoading,
        )
//         child: LoadMore(
//           whenEmptyLoad: true,
//           isFinish: false,
//           onLoadMore: () => model.loadMore(),
//           textBuilder: (status) => 'Load More...',
//           child: ListView.builder(
//             physics: ClampingScrollPhysics(),
//             shrinkWrap: true,
//             itemCount: model.latestPost.length,
//             itemBuilder: (BuildContext cont, int ind) {
// //              model.isCallingPage == false
// //                  ? model.isCallingPage = true
// //                  : model.isCallingPage = false;
//               return assignmentItem(ind, model, model.latestPost.length, ind);
//             },
//           ),
//         ),
        //   ),
        );
  }

  assignmentItem(int index, HomeViewModel model, int length, int ind) {
    MaterialModel postModel = model.latestPost[index];
    int colorPosition = index;
    if (index > 9) {
      String strIndex = index.toString();
      strIndex = strIndex.substring(strIndex.length - 1);
      print('last character ==> $strIndex');
      colorPosition = int.parse(strIndex);
    }
    double c_width = MediaQuery.of(context).size.width * 0.59;
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, Screen.MaterialDetail.toString(),
            arguments: {
              "material": postModel,
              'screen_type': API.postScreenType,
              "color": getColorFromType(postModel.type)
            });
      },
      child: Container(
        child: Card(
          elevation: 2,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(UIHelper.cardRadius)),
          child: Container(
            padding: EdgeInsets.all(22),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: const EdgeInsets.only(left: 3),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      CircleAvatar(
                        child: loadCircleNetworkImage(
                            postModel.author.avatar.src, 40, model.header),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Container(
                        //decoration: BoxDecoration(border: Border.all(width: 1)),
                        width: c_width,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              // crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(postModel.author.name.firstname.trim(),
                                    style: AppTextStyle.getDynamicFontStyle(
                                        Colors.black, 16, FontType.SemiBol)),
                              ],
                            ),
                            postModel.classBean != null
                                ? Text(
                                    // "   " +
                                    'in ' + " " + postModel.classBean.name,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    style: AppTextStyle.getDynamicFontStyle(
                                        Colors.black, 16, FontType.Regular))
                                : Container(),
                            SizedBox(
                              height: 1,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                postModel.title != null
                    ? Text(postModel.title,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: AppTextStyle.getDynamicFontStyle(
                            Colors.black, 16, FontType.Regular))
                    : Container(),
                SizedBox(
                  height: 8,
                ),
                Html(
                  data: postModel.text.trim(),
                  linkStyle: TextStyle(
                    color: Colors.blue,
                  ),
                  shrinkToFit: true,
                  onLinkTap: (String link) {
                    Utility.launchURL(link);
                  },
                ),
                postModel.files.length > 0
                    ? new FileView(
                        files: postModel.files,
                        headers: model.header,
                      )
                    : Container(),
                Container(
                  padding: EdgeInsets.only(
                    top: 10,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 3),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        getLikeCommentText(
                            "${postModel.likes.toString()} Likes"),
                        SizedBox(
                          width: 10,
                        ),
                        getLikeCommentText(
                            "${postModel.comments.toString()} Comments"),
                        SizedBox(
                          width: 10,
                        ),
                        getLikeCommentText(
                            "${postModel.views.toString()} Views"),
                      ],
                    ),
                  ),
                ),
                Container(
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    child: Divider(
                      color: Palette.listColor,
                    )),
                Container(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            InkWell(
                              child: Text(
                                  " ${postModel.liked ? ' Liked ' : ' Like '}",
                                  style: AppTextStyle.getDynamicFontStyle(
                                      Palette.blue, 14, FontType.RobotoBold)),
                              onTap: () {
                                print('Post id =========> ${postModel.id}');
                                model.like(
                                    postModel.liked, postModel.id, index);
                              },
                            ),
                            InkWell(
                                onTap: () {
//                            Random rnd;
//                            int min = 0;
//                            int max = 9;
//                            rnd = new Random();
//                            int r = min + rnd.nextInt(max - min);
                                  Navigator.pushNamed(
                                      context, Screen.MaterialDetail.toString(),
                                      arguments: {
                                        "material": postModel,
                                        "autofocus": true,
                                        'screen_type': API.postScreenType,
                                        "color": model.typeColor[colorPosition]
                                      });
                                },
                                child: Container(
                                    padding: EdgeInsets.only(left: 15),
                                    child: Text("Comment",
                                        style: AppTextStyle.getDynamicFontStyle(
                                            Palette.blue,
                                            14,
                                            FontType.RobotoBold))))
                          ]),
                      Row(
                        children: <Widget>[
                          Text(postModel.displayTime,
                              style: AppTextStyle.getDynamicFontStyle(
                                  Palette.listColor,
                                  13,
                                  FontType.RobotoRegular)),
                          SizedBox(
                            width: 5,
                          ),
                          Text(postModel.type,
                              style: AppTextStyle.getDynamicFontStyle(
                                  getColorFromType(postModel.type),
                                  13,
                                  FontType.RobotoRegular)),
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  getLikeCommentText(String text) {
    return Text(text,
        style: AppTextStyle.getDynamicFontStyle(
            Palette.listColor, 13, FontType.RobotoRegular));
  }
}
