import 'package:trainmoo/model/Notifications.dart';
import 'package:trainmoo/model/swipe_home_model.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:trainmoo/util/image_path.dart';

class NotificationViewModel extends BaseModel {
  bool isFirstTime = false;
  List<NotificationModel> notificationList = [];

  init() async {
    if (!isFirstTime) {
      isFirstTime = true;
      getNotificationList();
    }
  }

  void getNotificationList() {
    setState(ViewState.Busy);
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        notificationList = [];
        notificationList.addAll(await API.getNotifications());
        setState(ViewState.Idle);
        notifyListeners();
        print('lenghth of-->${notificationList.length}');
      } else {
        showToast(AppLocalizations.of(context).internetMessage, context);
      }
    });
  }

  Future<void> dismissNotification(notificationId) async {
    var val = await API.dismissNotifications(notificationId);
    if (val) {
      getNotificationList();
    }
  }

  Future<void> dismissAllNotification() async {
    var val = await API.dismissAllNotifications();
    if (val) {
      getNotificationList();
    }
  }
}
