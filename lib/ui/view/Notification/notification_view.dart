import 'package:flutter/material.dart';
import 'package:trainmoo/model/Notifications.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';

import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';

import 'package:trainmoo/ui/view/Notification/notification_view_model.dart';
import 'package:timeago/timeago.dart' as timeago;
import '../../router.dart';

class NotificationView extends StatefulWidget {
  @override
  _NotificationViewState createState() => _NotificationViewState();
}

class _NotificationViewState extends State<NotificationView>
    with BaseCommonWidget, SingleTickerProviderStateMixin {
  TabController tabController;
  int tabIndex = 0;

  @override
  void initState() {
    super.initState();
    tabController = TabController(vsync: this, length: 2);
    tabController.addListener(toggleTab);
  }

  void toggleTab() {
    setState(() {
      tabIndex = tabController.index;
    });
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<NotificationViewModel>(builder: (context, model, child) {
      model.init();
      return SafeArea(
          child: Scaffold(
              backgroundColor: Palette.screenBgColor, body: _getBody(model)));
    });
  }

  Widget _getBody(NotificationViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(NotificationViewModel model) {
    double c_width = MediaQuery.of(context).size.width;
    return Container(
        child: Column(children: <Widget>[
      commonAppBarWithCancelIcon(
          AppLocalizations.of(context).notification, context),
      Align(
        alignment: Alignment.centerRight,
        child: Container(
          // decoration: BoxDecoration(border: Border.all(width: 1)),
          width: c_width * 0.30,
          child: MaterialButton(
            onPressed: () {
              model.dismissAllNotification();
            },
            child: Text(
              "Dismiss All",
              style: TextStyle(color: Colors.red),
            ),
          ),
        ),
      ),
      completedList(model)
    ]));
  }

  completedList(NotificationViewModel model) {
    return Expanded(
      child: ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: model.notificationList.length,
        itemBuilder: (BuildContext cont, int ind) {
          return notificationItem(model, model.notificationList[ind], ind);
        },
      ),
    );
  }

  notificationItem(
      NotificationViewModel model, NotificationModel notification, int ind) {
    double c_width = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Card(
        elevation: 2,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Container(
          // decoration: BoxDecoration(border: Border.all(width: 1)),
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                //decoration: BoxDecoration(border: Border.all(width: 1)),
                width: c_width * 0.1,
                child: Icon(
                  Icons.notifications,
                  size: 25,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 1, right: 1),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: c_width * 0.5,
                      //decoration: BoxDecoration(border: Border.all(width: 1)),
                      //padding: const EdgeInsets.only(top: 6),
                      child: Text(notification.text,
                          style: AppTextStyle.getDynamicFontStyle(
                              Colors.black, 15, FontType.SemiBol)),
                    ),
                    Container(
                      padding: const EdgeInsets.only(top: 5),
                      child: Text(
                          timeago
                              .format(DateTime.parse(notification.updated_at)),
                          style: AppTextStyle.getDynamicFontStyle(
                              Palette.listColor, 14, FontType.RobotoRegular)),
                    ),
                  ],
                ),
              ),
              Container(
                // decoration: BoxDecoration(border: Border.all(width: 1)),
                width: c_width * 0.25,
                child: MaterialButton(
                  onPressed: () {
                    model.dismissNotification(notification.id);
                  },
                  child: Text("Dismiss"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
