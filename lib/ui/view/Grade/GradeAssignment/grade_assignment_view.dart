import 'package:flutter/material.dart';
import 'package:trainmoo/model/QuizUsersModel.dart';
import 'package:trainmoo/model/RepliesModel.dart';
import 'package:trainmoo/model/ReviewAssignment.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/widget/large_text_form_field_with_padding.dart';
import 'package:trainmoo/ui/widget/regular_button.dart';
import 'package:trainmoo/ui/widget/text_form_field_with_padding.dart';

import 'grade_assignment_view_model.dart';

class GradeAssignmentView extends StatefulWidget {
  final RepliesModel replies;
  final ReviewAssignemt reviewAssignemt;

  const GradeAssignmentView({Key key, this.replies, this.reviewAssignemt})
      : super(key: key);
  @override
  _GradeAssignmentViewState createState() => _GradeAssignmentViewState();
}

class _GradeAssignmentViewState extends State<GradeAssignmentView>
    with BaseCommonWidget {
  TextEditingController marksController = new TextEditingController();
  TextEditingController commentController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BaseView<GradeAssignmentViewModel>(builder: (context, model, child) {
      model.init(widget.replies, widget.reviewAssignemt);
      return SafeArea(child: _getBody(model));
    });
  }

  Widget _getBody(GradeAssignmentViewModel model) {
    return Stack(
      children: <Widget>[_getBaseContainer(model), getProgressBar(model.state)],
    );
  }

  Widget _getBaseContainer(GradeAssignmentViewModel model) {
    return Column(
      children: <Widget>[
        commonAppBar(AppLocalizations.of(context).gradeAssignment, context),
        Expanded(
          child: SingleChildScrollView(
            child: Container(
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 24, right: 24, bottom: 5, top: 10),
                child: Column(
                  children: <Widget>[
                    RegularTextFormFieldWithPadding(
                      textEditingController: marksController,
                      hintText: AppLocalizations.of(context).marks,
                      textAlign: TextAlign.left,
                      inputType: TextInputType.number,
                    ),
                    LargeTextFormFieldWithPadding(
                      textEditingController: commentController,
                      hintText: AppLocalizations.of(context).comments,
                      textAlign: TextAlign.left,
                      inputType: TextInputType.text,
                    ),
                    RegularButton(
                      buttonLabel: AppLocalizations.of(context).submitEd,
                      onTap: () {
                        print('Button pressed');
                        model.validateGrade(marksController.text,
                            commentController.text, context);
//Navigator.pushNamed(context, Screen.SyllabusView.toString());
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
