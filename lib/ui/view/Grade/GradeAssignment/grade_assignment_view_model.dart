import 'package:flutter/material.dart';
import 'package:trainmoo/model/GetCommentModel.dart';
import 'package:trainmoo/model/QuizUsersModel.dart';
import 'package:trainmoo/model/RepliesModel.dart';
import 'package:trainmoo/model/ReviewAssignment.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:intl/intl.dart';
import '../../../router.dart';

class GradeAssignmentViewModel extends BaseModel {
  bool isCalledFirstTime = false;
  ReviewAssignemt _reviewAssignemt;
  RepliesModel replyList;

  init(RepliesModel replies, ReviewAssignemt reviewAssignemt) async {
    if (!isCalledFirstTime) {
      isCalledFirstTime = true;
      _reviewAssignemt = reviewAssignemt;
      replyList = replies;
      print("replyList : ${replyList.author.name.firstname}");
    }
  }

  void validateGrade(String marks, String comments, BuildContext context) {
    if (marks.isEmpty && comments.isEmpty) {
      showToast("Please fill the grade details", context);
    } else if (marks.isEmpty) {
      showToast("Please enter marks", context);
    } else if (comments.isEmpty) {
      showToast("Please enter comments", context);
    } else if (comments.length < 10) {
      showToast("Comments must be greater than 10 characters", context);
    } else {
      gradeAssignment(comments, marks);
    }
  }

  void gradeAssignment(String comment, String mark) {
    var date = new DateTime.now();
    var answeredAt = DateFormat("yyyy-MM-ddTHH:mm:ss").format(date);
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
        var response = await API.getAssesment(_reviewAssignemt.postId);
        print(
            "response author ===========>>>>: ${response['replies'][0]['author']}");
        print("response task ===========>>>>: ${response['task']}");
        var data = {
          "anonymous": false,
          "author": response['replies'][0]['author'],
          "class": response['class']['_id'],
          "comments": 0,
          "commentsDisabled": false,
          "created_at": response['created_at'],
          "files": [],
          "graded": 0,
          "isCode": false,
          "likes": 0,
          "locked": false,
          "minified": true,
          "parent": response['replies'][0]['parent']['_id'],
          "pinned": false,
          "replies": [],
          "responseType": "submission",
          "school": response['school']['_id'],
          "solutions": 0,
          "submissionDetails": {"results": []},
          "submissions": 0,
          "submittable": true,
          "task": {
            "additionalMarks": 0,
            "additionalMaxMarks": 0,
            "comment": comment,
            "dueby": response['task']['dueby'],
            "grade": mark,
            "start": response['task']['start'],
            "title": response['task']['title']
          },
          "text": response['text'],
          "type": "Assignment",
          "updated_at": response['updated_at'],
          "users": [],
          "views": 0,
          "__v": 0,
          "_id": _reviewAssignemt.postId
        };
        print("data : $data");
        String message = await API.gradeAssignment(data);
        if (message != null) {
          Navigator.pop(context);
        }
        setState(ViewState.Idle);
//      showToast(message, context);
      }
    });
  }
}
