import 'package:flutter_redux/flutter_redux.dart';
import 'package:trainmoo/actions/home_actions.dart';
import 'package:trainmoo/app_state.dart';
import 'package:trainmoo/model/GradeModel.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';

class GradeListViewModel extends BaseModel {
  bool isFirstTime = false;
  GradeModel gradeModel;
  String classId;

  Map<String, String> header;

  init(String classId, state, context) async {
    if (!isFirstTime) {
      if (state.loadFirstTimeGrade == false) {
        header = await API.getAuthHeader();
        isFirstTime = true;
        this.classId = classId;
        getGradeList(context);
      } else {
        this.gradeModel = state.gradeList;
      }
    }
  }

  void getGradeList(context) {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
        gradeModel = await API.getGradeList(classId);
        StoreProvider.of<AppState>(context).dispatch(SetGradeList(gradeModel));
        StoreProvider.of<AppState>(context)
            .dispatch(SetLoadFirstTimeGrade(true));
      } else {
        showToast(AppLocalizations.of(context).internetMessage, context);
      }
      setState(ViewState.Idle);
    });
  }
}
