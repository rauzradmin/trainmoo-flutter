import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:trainmoo/app_state.dart';
import 'package:trainmoo/model/GradeModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';
import 'package:trainmoo/ui/view/Grade/GradeList/GradeListViewModel.dart';
import 'package:trainmoo/util/image_path.dart';

class GradeListView extends StatefulWidget {
  final String classId;

  const GradeListView({Key key, this.classId}) : super(key: key);
  @override
  _GradeListViewState createState() => _GradeListViewState();
}

class _GradeListViewState extends State<GradeListView> with BaseCommonWidget {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  @override
  Widget build(BuildContext context) {
    print("this is grade");
    return StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          print("state : ${state.loadFirstTimeGrade}");
          return BaseView<GradeListViewModel>(builder: (context, model, child) {
            model.init(widget.classId, state, context);
            return SafeArea(child: _getBody(model));
          });
        });
  }

  Widget _getBody(GradeListViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(GradeListViewModel model) {
    return Container(
      child: Column(
        children: <Widget>[
          commonAppBar(AppLocalizations.of(context).grades, context),
          Expanded(
              child: model.gradeModel != null &&
                      model.gradeModel.gradeDetails.length > 0
                  ? getGradeList(model)
                  : showMessageInCenter('Grade data not found'))
        ],
      ),
    );
  }

  getGradeList(GradeListViewModel model) {
    return Container(
      padding: const EdgeInsets.only(left: 24, right: 24, top: 10, bottom: 5),
      child: SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        header: WaterDropMaterialHeader(
          backgroundColor: Palette.accentColor,
        ),
        controller: _refreshController,
        onRefresh: () async {
          model.getGradeList(context);
          _refreshController.refreshCompleted();
        },
        child: ListView.builder(
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          itemCount: model.gradeModel.gradeDetails.length,
          itemBuilder: (BuildContext cont, int ind) {
            return model.gradeModel.gradeDetails[ind].gradeDetailsObject !=
                        null &&
                    model.gradeModel.gradeDetails[ind].gradeDetailsObject
                            .headersBean !=
                        null
                ? gradeItem(model.gradeModel.gradeDetails[ind], model)
                : Container();
          },
        ),
      ),
    );
  }

  gradeItem(GradeDetails gradeList, GradeListViewModel model) {
    return Card(
      elevation: 0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(UIHelper.cardRadius)),
      child: Container(
        margin: EdgeInsets.all(20),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            CircleAvatar(
                child: gradeList.studentModel.avatarThumbnail != null
                    ? loadCircleNetworkImage(
                        gradeList.studentModel.avatarThumbnail,
                        40,
                        model.header)
                    : Image.asset(
                        ImagePath.USER,
                        height: 40,
                        width: 40,
                      )),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    gradeList.studentModel.name,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: AppTextStyle.getDynamicFontStyle(
                        Colors.black, 16, FontType.Regular),
                  ),
                  Text(getSubtitleName(gradeList),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: AppTextStyle.getDynamicFontStyle(
                          Palette.listColor, 13, FontType.RobotoRegular)),
                ],
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(
                    gradeList.gradeDetailsObject.percentile == "null"
                        ? "0"
                        : gradeList.gradeDetailsObject.percentile,
                    style: AppTextStyle.getDynamicFontStyle(
                        Colors.black, 20, FontType.SemiBol)),
                Container(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Text("%",
                        style: AppTextStyle.getDynamicFontStyle(
                            Colors.black, 10, FontType.SemiBol))),
              ],
            )
          ],
        ),
      ),
    );
  }

  String getSubtitleName(GradeDetails gradeList) {
    return '${gradeList.gradeDetailsObject.headersBean.name} (${gradeList.gradeDetailsObject.status})';
  }
}
