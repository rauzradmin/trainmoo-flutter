import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:trainmoo/model/user_login_model.dart';
import 'package:trainmoo/shared_preference/user_preference.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/router.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:trainmoo/ui/widget/regular_button.dart';
import 'package:trainmoo/util/image_path.dart';

class UserDetailView extends StatefulWidget {
  @override
  _UserDetailViewState createState() => _UserDetailViewState();
}

class _UserDetailViewState extends State<UserDetailView> with BaseCommonWidget {
  UserLoginModel user;
  Map<String, String> header;

  String version;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Palette.screenBgColor,
        body: _getBaseContainer(),
      ),
    );
  }

  @override
  void initState() {
    _getUserDetail();
    _getAppVersion();
  }

  void _getUserDetail() async {
    Future<UserLoginModel> usd = UserPreference().getSavedInfo();
    usd.then((value) {
      user = value;
      setState(() {});
    });

    header = await API.getAuthHeader();
    setState(() {});
  }

  _getWidget(String name, String value) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(name,
            textAlign: TextAlign.left,
            style: AppTextStyle.getDynamicFontStyle(
                Palette.listColor, 10, FontType.RobotoRegular)),
        SizedBox(
          height: 5,
        ),
        Text(value != null ? value : '',
            style: AppTextStyle.getDynamicFontStyle(
                Colors.black, 15, FontType.Medium)),
      ],
    );
  }

  _getImage() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          width: 40,
          height: 40,
          decoration:
              BoxDecoration(shape: BoxShape.circle, color: Palette.blue),
          child: InkWell(
            onTap: () {
              Utility.launchURL("tel://${user.user.mobile}");
            },
            child: Image.asset(
              ImagePath.IC_CALL,
              height: 13,
              width: 13,
              alignment: Alignment.center,
            ),
          ),
        ),
        UIHelper.horizontalSpaceMedium,
        Container(
          width: 40,
          height: 40,
          decoration:
              BoxDecoration(shape: BoxShape.circle, color: Palette.blue),
          child: InkWell(
            onTap: () {
              Utility.launchURL("sms://${user.user.mobile}");
            },
            child: Image.asset(
              ImagePath.IC_PROFILE_CHAT,
              height: 13,
              width: 13,
              alignment: Alignment.center,
            ),
          ),
        ),
      ],
    );
  }

  Widget showFile() {
    return loadCircleNetworkImage(user.user.avatar.src, 90, header);
  }

  _getNameWidget() {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(UIHelper.cardRadius)),
      child: Container(
        padding: EdgeInsets.all(22),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(AppLocalizations.of(context).profilePicture,
                  style: AppTextStyle.getDynamicFontStyle(
                      Palette.listColor, 12, FontType.RobotoRegular)),
              UIHelper.verticalSpaceSmall,
              showFile(),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      _getWidget(
                          AppLocalizations.of(context).name,
                          user.user.name.firstname +
                              " " +
                              user.user.name.lastname),
                      UIHelper.verticalSpaceExtraSmall,
                      _getWidget(AppLocalizations.of(context).gender,
                          user.user.gender),
                      UIHelper.verticalSpaceExtraSmall,
                      user.user.dob != null
                          ? _getWidget(AppLocalizations.of(context).dateOfBirth,
                              user.user.dob)
                          : Container(),
                      UIHelper.verticalSpaceExtraSmall,
                      user.user.address != null
                          ? _getWidget(
                              AppLocalizations.of(context).address,
                              user.user.address.zip != null
                                  ? user.user.address.zip.toString()
                                  : " " +
                                      " " +
                                      user.user.address.city +
                                      " " +
                                      user.user.address.state +
                                      " " +
                                      user.user.address.country)
                          : Container(),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              _getImage()
            ]),
      ),
    );
  }

  _getGuardianWidget() {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(UIHelper.cardRadius)),
      child: Container(
        width: MediaQuery.of(context).size.width - 30,
        padding: EdgeInsets.all(22),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _getWidget(AppLocalizations.of(context).gurdianName,
                  user.user.name.firstname + " " + user.user.name.lastname),
              UIHelper.verticalSpaceExtraSmall,
              user.user.address != null
                  ? _getWidget(
                      AppLocalizations.of(context).address,
                      user.user.address.zip.toString() +
                          " " +
                          user.user.address.city +
                          " " +
                          user.user.address.state +
                          " " +
                          user.user.address.country)
                  : Container(),
              SizedBox(
                height: 20,
              ),
              _getImage()
            ]),
      ),
    );
  }

  _getSkillWidget() {
    return Card(
      elevation: 2,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(UIHelper.cardRadius)),
      child: Container(
        width: MediaQuery.of(context).size.width - 30,
        padding: EdgeInsets.all(22),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              user.user.skills != null && user.user.skills.length > 0
                  ? _getWidget(AppLocalizations.of(context).skills, _getSKill())
                  : Container(),
            ]),
      ),
    );
  }

  String _getSKill() {
    String skill = "";

    for (int i = 0; i < user.user.skills.length; i++) {
      if (i == user.user.skills.length - 1) {
        skill += user.user.skills[i];
      } else {
        skill += user.user.skills[i] + ", ";
      }
    }

    return skill;
  }

  Widget _getBaseContainer() {
    return Container(
      padding: EdgeInsets.only(bottom: 20),
      child: Column(
        children: <Widget>[
          commonAppBarWithIcon(AppLocalizations.of(context).myDetails, context),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.only(left: 12, right: 12),
                child: Column(
                  children: <Widget>[
                    _getNameWidget(),
                    _getGuardianWidget(),
                    _getSkillWidget(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 100,
                          height: 50,
                          child: RegularButton(
                            buttonLabel: AppLocalizations.of(context).logout,
                            onTap: () {
                              _showLogoutDialog();
                            },
                          ),
                        ),
                        UIHelper.horizontalSpaceMedium,
                        Container(
                          margin: EdgeInsets.only(top: 5),
                          child: Text(version,
                              style: AppTextStyle.getDynamicFontStyle(
                                  Palette.lightBlackColor,
                                  15,
                                  FontType.Medium)),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _getAppVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    version = packageInfo.version;
    setState(() {});
  }

  void _showLogoutDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(AppLocalizations.of(context).logout),
          content:
              new Text(AppLocalizations.of(context).areSureYouWantToLogout),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text(AppLocalizations.of(context).close),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text(AppLocalizations.of(context).logout),
              onPressed: () {
                Navigator.of(context).pop();
                UserPreference.logOut();
                Navigator.pushNamed(context, Screen.Login.toString());
              },
            ),
          ],
        );
      },
    );
  }
}
