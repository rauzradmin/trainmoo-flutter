import 'package:trainmoo/model/SessionModel.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:intl/intl.dart';

class MySessionViewModel extends BaseModel {
  bool isFirstTime = false;
  List<SessionModel> todaySessionList = [];
  List<SessionModel> thisWeekSessionList = [];
  List<SessionModel> thisMonthSessionList = [];
  List weekSessionList = [];
  List monthSessionList = [];
  List data = [];
  List dateListArray = [];
  static const int sunday = 1;
  static const int monday = 2;
  static const int tuesday = 3;
  static const int wednesday = 4;
  static const int thursday = 5;
  static const int friday = 6;
  static const int saturday = 7;
  DateTime currentDateTime = DateTime.now();

  init(state, setSessionList) async {
    if (!isFirstTime) {
      isFirstTime = true;
      if (state.loadFirstTimeSessionList == false) {
        getSessionList(setSessionList);
      } else {
        todaySessionList = state.todaySessionList;
        weekSessionList = state.weekSessionList;
        monthSessionList = state.monthSessionList;
      }
    }
  }

  void setToday() {
    setState(ViewState.Busy);
    currentDateTime = DateTime.now();
    setState(ViewState.Idle);
  }

  void getSessionList([setSessionList]) {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        todaySessionList = [];
        thisWeekSessionList = [];
        thisMonthSessionList = [];
        setState(ViewState.Busy);

        //today
        String todayDate = Utility.getSessionFormatDate(currentDateTime);
        todaySessionList = await API.getSessionData(todayDate, todayDate);

        //this Week
        DateTime weekLastDate = DateTime(currentDateTime.year,
            currentDateTime.month + 1, currentDateTime.day + 6);
        thisWeekSessionList = await API.getSessionData(
            Utility.getSessionFormatDate(currentDateTime),
            Utility.getSessionFormatDate(weekLastDate));
        weekSessionList = [];
        List data = [];
        List dateListArray = [];

        thisWeekSessionList.forEach((session) {
          var date = new DateFormat("dd-MMM-yyyy")
              .format(DateTime.parse(session.eventDate));
          if (dateListArray.indexOf(date) == -1) {
            dateListArray.add(date);
            data = thisWeekSessionList
                .where((element) =>
                    new DateFormat("dd-MMM-yyyy")
                        .format(DateTime.parse(element.eventDate)) ==
                    date)
                .toList();
            weekSessionList.add({"date": date, "data": data});
          }
        });

        //this month
        int lastDay =
            DateTime(currentDateTime.year, currentDateTime.month + 1, 0).day;
        DateTime monthStartDate =
            new DateTime(currentDateTime.year, currentDateTime.month, 1);
        DateTime monthEndDate =
            new DateTime(currentDateTime.year, currentDateTime.month, lastDay);
        thisMonthSessionList = await API.getSessionData(
            Utility.getSessionFormatDate(monthStartDate),
            Utility.getSessionFormatDate(monthEndDate));
        monthSessionList = [];
        data = [];
        dateListArray = [];

        thisMonthSessionList.forEach((session) {
          var date = new DateFormat("dd-MMM-yyyy")
              .format(DateTime.parse(session.eventDate));
          if (dateListArray.indexOf(date) == -1) {
            dateListArray.add(date);
            data = thisMonthSessionList
                .where((element) =>
                    new DateFormat("dd-MMM-yyyy")
                        .format(DateTime.parse(element.eventDate)) ==
                    date)
                .toList();
            monthSessionList.add({"date": date, "data": data});
          }
        });

        setState(ViewState.Idle);
        setSessionList(todaySessionList, weekSessionList, monthSessionList);
      } else {
        showToast(AppLocalizations.of(context).internetMessage, context);
      }
    });
  }

  List getSessionsFromIndex(int tabIndex) {
    switch (tabIndex) {
      case 0:
        return todaySessionList;
      case 1:
        return weekSessionList;
      case 2:
        return monthSessionList;
    }
  }
}
