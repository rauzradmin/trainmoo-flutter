import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:trainmoo/actions/home_actions.dart';
import 'package:trainmoo/app_state.dart';
import 'package:trainmoo/model/SessionModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:trainmoo/util/image_path.dart';
import 'package:intl/intl.dart';
import 'my_session_view_model.dart';

class MySessionView extends StatefulWidget {
  @override
  _MySessionViewState createState() => _MySessionViewState();
}

class _MySessionViewState extends State<MySessionView>
    with BaseCommonWidget, SingleTickerProviderStateMixin {
  TabController tabController;
  int tabIndex = 0;

  CalendarController _calendarController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(vsync: this, length: 3);
    tabController.addListener(toggleTab);
    _calendarController = CalendarController();
  }

  void toggleTab() {
    setState(() {
      tabIndex = tabController.index;
    });
  }

  @override
  void dispose() {
    _calendarController.dispose();
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          return BaseView<MySessionViewModel>(builder: (context, model, child) {
            print("the state is 2 : ${state.loadFirstTimeSessionList}");
            model.init(state,
                (todaySessionList, weekSessionList, monthSessionList) {
              StoreProvider.of<AppState>(context)
                  .dispatch(SetLoadFirstTimeSessionList(true));
              StoreProvider.of<AppState>(context)
                  .dispatch(SetTodaySessionList(todaySessionList));
              StoreProvider.of<AppState>(context)
                  .dispatch(SetWeekSessionList(weekSessionList));
              StoreProvider.of<AppState>(context)
                  .dispatch(SetMonthSessionList(monthSessionList));
            });

            return SafeArea(child: _getBody(model));
          });
        });
  }

  Widget _getBody(MySessionViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(MySessionViewModel model) {
    //print("this is data ${model.weekSessionList}");
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        commonAppBarWithoutArrow(
            AppLocalizations.of(context).mySession, context),
        Expanded(
            child: Container(
                padding: const EdgeInsets.only(left: 24, right: 24),
                child: DefaultTabController(
                  length: 3,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 50,
                        margin: EdgeInsets.only(top: 10, bottom: 10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              new BorderRadius.all(Radius.circular(3)),
                        ),
                        child: Row(
                          children: <Widget>[
                            IconButton(
                              icon: Icon(
                                Icons.arrow_back_ios,
                                color: Palette.listColor,
                              ),
                              onPressed: () {
                                newDate(-1, model);
                              },
                            ),
                            Expanded(
                              child: InkWell(
                                onTap: () {
                                  datePicker(context, model);
                                },
                                child: Text(
                                  showDate(model),
                                  overflow: TextOverflow.clip,
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: AppTextStyle.getDynamicFontStyle(
                                          Palette.hintTextColor,
                                          20,
                                          FontType.Regular)
                                      .copyWith(
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                            ),
                            IconButton(
                              icon: Icon(
                                Icons.arrow_forward_ios,
                                color: Palette.listColor,
                              ),
                              onPressed: () {
                                newDate(1, model);
                              },
                            ),
                          ],
                        ),
                      ),
//                      Container(
//                        margin: EdgeInsets.only(top: 15),
//                        child: tableWidget(),
//                      ),
                      Card(
                        margin: EdgeInsets.only(top: 10, left: 5, right: 5),
                        elevation: 1,
                        child: Container(
                          child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                getTitleWidget(
                                    DateFormat("dd-MMM-yyyy")
                                                .format(DateTime.now()) ==
                                            DateFormat("dd-MMM-yyyy")
                                                .format(model.currentDateTime)
                                        ? 'Today'
                                        : DateFormat("dd")
                                            .format(model.currentDateTime)
                                            .toString(),
                                    model.todaySessionList.length,
                                    0),
                                Container(
                                    height: 40,
                                    child: VerticalDivider(
                                      thickness: 2,
                                      color: Palette.dividerColor,
                                    )),
                                getTitleWidget(
                                    'Week', model.weekSessionList.length, 1),
                                Container(
                                    height: 40,
                                    child: VerticalDivider(
                                      thickness: 2,
                                      color: Palette.dividerColor,
                                    )),
                                getTitleWidget(
                                    DateFormat("dd-MMM-yyyy")
                                                .format(DateTime.now()) ==
                                            DateFormat("dd-MMM-yyyy")
                                                .format(model.currentDateTime)
                                        ? 'Month'
                                        : DateFormat("MMMM")
                                            .format(model.currentDateTime)
                                            .toString(),
                                    model.monthSessionList.length,
                                    2),
//                  Container(
//                      height: 40,
//                      child: VerticalDivider(
//                        thickness: 2,
//                        color: Palette.dividerColor,
//                      )),
//                  getTitleWidget('Leave', 0, 3),
                              ]),
                        ),
                      ),
                      Container(
                          padding: const EdgeInsets.only(
                            top: 10,
                          ),
                          child: MaterialButton(
                              onPressed: () {
                                model.setToday();
                              },
                              color: Palette.accentColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                              child: Text(
                                "Go To Today",
                                style: AppTextStyle.getDynamicFontStyle(
                                    Colors.white, 15, FontType.RobotoMedium),
                              ))),

                      Expanded(
                          child: sessionList(
                              model.getSessionsFromIndex(tabIndex))),
                    ],
                  ),
                ))
            /*NestedScrollView(
              headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  SliverList(
                    delegate: SliverChildListDelegate([
                      Container(
                        padding: EdgeInsets.only(top: 5, bottom: 5),
                      )
                    ]),
                  ),
                ];
              },
              body: Container(
                  padding: const EdgeInsets.only(left: 24, right: 24),
                  child: DefaultTabController(
                    length: 3,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 15),
                          child: tableWidget(),
                        ),
                        Card(
                          margin: EdgeInsets.only(top: 10, left: 5, right: 5),
                          elevation: 1,
                          child: Container(
                            child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  getTitleWidget('Today',
                                      model.todaySessionList.length, 0),
                                  Container(
                                      height: 40,
                                      child: VerticalDivider(
                                        thickness: 2,
                                        color: Palette.dividerColor,
                                      )),
                                  getTitleWidget('Week',
                                      model.thisWeekSessionList.length, 1),
                                  Container(
                                      height: 40,
                                      child: VerticalDivider(
                                        thickness: 2,
                                        color: Palette.dividerColor,
                                      )),
                                  getTitleWidget('Month',
                                      model.thisMonthSessionList.length, 2),
//                  Container(
//                      height: 40,
//                      child: VerticalDivider(
//                        thickness: 2,
//                        color: Palette.dividerColor,
//                      )),
//                  getTitleWidget('Leave', 0, 3),
                                ]),
                          ),
                        ),
                        Expanded(
                            child: sessionList(
                                model.getSessionsFromIndex(tabIndex))),
                      ],
                    ),
                  ))),*/
            )
      ],
    );
  }

  sessionList(sessionList) {
    String today = new DateFormat("dd-MMM-yyyy").format(DateTime.now());
    return sessionList == null || sessionList.length <= 0
        ? Center(
            child: Text('No Data'),
          )
        : tabIndex == 1 || tabIndex == 2
            ? Container(
                padding: EdgeInsets.only(top: 10),
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: sessionList.length,
                  itemBuilder: (BuildContext context, int i) {
                    return Column(
                      children: [
                        Container(
                            margin: EdgeInsets.only(bottom: 10, top: 10),
                            padding: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 10),
                            decoration: BoxDecoration(
                                color: Colors.blue,
                                borderRadius: BorderRadius.circular(50)),
                            child: Text(
                                today != sessionList[i]['date']
                                    ? sessionList[i]['date']
                                    : "Today",
                                style: AppTextStyle.getDynamicFontStyle(
                                    Colors.white, 13, FontType.Medium))),
                        ListView.builder(
                          shrinkWrap: true,
                          primary: false,
                          itemCount: sessionList[i]['data'].length,
                          itemBuilder: (BuildContext context, int ind) {
                            return studentItem(sessionList[i]['data'][ind]);
                          },
                        )
                      ],
                    );
                  },
                ),
              )
            : Container(
                padding: EdgeInsets.only(top: 10),
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: sessionList.length,
                  itemBuilder: (BuildContext context, int ind) {
                    return studentItem(sessionList[ind]);
                  },
                ),
              );
  }

  studentItem(SessionModel sessionList) {
    return Container(
      child: GestureDetector(
        child: Card(
          elevation: 1,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Padding(
            padding:
                EdgeInsets.only(top: 10.0, bottom: 10.0, left: 5, right: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: 5,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      sessionList.start,
                      style: AppTextStyle.getDynamicFontStyle(
                          Colors.black, 20, FontType.Medium),
                    ),
                    Text(
                      sessionList.end,
                      style: AppTextStyle.getDynamicFontStyle(
                          Palette.listColor, 14, FontType.RobotoMedium),
                    ),
                    IconButton(
                        iconSize: 33,
                        icon: sessionList.type == 'event' &&
                                sessionList.attendanceDone == true
                            ? Image.asset(ImagePath.SELECTED_TRUE)
                            : Image.asset(ImagePath.UNSELECTED_TRUE),
                        onPressed: () {}),
                  ],
                ),
                Container(
                    height: 90,
                    margin: EdgeInsets.only(left: 10, right: 10),
                    child: VerticalDivider(
                      color: Palette.listColor,
                      width: 2,
                    )),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        sessionList.title,
                        style: AppTextStyle.getDynamicFontStyle(
                            Colors.black, 20, FontType.SemiBol),
                      ),
                      Container(
                          child: Html(
                              data: sessionList.details == null
                                  ? ''
                                  : sessionList.details,
                              onLinkTap: (String link) {
                                Utility.launchURL(link);
                              },
                              showImages: true,
                              useRichText: true,
                              linkStyle: AppTextStyle.getDynamicFontStyle(
                                  Colors.blue, 16, FontType.RobotoRegular),
                              renderNewlines: true,
                              defaultTextStyle:
                                  AppTextStyle.getDynamicFontStyle(Colors.black,
                                      13, FontType.RobotoMedium))),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        onTap: () {},
      ),
    );
  }

  tableWidget() {
    return TableCalendar(
      headerStyle: HeaderStyle(
        headerMargin: EdgeInsets.only(bottom: 22),
        headerPadding: EdgeInsets.only(bottom: 2, top: 1),
        formatButtonVisible: false,
        centerHeaderTitle: true,
        leftChevronIcon: Icon(
          Icons.keyboard_arrow_left,
          color: Palette.listColor,
        ),
        rightChevronIcon: Icon(
          Icons.keyboard_arrow_right,
          color: Palette.listColor,
        ),
        decoration: new BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            color: Colors.white),
      ),
      locale: 'en_US',
      calendarStyle: CalendarStyle(
          selectedColor: Palette.blue,
          todayColor: Palette.blue,
          markersColor: Colors.blue,
          contentPadding: EdgeInsets.all(0),
          canEventMarkersOverflow: false,
          outsideWeekendStyle:
              TextStyle(color: Palette.listColor, fontSize: 16),
          weekdayStyle: TextStyle(
              color: Palette.primaryTextColor,
              fontSize: 16,
              fontWeight: FontWeight.w500),
          outsideStyle: TextStyle(
              color: Palette.listColor,
              fontSize: 16,
              fontWeight: FontWeight.w500),
          selectedStyle: TextStyle(
              color: Palette.white, fontSize: 16, fontWeight: FontWeight.w500),
          outsideHolidayStyle:
              TextStyle(color: Palette.listColor, fontSize: 16),
          todayStyle: TextStyle(
              color: Palette.lightBlackColor,
              fontSize: 16,
              fontWeight: FontWeight.w500),
          unavailableStyle: TextStyle(color: Palette.listColor, fontSize: 16),
          outsideDaysVisible: false,
          weekendStyle: TextStyle(color: Palette.listColor, fontSize: 16),
          holidayStyle: TextStyle(color: Palette.redColor, fontSize: 16)),
      daysOfWeekStyle: DaysOfWeekStyle(
        weekendStyle:
            TextStyle().copyWith(color: Palette.listColor, fontSize: 16),
        weekdayStyle:
            TextStyle().copyWith(color: Palette.lightBlackColor, fontSize: 16),
      ),
      startingDayOfWeek: StartingDayOfWeek.sunday,
      initialCalendarFormat: CalendarFormat.month,
      availableGestures: AvailableGestures.horizontalSwipe,
      initialSelectedDay: _calendarController.focusedDay,
      calendarController: _calendarController,
    );
  }

  getTitleWidget(String title, int count, int position) {
    return InkWell(
      onTap: () {
        tabIndex = position;
        setState(() {});
      },
      child: Container(
        padding: EdgeInsets.only(top: 10),
        width: 70,
        child: Column(
          children: <Widget>[
            Text(count.toString(),
                style: AppTextStyle.getDynamicFontStyle(
                    tabIndex == position
                        ? Palette.accentColor
                        : Palette.primaryTextColor,
                    21,
                    FontType.RobotoMedium)),
            Text(title,
                style: AppTextStyle.getDynamicFontStyle(
                    Palette.listColor, 13, FontType.Medium)),
            SizedBox(
              height: 10,
            ),
            Container(
              width: double.infinity,
              height: 7,
              color: tabIndex == position
                  ? Palette.accentColor
                  : Colors.transparent,
            )
          ],
        ),
      ),
    );
  }

  String showDate(MySessionViewModel model) {
    return Utility.attendaceDateFormate(model.currentDateTime);
  }

  newDate(int day, MySessionViewModel model) {
    // var dTime = model.currentDateTime.add(new Duration(days: day));
    // if (new DateTime.now().isAfter(dTime)) {
    //   model.currentDateTime =
    //       model.currentDateTime.add(new Duration(days: day));
    //   model.getSessionList();
    //   setState(() {});
    // }
    model.currentDateTime = model.currentDateTime.add(new Duration(days: day));
    model.getSessionList();
    // setState(() {});
  }

  void datePicker(BuildContext context, MySessionViewModel model) async {
    DateTime time = await showDatePicker(
      context: context,
      initialDate: model.currentDateTime,
      firstDate: DateTime(2019),
      lastDate: DateTime(
          int.parse(DateFormat('yyyy').format(new DateTime.now())) + 10),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light(),
          child: child,
        );
      },
    );
    if (time != null) {
      model.currentDateTime = time;
      model.getSessionList();
      setState(() {});
    }
  }
}
