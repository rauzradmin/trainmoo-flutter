import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:trainmoo/model/Database/DatabaseHelper.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/Question.dart';
import 'package:trainmoo/model/QuestionCategory.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';

class CreateQuizViewModel extends BaseModel {
  DateTime dueDate;
  bool isFirstTime = false;
  List<ProgramClassesModel> classList = [];
  List<String> questionIdList = [];
  ProgramClassesModel selectedClass;
  bool isShowSetting = true;
  List<Map<String, dynamic>> attendanceData = [];
  List<QuestionCategory> questionCategories = [];

  DateTime selectedStartDate;

  bool isAllowTrack = true, isRandom = true, isChangeView = true;

  init() async {
    if (!isFirstTime) {
      isFirstTime = true;
      classList = await DatabaseHelper().getClasses();
      getQuestionCategories();
    }
  }

  void validateQuizDetail(String title, String desc, String mark,
      String duration, BuildContext context, List<Question> result) {
    if (title.isEmpty && desc.isEmpty) {
      showToast("Please fill the Quiz details", context);
    } else if (selectedClass == null) {
      showToast("Please select class", context);
    } else if (title.isEmpty) {
      showToast("Please enter title", context);
    } else if (dueDate == null) {
      print('date$dueDate');
      showToast("select date", context);
    } else if (selectedStartDate == null) {
      showToast("Please select start date", context);
    } else if (duration == null && duration.length > 1 && duration != '0') {
      showToast("Please add quiz duration time", context);
    } else if (desc.isEmpty) {
      showToast("Please enter description", context);
    } else if (desc.length < 10) {
      showToast("Description must be greater than 10 characters", context);
    } else {
      for (int i = 0; i < result.length; i++) {
        questionIdList.add(result[i].id);
      }
      createQuiz(title, desc, mark, result, questionIdList, duration);
    }
  }

  void createQuiz(String title, String desc, String mark,
      List<Question> questionId, List<String> questionIdList, String duration) {
    var format = new DateFormat("hh:mm a");
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);

        String result = await API.createQuiz(
            selectedClass.id,
            title,
            desc,
            mark,
            format.format(selectedStartDate),
            format.format(dueDate),
            selectedStartDate.toIso8601String(),
            dueDate.toIso8601String(),
            isAllowTrack,
            isRandom,
            isChangeView,
            questionIdList,
            attendanceData,
            duration);
        if (result != null) {
          if (result.length > 0) {
            showToast("Quiz Created successfully.", context);
            Timer.periodic(new Duration(milliseconds: 6000), (timer) {
              Navigator.of(context).pop();
              timer.cancel();
            });
          }
          showToast(result, context);
        }
        setState(ViewState.Idle);
      }
    });
  }

  void getQuestionCategories() {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
        questionCategories = await API.getQuestionCategories();
        setState(ViewState.Idle);
      }
    });
  }

  List<String> getQuestionCategoriesStringArray() {
    List<String> categories = [];
    for (QuestionCategory category in questionCategories) {
      categories.add(category.id);
    }
    return categories;
  }
}
