import 'package:flutter/material.dart';
import 'package:trainmoo/model/Question.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:trainmoo/ui/widget/large_text_form_field_with_padding.dart';
import 'package:trainmoo/ui/widget/regular_button.dart';
import 'package:trainmoo/ui/widget/regular_text_form_field.dart';
import 'package:trainmoo/ui/widget/text_form_field_with_padding.dart';

import 'add_question_view_model.dart';

class AddQuestionView extends StatefulWidget {
  final List<String> questionCategories;
  final Question question;
  AddQuestionView({this.questionCategories, this.question});
  @override
  _AddQuestionViewState createState() => _AddQuestionViewState();
}

class _AddQuestionViewState extends State<AddQuestionView>
    with BaseCommonWidget {
  TextEditingController quizQuestionController = new TextEditingController();
  TextEditingController descController = new TextEditingController();
  TextEditingController answerAController = new TextEditingController();
  TextEditingController answerBController = new TextEditingController();
  TextEditingController answerCController = new TextEditingController();
  TextEditingController answerDController = new TextEditingController();

  @override
  void initState() {
    setQuestionForEdit(widget.question);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<AddQuestionViewModel>(builder: (context, model, child) {
      model.init(widget.question);
      return SafeArea(child: _getBody(model));
    });
  }

  Widget _getBody(AddQuestionViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(AddQuestionViewModel model) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(245, 244, 240, 0),
      body: Column(
        children: <Widget>[
          commonAppBarWithCancelIcon(
              AppLocalizations.of(context).createQuestion, context),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 24, right: 24, bottom: 5, top: 10),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 50,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              new BorderRadius.all(Radius.circular(3)),
                        ),
                        padding: EdgeInsets.only(left: 18, right: 10),
                        child: DropdownButton<String>(
                          underline: Container(
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom:
                                        BorderSide(color: Colors.transparent))),
                          ),
                          isExpanded: true,
                          icon: Icon(
                            Icons.arrow_drop_down,
                            color: Palette.accentColor,
                            size: 25,
                          ),
                          hint: new Text(
                              AppLocalizations.of(context).selectCategory,
                              style: AppTextStyle.hintStyle()),
                          value: model.selectedQuestionCategory,
                          onChanged: (String newValue) {
                            setState(() {
                              model.selectedQuestionCategory = newValue;
                              model.getTempQuestion();
                            });
                          },
                          items: widget.questionCategories.map((String role) {
                            return new DropdownMenuItem<String>(
                              value: role,
                              child: new Text(
                                role,
                                style: TextStyle(color: Colors.black),
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        height: 50,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              new BorderRadius.all(Radius.circular(3)),
                        ),
                        padding: EdgeInsets.only(left: 18, right: 10),
                        child: DropdownButton<String>(
                          underline: Container(
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom:
                                        BorderSide(color: Colors.transparent))),
                          ),
                          isExpanded: true,
                          icon: Icon(
                            Icons.arrow_drop_down,
                            color: Palette.accentColor,
                            size: 25,
                          ),
                          hint: new Text(
                              AppLocalizations.of(context).selectQuestionLevel,
                              style: AppTextStyle.hintStyle()),
                          value: model.selectedQuestionLevel,
                          onChanged: (String newValue) {
                            setState(() {
                              model.selectedQuestionLevel = newValue;
                              model.getTempQuestion();
                            });
                          },
                          items: model.selectQuestionLevel.map((String role) {
                            return new DropdownMenuItem<String>(
                              value: role,
                              child: new Text(
                                role,
                                style: TextStyle(color: Colors.black),
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                      Container(
                        height: 50,
                        margin: EdgeInsets.only(top: 10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              new BorderRadius.all(Radius.circular(3)),
                        ),
                        padding: EdgeInsets.only(left: 18, right: 10),
                        child: DropdownButton<Question>(
                          underline: Container(
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom:
                                        BorderSide(color: Colors.transparent))),
                          ),
                          isExpanded: true,
                          icon: Icon(
                            Icons.arrow_drop_down,
                            color: Palette.accentColor,
                            size: 25,
                          ),
                          hint: new Text(
                              AppLocalizations.of(context).selectFromTemplate,
                              style: AppTextStyle.hintStyle()),
                          value: model.selectedQuestion,
                          onChanged: (Question newValue) {
                            setState(() {
                              model.selectedQuestion = newValue;
                              setQuestionValues(model, model.selectedQuestion);
                            });
                          },
                          items: model.tempQuestionList.map((Question role) {
                            return new DropdownMenuItem<Question>(
                              value: role,
                              child: new Text(
                                Utility.removeAllHtmlTags(role.question),
                                style: TextStyle(color: Colors.black),
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                      RegularTextFormFieldWithPadding(
                        textEditingController: quizQuestionController,
                        hintText: AppLocalizations.of(context).quizQuestion,
                        textAlign: TextAlign.left,
                        inputType: TextInputType.text,
                      ),
                      LargeTextFormFieldWithPadding(
                        textEditingController: descController,
                        hintText: AppLocalizations.of(context).description,
                        textAlign: TextAlign.left,
                        inputType: TextInputType.text,
                      ),
                      Container(
                        alignment: Alignment.topLeft,
                        margin: EdgeInsets.only(top: 15, bottom: 5),
                        child: Text(
                          AppLocalizations.of(context).answers + ':',
                          style: AppTextStyle.getDynamicFontStyle(
                              Palette.listColor, 16, FontType.Medium),
                        ),
                      ),
                      Container(
                        color: Palette.white,
                        padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: RegularTextFormField(
                                textEditingController: answerAController,
                                hintText:
                                    AppLocalizations.of(context).answer + ' A',
                                textAlign: TextAlign.left,
                                inputType: TextInputType.text,
                              ),
                            ),
                            Checkbox(
                              value: model.answer1,
                              onChanged: (value) {
                                model.answer1 = value;
                                model.setAnswerOne();
                              },
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
                        color: Palette.white,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: RegularTextFormField(
                                textEditingController: answerBController,
                                hintText:
                                    AppLocalizations.of(context).answer + ' B',
                                textAlign: TextAlign.left,
                                inputType: TextInputType.text,
                              ),
                            ),
                            Checkbox(
                              value: model.answer2,
                              onChanged: (value) {
                                model.answer2 = value;
                                model.setAnswerTwo();
                              },
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
                        color: Palette.white,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: RegularTextFormField(
                                textEditingController: answerCController,
                                hintText:
                                    AppLocalizations.of(context).answer + ' C',
                                textAlign: TextAlign.left,
                                inputType: TextInputType.text,
                              ),
                            ),
                            Checkbox(
                              value: model.answer3,
                              onChanged: (value) {
                                model.answer3 = value;
                                model.setAnswerThree();
                              },
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
                        color: Palette.white,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: RegularTextFormField(
                                textEditingController: answerDController,
                                hintText:
                                    AppLocalizations.of(context).answer + ' D',
                                textAlign: TextAlign.left,
                                inputType: TextInputType.text,
                              ),
                            ),
                            Checkbox(
                              value: model.answer4,
                              onChanged: (value) {
                                model.answer4 = value;
                                model.setAnswerFour();
                              },
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(bottom: 30, top: 30),
                        child: RegularButton(
                          buttonLabel: AppLocalizations.of(context).addQuestion,
                          onTap: () {
                            model.validateQuestion(
                              quizQuestionController.text,
                              descController.text,
                              answerAController.text,
                              answerBController.text,
                              answerCController.text,
                              answerDController.text,
                              context,
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  void setQuestionValues(
      AddQuestionViewModel model, Question selectedQuestion) {
    quizQuestionController.text =
        Utility.removeAllHtmlTags(selectedQuestion.question);
    descController.text =
        Utility.removeAllHtmlTags(selectedQuestion.explanation);
    if (selectedQuestion.options != null &&
        selectedQuestion.options.length > 0) {
      for (int i = 0; i < selectedQuestion.options.length; i++) {
        switch (i) {
          case 0:
            answerAController.text =
                Utility.removeAllHtmlTags(selectedQuestion.options[i].text);
            model.answer1 = selectedQuestion.options[i].answer;
            break;
          case 1:
            answerBController.text =
                Utility.removeAllHtmlTags(selectedQuestion.options[i].text);
            model.answer2 = selectedQuestion.options[i].answer;
            break;
          case 2:
            answerCController.text =
                Utility.removeAllHtmlTags(selectedQuestion.options[i].text);
            model.answer3 = selectedQuestion.options[i].answer;
            break;
          case 3:
            answerDController.text =
                Utility.removeAllHtmlTags(selectedQuestion.options[i].text);
            model.answer4 = selectedQuestion.options[i].answer;
            break;
        }
      }
    }
    setState(() {});
  }

  void setQuestionForEdit(Question selectedQuestion) {
    if (selectedQuestion == null) return;
    quizQuestionController.text = selectedQuestion.question;
    descController.text = selectedQuestion.explanation;
    if (selectedQuestion.options != null &&
        selectedQuestion.options.length > 0) {
      for (int i = 0; i < selectedQuestion.options.length; i++) {
        switch (i) {
          case 0:
            answerAController.text = selectedQuestion.options[i].text;
            break;
          case 1:
            answerBController.text = selectedQuestion.options[i].text;
            break;
          case 2:
            answerCController.text = selectedQuestion.options[i].text;
            break;
          case 3:
            answerDController.text = selectedQuestion.options[i].text;
            break;
        }
      }
    }
    setState(() {});
  }
}
