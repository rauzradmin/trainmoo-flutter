import 'dart:io';

import 'package:flutter/material.dart';
import 'package:trainmoo/model/Question.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';

class AddQuestionViewModel extends BaseModel {
  Future<File> imageFile;

  String selectedTemplate;
  String selectedQuestionCategory;
  Question selectedQuestion;
  List<Question> questionList = [];
  List<Question> tempQuestionList = [];
  List<String> selectTemplateArray = ["Mc", "Sc"];
  List<String> selectQuestionLevel = ["Easy", "Medium", "Hard"];
  String selectedQuestionLevel;
  bool answer1 = false, answer2 = false, answer3 = false, answer4 = false;
  Question question;
  bool isFirstTime = true;

  void init(Question question) {
    if (isFirstTime) {
      this.question = question;
      isFirstTime = false;
      setQuestionData(question);
      getTempQuestion();
    }
  }

  void validateQuestion(String question, String desc, String answerA,
      String answerB, String answerC, String answerD, BuildContext context) {
    if (question.isEmpty &&
        desc.isEmpty &&
        answerA.isEmpty &&
        answerB.isEmpty &&
        answerC.isEmpty &&
        answerD.isEmpty) {
      showToast("Please fill the Question Answer", context);
    } else if (question.isEmpty) {
      showToast("Please enter Question", context);
      return;
    } else if (selectedQuestionLevel == null) {
      showToast("Please select Question Type", context);
      return;
    } else if (selectedQuestionCategory == null) {
      showToast("Please select Question category", context);
      return;
    } else if (desc.length < 10) {
      showToast("Description must be greater than 10 characters", context);
      return;
    } else if (answerA.isEmpty) {
      showToast("Please enter Answer A", context);
      return;
    } else if (answerB.isEmpty) {
      showToast("Please enter Answer B", context);
      return;
    } else if (answerC.isEmpty) {
      showToast("Please enter Answer C", context);
      return;
    } else if (answerD.isEmpty) {
      showToast("Please enter Answer D", context);
      return;
    } else if (answer1 || answer2 || answer3 || answer4) {
      addNewQuestion(
          question,
          answerA,
          answerB,
          answerC,
          answerD,
          answer1 == true
              ? "answer1"
              : answer2 == true
                  ? "answer2"
                  : answer3 == true
                      ? "answer3"
                      : answer4 == true ? "answer4" : "",
          desc,
          context);
    } else {
      showToast("Please mark on right answer", context);
    }
  }

  void addNewQuestion(
      String question,
      String option1,
      String option2,
      String option3,
      String option4,
      String answer,
      String desc,
      BuildContext context) {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
        Question returnQuestion = await API.saveQuestion(
            question,
            desc,
            option1,
            option2,
            option3,
            option4,
            answer,
            context,
            selectedQuestionLevel,
            selectedQuestionCategory);
        setState(ViewState.Idle);

        if (returnQuestion != null) Navigator.pop(context, returnQuestion);
      }
    });
  }

  void getTempQuestion() {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
        selectedQuestion = null;
        String id = '';
        if (selectedQuestionCategory != null) id = selectedQuestionCategory;
        tempQuestionList = await API.getTempQuestion(id, selectedQuestionLevel);
        setState(ViewState.Idle);
      }
    });
  }

  void setAnswerFour() {
    answer4 = true;
    answer3 = false;
    answer2 = false;
    answer1 = false;
    notifyListeners();
  }

  void setAnswerThree() {
    answer4 = false;
    answer3 = true;
    answer2 = false;
    answer1 = false;
    notifyListeners();
  }

  void setAnswerTwo() {
    answer4 = false;
    answer3 = false;
    answer2 = true;
    answer1 = false;
    notifyListeners();
  }

  void setAnswerOne() {
    answer4 = false;
    answer3 = false;
    answer2 = false;
    answer1 = true;
    notifyListeners();
  }

  void setQuestionData(Question question) {
    if (question == null) return;
    selectedQuestionLevel = question.level;
    selectedQuestionCategory = question.category;

    if (question.options != null && question.options.length > 0) {
      for (int i = 0; i < question.options.length; i++) {
        switch (i) {
          case 0:
            answer1 = question.options[i].answer;
            break;
          case 1:
            answer2 = question.options[i].answer;
            break;
          case 2:
            answer3 = question.options[i].answer;
            break;
          case 3:
            answer4 = question.options[i].answer;
            break;
        }
      }
    }
    question = null;
  }
}
