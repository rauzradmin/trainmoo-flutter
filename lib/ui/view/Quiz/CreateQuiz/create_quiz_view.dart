import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/Question.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:trainmoo/ui/view/Quiz/CreateQuiz/create_quiz_view_model.dart';
import 'package:trainmoo/ui/widget/date_picker.dart';
import 'package:trainmoo/ui/widget/drop_down_student_list.dart';
import 'package:trainmoo/ui/widget/large_text_form_field_with_padding.dart';
import 'package:trainmoo/ui/widget/regular_button.dart';
import 'package:trainmoo/ui/widget/text_form_field_with_padding.dart';

import 'AddQuestion/add_question_view.dart';

class CreateQuizView extends StatefulWidget {
  @override
  _CreateQuizViewState createState() => _CreateQuizViewState();
}

class _CreateQuizViewState extends State<CreateQuizView> with BaseCommonWidget {
  TextEditingController titleController = new TextEditingController();
  TextEditingController markController = new TextEditingController();
  TextEditingController durationController = new TextEditingController();

  TextEditingController descController = new TextEditingController();
  TextEditingController questionControlle = new TextEditingController();

  List<Question> result = [];

  @override
  Widget build(BuildContext context) {
    return BaseView<CreateQuizViewModel>(builder: (context, model, child) {
      model.init();
      return SafeArea(
          child: _getBody(model));
    });
  }

  Widget _getBody(CreateQuizViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(CreateQuizViewModel model) {
    return Column(
      children: <Widget>[
        commonAppBarWithCancelIcon(
            AppLocalizations.of(context).createQuiz, context),
        Expanded(
          child: SingleChildScrollView(
            child: Container(
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 24, right: 24, bottom: 5, top: 10),
                child: Column(
                  children: <Widget>[
                    DropDownList(
                      classList: model.classList,
                      onTap: (ProgramClassesModel selectedChooseRole) {
                        setState(() {
                          model.selectedClass = selectedChooseRole;
                        });
                      },
                      selectedClass: model.selectedClass,
                    ),
                    RegularTextFormFieldWithPadding(
                      textEditingController: titleController,
                      hintText: AppLocalizations.of(context).quizTitle,
                      textAlign: TextAlign.left,
                      inputType: TextInputType.text,
                    ),
                    RegularTextFormFieldWithPadding(
                      textEditingController: markController,
                      hintText: AppLocalizations.of(context).mark,
                      textAlign: TextAlign.left,
                      inputType: TextInputType.number,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: DatePicker(
                            hint: AppLocalizations.of(context).startDateTime,
                            onChanged: (dt) =>
                                setState(() => model.selectedStartDate = dt),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: DatePicker(
                            hint: AppLocalizations.of(context).dueDate,
                            onChanged: (dt) =>
                                setState(() => model.dueDate = dt),
                          ),
                        ),
                      ],
                    ),
                    RegularTextFormFieldWithPadding(
                      textEditingController: durationController,
                      hintText: AppLocalizations.of(context).duration,
                      textAlign: TextAlign.left,
                      inputType: TextInputType.number,
                    ),
                    LargeTextFormFieldWithPadding(
                      textEditingController: descController,
                      hintText: AppLocalizations.of(context).description,
                      textAlign: TextAlign.left,
                      inputType: TextInputType.text,
                    ),
                    Column(
                      children: List.generate(result.length, (index) {
                        return InkWell(
                          onTap: () async {
                            Question question = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AddQuestionView(
                                        questionCategories: model
                                            .getQuestionCategoriesStringArray(),
                                        question: result[index],
                                      )),
                            );
                            if (question != null) {
                              result.add(question);
                            }
                            setState(() {});
                          },
                          child: Container(
                            height: 50,
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.only(left: 0, right: 0, top: 10),
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  new BorderRadius.all(Radius.circular(3)),
                            ),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    'Question:  ' +
                                        Utility.removeAllHtmlTags(
                                            result[index].question),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                                Icon(
                                  Icons.chevron_right,
                                  color: Colors.grey,
                                )
                              ],
                            ),
                          ),
                        );
                      }),
                    ),
                    UIHelper.verticalSpaceSmall,
                    GestureDetector(
                      child: Container(
                        height: 50,
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(left: 0, right: 0, top: 10),
                        color: Palette.white,
                        child: Text(
                          '+ ' + AppLocalizations.of(context).addQuestion,
                          style: AppTextStyle.getDynamicFontStyle(
                              Palette.blue, 16, FontType.Medium),
                        ),
                      ),
                      onTap: () async {
                        Question question = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AddQuestionView(
                                  questionCategories: model
                                      .getQuestionCategoriesStringArray())),
                        );
                        if (question != null) {
                          result.add(question);
                        }
                        setState(() {});
                      },
                    ),
                    UIHelper.verticalSpaceSmall,
                    InkWell(
                      onTap: () {
                        setState(() {
                          model.isShowSetting = !model.isShowSetting;
                        });
                      },
                      child: Container(
                        alignment: Alignment.topLeft,
                        child: Text(
                          model.isShowSetting
                              ? AppLocalizations.of(context).showAdvanceSetting
                              : AppLocalizations.of(context).hideAdvanceSetting,
                          style: AppTextStyle.getDynamicFontStyle(
                              Palette.blue, 16, FontType.Medium),
                        ),
                      ),
                    ),
                    model.isShowSetting == false
                        ? Column(
                            children: <Widget>[
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Text(
                                        AppLocalizations.of(context)
                                            .allowRetake,
                                        style: AppTextStyle.getDynamicFontStyle(
                                            Colors.black,
                                            16,
                                            FontType.RobotoMedium),
                                      ),
                                      Flexible(
                                        child: Container(),
                                      ),
                                      Switch(
                                        value: model.isAllowTrack,
                                        onChanged: (value) {
                                          setState(() {
                                            model.isAllowTrack = value;
                                          });
                                        },
                                        activeTrackColor: Palette.dividerColor,
                                        activeColor: Palette.blue,
                                      )
                                    ],
                                  ),
                                  Container(
                                    transform: Matrix4.translationValues(
                                        0.0, -10, 0.0),
                                    child: Text(
                                      AppLocalizations.of(context)
                                          .allowRetakeDesc,
                                      style: AppTextStyle.getDynamicFontStyle(
                                          Palette.listColor,
                                          13,
                                          FontType.RobotoRegular),
                                    ),
                                  ),
                                ],
                              ),
                              UIHelper.verticalSpaceSmall,
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Text(
                                        AppLocalizations.of(context)
                                            .changeToHorizonView,
                                        style: AppTextStyle.getDynamicFontStyle(
                                            Colors.black,
                                            16,
                                            FontType.RobotoMedium),
                                      ),
                                      Flexible(
                                        child: Container(),
                                      ),
                                      Switch(
                                        value: model.isChangeView,
                                        onChanged: (value) {
                                          setState(() {
                                            model.isChangeView = value;
                                          });
                                        },
                                        activeTrackColor: Palette.dividerColor,
                                        activeColor: Palette.blue,
                                      )
                                    ],
                                  ),
                                  Container(
                                    transform: Matrix4.translationValues(
                                        0.0, -10, 0.0),
                                    child: Text(
                                      AppLocalizations.of(context)
                                          .changeToHorizonDesc,
                                      style: AppTextStyle.getDynamicFontStyle(
                                          Palette.listColor,
                                          13,
                                          FontType.RobotoRegular),
                                    ),
                                  ),
                                ],
                              ),
                              UIHelper.verticalSpaceSmall,
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Text(
                                        AppLocalizations.of(context)
                                            .randomizeQuestions,
                                        style: AppTextStyle.getDynamicFontStyle(
                                            Colors.black,
                                            16,
                                            FontType.RobotoMedium),
                                      ),
                                      Flexible(
                                        child: Container(),
                                      ),
                                      Switch(
                                        value: model.isRandom,
                                        onChanged: (value) {
                                          setState(() {
                                            model.isRandom = value;
                                          });
                                        },
                                        activeTrackColor: Palette.dividerColor,
                                        activeColor: Palette.blue,
                                      )
                                    ],
                                  ),
                                  Container(
                                    transform: Matrix4.translationValues(
                                        0.0, -10, 0.0),
                                    child: Text(
                                      AppLocalizations.of(context)
                                          .randomizeQuestionsDesc,
                                      style: AppTextStyle.getDynamicFontStyle(
                                          Palette.listColor,
                                          13,
                                          FontType.RobotoRegular),
                                    ),
                                  ),
                                ],
                              ),
                              UIHelper.verticalSpaceExtraSmall,
                            ],
                          )
                        : Container(),
                    Container(
                      padding: const EdgeInsets.only(bottom: 30),
                      child: RegularButton(
                        buttonLabel: AppLocalizations.of(context).submit,
                        onTap: () {
                          print('Button pressed');
                          model.validateQuizDetail(
                              titleController.text,
                              descController.text,
                              markController.text,
                              durationController.text,
                              context,
                              result);
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
