import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/model/AttendanceModel.dart';
import 'package:trainmoo/model/SessionModel.dart';
import 'package:trainmoo/model/StudentModel.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';

class AttendanceViewModel extends BaseModel {
  bool isFirstTime = false;
  List<AttendanceModel> attendance = [];
  List<StudentModel> all = [];
  List<StudentModel> present = [];
  List<StudentModel> absent = [];
  List<StudentModel> leave = [];
  List<SessionModel> eventList = [];
  List<SessionModel> eventDataList = [];

  String displayDate = 'Today';
  String todaysDate;
  String paramDate;
  DateTime currentDisplayingDate;

  String selectedEventId;

  String classId;

  init(String classId) async {
    if (!isFirstTime) {
      isFirstTime = true;
      this.classId = classId;
      currentDisplayingDate = new DateTime.now();
      getCurrentDate();
      getEvent(paramDate);
    }
  }

  Future<void> getEvent(String dateTime) async {
    eventDataList = [];
    // String todayDate = Utility.getSessionFormatDate(dateTime);
    print("dateTime : $dateTime");
    eventDataList = await API.getSessionData(dateTime, dateTime);
    //eventDataList = await API.getEventList(dateTime, selectedClass.id);
    print("online attendance");

    print("eventList {{{{{{{{{{}}}}}}}}}}===> $eventDataList");
    eventList = [];
    for (var event in eventDataList) {
      if (event.classId == classId) {
        eventList.add(event);
      }
    }
    print("eventList {{{{{{{{{{}}}}}}}}}}===> $eventList");
    attendance.clear();
    all.clear();
    present.clear();
    absent.clear();
    leave.clear();
    if (eventList.length > 0) {
      selectedEventId = eventList[0].event;
      getStudentList(eventList[0].event);
    }

    notifyListeners();
  }

  void getStudentList(eventId) {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
        attendance.clear();
        all.clear();
        present.clear();
        absent.clear();
        leave.clear();
        attendance
            .addAll(await API.getUserAttendance(classId, paramDate, eventId));
        for (AttendanceModel attendanceModel in attendance) {
          all.add(attendanceModel.schoolUser);
          if (attendanceModel.present != null && attendanceModel.present)
            present.add(attendanceModel.schoolUser);
          else
            absent.add(attendanceModel.schoolUser);
        }
        setState(ViewState.Idle);
      } else {
        setState(ViewState.Idle);
        showToast(AppLocalizations.of(context).internetMessage, context);
      }
      setState(ViewState.Idle);
    });
  }

  Future<Null> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: currentDisplayingDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != currentDisplayingDate) {
      currentDisplayingDate = picked;
      getEvent(Utility.getSessionFormatDate(currentDisplayingDate));
      dateIncreseDecrease(0);
    }
    notifyListeners();
  }

  List<StudentModel> getArrayOfStudents(int tabIndex) {
    switch (tabIndex) {
      case 0:
        return all;

      case 1:
        return present;

      case 2:
        return absent;

      case 3:
        return [];
    }
  }

  void getCurrentDate() {
    todaysDate = Utility.attendaceDateFormate(currentDisplayingDate);
    displayDate = 'Today';
    paramDate = Utility.attendaceParamDateFormate(currentDisplayingDate);
  }

  void dateIncreseDecrease(int no) {
    currentDisplayingDate = currentDisplayingDate.subtract(Duration(days: no));
    displayDate = Utility.attendaceDateFormate(currentDisplayingDate);
    if (displayDate == todaysDate) {
      displayDate = 'Today';
    }
    paramDate = Utility.attendaceParamDateFormate(currentDisplayingDate);
    getEvent(Utility.getSessionFormatDate(currentDisplayingDate));
  }
}
