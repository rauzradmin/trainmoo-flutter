import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:trainmoo/model/Database/AttendanceTable.dart';
import 'package:trainmoo/model/Database/DatabaseHelper.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/SessionModel.dart';
import 'package:trainmoo/model/StudentModel.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/router.dart';

import 'package:trainmoo/ui/util/util.dart';

class TakeAttendanceViewModel extends BaseModel {
  bool isFirstTime = false;
  List<StudentModel> studentsList = [];
  DateTime selectedDate = DateTime.now();
  List<SessionModel> eventList = [];
  List<SessionModel> eventDataList = [];
  String selectedEventId;
  String displayDate;
  List<ProgramClassesModel> classList = [];
  ProgramClassesModel selectedClass;
  bool takeAttendanceOffline = false;
//  String eventId = '5d53bdf037e37b005aa14941';
  //new for (trainmoo.in)
  String eventId = '5ece47fd30bfa80070b85729';
  List<Map<String, dynamic>> attendanceData = [];

  Map<String, String> header;

  init() async {
    if (!isFirstTime) {
      header = await API.getAuthHeader();
      isFirstTime = true;
      takeAttendanceOffline = !await Utility.isInternetAvailable();

      classList = await DatabaseHelper().getClasses();

      notifyListeners();
    }
  }

  void setTakeAttendanceOffline(value) {
    takeAttendanceOffline = value;

    notifyListeners();
  }

  Future<void> getEvent(DateTime dateTime) async {
    eventDataList = [];
    if (takeAttendanceOffline) {
      eventDataList = SessionModel.fromJsonArray(
          await DatabaseHelper().getEvent(dateTime, selectedClass.id));
      print("offline attendance");
    } else {
      String todayDate = Utility.getSessionFormatDate(dateTime);
      eventDataList = await API.getSessionData(todayDate, todayDate);
      //eventDataList = await API.getEventList(dateTime, selectedClass.id);
      print("online attendance");
    }
    print("eventList {{{{{{{{{{}}}}}}}}}}===> $eventDataList");
    eventList = [];
    for (var event in eventDataList) {
      if (event.classId == selectedClass.id) {
        eventList.add(event);
      }
    }

    studentsList = [];
    notifyListeners();
  }

  void getStudentList(DateTime dateTime) async {
    setState(ViewState.Busy);
    if (takeAttendanceOffline) {
      studentsList = await DatabaseHelper().getStudents(selectedClass.id);
    } else {
      studentsList = await API.getStudents(selectedClass.id);
    }
    setState(ViewState.Idle);
    // eventDataList = await API.getEventList(dateTime, selectedClass.id);

    notifyListeners();
  }

  void submitAttendance(List<StudentModel> studentsList, DateTime dateTime,
      String selectedEventId) {
    displayDate = Utility.getyearMonthDate(dateTime.millisecondsSinceEpoch);
    if (selectedEventId == null) {
      showToast('Please Select Event.', context);
    } else if (takeAttendanceOffline)
      takeAttendance();
    else
      Utility.isInternetAvailable().then((isConnected) async {
        if (isConnected) {
          setState(ViewState.Busy);
          for (StudentModel studentModel in studentsList) {
            Map<String, dynamic> studentAttendance = Map();
            studentAttendance['id'] = studentModel.id;
            studentAttendance['present'] = studentModel.isPresent;
            attendanceData.add(studentAttendance);
          }
          // print(
          //     "studentAttendance :=============---------->>>>  $attendanceData");
          // print(
          //     "selectedClass.id :=============---------->>>>  ${selectedClass.id}");
          print(
              "selectedEventId :=============---------->>>>  $selectedEventId");
          // print("displayDate :=============---------->>>>  $displayDate");
          await API.takeAttendance(attendanceData, selectedClass.id,
              selectedEventId, displayDate, false);
          selectedClass = null;
          studentsList = [];
          Navigator.of(context).pop();
          showToast('Attendance submitted', context);
        } else {
          showToast(AppLocalizations.of(context).internetMessage, context);
        }
        setState(ViewState.Idle);
      });
  }

  void takeAttendance() {
    attendanceData.clear();
    for (StudentModel studentModel in studentsList) {
      Map<String, dynamic> studentAttendance = Map();
      studentAttendance['id'] = studentModel.id;
      studentAttendance['present'] = studentModel.isPresent;
      attendanceData.add(studentAttendance);
    }
    displayDate =
        Utility.getyearMonthDate(new DateTime.now().millisecondsSinceEpoch);
    AttendanceTable attendanceTable = new AttendanceTable(
        id: new DateTime.now().millisecondsSinceEpoch,
        data: jsonEncode(attendanceData),
        isSubmitted: 0,
        classID: selectedClass.id,
        eventId: selectedEventId,
        date: displayDate);
    showToast('Attendance submitted offline', context);
    selectedClass = null;
    studentsList = [];
    DatabaseHelper().saveAttendanceData(attendanceTable);
    notifyListeners();
//    Utility.isInternetAvailable().then((isConnected) async {
//      if (isConnected) {
//        setState(ViewState.Busy);
//        await API.takeAttendance(
//            attendanceData, selectedClass.id, eventId, displayDate, true);
//      } else {
//        showToast(AppLocalizations.of(context).internetMessage, context);
//      }
//      setState(ViewState.Idle);
//    });
//    displayDate = '2020-02-14';
  }
}
