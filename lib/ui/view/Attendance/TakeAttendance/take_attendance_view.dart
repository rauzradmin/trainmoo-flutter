import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/StudentModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:trainmoo/ui/view/Attendance/TakeAttendance/take_attendance_view_model.dart';
import 'package:trainmoo/ui/widget/drop_down_student_list.dart';
import 'package:trainmoo/ui/widget/event_drop_down.dart';
import 'package:trainmoo/ui/widget/regular_button.dart';
import 'package:trainmoo/util/image_path.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:intl/intl.dart';

class TakeAttendanceView extends StatefulWidget {
  @override
  _TakeAttendanceViewState createState() => _TakeAttendanceViewState();
}

class _TakeAttendanceViewState extends State<TakeAttendanceView>
    with BaseCommonWidget, SingleTickerProviderStateMixin {
  DateTime dateTime = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return BaseView<TakeAttendanceViewModel>(builder: (context, model, child) {
      model.init();
      return SafeArea(child: _getBody(model));
    });
  }

  Widget _getBody(TakeAttendanceViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(TakeAttendanceViewModel model) {
    print("Attendance id : ${model.selectedEventId}");
    return Column(
      children: <Widget>[
        commonAppBarWithCancelIcon(
            AppLocalizations.of(context).takeAttendance, context),
        Expanded(
          child: SingleChildScrollView(
            child: Container(
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 24, right: 24, bottom: 5, top: 10),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 10, bottom: 10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              'Take attendance offline',
                              overflow: TextOverflow.clip,
                              maxLines: 2,
                              style: AppTextStyle.getDynamicFontStyle(
                                  Palette.hintTextColor, 20, FontType.Regular),
                            ),
                          ),
                          Switch(
                            value: model.takeAttendanceOffline,
                            activeColor: Palette.blue,
                            activeTrackColor: Palette.hintTextColor,
                            onChanged: (bool value) {
                              print("value of the switch : $value");
                              model.setTakeAttendanceOffline(value);
                              //model.takeAttendanceOffline = value;
                            },
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: 50,
                      margin: EdgeInsets.only(top: 10, bottom: 10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: new BorderRadius.all(Radius.circular(3)),
                      ),
                      child: Row(
                        children: <Widget>[
                          IconButton(
                            icon: Icon(
                              Icons.arrow_back_ios,
                              color: Palette.listColor,
                            ),
                            onPressed: () {
                              newDate(-1);
                            },
                          ),
                          Expanded(
                            child: InkWell(
                              onTap: () {
                                datePicker(context);
                              },
                              child: Text(
                                showDate(),
                                overflow: TextOverflow.clip,
                                maxLines: 1,
                                textAlign: TextAlign.center,
                                style: AppTextStyle.getDynamicFontStyle(
                                        Palette.hintTextColor,
                                        20,
                                        FontType.Regular)
                                    .copyWith(
                                  color: Colors.black,
                                ),
                              ),
                            ),
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.arrow_forward_ios,
                              color: Palette.listColor,
                            ),
                            onPressed: () {
                              newDate(1);
                            },
                          ),
                        ],
                      ),
                    ),
                    DropDownList(
                      classList: model.classList,
                      onTap: (ProgramClassesModel selectedChooseRole) {
                        setState(() {
                          model.selectedClass = selectedChooseRole;
                          model.selectedEventId = null;
                          model.getEvent(dateTime);
                        });
                      },
                      selectedClass: model.selectedClass,
                    ),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    EventDropDownList(
                      eventList: model.eventList,
                      onTap: (selectedEvent) {
                        setState(() {
                          model.selectedEventId = selectedEvent;
                          model.getStudentList(dateTime);
                        });
                      },
                      selectedEventId: model.selectedEventId,
                    ),
                    model.eventList.length > 0
                        ? takeAttendanceList(model)
                        : Container(),
                    model.studentsList.length > 0 && model.eventList.length > 0
                        ? Container(
                            padding: const EdgeInsets.only(bottom: 30),
                            child: RegularButton(
                              buttonLabel:
                                  AppLocalizations.of(context).submitAttendance,
                              onTap: () {
                                model.submitAttendance(model.studentsList,
                                    dateTime, model.selectedEventId);
                              },
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  takeAttendanceList(TakeAttendanceViewModel model) {
    return Container(
      padding: EdgeInsets.only(top: 4),
      child: ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: model.studentsList.length,
        itemBuilder: (BuildContext context, int ind) {
          return attendanceItem(model.studentsList[ind], model);
        },
      ),
    );
  }

  attendanceItem(StudentModel student, TakeAttendanceViewModel model) {
    return Container(
      height: 85,
      child: GestureDetector(
        child: Card(
          elevation: 1,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(UIHelper.cardRadius)),
          child: Container(
            margin: EdgeInsets.only(left: 18, right: 5),
            child: Row(
              children: <Widget>[
                CircleAvatar(
                    child: student.avatarThumbnail != null
                        ? loadCircleNetworkImage(
                            student.avatarThumbnail, 40, model.header)
                        : Image.asset(
                            ImagePath.USER,
                            height: 40,
                            width: 40,
                          )),
                UIHelper.horizontalSpaceSmall,
                Expanded(
                  child: Text(
                    student.name,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: AppTextStyle.getDynamicFontStyle(
                        Colors.black, 16, FontType.SemiBol),
                  ),
                ),
                IconButton(
                    iconSize: 33,
                    icon: Image.asset(
                      student.isPresent
                          ? ImagePath.SELECTED_TRUE
                          : ImagePath.UNSELECTED_TRUE,
                    ),
                    onPressed: () {
                      setState(() {
                        student.isPresent = true;
                      });
                    }),
                IconButton(
                    iconSize: 33,
                    icon: Image.asset(
                      student.isPresent
                          ? ImagePath.UNSELECTED_FALSE
                          : ImagePath.SELECTED_FALSE,
                    ),
                    onPressed: () {
                      setState(() {
                        student.isPresent = false;
                      });
                    })
              ],
            ),
          ),
        ),
        onTap: () {
//          Navigator.pushNamed(context, Screen.CreateAssignment.toString());
        },
      ),
    );
  }

  String showDate() {
    return Utility.attendaceDateFormate(dateTime);
  }

  newDate(int day) {
    var dTime = dateTime.add(new Duration(days: day));
    setState(() {
      dateTime = dTime;
    });
    // if (new DateTime.now().isAfter(dTime)) {
    //   setState(() {
    //     dateTime = dTime;
    //   });
    // }
  }

  void datePicker(BuildContext context) async {
    var dTime = await showDatePicker(
      context: context,
      initialDate: dateTime,
      firstDate: DateTime(2019),
      lastDate: DateTime(
          int.parse(DateFormat('yyyy').format(new DateTime.now())) + 10),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light(),
          child: child,
        );
      },
    );
    print("this date : $dTime");
    if (dTime != null) {
      setState(() {
        dateTime = dTime;
      });
    }
  }
}
