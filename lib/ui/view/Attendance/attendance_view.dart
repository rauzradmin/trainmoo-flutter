import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/StudentModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/view/Attendance/attendance_view_model.dart';
import 'package:trainmoo/ui/view/StudentList/StudentView.dart';
import 'package:trainmoo/ui/widget/event_drop_down.dart';
import 'package:trainmoo/util/image_path.dart';

class AttendanceView extends StatefulWidget {
  final String classId;

  const AttendanceView({Key key, this.classId}) : super(key: key);
  @override
  _AttendanceViewState createState() => _AttendanceViewState();
}

class _AttendanceViewState extends State<AttendanceView> with BaseCommonWidget {
  int tabIndex = 0;
  ProgramClassesModel classObject;
  CalendarController _calendarController;

  @override
  void initState() {
    super.initState();
    _calendarController = CalendarController();
  }

  @override
  void dispose() {
    _calendarController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<AttendanceViewModel>(builder: (context, model, child) {
      model.init(widget.classId);
      return RefreshIndicator(
          onRefresh: () async {
            model.getStudentList(model.selectedEventId);
            return;
          },
          child: SafeArea(child: _getBody(model)));
    });
  }

  Widget _getBody(AttendanceViewModel model) {
    return Stack(
      children: <Widget>[_getBaseContainer(model), getProgressBar(model.state)],
    );
  }

  Widget _getBaseContainer(AttendanceViewModel model) {
    return Column(
      children: <Widget>[
        commonAppBar(AppLocalizations.of(context).attendance, context),
        _showDatePicker(model),
        Card(
          margin: EdgeInsets.only(top: 15, left: 25, right: 25),
          elevation: 1,
          child: EventDropDownList(
            eventList: model.eventList,
            onTap: (selectedEvent) {
              setState(() {
                model.selectedEventId = selectedEvent;
                model.getStudentList(model.selectedEventId);
              });
            },
            selectedEventId: model.selectedEventId,
          ),
        ),
        Card(
          margin: EdgeInsets.only(
            left: 24,
            right: 24,
            top: 10,
          ),
          elevation: 1,
          child: Container(
            margin: EdgeInsets.only(top: 10, left: 5, right: 5),
            child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  getTitleWidget('All', model.all.length, 0),
                  Container(
                      height: 40,
                      child: VerticalDivider(
                        thickness: 2,
                        color: Palette.dividerColor,
                      )),
                  getTitleWidget('Present', model.present.length, 1),
                  Container(
                      height: 40,
                      child: VerticalDivider(
                        thickness: 2,
                        color: Palette.dividerColor,
                      )),
                  getTitleWidget('Absent', model.absent.length, 2),
//                  Container(
//                      height: 40,
//                      child: VerticalDivider(
//                        thickness: 2,
//                        color: Palette.dividerColor,
//                      )),
//                  getTitleWidget('Leave', 0, 3),
                ]),
          ),
        ),
        studentList(model.getArrayOfStudents(tabIndex))
      ],
    );
  }

  studentList(List<StudentModel> students) {
    return Expanded(
      child: students.length > 0
          ? Container(
              margin: EdgeInsets.only(left: 20, right: 20),
              padding: EdgeInsets.only(top: 20),
              child: ListView.builder(
                physics: ClampingScrollPhysics(),
                shrinkWrap: true,
                itemCount: students.length,
                itemBuilder: (BuildContext context, int ind) {
                  return StudentView(studentModel: students[ind]);
                },
              ),
            )
          : showMessageInCenter('No students found'),
    );
  }

  Widget _showDatePicker(AttendanceViewModel model) {
    return Container(
        margin: EdgeInsets.only(top: 20, left: 20, right: 20),
        height: 55,
        child: Card(
          elevation: 0.3,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Palette.dividerColor,
                  size: 15,
                ),
                onPressed: () {
                  model.dateIncreseDecrease(1);
                },
              ),
              InkWell(
                child: Row(
                  children: <Widget>[
                    Image.asset(
                      ImagePath.CALENDAR_IMG,
                      height: 20,
                      width: 18,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      model.displayDate,
                      style: AppTextStyle.getDynamicFontStyleWithWeight(
                          false,
                          Palette.lightBlackColor,
                          18,
                          FontType.Medium,
                          FontWeight.normal),
                    ),
                  ],
                ),
                onTap: () {
                  model.selectDate(context);
                },
              ),
              IconButton(
                icon: Icon(
                  Icons.arrow_forward_ios,
                  color: Palette.dividerColor,
                  size: 15,
                ),
                onPressed: () {
                  model.dateIncreseDecrease(-1);
                },
              ),
            ],
          ),
        ));
  }

  getTitleWidget(String title, int count, int position) {
    return InkWell(
      onTap: () {
        tabIndex = position;
        setState(() {});
      },
      child: Container(
        width: 70,
        child: Column(
          children: <Widget>[
            Text(count.toString(),
                style: AppTextStyle.getDynamicFontStyle(
                    tabIndex == position
                        ? Palette.accentColor
                        : Palette.primaryTextColor,
                    21,
                    FontType.RobotoMedium)),
            Text(title,
                style: AppTextStyle.getDynamicFontStyle(
                    Palette.listColor, 13, FontType.Medium)),
            SizedBox(
              height: 10,
            ),
            Container(
              width: double.infinity,
              height: 7,
              color: tabIndex == position ? Palette.accentColor : Colors.white,
            )
          ],
        ),
      ),
    );
  }
}
