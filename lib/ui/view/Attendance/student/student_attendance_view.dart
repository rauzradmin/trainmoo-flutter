import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/studentModuel/AttendanceClassList.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/view/Attendance/student/class_attendance_view.dart';
import 'package:trainmoo/ui/view/Attendance/student/student_attendance_view_model.dart';
import 'package:trainmoo/ui/widget/drop_down_student_list.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart'
    show CalendarCarousel;
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:intl/intl.dart' show DateFormat;

class StudentAttendanceView extends StatefulWidget {
  const StudentAttendanceView({Key key}) : super(key: key);
  @override
  _StudentAttendanceViewState createState() => _StudentAttendanceViewState();
}

class _StudentAttendanceViewState extends State<StudentAttendanceView>
    with BaseCommonWidget {
  int tabIndex = 0;
  ProgramClassesModel classObject;
  CalendarController _calendarController;

  DateTime _currentDate = DateTime(2019, 2, 3);
  DateTime _currentDate2 = new DateTime.now();
  String _currentMonth = DateFormat.yMMM().format(DateTime(2019, 2, 3));
  DateTime _targetDateTime = new DateTime.now();
  CalendarCarousel _calendarCarousel, _calendarCarouselNoHeader;
  String selectedDay;

  @override
  void initState() {
    super.initState();
    _calendarController = CalendarController();
  }

  @override
  void dispose() {
    _calendarController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<StudentAttendanceViewModel>(
        builder: (context, model, child) {
      model.init();
      return RefreshIndicator(
          onRefresh: () async {
            model.getClassAttendance();
            return;
          },
          child: SafeArea(child: _getBody(model)));
    });
  }

  Widget _getBody(StudentAttendanceViewModel model) {
    return Stack(
      children: <Widget>[_getBaseContainer(model), getProgressBar(model.state)],
    );
  }

  Widget _getBaseContainer(StudentAttendanceViewModel model) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          commonAppBar(AppLocalizations.of(context).attendance, context),
          //_showDatePicker(model),
          Card(
            margin: EdgeInsets.only(
              left: 24,
              right: 24,
              top: 10,
            ),
            elevation: 1,
            child: DropDownList(
              classList: model.classList,
              onTap: (ProgramClassesModel selectedChooseRole) {
                setState(() {
                  model.selectedClass = selectedChooseRole;
                  model.getClassAttendance();
                });
              },
              selectedClass: model.selectedClass,
            ),
          ),

          Card(
            margin: EdgeInsets.only(
              left: 24,
              right: 24,
              top: 10,
            ),
            elevation: 1,
            child: Container(
                margin: EdgeInsets.only(top: 10, left: 5, right: 5),
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 10, bottom: 30),
                      child: Text(model.percentage.toString() + "%",
                          style: AppTextStyle.getDynamicFontStyle(
                              Palette.primaryTextColor,
                              25,
                              FontType.RobotoMedium)),
                    ),
                    Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          getTitleWidget('Sessions', model.sessions, 0),
                          Container(
                              height: 40,
                              child: VerticalDivider(
                                thickness: 2,
                                color: Palette.dividerColor,
                              )),
                          getTitleWidget('Present', model.present, 1),
                          Container(
                              height: 40,
                              child: VerticalDivider(
                                thickness: 2,
                                color: Palette.dividerColor,
                              )),
                          getTitleWidget('Absent', model.absent, 2),
                        ]),
                  ],
                )),
          ),
          Card(
              margin: EdgeInsets.only(left: 24, right: 24, bottom: 10, top: 10),
              elevation: 1,
              child: calenderView()),
          attendanceClassList(model.getStudentAttendanceFromIndex(tabIndex))
        ],
      ),
    );
  }

  attendanceClassList(List<AttendanceClassModel> classAttendance) {
    return Container(
      child: classAttendance.length > 0
          ? Container(
              margin: EdgeInsets.only(left: 20, right: 20),
              child: ListView.builder(
                physics: ClampingScrollPhysics(),
                shrinkWrap: true,
                itemCount: classAttendance.length,
                itemBuilder: (BuildContext context, int ind) {
                  print(
                      "event date : ${DateFormat('dd MMM yyyy').format(_currentDate2)} === ${classAttendance[ind].eventDate}");
                  return classAttendance[ind].eventDate ==
                          DateFormat('dd MMM yyyy').format(_currentDate2)
                      ? ClassAttendanceView(
                          classattendanceModel: classAttendance[ind],
                        )
                      : Container();
                },
              ),
            )
          : showMessageInCenter('No Data found'),
    );
  }

  getTitleWidget(String title, int count, int position) {
    return InkWell(
      onTap: () {
        tabIndex = position;
        // _controllerTab.update(true);
        setState(() {});
      },
      child: Container(
        width: 70,
        child: Column(
          children: <Widget>[
            Text(count.toString(),
                style: AppTextStyle.getDynamicFontStyle(
                    tabIndex == position
                        ? Palette.accentColor
                        : Palette.primaryTextColor,
                    18,
                    FontType.RobotoMedium)),
            Text(title,
                style: AppTextStyle.getDynamicFontStyle(
                    Palette.listColor, 13, FontType.Medium)),
            SizedBox(
              height: 10,
            ),
            Container(
              width: double.infinity,
              height: 7,
              color: tabIndex == position ? Palette.accentColor : Colors.white,
            )
          ],
        ),
      ),
    );
  }

  calenderView() {
    /// Example Calendar Carousel without header and custom prev & next button
    _calendarCarouselNoHeader = CalendarCarousel<Event>(
      todayBorderColor: Palette.accentColor,
      selectedDayBorderColor: Palette.accentColor,
      selectedDayButtonColor: Palette.accentColor,
      onDayPressed: (DateTime date, List<Event> events) {
        print("event date : $date");
        this.setState(() => _currentDate2 = date);
        events.forEach((event) => print(event.title));
      },
      daysHaveCircularBorder: true,
      showOnlyCurrentMonthDate: false,
      weekdayTextStyle: TextStyle(color: Colors.black),
      weekendTextStyle: TextStyle(
        color: Colors.black,
      ),
      thisMonthDayBorderColor: Colors.transparent,

      height: 330.0,
      selectedDateTime: _currentDate2,
      targetDateTime: _targetDateTime,
      customGridViewPhysics: NeverScrollableScrollPhysics(),
      // markedDateCustomShapeBorder:
      //     CircleBorder(side: BorderSide(color: Colors.yellow)),
      // markedDateCustomTextStyle: TextStyle(
      //   fontSize: 18,
      //   color: Colors.blue,
      // ),
      showHeader: false,
      todayTextStyle: TextStyle(
        color: _currentDate2.compareTo(_targetDateTime) == 0
            ? Colors.white
            : Palette.accentColor,
      ),
      // markedDateShowIcon: true,
      // markedDateIconMaxShown: 2,
      // markedDateIconBuilder: (event) {
      //   return event.icon;
      // },
      // markedDateMoreShowTotal:
      //     true,

      todayButtonColor: _currentDate2.compareTo(_targetDateTime) == 0
          ? Palette.accentColor
          : Colors.transparent,
      selectedDayTextStyle: TextStyle(
        color: Colors.white,
      ),
      minSelectedDate: DateTime(2019),
      maxSelectedDate: DateTime(
          int.parse(DateFormat('yyyy').format(new DateTime.now())) + 10),
      // prevDaysTextStyle: TextStyle(
      //   fontSize: 16,
      //   color: Colors.pinkAccent,
      // ),
      // inactiveDaysTextStyle: TextStyle(
      //   color: Colors.tealAccent,
      //   fontSize: 16,
      // ),
      onCalendarChanged: (DateTime date) {
        this.setState(() {
          _targetDateTime = date;
          _currentMonth = DateFormat.yMMM().format(_targetDateTime);
        });
        print("date : $date");
      },
      onDayLongPressed: (DateTime date) {
        print('long pressed date $date');
      },
    );

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            top: 10.0,
            bottom: 16.0,
            left: 16.0,
            right: 16.0,
          ),
          decoration: BoxDecoration(
              border: Border(bottom: BorderSide(width: 2, color: Colors.grey))),
          child: new Row(
            children: <Widget>[
              FlatButton(
                child: Text('PREV'),
                onPressed: () {
                  setState(() {
                    _targetDateTime = DateTime(
                        _targetDateTime.year, _targetDateTime.month - 1);
                    _currentMonth = DateFormat.yMMM().format(_targetDateTime);
                  });
                },
              ),
              Expanded(
                  child: Text(
                _currentMonth,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                ),
              )),
              FlatButton(
                child: Text('NEXT'),
                onPressed: () {
                  setState(() {
                    _targetDateTime = DateTime(
                        _targetDateTime.year, _targetDateTime.month + 1);
                    _currentMonth = DateFormat.yMMM().format(_targetDateTime);
                  });
                },
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 16.0),
          child: _calendarCarouselNoHeader,
        ), //
      ],
    );
  }
}
