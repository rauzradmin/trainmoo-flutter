import 'package:flutter/material.dart';
import 'package:trainmoo/model/studentModuel/AttendanceClassList.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';
import 'package:intl/intl.dart';
import 'package:trainmoo/util/image_path.dart';

class ClassAttendanceView extends StatelessWidget with BaseCommonWidget {
  final AttendanceClassModel classattendanceModel;

  const ClassAttendanceView({Key key, this.classattendanceModel})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return studentItem(classattendanceModel);
  }

  String removeAllHtmlTags(String htmlText) {
    RegExp exp = RegExp(r"<[^>]*>", multiLine: true, caseSensitive: true);
    return htmlText.replaceAll(exp, '');
  }

  studentItem(AttendanceClassModel classObj) {
    return Card(
      elevation: 1,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(UIHelper.cardRadius)),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 5),
        child: Padding(
          padding: const EdgeInsets.only(left: 16, top: 15, right: 16),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        classObj.className,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: AppTextStyle.getDynamicFontStyle(
                            Colors.black, 16, FontType.SemiBol),
                      ),
                      Padding(padding: EdgeInsets.only(top: 5)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            //mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Container(
                                padding: EdgeInsets.all(3),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        width: 1, color: Palette.dividerColor),
                                    borderRadius: BorderRadius.circular(5)),
                                child: Text(classObj.eventTime,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: AppTextStyle.getDynamicFontStyle(
                                        Palette.listColor,
                                        10,
                                        FontType.RobotoBold)),
                              ),
                              Padding(padding: EdgeInsets.only(right: 5)),
                              Container(
                                padding: EdgeInsets.all(3),
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        width: 1, color: Palette.dividerColor),
                                    borderRadius: BorderRadius.circular(5)),
                                child: Text(classObj.eventDate,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: AppTextStyle.getDynamicFontStyle(
                                        Palette.listColor,
                                        10,
                                        FontType.RobotoBold)),
                              ),
                              Padding(padding: EdgeInsets.only(right: 5)),
                            ],
                          ),
                          classObj.present
                              ? Image.asset(
                                  ImagePath.SELECTED_TRUE,
                                  width: 30,
                                  height: 30,
                                )
                              : Container()

                          //  Image.asset(
                          //     ImagePath.UNSELECTED_FALSE,
                          //     color: Colors.grey,
                          //     width: 30,
                          //     height: 30,
                          //   )
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        child: Text(
                            classObj.eventDetails != null
                                ? removeAllHtmlTags(classObj.eventDetails)
                                : " ",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: AppTextStyle.getDynamicFontStyle(
                                Palette.listColor, 13, FontType.RobotoBold)),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        decoration: BoxDecoration(
                          border: Border(
                              top: BorderSide(
                                  width: 1, color: Palette.dividerColor)),
                        ),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                classObj.present == true
                                    ? Text("Captured On : ",
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: AppTextStyle.getDynamicFontStyle(
                                            Palette.listColor,
                                            10,
                                            FontType.RobotoMedium))
                                    : Container(),
                                Text(
                                    classObj.present == true
                                        ? DateFormat("dd/mm/yyyy k:m:s a")
                                            .format(DateTime.parse(
                                                classObj.sessionStartTime))
                                        : "_ _ _ _ _ _ _ _ _ _ _ _",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: AppTextStyle.getDynamicFontStyle(
                                        classObj.present == true
                                            ? Colors.black
                                            : Colors.grey,
                                        10,
                                        FontType.RobotoRegular)),
                              ],
                            ),
                            Padding(padding: EdgeInsets.only(top: 5)),
                            // Row(
                            //   children: [
                            //     Text("Location : ",
                            //         maxLines: 1,
                            //         overflow: TextOverflow.ellipsis,
                            //         style:
                            //             AppTextStyle.getDynamicFontStyle(
                            //                 Palette.listColor,
                            //                 10,
                            //                 FontType.RobotoMedium)),
                            //     Text("(Location not captured)",
                            //         maxLines: 1,
                            //         overflow: TextOverflow.ellipsis,
                            //         style:
                            //             AppTextStyle.getDynamicFontStyle(
                            //                 Colors.red,
                            //                 10,
                            //                 FontType.RobotoRegular)),
                            //   ],
                            // )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
