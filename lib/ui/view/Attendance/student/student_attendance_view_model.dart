import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/model/AttendanceModel.dart';
import 'package:trainmoo/model/Database/DatabaseHelper.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/StudentModel.dart';
import 'package:trainmoo/model/studentModuel/AttendanceClassList.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';

class StudentAttendanceViewModel extends BaseModel {
  bool isFirstTime = false;

  List<ProgramClassesModel> classList = [];
  List<AttendanceClassModel> attendanceClassList = [];
  List<AttendanceClassModel> attendanceClassPresentList = [];
  List<AttendanceClassModel> attendanceClassAbsentList = [];
  ProgramClassesModel selectedClass;
  int present = 0;
  int absent = 0;
  int sessions = 0;
  int percentage = 0;
  List percentageClassList = [];

  init() async {
    if (!isFirstTime) {
      isFirstTime = true;
      classList = await DatabaseHelper().getClasses();
      //print("classList :---> $classList");
      if (classList.length > 0) {
        setState(ViewState.Busy);
        // selectedClass = classList[0];
        if (selectedClass != null) {
          getClassAttendance();
        } else {
          setState(ViewState.Idle);
        }
      }
      notifyListeners();
    }
  }

  void getClassAttendance() {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
        var attendanceData = await API.getClassAttendance(selectedClass.id);
        if (attendanceData != null) {
          present = attendanceData['present'];
          absent = attendanceData['absent'];
          sessions = attendanceData['total'];
          percentage =
              (attendanceData['present'] * 100 / attendanceData['total'])
                  .round();
        }
        var attendanceClassData =
            await API.getStudentAttendance(selectedClass.id);
        attendanceClassList =
            AttendanceClassModel.fromDatabaseJsonArray(attendanceClassData);
        attendanceClassPresentList =
            attendanceClassList.where((element) => element.present).toList();
        attendanceClassAbsentList = attendanceClassList
            .where((element) => element.present == false)
            .toList();
      } else {
        showToast(AppLocalizations.of(context).internetMessage, context);
      }
      setState(ViewState.Idle);
    });
    notifyListeners();
  }

  List getStudentAttendanceFromIndex(int tabIndex) {
    switch (tabIndex) {
      case 0:
        return attendanceClassList;
      case 1:
        return attendanceClassPresentList;
      case 2:
        return attendanceClassAbsentList;
    }
  }
}
