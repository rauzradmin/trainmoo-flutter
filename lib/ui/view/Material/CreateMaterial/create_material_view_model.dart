import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cloudinary_client/cloudinary_client.dart';
import 'package:cloudinary_client/models/CloudinaryResponse.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as path;
import 'package:trainmoo/model/Database/DatabaseHelper.dart';
import 'package:trainmoo/model/MaterialModel.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/UploadFileModel.dart';
import 'package:trainmoo/model/User.dart';
import 'package:trainmoo/shared_preference/user_preference.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import '../../../../model/FileModel.dart';

class CreateMaterialViewModel extends BaseModel {
  BuildContext context;
  List images;
  int maxImageNo = 10;
  bool selectSingleImage = false;
  String error = 'No Error Dectected';
  DateTime selectedStartDate;
  DateTime selectedDueDate;
  List<Map<String, dynamic>> filePath = [];
  List<UploadFileModel> files = [];
  Map<String, String> header;
  MaterialModel materialObject;

  bool isFirstTime = false;
  List<ProgramClassesModel> classList = [];
  ProgramClassesModel selectedClass;

  String title, desc, userType, type;

  init() async {
    if (!isFirstTime) {
      isFirstTime = true;
      classList = await DatabaseHelper().getClasses();
      print("classList : $classList");

      notifyListeners();
    }
  }

  void validateMaterialDetail(MaterialModel materialObject, String userType,
      String title, String desc, String type, BuildContext context) {
    this.title = title;
    this.desc = desc;
    this.userType = userType;
    this.type = type;
    this.materialObject = materialObject;
    if (title.isEmpty && desc.isEmpty == null) {
      showToast("Please fill the All details", context);
    } else if (title.isEmpty && type != "Notification") {
      showToast("Please enter title", context);
    } else if (desc.isEmpty) {
      showToast("Please enter description", context);
    } else if (desc.length < 10) {
      showToast("Description must be greater than 10 characters", context);
    } else {
      uploadFiles();
//      Navigator.pop(context);

    }
  }

  void createMaterial(String classID, String title, String desc, String type) {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
//        await uploadFiles();
        filePath.clear();
        for (UploadFileModel uploadFileModel in files) {
          filePath.add(uploadFileModel.fileModel.toMap());
        }
        var data =
            await API.createMaterial(classID, title, desc, type, filePath);
        setState(ViewState.Idle);
        if (data == null) {
          showToast(type + " created", context);
          Timer.periodic(new Duration(milliseconds: 6000), (timer) {
            Navigator.of(context).pop();
            timer.cancel();
          });
        } else
          print("this is response data: ===> $data");
        showToast(data, context);
      }
    });
  }

  Future<void> uploadFiles1() async {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        if (getState() != ViewState.Busy) setState(ViewState.Busy);
        for (UploadFileModel uploadFileModel in files) {
          if (!uploadFileModel.isDownloaded) {
            FileModel uploadUrl =
                await uploadFile(uploadFileModel.localFileUrl);
            uploadFileModel.isDownloaded = true;
            uploadFileModel.fileModel = uploadUrl;
            Future.delayed(const Duration(milliseconds: 500), () {});
//            validateMaterialDetail(title, desc, context);
//            continue;
          }
        }
//        createMaterial(selectedClass.id, title, desc);
      }
    });
  }

  void submitAssignment(String title, String desc) {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);

        filePath.clear();
        for (UploadFileModel uploadFileModel in files) {
          filePath.add(uploadFileModel.fileModel.toMap());
        }
        print("materialObject =========>: ${this.materialObject.id}");

        String data = await API.submitAssignment(title, desc, selectedStartDate,
            selectedDueDate, filePath, this.materialObject);
        setState(ViewState.Idle);
        print("this is the response : $data");
        if (data == null) {
          showToast("Assignment Submitted", context);
          Timer.periodic(new Duration(milliseconds: 6000), (timer) {
            Navigator.of(context).pop();
            timer.cancel();
          });
        } else {
          print("this is response data: ===> $data");
          showToast(data, context);
        }
      }
    });
  }

  uploadFiles() {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        if (getState() != ViewState.Busy) setState(ViewState.Busy);
        if (await hasFileForUpload()) {
          int position = await getPositionOfFileToUpload();
          downloadFileFromList(position);
        } else {
          print("userType : $userType  type: $type");
          userType == API.StudentUser
              ? type == "Assignment"
                  ? submitAssignment(title, desc)
                  : createMaterial(selectedClass.id, title, desc, type)
              : createMaterial(selectedClass.id, title, desc, type);
        }
      }
    });
  }

  Future<bool> hasFileForUpload() async {
    bool hasFileForUpload = false;
    for (UploadFileModel uploadFileModel in files) {
      if (!uploadFileModel.isDownloaded) {
        hasFileForUpload = true;
      }
    }
    return hasFileForUpload;
  }

  Future<int> getPositionOfFileToUpload() async {
    int position = -1;
    for (int i = 0; i < files.length; i++) {
      if (!files[i].isDownloaded) {
        position = i;
        break;
      }
    }
    return position;
  }

  Future<FileModel> uploadFile(File image) async {
    String fileName = path.basename(image.path);
    CloudinaryClient client = new CloudinaryClient(
        '922793423812823', 'dTTYyz0DCI7AFR2IyhpLjteU9gw', 'rauzr');
    CloudinaryResponse response = await client.uploadImage(
        Platform.isIOS ? "file://" + image.path : image.path,
        filename: fileName);
//    CloudinaryResponse response1 =
//        await client.uploadImages(image.path, filename: fileName);
    print('Image upload response ==> ${response.toJson()}');
    String thumb = '';
    if (response.url.toLowerCase().contains('.jpg') ||
        response.url.toLowerCase().contains('.png') ||
        response.url.toLowerCase().contains('.jpeg')) {
      thumb = response.url;
    }

    FileModel fileModel = new FileModel(
        fileName,
        fileName,
        response.url,
        thumb,
        'upload',
        response.bytes,
        response.height,
        response.width,
        response.public_id,
        response.created_at,
        false);
    //response.url
    return fileModel;
  }

  void downloadFileFromList(int position) async {
    print('Start upload file');
    FileModel uploadUrl = await uploadFile(files[position].localFileUrl);
    files[position].isDownloaded = true;
    files[position].fileModel = uploadUrl;
    Future.delayed(const Duration(milliseconds: 1000), () {
      print('check for upload new file');

      uploadFiles();
    });
  }
}
