import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:trainmoo/model/MaterialModel.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/UploadFileModel.dart';
import 'package:trainmoo/model/User.dart';
import 'package:trainmoo/shared_preference/user_preference.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/widget/FileView.dart';
import 'package:trainmoo/ui/widget/drop_down_student_list.dart';
import 'package:trainmoo/ui/widget/large_text_form_field_with_padding.dart';
import 'package:trainmoo/ui/widget/regular_button.dart';
import 'package:trainmoo/ui/widget/showFile.dart';
import 'package:trainmoo/ui/widget/text_form_field_with_padding.dart';

import 'create_material_view_model.dart';

class CreateMaterialView extends StatefulWidget {
  final MaterialModel materialObject;
  final String materialType;
  CreateMaterialView(this.materialObject, this.materialType);
  @override
  _CreateMaterialViewState createState() => _CreateMaterialViewState();
}

class _CreateMaterialViewState extends State<CreateMaterialView>
    with BaseCommonWidget {
  String userType;
  String currentClass;
  String currentClassId;
  String currentAssignmentTitle = "";
  TextEditingController titleController = new TextEditingController();
  TextEditingController classTittleController = new TextEditingController();
  TextEditingController descController = new TextEditingController();
  ProgramClassesModel selectedClass;
  bool pandding = false;
  @override
  Future<void> initState() {
    getUserType();
    getPanddingAssignment();
  }

  getPanddingAssignment() async {
    User user = await UserPreference.getUserDetail();
    print("userid : ${user.id}");
    var data = widget.materialObject != null
        ? widget.materialObject.replies.where((e) => e.author.sId == user.id)
        : [];
    print("data : ${data.length}");
    if (data.length == 0) {
      setState(() {
        pandding = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    titleController.text = userType == API.StudentUser
        ? widget.materialObject != null ? widget.materialObject.text : null
        : null;
    classTittleController.text = userType == API.StudentUser
        ? widget.materialObject != null
            ? pandding
                ? widget.materialObject.title
                : widget.materialObject.title
            : currentClass
        : null;
    ;

    descController.text = userType == API.StudentUser
        ? widget.materialObject != null
            ? pandding ? null : widget.materialObject.text
            : null
        : null;

    return BaseView<CreateMaterialViewModel>(builder: (context, model, child) {
      model.init();
      print("matrialobject : ${widget.materialObject}");
      return SafeArea(child: _getBody(model));
    });
  }

  Widget _getBody(CreateMaterialViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(CreateMaterialViewModel model) {
    String type = widget.materialType ==
            AppLocalizations.of(context).askQuestion
        ? "Question"
        : widget.materialType == AppLocalizations.of(context).startDiscussion
            ? "Discussion"
            : widget.materialType ==
                    AppLocalizations.of(context).createNotification
                ? "Notification"
                : widget.materialObject != null &&
                        widget.materialObject.type == "Assignment"
                    ? widget.materialObject.type
                    : "Material";
    return Column(
      children: <Widget>[
        commonAppBarWithCancelIcon(
            userType == API.StudentUser
                ? widget.materialObject != null
                    ? pandding
                        ? AppLocalizations.of(context).submitAssignment
                        : AppLocalizations.of(context).reviewAssignment
                    : widget.materialType != null
                        ? widget.materialType
                        : AppLocalizations.of(context).submitAssignment
                : widget.materialType != null
                    ? widget.materialType
                    : AppLocalizations.of(context).createMaterial,
            context),
        Expanded(
          child: SingleChildScrollView(
            child: Container(
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 24, right: 24, bottom: 5, top: 10),
                child: Column(
                  children: <Widget>[
                    userType == API.StudentUser && widget.materialObject != null
                        ? RegularTextFormFieldWithPadding(
                            enabled: pandding,
                            textEditingController: classTittleController,
                            hintText:
                                AppLocalizations.of(context).materialTitle,
                            textAlign: TextAlign.left,
                            inputType: TextInputType.text,
                          )
                        : DropDownList(
                            classList: model.classList,
                            onTap: (ProgramClassesModel selectedChooseRole) {
                              setState(() {
                                model.selectedClass = selectedChooseRole;
                              });
                            },
                            selectedClass: model.selectedClass,
                          ),
                    // DropDownList(
                    //   classList: model.classList,
                    //   onTap: (ProgramClassesModel selectedChooseRole) {
                    //     setState(() {
                    //       model.selectedClass = selectedChooseRole;
                    //     });
                    //   },
                    //   selectedClass: model.selectedClass,
                    // ),
                    widget.materialType ==
                                AppLocalizations.of(context)
                                    .createNotification ||
                            widget.materialObject != null &&
                                widget.materialObject.type == "Assignment"
                        ? Container()
                        : RegularTextFormFieldWithPadding(
                            enabled:
                                widget.materialObject != null ? pandding : true,
                            textEditingController: titleController,
                            hintText: widget.materialType ==
                                    AppLocalizations.of(context).askQuestion
                                ? AppLocalizations.of(context).questionTitle
                                : widget.materialType ==
                                        AppLocalizations.of(context)
                                            .startDiscussion
                                    ? AppLocalizations.of(context)
                                        .dicussionTitle
                                    : AppLocalizations.of(context)
                                        .materialTitle,
                            textAlign: TextAlign.left,
                            inputType: TextInputType.text,
                          ),
                    LargeTextFormFieldWithPadding(
                      enabled: widget.materialObject != null ? pandding : true,
                      textEditingController: descController,
                      hintText: widget.materialType ==
                              AppLocalizations.of(context).createNotification
                          ? "Enter Message"
                          : AppLocalizations.of(context).description,
                      textAlign: TextAlign.left,
                      inputType: TextInputType.text,
                    ),
                    Padding(padding: EdgeInsets.only(top: 20)),
                    widget.materialObject != null
                        ? pandding
                            ? ShowFileView(
                                function: (List<UploadFileModel> files) {
                                  model.files = files;
                                  print(
                                      'length of files ==>${model.files.length}');
                                },
                              )
                            : widget.materialObject.files.length <= 0
                                ? Container()
                                : new FileView(
                                    files: widget.materialObject.files,
                                    headers: model.header,
                                    showName: true,
                                  )
                        : ShowFileView(
                            function: (List<UploadFileModel> files) {
                              model.files = files;
                              print('length of files ==>${model.files.length}');
                            },
                          ),

                    widget.materialObject != null
                        ? pandding ||
                                widget.materialObject != null &&
                                    widget.materialObject.assessment.retake
                            ? Container(
                                margin:
                                    const EdgeInsets.only(bottom: 10, top: 20),
                                child: RegularButton(
                                  buttonLabel:
                                      AppLocalizations.of(context).submit,
                                  // buttonLabel: "jitendra",
                                  onTap: () {
                                    model.validateMaterialDetail(
                                        widget.materialObject,
                                        userType,
                                        titleController.text,
                                        descController.text,
                                        type,
                                        context);
                                  },
                                ),
                              )
                            : Container()
                        : Container(
                            margin: const EdgeInsets.only(bottom: 10, top: 20),
                            child: RegularButton(
                              buttonLabel: AppLocalizations.of(context).submit,
                              // buttonLabel: "jitendra",
                              onTap: () {
                                model.validateMaterialDetail(
                                    null,
                                    userType,
                                    titleController.text,
                                    descController.text,
                                    type,
                                    context);
                              },
                            ),
                          )
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  void getUserType() async {
    userType = await UserPreference.getUserType();
    currentClass = await UserPreference.getCurrentClass();
    currentClassId = await UserPreference.getCurrentClassID();
    currentAssignmentTitle = await UserPreference.getCurrentAssignmentTitle();
    // print("is value is get or not ::====>> $currentAssignmentTitle");
    // print("value of userType is ::====>>> $userType");
    setState(() {
      userType = userType;
    });
  }
}
