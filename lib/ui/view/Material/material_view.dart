import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:trainmoo/app_state.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/shared_preference/user_preference.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/router.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:trainmoo/ui/view/Material/material_view_model.dart';
import 'package:trainmoo/ui/widget/ItemDetailView.dart';

class MaterialView extends StatefulWidget {
  final ProgramClassesModel classObject;
  final String screenType;

  const MaterialView({Key key, this.classObject, this.screenType})
      : super(key: key);
  @override
  _MaterialViewState createState() => _MaterialViewState();
}

class _MaterialViewState extends State<MaterialView> with BaseCommonWidget {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          print(
              "the state is 1 : ${state.loadFirstTimeAssignment} ${state.loadFirstTimeMaterial} ${state.loadFirstTimeQuiz}");
          return BaseView<MaterialViewModel>(builder: (context, model, child) {
            model.init(widget.classObject, widget.screenType, state, context);
            return SafeArea(child: _getBody(model));
          });
        });
  }

  Widget _getBody(MaterialViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(MaterialViewModel model) {
    return Container(
      child: Column(
        children: <Widget>[
          commonAppBar(getTitle(widget.screenType, context), context),
          Expanded(
              child: model.materialLists.length > 0
                  ? getMaterialList(model)
                  : showMessageInCenter('No data found'))
        ],
      ),
    );
  }

  getMaterialList(MaterialViewModel model) {
    return Container(
      padding: const EdgeInsets.only(left: 24, right: 24, bottom: 5, top: 10),
      child: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        header: WaterDropMaterialHeader(
          backgroundColor: Palette.accentColor,
        ),
        controller: _refreshController,
        onRefresh: () async {
          await model.getMaterialList(context);
          _refreshController.refreshCompleted();
        },
        // onLoading: () async {
        //   _refreshController.loadComplete();
        // },
        child: ListView.builder(
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          itemCount: model.materialLists.length,
          itemBuilder: (BuildContext cont, int ind) {
            return model.materialLists[ind].text != null
                ? materialItem(model, ind)
                : Container();
          },
        ),
      ),
    );
  }

  materialItem(MaterialViewModel model, int index) {
    return GestureDetector(
      child: ItemDetailView(
        title: Utility.removeAllHtmlTags(model.materialLists[index].text),
        time: model.materialLists[index].displayTime,
        details: model.materialLists[index].text,
        isHtmlData: true,
        files: widget.screenType == API.quizScreenType
            ? null
            : model.materialLists[index].files,
        header: model.header,
      ),
      onTap: () {
        Navigator.pushNamed(context, Screen.MaterialDetail.toString(),
            arguments: {
              "material": model.materialLists[index],
              'screen_type': widget.screenType
            });
        // print("jitndra=========>>>>>>jiit");
        // print(Utility.removeAllHtmlTags(model.materialLists[index].text));
        UserPreference.saveCurrentAssignmentTitle(
            model.materialLists[index].text, model.materialLists[index].id);
      },
    );
  }

  String getTitle(String screenType, BuildContext context) {
    switch (screenType) {
      case API.materialScreenType:
        return AppLocalizations.of(context).material;
      case API.assignmentScreenType:
        return AppLocalizations.of(context).assignments;
      case API.quizScreenType:
        return AppLocalizations.of(context).quiz;
    }
    return '';
  }
}
