import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/model/FileModel.dart';
import 'package:trainmoo/model/MaterialModel.dart';
import 'package:trainmoo/model/UploadFileModel.dart';
import 'package:trainmoo/shared_preference/user_preference.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:trainmoo/ui/view/Material/MaterialDetail/material_detail_view_model.dart';
//import 'package:trainmoo/ui/widget/CommentFormWithFileSelection.dart';
import 'package:trainmoo/ui/widget/DetailView.dart';
import 'package:trainmoo/ui/widget/FileView.dart';
import 'package:trainmoo/ui/widget/text_form_field_with_padding.dart';
import 'package:percent_indicator/percent_indicator.dart';
import '../../../router.dart';

class MaterialDetailView extends StatefulWidget {
  final MaterialModel materialObject;
  final String screenType;
  final Color color;
  final bool autoFocusToComment;

  const MaterialDetailView(
      {Key key,
      this.materialObject,
      this.screenType,
      this.color,
      this.autoFocusToComment})
      : super(key: key);

  @override
  _MaterialDetailViewState createState() => _MaterialDetailViewState();
}

class _MaterialDetailViewState extends State<MaterialDetailView>
    with BaseCommonWidget, SingleTickerProviderStateMixin {
  String userType;
  bool commentAutoFocus = false;
  FocusNode commentFocus = new FocusNode();
  TextEditingController _commentController = new TextEditingController();
  double c_width;
  TabController tabController;
  int tabIndex = 0;

  @override
  Widget build(BuildContext context) {
    c_width = MediaQuery.of(context).size.width * 0.9;
    return BaseView<MaterialDetailViewModel>(builder: (context, model, child) {
      model.init(widget.materialObject, widget.screenType);
      return SafeArea(child: _getBody(model));
    });
  }

  void toggleTab() {
    setState(() {
      tabIndex = tabController.index;
    });
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    print('material detail view ==> ${widget.materialObject.type}');
    if (widget.autoFocusToComment != null && widget.autoFocusToComment)
      commentFocus.requestFocus();

    _getUserType();
    print("userType: $userType");
    tabController = TabController(vsync: this, length: 2);
    tabController.addListener(toggleTab);
  }

  Widget _getBody(MaterialDetailViewModel model) {
    return Stack(
      children: <Widget>[_getBaseContainer(model), getProgressBar(model.state)],
    );
  }

  Widget _getBaseContainer(MaterialDetailViewModel model) {
    print(" value is : ${widget.materialObject.submissionDetails}");
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          commonAppBar('', context),
          Expanded(
              child: Container(
            padding:
                const EdgeInsets.only(left: 24, right: 24, bottom: 5, top: 10),
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                DetailView(
                  title: widget.materialObject.title,
                  time: widget.materialObject.displayTime,
                  details: widget.materialObject.text,
                  files: widget.screenType == API.quizScreenType
                      ? null
                      : widget.materialObject.files,
                  header: model.header,
                  type: widget.materialObject.type,
                  screenType: widget.screenType,
                  color: widget.color,
                  linkPreview: widget.materialObject.linkPreview,
                  className: widget.materialObject.classBean != null
                      ? widget.materialObject.classBean.name
                      : "",
                ),
                (widget.screenType == API.assignmentScreenType ||
                        widget.materialObject.type == API.assignmentScreenType)
                    ? Container(
                        padding: EdgeInsets.only(bottom: 20, top: 20),
                        child: GestureDetector(
                          child: reviewButton(
                              userType != null && userType == API.StudentUser
                                  ? widget.materialObject.assessment.retake ==
                                          true
                                      ? AppLocalizations.of(context).retake
                                      : model.pandding
                                          ? AppLocalizations.of(context)
                                              .submitAssignment
                                          : AppLocalizations.of(context).review
                                  : AppLocalizations.of(context)
                                      .reviewAssignment,
                              model.pandding
                                  ? ''
                                  : model.getPostCommentList.length > 0
                                      ? 'Submited on ${Utility.getDisplayFormatDate(model.getPostCommentList[model.getPostCommentList.length - 1].createdAt)}'
                                      : ""
                              // '${widget.materialObject.submissions} Submitted | ${widget.materialObject.pending} Pending'
                              ),
                          onTap: () {
                            if (userType == API.StudentUser) {
                              Navigator.pushNamed(
                                  context, Screen.CreateMaterial.toString(),
                                  arguments: {
                                    "materialObject": widget.materialObject
                                  });
                            } else {
                              Navigator.pushNamed(
                                  context, Screen.ReviewAssignment.toString(),
                                  arguments: {
                                    "assignment_id": widget.materialObject.id
                                  });
                            }
                          },
                        ),
                      )
                    : Container(),
                (widget.screenType == API.quizScreenType ||
                        widget.materialObject.type == API.quizScreenType)
                    ? Container(
                        margin: EdgeInsets.only(top: 16, bottom: 16),
                        child: GestureDetector(
                          child: model.pandding
                              ? widget.materialObject == null
                                  ? reviewButton(
                                      userType != null &&
                                              userType == API.StudentUser
                                          ? AppLocalizations.of(context)
                                              .takeQuiz
                                          : AppLocalizations.of(context)
                                              .reviewQuiz,
                                      '')
                                  : widget.materialObject.assessment.retake ==
                                          true
                                      ? reviewButton(
                                          userType != null &&
                                                  userType == API.StudentUser
                                              ? AppLocalizations.of(context)
                                                  .retakeQuiz
                                              : AppLocalizations.of(context)
                                                  .reviewQuiz,
                                          '')
                                      : reviewButton(
                                          userType != null &&
                                                  userType == API.StudentUser
                                              ? 'Completed'
                                              : AppLocalizations.of(context)
                                                  .reviewQuiz,
                                          '')
                              : Container(),
                          onTap: () {
                            if (userType == API.StudentUser) {
                              widget.materialObject.assessment.retake
                                  ? Navigator.pushNamed(context,
                                      Screen.StudentQuizView.toString(),
                                      arguments: {
                                          "quizTitle":
                                              widget.materialObject.text,
                                          'quiz_id': widget.materialObject.id
                                        })
                                  : null;
                            } else {
                              // Navigator.of(context).pushNamed(
                              //     Screen.MyWork.toString(),
                              //     arguments: {"postType": "Review Quiz"});
                              // Navigator.pushNamed(
                              //     context, Screen.ReviewQuiz.toString(),
                              //     arguments: {
                              //       'quiz_id': widget.materialObject.id
                              //     });
                              Navigator.pushNamed(
                                  context, Screen.ReviewAssignment.toString(),
                                  arguments: {
                                    "assignment_id": widget.materialObject.id,
                                    "postType": "Review Quiz"
                                  });
                            }
                          },
                        ),
                      )
                    : Container(),
                widget.materialObject.type == "Poll" &&
                        userType == API.TeacherUser
                    ? pollList(model)
                    : Container(),
                Padding(padding: EdgeInsets.only(top: 10)),
                Text(AppLocalizations.of(context).comment,
                    style: AppTextStyle.commentTextStyle()),
                Padding(
                    padding: const EdgeInsets.only(top: 10.0),
//                   child: CommentFormWithFileSelection(
//                     textInputAction: TextInputAction.done,
// //                    onTextSubmitted: (value) {
// //                      model.postComment(_commentController.text);
// //                      _commentController.text = "";
// //                    },
//                     submitedCommentsAndFiles:
//                         (String comment, List<UploadFileModel> files) {
//                       model.files = files;
//                       model.comment = comment;
//                       print('material View files========>${files.length}');
//                       model.postComment1();
//                     },
//                     focusNode: commentFocus,
//                     textEditingController: _commentController,
//                     hintText: AppLocalizations.of(context).writeSomethingHere,
//                     inputType: TextInputType.text,
//                   ),
                    child: Card(
                      child: Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                          child: TextField(
                            keyboardType: TextInputType.text,
                            focusNode: commentFocus,
                            controller: _commentController,
                            textInputAction: TextInputAction.done,
                            onSubmitted: (value) {
                              if (_commentController.text != "") {
                                model.postComment(value);
                                _commentController.text = "";
                              }
                            },
                            decoration: InputDecoration(
                              hintText: AppLocalizations.of(context)
                                  .writeSomethingHere,
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding: EdgeInsets.only(
                                  left: 15, bottom: 11, top: 11, right: 15),
                            ),
                          )),
                    )),
                model.getPostCommentList != null &&
                        model.getPostCommentList.length > 0
                    ? Expanded(
                        flex: 1,
                        child: ListView.builder(
                          physics: ClampingScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: model.getPostCommentList.length,
                          itemBuilder: (BuildContext context, int ind) {
                            return commentItemWidget(
                                model.getPostCommentList[ind].author.avatar !=
                                        null
                                    ? model.getPostCommentList[ind].author
                                        .avatar.src
                                    : '',
                                model.getPostCommentList[ind].author.name
                                        .firstname +
                                    " " +
                                    model.getPostCommentList[ind].author.name
                                        .lastname,
                                model.getPostCommentList[ind].text != null
                                    ? Utility.removeAllHtmlTags(
                                        model.getPostCommentList[ind].text)
                                    : '',
                                Utility.getDisplayFormatDate(
                                    model.getPostCommentList[ind].createdAt),
                                model.getPostCommentList[ind].files,
                                model.header,
                                model.getPostCommentList[ind].sId,
                                model);
                          },
                        ),
                      )
                    : Container(),
              ],
            ),
          ))
        ],
      ),
    );
  }

  pollList(MaterialDetailViewModel model) {
    return Column(
      children: [
        Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          elevation: 2,
          child: Padding(
            padding: EdgeInsets.all(15),
            child: Expanded(
              child: ListView.builder(
                  physics: ClampingScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: widget.materialObject.poll.options.length,
                  itemBuilder: (BuildContext context, int ind) {
                    return Container(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              padding: EdgeInsets.only(right: 15),
                              child: Text(
                                widget.materialObject.poll.options[ind]
                                    ["seqno"],
                                style: AppTextStyle.getDynamicFontStyle(
                                  Colors.grey,
                                  25,
                                  FontType.RobotoBold,
                                ),
                              )),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  padding: EdgeInsets.only(top: 5),
                                  child: Text(
                                    widget.materialObject.poll.options[ind]
                                        ["text"],
                                    style: AppTextStyle.getDynamicFontStyle(
                                      Colors.black,
                                      13,
                                      FontType.RobotoBold,
                                    ),
                                  )),
                              Padding(
                                padding: EdgeInsets.only(top: 5),
                                child: new LinearPercentIndicator(
                                  width:
                                      MediaQuery.of(context).size.width * 0.65,
                                  animation: true,
                                  lineHeight: 15.0,
                                  animationDuration: 1000,
                                  percent: (widget.materialObject.poll
                                          .options[ind]["noOfSelections"] /
                                      (widget.materialObject.submissions +
                                          widget.materialObject.pending)),
                                  center: Text(
                                    widget.materialObject.poll.options[ind]
                                                ["noOfSelections"] ==
                                            0
                                        ? ""
                                        : widget.materialObject.poll
                                            .options[ind]["noOfSelections"]
                                            .toString(),
                                    style: new TextStyle(fontSize: 12.0),
                                  ),
                                  linearStrokeCap: LinearStrokeCap.roundAll,
                                  progressColor: Colors.blue,
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    );
                  }),
            ),
          ),
        ),
        Padding(padding: EdgeInsets.only(top: 15)),
        GestureDetector(
          onTap: () {
            model.getMaterialList(widget.materialObject.id);
          },
          child:
              reviewButton(AppLocalizations.of(context).sendReminderToAll, ''),
        )
      ],
    );
  }

  commentItemWidget(
      String imagePath,
      String name,
      String comment,
      String date,
      List<FileModel> files,
      Map<String, String> header,
      String replyId,
      MaterialDetailViewModel model) {
    if (imagePath.contains('https')) {
      imagePath = imagePath;
    } else {
      imagePath = API.imageURL + imagePath;
    }
    return Padding(
      padding: const EdgeInsets.only(top: 20, bottom: 16),
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            loadCommentImage(imagePath, 50.0, 50.0, header),
            Padding(
              padding: const EdgeInsets.only(left: 15),
              child: Container(
                width: userType == API.TeacherUser
                    //&& widget.materialObject.type == "Poll"
                    ? c_width * 0.56
                    : c_width * 0.80,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      name,
                      overflow: TextOverflow.ellipsis,
                      style: AppTextStyle.getDynamicFontStyle(
                          Colors.black, 16, FontType.Medium),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      comment,
                      //overflow: TextOverflow.ellipsis,
                      style: AppTextStyle.getDynamicFontStyle(
                        Colors.black,
                        13,
                        FontType.RobotoRegular,
                      ),
                    ),
                    new FileView(
                      files: files,
                      headers: header,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: Text(
                        date,
                        style: AppTextStyle.getDynamicFontStyle(
                            Palette.listColor, 14, FontType.RobotoMedium),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            userType == API.TeacherUser
                //&& widget.materialObject.type == "Poll"
                ? MaterialButton(
                    onPressed: () {
                      model.removeComment(replyId);
                    },
                    child: Icon(Icons.delete, color: Color(0xFFCB4335))
                    // Text(
                    //   "Remove",
                    //   style: TextStyle(color: Palette.redColor),
                    // ),
                    )
                : Container()
          ],
        ),
      ),
    );
  }

  void _getUserType() async {
    userType = await UserPreference.getUserType();
    setState(() {
      userType = userType;
    });
  }
}
