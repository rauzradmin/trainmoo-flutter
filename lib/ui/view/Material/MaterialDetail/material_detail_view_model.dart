import 'dart:io';

import 'package:flutter/material.dart';
import 'package:trainmoo/model/FileModel.dart';
import 'package:trainmoo/model/GetCommentModel.dart';
import 'package:trainmoo/model/MaterialModel.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/UploadFileModel.dart';
import 'package:trainmoo/model/User.dart';
import 'package:trainmoo/model/assignment_list_model.dart';
import 'package:trainmoo/shared_preference/user_preference.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:path/path.dart' as path;
import 'package:cloudinary_client/cloudinary_client.dart';
import 'package:cloudinary_client/models/CloudinaryResponse.dart';

class MaterialDetailViewModel extends BaseModel {
  bool isFirstTime = false;
  List<GetAssignmentListModel> assignmentLists = [];
  MaterialModel materialObject;
  List<Replies> getPostCommentList = [];

  GetCommonModel getAllCommentsList;
  Map<String, String> header;
  int userType;
  String screenType;
  List<Map<String, dynamic>> filePath = [];
  List<UploadFileModel> files = [];
//  ProgramClassesModel selectedClass;
//  String title, desc;
  String comment = 'test';
  bool pandding = false;

  init(MaterialModel materialObject, String screenType) async {
    if (!isFirstTime) {
//      userType=await UserPreference.getUserType();
      print('userType--->$userType');
      header = await API.getAuthHeader();
      this.materialObject = materialObject;
      this.screenType = screenType;
      getPostCommentListApi(this.materialObject.id);

      isFirstTime = true;
    }
  }

  void sendReminder(len, data, i, postId) {
    if (len == i) {
      setState(ViewState.Idle);
    }
    if (len > i) {
      print("=========>> data : ${data[i].id} $i");
      API.notifyPendingUser(data[i], postId).then((value) {
        if (value != null) {
          print("response : $value");
          sendReminder(len, data, i = i + 1, postId);
        }
      });
    }
  }

  void getMaterialList(postId) async {
    setState(ViewState.Busy);
    print("classid : $postId");
    var pollList = await API.getQuizSubmittedUsers(postId);
    if (pollList != null) {
      sendReminder(
          pollList.pendingUsers.length, pollList.pendingUsers, 0, postId);
    }

    // setState(ViewState.Idle);
    // notifyListeners();
  }

  void getAssignmentList(String schoolId, String classId) {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
//        await API.getMaterialList('', '');
        setState(ViewState.Idle);
      }
    });
  }

  void getPostCommentListApi(String id) {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
        getAllCommentsList = await API.getPostComments(id);
        User user = await UserPreference.getUserDetail();
        var data =
            getAllCommentsList.replies.where((e) => e.author.sId == user.id);
        if (data.length == 0) {
          this.pandding = true;
        }
        if (getAllCommentsList != null && getAllCommentsList.replies != null) {
          getPostCommentList = getAllCommentsList.replies;
        }
        print('getCommentList---->${getPostCommentList}');
      } else {
        showToast(AppLocalizations.of(context).internetMessage, context);
      }
      setState(ViewState.Idle);
    });
  }

  void postComment1() {
    if (comment.isEmpty && files.isEmpty) {
      showToast('Please write comment Or attach file', context);
    } else if (comment.length < 10) {
      showToast('Comment must be more than 10 characters', context);
    } else {
      uploadFiles();
    }
  }

  void postComment(String comment) {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
        // filePath.clear();
        // for (UploadFileModel uploadFileModel in files) {
        //   filePath.add(uploadFileModel.fileModel.toMap());
        // }

        String commentMsg = await API.postComment(
          getAllCommentsList.sId,
          getAllCommentsList.type,
          getAllCommentsList.classModel.sId,
          comment,
          filePath,
        );
        if (commentMsg != null) {
          getPostCommentListApi(this.materialObject.id);
        }
      } else {
        showToast(AppLocalizations.of(context).internetMessage, context);
        setState(ViewState.Idle);
      }
    });
  }

  uploadFiles() {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        if (getState() != ViewState.Busy) setState(ViewState.Busy);
        if (await hasFileForUpload()) {
          int position = await getPositionOfFileToUpload();
          downloadFileFromList(position);
        } else {
          postComment("");
        }
      }
    });
  }

  removeComment(replyId) async {
    bool deleted = await API.deletePostComments(replyId);
    if (deleted) {
      showToast("Comment is Successfully Deleted!", context);
      getPostCommentListApi(this.materialObject.id);
    } else {
      showToast("Something want to wrong!", context);
    }
  }

  void downloadFileFromList(int position) async {
    print('Start upload file');
    FileModel uploadUrl = await uploadFile(files[position].localFileUrl);
    files[position].isDownloaded = true;
    files[position].fileModel = uploadUrl;
    Future.delayed(const Duration(milliseconds: 1000), () {
      print('check for upload new file');

      uploadFiles();
    });
  }

  Future<bool> hasFileForUpload() async {
    bool hasFileForUpload = false;
    for (UploadFileModel uploadFileModel in files) {
      if (!uploadFileModel.isDownloaded) {
        hasFileForUpload = true;
      }
    }
    return hasFileForUpload;
  }

  Future<int> getPositionOfFileToUpload() async {
    int position = -1;
    for (int i = 0; i < files.length; i++) {
      if (!files[i].isDownloaded) {
        position = i;
        break;
      }
    }
    return position;
  }

  Future<FileModel> uploadFile(File image) async {
    String fileName = path.basename(image.path);
    CloudinaryClient client = new CloudinaryClient(
        '922793423812823', 'dTTYyz0DCI7AFR2IyhpLjteU9gw', 'rauzr');
    CloudinaryResponse response =
        await client.uploadImage(image.path, filename: fileName);
//    CloudinaryResponse response1 =
//        await client.uploadImages(image.path, filename: fileName);
    print('Image upload response ==> ${response.toJson()}');
    String thumb = '';
    if (response.url.toLowerCase().contains('.jpg') ||
        response.url.toLowerCase().contains('.png') ||
        response.url.toLowerCase().contains('.jpeg')) {
      thumb = response.url;
    }

    FileModel fileModel = new FileModel(
        fileName,
        fileName,
        response.url,
        thumb,
        'upload',
        response.bytes,
        response.height,
        response.width,
        response.public_id,
        response.created_at,
        false);
    //response.url
    return fileModel;
  }
}
