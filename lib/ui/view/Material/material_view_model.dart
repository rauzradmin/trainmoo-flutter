import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:trainmoo/actions/home_actions.dart';
import 'package:trainmoo/app_state.dart';
import 'package:trainmoo/model/FileModel.dart';
import 'package:trainmoo/model/MaterialModel.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/UploadFileModel.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:path/path.dart' as path;
import 'package:cloudinary_client/cloudinary_client.dart';
import 'package:cloudinary_client/models/CloudinaryResponse.dart';

class MaterialViewModel extends BaseModel {
  bool isFirstTime = false;
  List<MaterialModel> materialLists = [];
  ProgramClassesModel classObject;
  String screenType;
  Map<String, String> header;
  bool panding = true;
//  ProgramClassesModel selectedClass;
//  String title, desc;
//  List<UploadFileModel> files = [];
//  List<Map<String, dynamic>> filePath = [];

  init(ProgramClassesModel classObject, String screenType, state,
      context) async {
    if (!isFirstTime) {
      header = await API.getAuthHeader();
      this.screenType = screenType;
      this.classObject = classObject;
      isFirstTime = true;
      if (materialLists != null && materialLists.length > 0) {
        materialLists.clear();
      }
      print("screen type: $screenType");
      if ((state.loadFirstTimeAssignment == false &&
              screenType == "Assignment") ||
          (state.loadFirstTimeMaterial == false && screenType == "Material") ||
          (state.loadFirstTimeQuiz == false && screenType == "Assessment")) {
        getMaterialList(context);
      } else {
        print(
            "screenType $screenType  assignmentList : ${state.quizList.length} ");
        if (screenType == "Assignment") {
          this.materialLists = state.assignmentList;
        }
        if (screenType == "Material") {
          this.materialLists = state.materialList;
        }
        if (screenType == "Assessment") {
          this.materialLists = state.quizList;
        }
        notifyListeners();
      }
    }
  }

  getMaterialList(context) {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
        print("class id : ${classObject.id}");
        materialLists = await API.getMaterials(
            classObject.id, screenType, 'Latest%20Posts', null);
        setState(ViewState.Idle);
        if (screenType == "Assignment") {
          StoreProvider.of<AppState>(context)
              .dispatch(SetAssignmentList(materialLists));
          StoreProvider.of<AppState>(context)
              .dispatch(SetLoadFirstTimeAssignment(true));
        }
        if (screenType == "Material") {
          StoreProvider.of<AppState>(context)
              .dispatch(SetMaterialList(materialLists));
          StoreProvider.of<AppState>(context)
              .dispatch(SetLoadFirstTimeMaterial(true));
        }
        if (screenType == "Assessment") {
          StoreProvider.of<AppState>(context)
              .dispatch(SetQuizList(materialLists));
          StoreProvider.of<AppState>(context)
              .dispatch(SetLoadFirstTimeQuiz(true));
        }
      } else {
        showToast(AppLocalizations.of(context).internetMessage, context);
      }
    });
  }
}
