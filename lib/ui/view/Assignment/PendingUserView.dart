import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/model/StudentModel.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:trainmoo/util/image_path.dart';

import 'ReviewAssignment/review_assignment_view_model.dart';

class PendingUserView extends StatelessWidget {
  final StudentModel studentModel;
  final String id;

  PendingUserView({Key key, this.studentModel, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BaseView<ReviewAssignmentViewModel>(
        builder: (context, model, child) {
      model.init(id);
      return Card(
        elevation: 1,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                    child: Text(
                      studentModel.name,
                      maxLines: 1,
                      style: AppTextStyle.getDynamicFontStyle(
                          Colors.black, 20, FontType.SemiBol),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      height: 40,
                    ),
                  ),
                  studentModel.mobile != null
                      ? IconButton(
                          icon: Image.asset(
                            ImagePath.ICON_CALL,
                            width: 17.9,
                            height: 17.94,
                          ),
                          onPressed: () {
                            Utility.launchURL("tel://${studentModel.mobile}");
                          })
                      : Container(),
                  studentModel.mobile != null
                      ? IconButton(
                          icon: Image.asset(
                            ImagePath.ICON_MESSAGE,
                            width: 17.9,
                            height: 17.94,
                          ),
                          onPressed: () {
                            Utility.launchURL("sms://${studentModel.mobile}");
                          })
                      : Container()
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 0),
                child: GestureDetector(
                  child: sendReminderButton(
                    AppLocalizations.of(context).sendReminder,
                  ),
                  onTap: () {
                    model.sendReminder(studentModel);
                  },
                ),
              )
            ],
          ),
        ),
      );
    });
  }

  Widget sendReminderButton(String title) {
    return Container(
      width: 126,
      height: 32,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Palette.transparentBlue,
      ),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(title,
              textAlign: TextAlign.center,
              style: AppTextStyle.getDynamicFontStyle(
                  Palette.white, 13, FontType.RobotoMedium)),
        ],
      ),
    );
  }
}
