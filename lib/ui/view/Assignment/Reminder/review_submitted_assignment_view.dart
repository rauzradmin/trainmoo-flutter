import 'package:flutter/material.dart';
import 'package:trainmoo/model/QuizUsersModel.dart';
import 'package:trainmoo/model/RepliesModel.dart';
import 'package:trainmoo/model/ReviewAssignment.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/view/Assignment/Reminder/review_submitted_assignment_view_model.dart';
import 'package:trainmoo/ui/widget/DetailView.dart';
import 'package:trainmoo/ui/widget/regular_button.dart';
import 'package:intl/intl.dart';
import '../../../router.dart';

class ReviewSubmittedAssignmentView extends StatefulWidget {
  final RepliesModel replies;
  final ReviewAssignemt reviewAssignemt;

  const ReviewSubmittedAssignmentView(
      {Key key, this.replies, this.reviewAssignemt})
      : super(key: key);
  @override
  _ReviewSubmittedAssignmentViewState createState() =>
      _ReviewSubmittedAssignmentViewState();
}

class _ReviewSubmittedAssignmentViewState
    extends State<ReviewSubmittedAssignmentView> with BaseCommonWidget {
  @override
  Widget build(BuildContext context) {
    return BaseView<ReviewSubmittedAssignmentViewModel>(
        builder: (context, model, child) {
      return SafeArea(child: _getBody(model));
    });
  }

  Widget _getBody(ReviewSubmittedAssignmentViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
      ],
    );
  }

  Widget _getBaseContainer(ReviewSubmittedAssignmentViewModel model) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          commonAppBar('', context),
          Expanded(
              child: SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.only(
                  left: 24, right: 24, bottom: 5, top: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  DetailView(
                    title: widget.replies.author.name.firstname +
                        " " +
                        widget.replies.author.name.lastname,
                    time: new DateFormat("dd MMM yyyy")
                        .format(DateTime.parse(widget.replies.updatedAt)),
                    details: widget.replies.text,
                    files: widget.replies.files,
                  ),
                  RegularButton(
                    buttonLabel: AppLocalizations.of(context).gradeAssignment,
                    onTap: () {
                      Navigator.pushNamed(
                          context, Screen.GradeAssignment.toString(),
                          arguments: {
                            "replies": widget.replies,
                            "submitAssignment": widget.reviewAssignemt
                          });
                    },
                  ),
                ],
              ),
            ),
          ))
        ],
      ),
    );
  }
}
