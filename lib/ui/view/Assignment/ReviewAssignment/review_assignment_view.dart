import 'package:flutter/material.dart';
import 'package:trainmoo/model/RepliesModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/router.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/util/ui_helper.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:trainmoo/ui/view/Assignment/PendingUserView.dart';
import 'package:trainmoo/ui/view/Assignment/ReviewAssignment/review_assignment_view_model.dart';
import 'package:trainmoo/ui/widget/FileView.dart';
import 'package:trainmoo/ui/widget/ItemDetailView.dart';
import 'package:trainmoo/util/image_path.dart';
import 'package:intl/intl.dart';

class ReviewAssignmentView extends StatefulWidget {
  final String id, screenType, postType;

  const ReviewAssignmentView({Key key, this.id, this.screenType, this.postType})
      : super(key: key);
  @override
  _ReviewAssignmentViewState createState() => _ReviewAssignmentViewState();
}

class _ReviewAssignmentViewState extends State<ReviewAssignmentView>
    with BaseCommonWidget, SingleTickerProviderStateMixin {
  TabController tabController;
  int tabIndex = 0;

  @override
  void initState() {
    super.initState();
    tabController = TabController(vsync: this, length: 2);
    tabController.addListener(toggleTab);
  }

  void toggleTab() {
    setState(() {
      tabIndex = tabController.index;
    });
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<ReviewAssignmentViewModel>(
        builder: (context, model, child) {
      model.init(widget.id);
      return SafeArea(child: _getBody(model));
    });
  }

  Widget _getBody(ReviewAssignmentViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        // getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(ReviewAssignmentViewModel model) {
    return Container(
        child: Column(children: <Widget>[
      Container(
        margin: EdgeInsets.only(left: 12, top: 30, right: 12),
        child: Row(
          children: <Widget>[
            IconButton(
                icon: Image.asset(
                  ImagePath.ICON_BackArrow,
                  width: 16,
                  height: 12,
                ),
                onPressed: () {
                  backPress(context);
                }),
            Expanded(
              child: Text(
                widget.postType == "Grade Assignment"
                    ? AppLocalizations.of(context).gradeAssignment
                    : model.reviewAssignmentLists != null
                        ? model.reviewAssignmentLists.type ==
                                API.assignmentScreenType
                            ? AppLocalizations.of(context).reviewAssignment
                            : AppLocalizations.of(context).reviewQuiz
                        : "",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: AppTextStyle.getDynamicFontStyle(
                    Colors.black, 24, FontType.SemiBol),
              ),
            ),
            IconButton(
                icon: Image.asset(ImagePath.NOTIFICATION_IMG,
                    width: 19, height: 20),
                onPressed: () {})
          ],
        ),
      ),
      Expanded(
        child: Container(
          child: DefaultTabController(
            length: 2,
            child: Column(
              children: <Widget>[
                Container(
                  child: TabBar(
                    tabs: [
                      Tab(
                          child: Container(
                        width: 220,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Text(AppLocalizations.of(context).submitEd,
                                  style: AppTextStyle
                                      .getDynamicFontStyleWithoutColor(
                                    18,
                                    FontType.Medium,
                                  )),
                            ),
                            Container(
                                padding: EdgeInsets.only(right: 33),
                                height: 20,
                                child: VerticalDivider(
                                  color: Colors.black,
                                  thickness: 2.0,
                                ))
                          ],
                        ),
                      )),
                      Tab(
                          child: Container(
                        width: 130,
                        margin: EdgeInsets.only(right: 10),
                        transform: Matrix4.translationValues(-55, 0, 0),
                        child: Text(AppLocalizations.of(context).pending,
                            style: AppTextStyle.getDynamicFontStyleWithoutColor(
                              18,
                              FontType.Medium,
                            )),
                      )),
                    ],
                    indicatorColor: Colors.transparent,
                    labelColor: Colors.black,
                    unselectedLabelColor: Palette.secondaryTextColor,
                    controller: tabController,
                  ),
                ),
                Expanded(
                  child: TabBarView(
                    children: [
                      model.submittedStudents.length > 0
                          ? widget.postType == "Review Quiz"
                              ? quizsubmitedList(model,
                                  model.quizSubmmitedStudents.reversed.toList())
                              : submittedList(model)
                          : Container(),
                      model.pendingStudents.length > 0
                          ? pendingList(model)
                          : Container()
                    ],
                    controller: tabController,
                  ),
                ),
              ],
            ),
          ),
        ),
      )
    ]));
  }

  submittedList(ReviewAssignmentViewModel model) {
    return ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: model.submittedStudents.length,
        itemBuilder: (BuildContext cont, int ind) {
          return assignmentItem(model, ind);
        });
  }

  quizsubmitedList(ReviewAssignmentViewModel model,
      List<RepliesModel> quizSubmmitedStudentsList) {
    return ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: quizSubmmitedStudentsList.length,
        itemBuilder: (BuildContext cont, int ind) {
          //return assignmentItem(model, ind);
          return quizReviewItem(model, quizSubmmitedStudentsList, ind);
        });
  }

  quizReviewItem(
      ReviewAssignmentViewModel model, quizSubmmitedStudentsList, int ind) {
    var replayData = quizSubmmitedStudentsList[ind];
    print(" replayData=========> : $replayData");
    // if (model.reviewAssignmentLists.replies[ind].text == null)
    //   return Container();
    // else

    return GestureDetector(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
        child: Card(
          elevation: 2,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(UIHelper.cardRadius)),
          child: Container(
            padding: EdgeInsets.all(22),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CircleAvatar(
                  child: loadCircleNetworkImage(
                      replayData.author.avatar.src, 40, model.header),
                ),
                Container(
                  child: Text(
                    replayData.author.name.firstname +
                        " " +
                        replayData.author.name.lastname,
                    maxLines: 1,
                    overflow: TextOverflow.fade,
                    style: AppTextStyle.getDynamicFontStyleWithWeight(
                        false,
                        Palette.primaryTextColor,
                        18,
                        FontType.SemiBol,
                        FontWeight.normal),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: Text(
                    replayData.assessment.correct == 0
                        ? "0"
                        : (replayData.assessment.correct *
                                replayData.parent.task.maxmarks)
                            .toString(),
                    maxLines: 1,
                    overflow: TextOverflow.fade,
                    style: AppTextStyle.getDynamicFontStyleWithWeight(
                        false,
                        Palette.primaryTextColor,
                        18,
                        FontType.SemiBol,
                        FontWeight.normal),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      // onTap: () {
      //   if (widget.screenType == API.assignmentScreenType) {
      //     Navigator.pushNamed(
      //         context, Screen.ReviewSubmitedAssignment.toString(),
      //         arguments: {
      //           "replies": replayData[0],
      //           "submitAssignment": model.reviewAssignmentLists
      //         });
      //   }
      //},
    );
  }

  assignmentItem(ReviewAssignmentViewModel model, int ind) {
    print(" data=========> : ${model.submittedStudents[ind].user.id}");
    var replayData = model.reviewAssignmentLists.replies
        .where((element) =>
            element.author.id == model.submittedStudents[ind].user.id)
        .toList();
    print(" replayData=========> : $replayData");
    // if (model.reviewAssignmentLists.replies[ind].text == null)
    //   return Container();
    // else
    if (replayData.length > 0)
      return GestureDetector(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
          child: Card(
            elevation: 2,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(UIHelper.cardRadius)),
            child: Container(
              padding: EdgeInsets.all(22),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: Text(
                      Utility.removeAllHtmlTags(
                          model.submittedStudents[ind].name),
                      maxLines: 1,
                      overflow: TextOverflow.fade,
                      style: AppTextStyle.getDynamicFontStyleWithWeight(
                          false,
                          Palette.primaryTextColor,
                          18,
                          FontType.SemiBol,
                          FontWeight.normal),
                    ),
                  ),
                  Text(
                      new DateFormat("dd MMM yyyy")
                          .format(DateTime.parse(replayData[0].updatedAt)),
                      style: AppTextStyle.getDynamicFontStyle(
                          Palette.listColor, 13, FontType.RobotoRegular)),
                  SizedBox(
                    height: 10,
                  ),

                  Text(
                      replayData[0].type == API.assignmentScreenType
                          ? Utility.removeAllHtmlTags(replayData[0].text)
                          : "${replayData[0].assessment.answered}/${replayData[0].assessment.correct} Correct Answer",
                      style: AppTextStyle.detailDescriptionStyle()),
                  replayData[0].files.length > 0
                      ? Padding(
                          padding: const EdgeInsets.only(left: 3),
                          child: new FileView(
                            files: replayData[0].files,
                            headers: model.header,
                          ),
                        )
                      : Container(),
                  SizedBox(
                    height: 10,
                  ),
                  // Container(
                  //     child: postModel.type == 'Assessment'
                  //         ? Text(
                  //             Utility.removeAllHtmlTags(postModel.text),
                  //             style: AppTextStyle.getDynamicFontStyleWithWeight(
                  //                 false,
                  //                 Palette.primaryTextColor,
                  //                 14,
                  //                 FontType.Medium,
                  //                 FontWeight.normal),
                  //           )
                  //         : Text(
                  //             '${postModel.submissions} Submitted | ${postModel.pending} Pending')),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ),
        ),
        onTap: () {
          if (widget.screenType == API.assignmentScreenType) {
            Navigator.pushNamed(
                context, Screen.ReviewSubmitedAssignment.toString(),
                arguments: {
                  "replies": replayData[0],
                  "submitAssignment": model.reviewAssignmentLists
                });
          }
        },
      );
    // return GestureDetector(
    //   child: ItemDetailView(
    //     title: Utility.removeAllHtmlTags(
    //         model.reviewAssignmentLists.replies[ind].text),
    //     time: 'date of submission',
    //     details: model.reviewAssignmentLists.replies[ind].text,
    //     isHtmlData: false,
    //     files: model.reviewAssignmentLists.replies[ind].files,
    //   ),
    //   onTap: () {
    //     if (widget.screenType == API.assignmentScreenType) {
    //       Navigator.pushNamed(
    //           context, Screen.ReviewSubmitedAssignment.toString(),
    //           arguments: {
    //             "replies": model.reviewAssignmentLists.replies[ind],
    //             "submitAssignment": model.reviewAssignmentLists
    //           });
    //     }
    //   },
    // );
  }

  pendingList(ReviewAssignmentViewModel model) {
    return ListView.builder(
      physics: ClampingScrollPhysics(),
      shrinkWrap: true,
      itemCount: model.reviewAssignmentLists.pendingUsers.length,
      itemBuilder: (BuildContext cont, int ind) {
        return PendingUserView(
          studentModel: model.reviewAssignmentLists.pendingUsers[ind],
          id: widget.id,
        );
      },
    );
  }
}
