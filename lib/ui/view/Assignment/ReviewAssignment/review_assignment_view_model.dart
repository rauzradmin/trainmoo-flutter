import 'package:trainmoo/model/QuizUsersModel.dart';
import 'package:trainmoo/model/RepliesModel.dart';
import 'package:trainmoo/model/ReviewAssignment.dart';
import 'package:trainmoo/model/StudentModel.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';

class ReviewAssignmentViewModel extends BaseModel {
  bool isFirstTime = false;
  Map<String, String> header;
  ReviewAssignemt reviewAssignmentLists;
  List<StudentModel> pendingStudents = [];
  List<StudentModel> submittedStudents = [];
  List<Map<String, dynamic>> reviewPendingData = [];
  List<RepliesModel> quizSubmmitedStudents = [];

  String assignmentId;
  init(String assignmentId) async {
    if (!isFirstTime) {
      isFirstTime = true;
      this.assignmentId = assignmentId;

      getReviewAssignmentList();
    }
  }

  void getReviewAssignmentList() {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
        reviewAssignmentLists = await API.getQuizSubmittedUsers(assignmentId);
        pendingStudents = reviewAssignmentLists.pendingUsers;
        submittedStudents = reviewAssignmentLists.submittedUsers;
        print("submittedStudents : $submittedStudents");

        for (StudentModel student in submittedStudents) {
          for (RepliesModel repliesModel in reviewAssignmentLists.replies) {
            print("user id :  ${repliesModel.marks}");
            // print("user id :  ${student.user.id}");
            if (student.user.id == repliesModel.author.id) {
              quizSubmmitedStudents.add(repliesModel);
            }
            quizSubmmitedStudents.sort((a, b) => a.marks.compareTo(b.marks));
          }
        }
        if (reviewAssignmentLists != null) {
          setState(ViewState.Idle);
        }
      } else {
        showToast(AppLocalizations.of(context).internetMessage, context);
      }
    });
  }

  void sendReminder(StudentModel studentModel) {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        showToast('Notified', context);

        setState(ViewState.Busy);
        await API.notifyPendingUser(studentModel, assignmentId);

        setState(ViewState.Idle);
      } else {
        showToast(AppLocalizations.of(context).internetMessage, context);
      }
    });
  }
}
