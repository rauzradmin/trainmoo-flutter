import 'package:flutter/material.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/UploadFileModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/view/Assignment/CreateAssignment/create_assignment_view_model.dart';
import 'package:trainmoo/ui/widget/date_picker.dart';
import 'package:trainmoo/ui/widget/drop_down_student_list.dart';
import 'package:trainmoo/ui/widget/large_text_form_field_with_padding.dart';
import 'package:trainmoo/ui/widget/regular_button.dart';
import 'package:trainmoo/ui/widget/showFile.dart';
import 'package:trainmoo/ui/widget/text_form_field_with_padding.dart';

class CreateAssignmentView extends StatefulWidget {
  @override
  _CreateAssignmentViewState createState() => _CreateAssignmentViewState();
}

class _CreateAssignmentViewState extends State<CreateAssignmentView>
    with BaseCommonWidget {
  TextEditingController titleController = new TextEditingController();
  TextEditingController markController = new TextEditingController();
  TextEditingController descController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BaseView<CreateAssignmentViewModel>(
        builder: (context, model, child) {
      model.init();
      return SafeArea(child: _getBody(model));
    });
  }

  Widget _getBody(CreateAssignmentViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(CreateAssignmentViewModel model) {
    return Column(
      children: <Widget>[
        commonAppBarWithCancelIcon(
            AppLocalizations.of(context).createAssignment, context),
        Expanded(
          child: SingleChildScrollView(
            child: Container(
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 24, right: 24, bottom: 5, top: 10),
                child: Column(
                  children: <Widget>[
                    DropDownList(
                      classList: model.classList,
                      onTap: (ProgramClassesModel selectedChooseRole) {
                        setState(() {
                          model.selectedClass = selectedChooseRole;
                        });
                      },
                      selectedClass: model.selectedClass,
                    ),
                    RegularTextFormFieldWithPadding(
                      textEditingController: titleController,
                      hintText: AppLocalizations.of(context).assignmentTitle,
                      textAlign: TextAlign.left,
                      inputType: TextInputType.text,
                    ),
                    RegularTextFormFieldWithPadding(
                      textEditingController: markController,
                      hintText: AppLocalizations.of(context).mark,
                      textAlign: TextAlign.left,
                      inputType: TextInputType.number,
                    ),
                    DatePicker(
                      hint: AppLocalizations.of(context).startDateTime,
                      onChanged: (dt) =>
                          setState(() => model.selectedStartDate = dt),
                    ),
                    DatePicker(
                      hint: AppLocalizations.of(context).dueDateTime,
                      onChanged: (dt) =>
                          setState(() => model.selectedDueDate = dt),
                    ),
                    LargeTextFormFieldWithPadding(
                      textEditingController: descController,
                      hintText: AppLocalizations.of(context).description,
                      textAlign: TextAlign.left,
                      maxLines: 7,
                      inputType: TextInputType.text,
                    ),
                    ShowFileView(
                      function: (List<UploadFileModel> files) {
                        model.files = files;
                      },
                    ),
                    Container(
                      padding: const EdgeInsets.only(bottom: 30),
                      child: RegularButton(
                        buttonLabel: AppLocalizations.of(context).submit,
                        onTap: () {
                          model.validateAssignmentDetail(
                              titleController.text,
                              markController.text,
                              descController.text,
                              context);
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
