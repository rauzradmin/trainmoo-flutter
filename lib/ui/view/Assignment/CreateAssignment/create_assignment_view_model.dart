import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/model/Database/DatabaseHelper.dart';
import 'package:trainmoo/model/FileModel.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/UploadFileModel.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:path/path.dart' as path;

import 'package:cloudinary_client/cloudinary_client.dart';
import 'package:cloudinary_client/models/CloudinaryResponse.dart';

class CreateAssignmentViewModel extends BaseModel {
  List<Map<String, dynamic>> filePath = [];
  DateTime selectedStartDate;
  DateTime selectedDueDate;
  bool isFirstTime = false;
  List<ProgramClassesModel> classList = [];
  ProgramClassesModel selectedClass;
//  CloudinaryClient client = new CloudinaryClient(
//      '922793423812823', 'dTTYyz0DCI7AFR2IyhpLjteU9gw', 'rauzr');

  List<UploadFileModel> files = [];
  String title, desc, marks;

  init() async {
    if (!isFirstTime) {
      isFirstTime = true;
      classList = await DatabaseHelper().getClasses();
      notifyListeners();
    }
  }

  void validateAssignmentDetail(
      String title, String marks, String desc, BuildContext context) {
    this.title = title;
    this.marks = marks;
    this.desc = desc;
    if (title.isEmpty && desc.isEmpty) {
      showToast("Please fill the All details", context);
    } else if (selectedClass == null) {
      showToast("Please select class", context);
    } else if (title.isEmpty) {
      showToast("Please enter title", context);
    } else if (selectedStartDate == null) {
      showToast("Please select start date", context);
    } else if (selectedDueDate == null) {
      showToast("Please select due date", context);
    } else if (desc.isEmpty) {
      showToast("Please enter description", context);
    } else if (desc.length < 10) {
      showToast("Description must be greater than 10 characters", context);
    } else {
      uploadFiles();
      //  createAssignment(classID, title, marks, desc);
    }
  }

  void createAssignment(
      String classID, String title, String marks, String desc) {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        setState(ViewState.Busy);
        //  await uploadFiles();
        filePath.clear();
        for (UploadFileModel uploadFileModel in files) {
          filePath.add(uploadFileModel.fileModel.toMap());
        }
        String data = await API.createAssignment(
          classID,
          title,
          marks,
          desc,
          selectedStartDate,
          selectedDueDate,
          filePath,
        );

        setState(ViewState.Idle);
        if (data == null)
          Navigator.pop(context);
        else
          showMessage(data, true);
      }
    });
  }

  Future<void> uploadFiles1() async {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        if (getState() != ViewState.Busy) setState(ViewState.Busy);
        for (UploadFileModel uploadFileModel in files) {
          if (!uploadFileModel.isDownloaded) {
            FileModel uploadUrl =
                await uploadFile(uploadFileModel.localFileUrl);
            uploadFileModel.isDownloaded = true;
            uploadFileModel.fileModel = uploadUrl;
            Future.delayed(const Duration(milliseconds: 500), () {});
//            validateMaterialDetail(title, desc, context);
//            continue;
          }
        }
//        createMaterial(selectedClass.id, title, desc);
      }
    });
  }

  uploadFiles() {
    Utility.isInternetAvailable().then((isConnected) async {
      if (isConnected) {
        if (getState() != ViewState.Busy) setState(ViewState.Busy);
        if (await hasFileForUpload()) {
          int position = await getPositionOfFileToUpload();
          downloadFileFromList(position);
        } else {
          createAssignment(selectedClass.id, title, marks, desc);
        }
      }
    });
  }

  Future<bool> hasFileForUpload() async {
    bool hasFileForUpload = false;
    for (UploadFileModel uploadFileModel in files) {
      if (!uploadFileModel.isDownloaded) {
        hasFileForUpload = true;
      }
    }
    return hasFileForUpload;
  }

  Future<int> getPositionOfFileToUpload() async {
    int position = -1;
    for (int i = 0; i < files.length; i++) {
      if (!files[i].isDownloaded) {
        position = i;
        break;
      }
    }
    return position;
  }

  Future<FileModel> uploadFile(File image) async {
    String fileName = path.basename(image.path);
    CloudinaryClient client = new CloudinaryClient(
        '922793423812823', 'dTTYyz0DCI7AFR2IyhpLjteU9gw', 'rauzr');
    CloudinaryResponse response =
        await client.uploadImage(image.path, filename: fileName);
    String thumb = '';
    if (response.url.toLowerCase().contains('.jpg') ||
        response.url.toLowerCase().contains('.png') ||
        response.url.toLowerCase().contains('.jpeg')) {
      thumb = response.url;
    }

    FileModel fileModel = new FileModel(
        fileName,
        fileName,
        response.url,
        thumb,
        'upload',
        response.bytes,
        response.height,
        response.width,
        response.public_id,
        response.created_at,
        false);
    return fileModel;
  }

  void downloadFileFromList(int position) async {
    print('Start upload file');
    FileModel uploadUrl = await uploadFile(files[position].localFileUrl);
    files[position].isDownloaded = true;
    files[position].fileModel = uploadUrl;
    Future.delayed(const Duration(milliseconds: 1000), () {
      print('check for upload new file');
      uploadFiles();
    });
  }

//  Future<bool> uploadFiles() async {
//    for (UploadFileModel uploadFileModel in files) {
//      if (!uploadFileModel.isDownloaded) {
//        String uploadUrl = await uploadFile(uploadFileModel.localFileUrl);
//        uploadFileModel.isDownloaded = true;
//        uploadFileModel.serverFileURl = uploadUrl;
//      }
//    }
//    return true;
//  }
//
//  Future<String> uploadFile(File image) async {
//    String fileName = path.basename(image.path);
//
//    CloudinaryResponse response =
//        await client.uploadImage(image.path, filename: fileName);
//    //response.url
//    print('Image upload response ==> ${response.toJson()}');
//    return response.url;
//  }
}
