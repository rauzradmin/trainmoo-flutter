import 'package:flutter/material.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/UploadFileModel.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/base/base_view.dart';
import 'package:trainmoo/ui/localization/app_localization.dart';
import 'package:trainmoo/ui/theme/app_text_styles.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/view/Assignment/CreateAssignment/create_assignment_view_model.dart';
import 'package:trainmoo/ui/view/Event/create_event_view_model.dart';
import 'package:trainmoo/ui/widget/date_picker.dart';
import 'package:trainmoo/ui/widget/drop_down_student_list.dart';
import 'package:trainmoo/ui/widget/large_text_form_field_with_padding.dart';
import 'package:trainmoo/ui/widget/regular_button.dart';
import 'package:trainmoo/ui/widget/showFile.dart';
import 'package:trainmoo/ui/widget/text_form_field_with_padding.dart';

class CreateEventView extends StatefulWidget {
  @override
  _CreateEventViewState createState() => _CreateEventViewState();
}

class _CreateEventViewState extends State<CreateEventView>
    with BaseCommonWidget {
  TextEditingController titleController = new TextEditingController();
  TextEditingController markController = new TextEditingController();
  TextEditingController descController = new TextEditingController();
  bool onlineClass = false;

  @override
  Widget build(BuildContext context) {
    return BaseView<CreateCreateEventViewModel>(
        builder: (context, model, child) {
      model.init();
      return SafeArea(child: _getBody(model));
    });
  }

  Widget _getBody(CreateCreateEventViewModel model) {
    return Stack(
      children: <Widget>[
        _getBaseContainer(model),
        getProgressBar(model.state),
      ],
    );
  }

  Widget _getBaseContainer(CreateCreateEventViewModel model) {
    return Column(
      children: <Widget>[
        commonAppBarWithCancelIcon(
            AppLocalizations.of(context).createEvent, context),
        Expanded(
          child: SingleChildScrollView(
            child: Container(
              child: Padding(
                padding: const EdgeInsets.only(
                    left: 24, right: 24, bottom: 5, top: 10),
                child: Column(
                  children: <Widget>[
                    DropDownList(
                      classList: model.classList,
                      onTap: (ProgramClassesModel selectedChooseRole) {
                        setState(() {
                          model.selectedClass = selectedChooseRole;
                        });
                      },
                      selectedClass: model.selectedClass,
                    ),
                    LargeTextFormFieldWithPadding(
                      textEditingController: descController,
                      hintText: AppLocalizations.of(context).eventDescription,
                      textAlign: TextAlign.left,
                      maxLines: 7,
                      inputType: TextInputType.text,
                    ),
                    DatePicker(
                      hint: AppLocalizations.of(context).startDateTime,
                      onChanged: (dt) =>
                          setState(() => model.selectedStartDate = dt),
                    ),
                    DatePicker(
                      hint: AppLocalizations.of(context).endDateTime,
                      onChanged: (dt) =>
                          setState(() => model.selectedEndDate = dt),
                    ),
                    Padding(padding: EdgeInsets.only(top: 10)),
                    Container(
                      padding: EdgeInsets.only(
                          left: 15, top: 5, bottom: 5, right: 0),
                      decoration: BoxDecoration(color: Colors.white),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            AppLocalizations.of(context).onlineClass,
                            style: AppTextStyle.hintStyle(),
                          ),
                          Checkbox(
                              value: onlineClass,
                              activeColor: Palette.accentColor,
                              onChanged: (bool newValue) {
                                setState(() {
                                  onlineClass = newValue;
                                });
                              })
                        ],
                      ),
                    ),
                    // ShowFileView(
                    //   function: (List<UploadFileModel> files) {
                    //     model.files = files;
                    //   },
                    // ),
                    Container(
                      padding: const EdgeInsets.only(bottom: 30, top: 10),
                      child: RegularButton(
                        buttonLabel: AppLocalizations.of(context).create,
                        onTap: () {
                          model.validateAssignmentDetail(
                              descController.text, onlineClass, context);
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
