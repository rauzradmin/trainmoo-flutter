import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:trainmoo/model/Database/DatabaseHelper.dart';
import 'package:trainmoo/model/FileModel.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/UploadFileModel.dart';
import 'package:trainmoo/ui/base/base_model.dart';
import 'package:trainmoo/ui/network/API.dart';
import 'package:trainmoo/ui/util/util.dart';
import 'package:path/path.dart' as path;

import 'package:cloudinary_client/cloudinary_client.dart';
import 'package:cloudinary_client/models/CloudinaryResponse.dart';
import 'package:intl/intl.dart';
import 'package:crypto/crypto.dart';

class CreateCreateEventViewModel extends BaseModel {
  List<Map<String, dynamic>> filePath = [];
  DateTime selectedStartDate;
  DateTime selectedEndDate;
  bool isFirstTime = false;
  List<ProgramClassesModel> classList = [];
  ProgramClassesModel selectedClass;

  List<UploadFileModel> files = [];
  String title, desc, marks;

  init() async {
    if (!isFirstTime) {
      isFirstTime = true;
      classList = await DatabaseHelper().getClasses();
      notifyListeners();
    }
  }

  void validateAssignmentDetail(
      String desc, bool onlineClass, BuildContext context) {
    this.desc = desc;

    if (selectedClass == null) {
      showToast("Please select class", context);
    } else if (selectedStartDate == null) {
      showToast("Please select start date", context);
    } else if (selectedEndDate == null) {
      showToast("Please select due date", context);
    } else if (desc.isEmpty) {
      showToast("Please enter description", context);
    } else if (desc.length < 10) {
      showToast("Description must be greater than 10 characters", context);
    } else {
      createEvent(
          selectedClass, desc, selectedStartDate, selectedEndDate, onlineClass);
    }
  }

  void createEvent(ProgramClassesModel classData, String desc,
      DateTime selectedStartDate, DateTime selectedEndDate, bool onlineClass) {
    Utility.isInternetAvailable().then((isConnected) async {
      print("classData.tutor. ");
      var tutor = classData.tutor != null
          ? {
              "avatarThumbnail": classData.tutor.avatarThumbnail,
              "children": classData.tutor.children,
              "created_at": classData.tutor.createdAt,
              "dob": classData.tutor.dob,
              "email": classData.tutor.email,
              "gender": classData.tutor.gender,
              "isActive": classData.tutor.isActive,
              "last_login_at": classData.tutor.lastLoginAt,
              "name": classData.tutor.name,
              "registerUsing": classData.tutor.registerUsing,
              "school": classData.tutor.school,
              "schoolAdmin": classData.tutor.schoolAdmin,
              "status": classData.tutor.status,
              "type": classData.tutor.type,
              "updated_at": classData.tutor.updatedAt,
              "user": classData.tutor.user,
              "__v": classData.tutor.V,
              "_id": classData.tutor.Id
            }
          : {};

      if (isConnected) {
        var classobject = {
          "category": classData.category,
          "created_at": classData.createdAt,
          "displayPercentileToStudents": classData.displayPercentileToStudents,
          "displayTotalToStudents": classData.displayTotalToStudents,
          "end": classData.end,
          "enrollCode": classData.enrollCode,
          "eventCount": classData.eventCount,
          "from": classData.from,
          "grading": {
            "weightages": classData.grading.weightages,
            "by": classData.grading.by
          },
          "media": classData.media,
          "medium": classData.medium,
          "name": classData.name,
          "paymentSchedules": classData.paymentSchedules,
          "placeid": classData.placeid,
          "postCount": {
            "total": classData.postCount.total,
            "question": classData.postCount.total,
            "assessment": classData.postCount.assessment,
            "discussion": classData.postCount.discussion,
            "assignment": classData.postCount.assignment,
            "material": classData.postCount.material
          },
          "registrationStatus": classData.registrationStatus,
          "school": classData.school,
          // "sections": classData.sections,
          "start": classData.start,
          "status": classData.status,
          "summary": classData.summary,
          "to": classData.to,
          "tutor": tutor,
          "type": classData.type,
          "uniqueid": classData.uniqueid,
          "updated_at": classData.updatedAt,
          "users": {
            "teacher": classData.users.teacher,
            "student": classData.users.student
          },
          "__v": classData.V,
          "_id": classData.id
        };
        print("list : $classobject");
        // setState(ViewState.Busy);
        String startDate = DateFormat('MM/dd/yyyy').format(selectedStartDate);
        String startTime = DateFormat('kk:mma').format(selectedStartDate);
        String endDate = DateFormat('MM/dd/yyyy').format(selectedEndDate);
        String endTime = DateFormat('kk:mma').format(selectedEndDate);
        print("formattedDate : $startDate");
        print("startTime : $startTime");
        print("endDate : $endDate");
        print("endTime : $endTime");
        if (onlineClass) {
          var url = createMeeting(classData.program.name, selectedStartDate);
          API.creatMeeting(url);
        }
        String data = await API.createEvent(
            classobject,
            desc,
            selectedStartDate,
            selectedEndDate,
            startDate,
            startTime,
            endDate,
            endTime,
            classData.program.name,
            classData.tutor != null ? classData.tutor.name : null);
        setState(ViewState.Idle);
        if (data == null) {
          showToast('Event Successfully Created', context);
          Timer.periodic(new Duration(milliseconds: 6000), (timer) {
            Navigator.of(context).pop();
            timer.cancel();
          });
        } else
          showToast(data, context);
        // showMessage(data, true);
      }
    });
  }

  String createMeeting(programName, selectedStartDate) {
    //console.log("eventfactory varia", meetingID, event.class.program.name);
    var originalStr = programName;
    var meetingName = originalStr.split(' ').join('+');
    // var dt = moment(vm.event.start, "YYYY-MM-DD HH:mm:ss");
    // const id = Math.floor(Math.random() * (9999 - 1000)) + 1000;
    var rng = new Random();
    var randomvalue = rng.nextInt(9999);
    var id = (randomvalue * (9999 - 1000)).floor() + 1000;
    var meetingID =
        '$id$meetingName${DateFormat('MM').format(selectedStartDate)}${DateFormat('dd').format(selectedStartDate)}';
    var str =
        'createallowStartStopRecording=true&attendeePW=111222&autoStartRecording=false&meetingID=$meetingID&moderatorPW=333444&name=${meetingName}Meeting&record=falseHDp5MwD6UTao7XCctM4yKwx0y0LPxNYcQWoXfpZE';
    var bytes = utf8.encode(str); // data being hashed
    var digest = sha1.convert(bytes);
    var checksum = digest.toString();
    print("checksum:::::::, $checksum");
    var url =
        'https://rauzrrooms.in/bigbluebutton/api/create?allowStartStopRecording=true&attendeePW=111222&autoStartRecording=false&meetingID=$meetingID&moderatorPW=333444&name=${meetingName}Meeting&record=false&checksum=$checksum';
    print("Url : $url");
    return url;
  }
}
