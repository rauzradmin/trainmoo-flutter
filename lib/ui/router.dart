
import 'package:flutter/material.dart' hide Router;
import 'package:flutter/widgets.dart';
import 'package:trainmoo/model/FileModel.dart';
import 'package:trainmoo/model/MaterialModel.dart';
import 'package:trainmoo/model/ProgramClassesModel.dart';
import 'package:trainmoo/model/QuizUsersModel.dart';
import 'package:trainmoo/model/RepliesModel.dart';
import 'package:trainmoo/model/ReviewAssignment.dart';
import 'package:trainmoo/model/SyllabusModel.dart' as syllabus;
import 'package:trainmoo/model/studentModuel/grade/Group.dart';
import 'package:trainmoo/ui/theme/palette.dart';
import 'package:trainmoo/ui/view/Assignment/CreateAssignment/create_assignment_view.dart';
import 'package:trainmoo/ui/view/Assignment/Reminder/review_submitted_assignment_view.dart';
import 'package:trainmoo/ui/view/Assignment/ReviewAssignment/review_assignment_view.dart';
import 'package:trainmoo/ui/view/Attendance/TakeAttendance/take_attendance_view.dart';
import 'package:trainmoo/ui/view/Attendance/attendance_view.dart';
import 'package:trainmoo/ui/view/Attendance/student/student_attendance_view.dart';
import 'package:trainmoo/ui/view/Event/create_event_view.dart';
import 'package:trainmoo/ui/view/Grade/GradeAssignment/grade_assignment_view.dart';
import 'package:trainmoo/ui/view/Material/MaterialDetail/material_detail_view.dart';
import 'package:trainmoo/ui/view/Material/material_view.dart';
import 'package:trainmoo/ui/view/MySession/my_session_view.dart';
import 'package:trainmoo/ui/view/MyWork/my_work_teacher_view.dart';
import 'package:trainmoo/ui/view/MyWork/my_work_view.dart';
import 'package:trainmoo/ui/view/Notification/notification_view.dart';
import 'package:trainmoo/ui/view/Quiz/CreateQuiz/AddQuestion/add_question_view.dart';
import 'package:trainmoo/ui/view/Quiz/CreateQuiz/create_quiz_view.dart';
import 'package:trainmoo/ui/view/Registration/EnrollNow.dart';
import 'package:trainmoo/ui/view/Registration/registration_view.dart';
import 'package:trainmoo/ui/view/Registration/signupWithType.dart';
import 'package:trainmoo/ui/view/StudentList/student_list_view.dart';
import 'package:trainmoo/ui/view/Syllabus/SyllabusDetail/syllabus_detail_view.dart';
import 'package:trainmoo/ui/view/Syllabus/syllabus_view.dart';
import 'package:trainmoo/ui/view/class/class_item_view.dart';
import 'package:trainmoo/ui/view/class/class_view.dart';
import 'package:trainmoo/ui/view/fab_list_view/fab_list_view.dart';
import 'package:trainmoo/ui/view/home/HomeTab.dart';
import 'package:trainmoo/ui/view/home/home_view.dart';
import 'package:trainmoo/ui/view/login/login.dart';
import 'package:trainmoo/ui/view/login/notice.dart';
import 'package:trainmoo/ui/view/more/MoreListView.dart';
import 'package:trainmoo/ui/view/studentUserModuel/studentQuizView.dart';
import 'package:trainmoo/ui/view/studentUserModuel/student_grade_details.dart';
import 'package:trainmoo/ui/view/studentUserModuel/student_grade_view.dart';
import 'package:trainmoo/ui/view/studentUserModuel/student_quiz_result.dart';
import 'package:trainmoo/ui/view/tab_manager_view.dart';
import 'package:trainmoo/ui/view/user/user_detail_view.dart';
import 'package:trainmoo/ui/widget/ShowAttachments.dart';

import 'network/API.dart';
import 'view/Grade/GradeList/GradeListView.dart';
import 'view/Material/CreateMaterial/create_material_view.dart';

const String initialRoute = "login";

enum Screen {
  Login,
  HomeView,
  HomeTab,
  Registration,
  StudentList,
  MaterialView,
  MaterialDetail,
  ClassListView,
  AssignmentView,
  AssignmentDetail,
  ReviewAssignment,
  GradeAssignment,
  ReviewSubmitedAssignment,
  ClassItemView,
  SyllabusView,
  TakeAttendanceView,
  SyllabusDetail,
  GradeView,
  StudentAttendanceView,
  Attendance,
  QuizView,
  ReviewQuiz,
  CreateAssignment,
  CreateMaterial,
  CreateQuiz,
  AddQuestion,
  MyWork,
  MyWorkTeacher,
  MySession,
  TabManagerView,
  ShowAttachments,
  UserDetailView,
  StudentQuizView,
  FabListView,
  MoreListView,
  StudentGradeListView,
  StudentQuizResult,
  NotificationView,
  CreateEventView,
  StudentGradeDetailsView,
  NoticeView,
  SignupWithTypeView,
  EnrollNowView,
}

class RouterSceen {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    print("settings------> $settings");
    Screen screen;
    if (settings.name == Screen.Login.toString()) {
      screen = Screen.Login;
    } else if (settings.name == Screen.Registration.toString()) {
      screen = Screen.Registration;
    } else if (settings.name == Screen.SignupWithTypeView.toString()) {
      screen = Screen.SignupWithTypeView;
    } else if (settings.name == Screen.EnrollNowView.toString()) {
      screen = Screen.EnrollNowView;
    } else if (settings.name == Screen.StudentList.toString()) {
      screen = Screen.StudentList;
    } else if (settings.name == Screen.HomeView.toString()) {
      screen = Screen.HomeView;
    } else if (settings.name == Screen.MaterialView.toString()) {
      screen = Screen.MaterialView;
    } else if (settings.name == Screen.ClassListView.toString()) {
      screen = Screen.ClassListView;
    } else if (settings.name == Screen.MaterialDetail.toString()) {
      screen = Screen.MaterialDetail;
    } else if (settings.name == Screen.AssignmentView.toString()) {
      screen = Screen.AssignmentView;
    } else if (settings.name == Screen.AssignmentDetail.toString()) {
      screen = Screen.AssignmentDetail;
    } else if (settings.name == Screen.ReviewAssignment.toString()) {
      screen = Screen.ReviewAssignment;
    } else if (settings.name == Screen.GradeAssignment.toString()) {
      screen = Screen.GradeAssignment;
    } else if (settings.name == Screen.ClassItemView.toString()) {
      screen = Screen.ClassItemView;
    } else if (settings.name == Screen.ReviewSubmitedAssignment.toString()) {
      screen = Screen.ReviewSubmitedAssignment;
    } else if (settings.name == Screen.SyllabusView.toString()) {
      screen = Screen.SyllabusView;
    } else if (settings.name == Screen.SyllabusDetail.toString()) {
      screen = Screen.SyllabusDetail;
    } else if (settings.name == Screen.GradeView.toString()) {
      screen = Screen.GradeView;
    } else if (settings.name == Screen.StudentAttendanceView.toString()) {
      screen = Screen.StudentAttendanceView;
    } else if (settings.name == Screen.Attendance.toString()) {
      screen = Screen.Attendance;
    } else if (settings.name == Screen.QuizView.toString()) {
      screen = Screen.QuizView;
    } else if (settings.name == Screen.ReviewQuiz.toString()) {
      screen = Screen.ReviewQuiz;
    } else if (settings.name == Screen.CreateAssignment.toString()) {
      screen = Screen.CreateAssignment;
    } else if (settings.name == Screen.CreateMaterial.toString()) {
      screen = Screen.CreateMaterial;
    } else if (settings.name == Screen.CreateQuiz.toString()) {
      screen = Screen.CreateQuiz;
    } else if (settings.name == Screen.TakeAttendanceView.toString()) {
      screen = Screen.TakeAttendanceView;
    } else if (settings.name == Screen.AddQuestion.toString()) {
      screen = Screen.AddQuestion;
    } else if (settings.name == Screen.MyWork.toString()) {
      screen = Screen.MyWork;
    } else if (settings.name == Screen.MyWorkTeacher.toString()) {
      screen = Screen.MyWorkTeacher;
    } else if (settings.name == Screen.MySession.toString()) {
      screen = Screen.MySession;
    } else if (settings.name == Screen.TabManagerView.toString()) {
      screen = Screen.TabManagerView;
    } else if (settings.name == Screen.MySession.toString()) {
      screen = Screen.MySession;
    } else if (settings.name == Screen.ShowAttachments.toString()) {
      screen = Screen.ShowAttachments;
    } else if (settings.name == Screen.HomeTab.toString()) {
      screen = Screen.HomeTab;
    } else if (settings.name == Screen.UserDetailView.toString()) {
      screen = Screen.UserDetailView;
    } else if (settings.name == Screen.StudentQuizView.toString()) {
      screen = Screen.StudentQuizView;
    } else if (settings.name == Screen.FabListView.toString()) {
      screen = Screen.FabListView;
    } else if (settings.name == Screen.StudentGradeListView.toString()) {
      screen = Screen.StudentGradeListView;
    } else if (settings.name == Screen.StudentQuizResult.toString()) {
      screen = Screen.StudentQuizResult;
    } else if (settings.name == Screen.MoreListView.toString()) {
      screen = Screen.MoreListView;
    } else if (settings.name == Screen.NotificationView.toString()) {
      screen = Screen.NotificationView;
    } else if (settings.name == Screen.CreateEventView.toString()) {
      screen = Screen.CreateEventView;
    } else if (settings.name == Screen.StudentGradeDetailsView.toString()) {
      screen = Screen.StudentGradeDetailsView;
    } else if (settings.name == Screen.NoticeView.toString()) {
      screen = Screen.NoticeView;
    }

    return getRoute(screen, settings.arguments);
  }

  static Route<dynamic> getRoute(
      Screen screen, Map<String, dynamic> arguments) {
    if (screen == null) {
      return MaterialPageRoute(
          builder: (_) => Scaffold(
                body: Center(
                  child: Text('No route defined for screen'),
                ),
              ));
    } else {
      if (screen == Screen.Login) {
        return MaterialPageRoute(
            builder: (_) =>
                Material(color: Palette.screenBgColor, child: LoginView()));
      } else if (screen == Screen.NoticeView) {
        return MaterialPageRoute(
            builder: (_) =>
                Material(color: Palette.screenBgColor, child: Notice()));
      } else if (screen == Screen.SignupWithTypeView) {
        return MaterialPageRoute(
            builder: (_) => Material(
                color: Palette.screenBgColor, child: SignupWithTypeView()));
      } else if (screen == Screen.EnrollNowView) {
        return MaterialPageRoute(
            builder: (_) =>
                Material(color: Palette.screenBgColor, child: EnrollNowView()));
      } else if (screen == Screen.HomeView) {
        return MaterialPageRoute(
            builder: (_) =>
                Material(color: Palette.screenBgColor, child: HomeView()));
      } else if (screen == Screen.StudentGradeDetailsView) {
        Group groupDetails = arguments['groupDetails'];
        return MaterialPageRoute(
            builder: (_) => Material(
                color: Palette.screenBgColor,
                child: StudentGradeDetailsView(groupDetails: groupDetails)));
      } else if (screen == Screen.CreateEventView) {
        return MaterialPageRoute(
            builder: (_) => Material(
                color: Palette.screenBgColor, child: CreateEventView()));
      } else if (screen == Screen.NotificationView) {
        return MaterialPageRoute(
            builder: (_) => Material(
                color: Palette.screenBgColor, child: NotificationView()));
      } else if (screen == Screen.FabListView) {
        return MaterialPageRoute(
            builder: (_) =>
                Material(color: Palette.screenBgColor, child: FabListView()));
      } else if (screen == Screen.MoreListView) {
        return MaterialPageRoute(
            builder: (_) =>
                Material(color: Palette.screenBgColor, child: MoreListView()));
      } else if (screen == Screen.HomeTab) {
        return MaterialPageRoute(
            builder: (_) => HomeTab(Screen.HomeView.toString()));
      } else if (screen == Screen.Registration) {
        return MaterialPageRoute(
            builder: (_) => Material(
                color: Palette.screenBgColor, child: RegistrationView()));
      } else if (screen == Screen.StudentList) {
        String classId = arguments['class_id'];

        return MaterialPageRoute(
            builder: (_) => Material(
                  color: Palette.screenBgColor,
                  child: StudentListView(
                    classId: classId,
                  ),
                ));
      } else if (screen == Screen.MaterialView) {
        ProgramClassesModel classObject = arguments['class'];
        return MaterialPageRoute(
            builder: (_) => Material(
                  color: Palette.screenBgColor,
                  child: MaterialView(
                    classObject: classObject,
                    screenType: API.materialScreenType,
                  ),
                ));
      } else if (screen == Screen.MaterialDetail) {
        MaterialModel materialModel = arguments['material'];
        String screenType = arguments['screen_type'];
        Color colorName = arguments['color'];
        bool autoFocus = false;
        if (arguments.containsKey('autofocus')) {
          autoFocus = true;
        }
        return MaterialPageRoute(
            builder: (_) => Material(
                  color: Palette.screenBgColor,
                  child: MaterialDetailView(
                    materialObject: materialModel,
                    screenType: screenType,
                    color: colorName,
                    autoFocusToComment: autoFocus,
                  ),
                ));
      } else if (screen == Screen.AssignmentView) {
        ProgramClassesModel classObject = arguments['class'];
        return MaterialPageRoute(
            builder: (_) => Material(
                  color: Palette.screenBgColor,
                  child: MaterialView(
                    classObject: classObject,
                    screenType: API.assignmentScreenType,
                  ),
                ));
      } else if (screen == Screen.ReviewAssignment) {
        String assingmentId = arguments['assignment_id'];
        String postType = arguments['postType'];
        return MaterialPageRoute(
            builder: (_) => Material(
                  color: Palette.screenBgColor,
                  child: ReviewAssignmentView(
                      id: assingmentId,
                      screenType: API.assignmentScreenType,
                      postType: postType),
                ));
      } else if (screen == Screen.GradeAssignment) {
        RepliesModel replies = arguments['replies'];
        ReviewAssignemt reviewAssignemt = arguments['submitAssignment'];
        return MaterialPageRoute(
            builder: (_) => Material(
                  color: Palette.screenBgColor,
                  child: GradeAssignmentView(
                    replies: replies,
                    reviewAssignemt: reviewAssignemt,
                  ),
                ));
      } else if (screen == Screen.ClassListView) {
        return MaterialPageRoute(
            builder: (_) =>
                Material(color: Palette.screenBgColor, child: ClassListView()));
      } else if (screen == Screen.ReviewSubmitedAssignment) {
        RepliesModel replies = arguments['replies'];
        ReviewAssignemt reviewAssignemt = arguments['submitAssignment'];
        return MaterialPageRoute(
            builder: (_) => Material(
                  color: Palette.screenBgColor,
                  child: ReviewSubmittedAssignmentView(
                    replies: replies,
                    reviewAssignemt: reviewAssignemt,
                  ),
                ));
      } else if (screen == Screen.SyllabusView) {
        String classId = arguments['class_id'];
        return MaterialPageRoute(
            builder: (_) => Material(
                  color: Palette.screenBgColor,
                  child: SyllabusView(
                    classId: classId,
                  ),
                ));
      } else if (screen == Screen.SyllabusDetail) {
        syllabus.SectionsBean syllabusModel = arguments['syllabus'];
        return MaterialPageRoute(
            builder: (_) => Material(
                  color: Palette.screenBgColor,
                  child: SyllabusDetailView(
                    syllabus: syllabusModel,
                  ),
                ));
      } else if (screen == Screen.GradeView) {
        String classId = arguments['class_id'];
        return MaterialPageRoute(
            builder: (_) => Material(
                  color: Palette.screenBgColor,
                  child: GradeListView(
                    classId: classId,
                  ),
                ));
      } else if (screen == Screen.ClassItemView) {
        ProgramClassesModel classObject = arguments['class'];
        return MaterialPageRoute(
            builder: (_) => Material(
                  color: Palette.screenBgColor,
                  child: ClassItemView(
                    classObject: classObject,
                  ),
                ));
      } else if (screen == Screen.StudentAttendanceView) {
        // String classId = arguments['class_id'];
        return MaterialPageRoute(
            builder: (_) => Material(
                  color: Palette.screenBgColor,
                  child: StudentAttendanceView(),
                ));
      } else if (screen == Screen.Attendance) {
        String classId = arguments['class_id'];
        return MaterialPageRoute(
            builder: (_) => Material(
                  color: Palette.screenBgColor,
                  child: AttendanceView(classId: classId),
                ));
      } else if (screen == Screen.QuizView) {
        ProgramClassesModel classObject = arguments['class'];
        return MaterialPageRoute(
            builder: (_) => Material(
                  color: Palette.screenBgColor,
                  child: MaterialView(
                    classObject: classObject,
                    screenType: API.quizScreenType,
                  ),
                ));
      } else if (screen == Screen.ReviewQuiz) {
        String id = arguments['quiz_id'];
        return MaterialPageRoute(
            builder: (_) => Material(
                  color: Palette.screenBgColor,
                  child: ReviewAssignmentView(
                    id: id,
                    screenType: API.quizScreenType,
                  ),
                ));
//        return MaterialPageRoute(
//            builder: (_) => ReviewQuizView(quizID: quizID));
      } else if (screen == Screen.ShowAttachments) {
        int index = arguments['index'];
        List<FileModel> files = arguments['files'];
        Map<String, dynamic> header = arguments['header'];
        return MaterialPageRoute(
            builder: (_) => Material(
                  color: Palette.screenBgColor,
                  child: ShowAttachments(
                    index: index,
                    imgList: files,
                    headers: header,
                  ),
                ));
//        return MaterialPageRoute(
//            builder: (_) => ReviewQuizView(quizID: quizID));
      } else if (screen == Screen.CreateAssignment) {
        return MaterialPageRoute(
            builder: (_) => Material(
                color: Palette.screenBgColor, child: CreateAssignmentView()));
      } else if (screen == Screen.CreateMaterial) {
        var materialObject = arguments['materialObject'];
        var materialType = arguments['materialType'];
        return MaterialPageRoute(
            builder: (_) => Material(
                color: Palette.screenBgColor,
                child: CreateMaterialView(materialObject, materialType)));
      } else if (screen == Screen.CreateQuiz) {
        return MaterialPageRoute(
            builder: (_) => Material(
                color: Palette.screenBgColor, child: CreateQuizView()));
      } else if (screen == Screen.AddQuestion) {
        return MaterialPageRoute(
            builder: (_) => Material(
                color: Palette.screenBgColor, child: AddQuestionView()));
      } else if (screen == Screen.MyWork) {
        var postType = arguments['postType'];
        return MaterialPageRoute(
            builder: (_) => Material(
                color: Palette.screenBgColor, child: MyWorkView(postType)));
      } else if (screen == Screen.MyWorkTeacher) {
        return MaterialPageRoute(
            builder: (_) => Material(
                color: Palette.screenBgColor, child: MyWorkTeacherView()));
      } else if (screen == Screen.MySession) {
        return MaterialPageRoute(
            builder: (_) =>
                Material(color: Palette.screenBgColor, child: MySessionView()));
      } else if (screen == Screen.TakeAttendanceView) {
        return MaterialPageRoute(
            builder: (_) => Material(
                color: Palette.screenBgColor, child: TakeAttendanceView()));
      } else if (screen == Screen.TabManagerView) {
        return MaterialPageRoute(
            builder: (_) => Material(
                color: Palette.screenBgColor, child: TabManagerView()));
      } else if (screen == Screen.UserDetailView) {
        return MaterialPageRoute(
            builder: (_) => Material(
                color: Palette.screenBgColor, child: UserDetailView()));
      } else if (screen == Screen.StudentGradeListView) {
        return MaterialPageRoute(
            builder: (_) => Material(
                color: Palette.screenBgColor, child: StudentGradeListView()));
      } else if (screen == Screen.StudentQuizResult) {
        String percentage = arguments['percentage'];
        return MaterialPageRoute(
            builder: (_) => Material(
                color: Palette.screenBgColor,
                child: StudentQuizResult(percentage)));
      } else if (screen == Screen.StudentQuizView) {
        String quizTitle = arguments['quizTitle'];
        String quizId = arguments['quiz_id'];
        return MaterialPageRoute(
            builder: (_) => Material(
                  color: Palette.screenBgColor,
                  child: StudentQuizView(quizTitle: quizTitle, quizId: quizId),
                ));
      }
      return MaterialPageRoute(
          builder: (_) => Scaffold(
                body: Center(
                  child: Text('No route defined for screen'),
                ),
              ));
    }
  }
}
