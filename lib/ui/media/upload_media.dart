import 'dart:async';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_video_compress/flutter_video_compress.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as dir;
import 'package:trainmoo/model/asset_model.dart';
import 'package:trainmoo/ui/base/base_common_widget.dart';
import 'package:trainmoo/ui/theme/palette.dart';

class UploadMedia with BaseCommonWidget {
  File image;
  File cameraImage;
  File video;
  File cameraVideo;
  File document;
  BuildContext context;

  bool isImage = true;
  bool isDoc = false;

  final flutterVideoCompress = FlutterVideoCompress();

  String sasToken, sasContainer;
  Subscription _subscription;

  UploadMedia(BuildContext context) {
    this.context = context;
    _subscription =
        flutterVideoCompress.compressProgress$.subscribe((progress) {
      print('progress: $progress%');
    });
  }

  void dispose() {
    _subscription.unsubscribe();
  }

  // This funcion will helps you to pick and Image from Gallery
  Future<File> pickImageFromGallery() async {
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);
    isImage = true;
    isDoc = false;
    return this.image = image;
    // setState(() {});
    // updateUiAndCompressAndUploadMedia(_image, false);
  }

  // This funcion will helps you to pick and Image from Camera
  Future<File> pickImageFromCamera() async {
    File image = await ImagePicker.pickImage(source: ImageSource.camera);
    isImage = true;
    isDoc = false;
    return cameraImage = image;
    // setState(() {});
    // updateUiAndCompressAndUploadMedia(_cameraImage, false);
  }

  // This funcion will helps you to pick a Video File
  Future<File> pickVideoFromGallery() async {
    File video = await ImagePicker.pickVideo(source: ImageSource.gallery);
    isImage = false;
    isDoc = false;
    return this.video = video;
    // setState(() {});

    // updateUiAndCompressAndUploadMedia(_video, true);
  }

  // This funcion will helps you to pick a Video File from Camera
  Future<File> pickVideoFromCamera() async {
    File video = await ImagePicker.pickVideo(source: ImageSource.camera);
    isImage = false;
    isDoc = false;
    return cameraVideo = video;
    // setState(() {});
    // updateUiAndCompressAndUploadMedia(_cameraVideo, true);
  }

  Future<File> pickDocument() async {
    // File document = await FilePicker.getFile(
    //   type: FileType.any,
    // );
    FilePickerResult result = await FilePicker.platform.pickFiles();
    if (result != null) {
      File document = File(result.files.single.path);
      return this.document = document;
    }
    isImage = null;
    isDoc = true;
  }

  Icon pickFileIcon(File file) {
    String extension = path.extension(file.path);
    Icon icon;
    if (extension.contains('pdf')) {
      icon = Icon(FontAwesomeIcons.filePdf, color: Palette.redColor);
    } else if (extension.contains('doc')) {
      icon = Icon(FontAwesomeIcons.fileWord, color: Palette.accentColor);
    } else if (extension.contains('xls')) {
      icon = Icon(FontAwesomeIcons.fileExcel, color: Palette.accentColor);
    } else if (extension.contains('mp3') ||
        extension.contains('wav') ||
        extension.contains('ogg') ||
        extension.contains('wav')) {
      icon = Icon(FontAwesomeIcons.fileAudio, color: Palette.accentColor);
    } else if (extension.contains('txt')) {
      icon = Icon(FontAwesomeIcons.file, color: Palette.accentColor);
    } else if (extension.contains('rar') || extension.contains('zip')) {
      icon = Icon(FontAwesomeIcons.fileArchive, color: Palette.redColor);
    } else if (extension.contains('ppt')) {
      icon = Icon(FontAwesomeIcons.filePowerpoint, color: Palette.redColor);
    } else if (extension.contains('csv')) {
      icon = Icon(FontAwesomeIcons.fileCsv, color: Palette.accentColor);
    } else {
      icon = Icon(FontAwesomeIcons.file, color: Palette.accentColor);
    }
    return icon;
  }

  Future<AssetModel> buildUploadMediaUrl(File file) async {
    AssetModel assetModel;
//    = await API.uploadMedia(
//        sasContainer, getFileName(file), sasToken, file, isImage);
    return assetModel;
  }

  String getFileName(File file) {
    return path.basename(file.path);
  }

  Future<AssetModel> generateSasToken(File file) async {
    AssetModel model;
    if (sasToken != null &&
        sasToken.isNotEmpty &&
        sasContainer != null &&
        sasContainer.isNotEmpty) {
      print('token available');
      print('sas token -> $sasToken');
      print('sas container -> $sasContainer');
      model = await buildUploadMediaUrl(file);
      Navigator.of(context).pop();
      showToastMessage('Sucessfully uploaded...');
    } else {
      print('token null');
      //TODO commented create token for the sas token
//      Map<String, dynamic> result = //await API.generateSasToken();
//      print(result);
//      sasToken = result['sasToken'];
//      sasContainer = result['container'];
      print('sas token -> $sasToken');
      print('sas container -> $sasContainer');
      model = await buildUploadMediaUrl(file);
      Navigator.of(context).pop();
      showToastMessage('Sucessfully uploaded...');
    }
    return model;
  }

  Future<AssetModel> updateUiAndCompressAndUploadMedia(
      File file, bool isCompress) async {
    // _loadingStreamCtrl.sink.add(true);

    File uploadingFile = file;
    showCircularProgressDialog(context);
    // showUi();
    if (isCompress) {
      String fileName = getFileName(uploadingFile);
      print(fileName);

      if (isImage != null && !isImage) {
        final MediaInfo info = await flutterVideoCompress.compressVideo(
          uploadingFile.path,
          quality: VideoQuality
              .DefaultQuality, // default(VideoQuality.DefaultQuality)
          deleteOrigin: false, // default(false)
        );

        print('is compressing...' +
            flutterVideoCompress.isCompressing.toString());

        debugPrint(info.toJson().toString());
        uploadingFile = new File(info.path);
      } else if (isImage != null && isImage) {
        uploadingFile = await compressImage(file);
      }
      print(uploadingFile.path);
    }
    return await generateSasToken(uploadingFile);
    // _loadingStreamCtrl.sink.add(false);
  }

  // 2. compress file and get file.
  Future<File> compressImage(File file) async {
    String filename = getFileName(file);
    var tempDir = await dir.getTemporaryDirectory();
    var targetPath = tempDir.absolute.path + "/$filename";
    var result = await FlutterImageCompress.compressAndGetFile(
      file.absolute.path,
      targetPath,
      quality: 88,
    );

    print(file.lengthSync());
    print(result.lengthSync());

    return result;
  }
}
