import 'package:trainmoo/actions/home_actions.dart';
import '../app_state.dart';

AppState appReducers(AppState prevState, dynamic action) {
  AppState newState = AppState.fromAppState(prevState);
  print("actions is : $action");
  if (action is SetPostListAction) {
    newState.latestPost = action.latestPost;
  }
  if (action is SetLoadFirstTime) {
    newState.loadFirstTime = action.loadFirstTime;
  }
  if (action is SetTodaySessionList) {
    newState.todaySessionList = action.todaySessionList;
  }
  if (action is SetWeekSessionList) {
    newState.weekSessionList = action.weekSessionList;
  }
  if (action is SetMonthSessionList) {
    newState.monthSessionList = action.monthSessionList;
  }
  if (action is SetLoadFirstTimeSessionList) {
    newState.loadFirstTimeSessionList = action.loadFirstTimeSessionList;
  }
  if (action is SetLoadFirstTimeMyWork) {
    newState.loadFirstTimeMyWork = action.loadFirstTimeMyWork;
  }
  if (action is SetSubmitedMaterialLists) {
    newState.submitedMaterialLists = action.submitedMaterialLists;
  }
  if (action is SetPaddingMaterialLists) {
    newState.paddingMaterialLists = action.paddingMaterialLists;
  }
  if (action is SetLoadFirstTimeGradAssignment) {
    newState.loadFirstTimeGradAssignment = action.loadFirstTimeGradAssignment;
  }
  if (action is SetPaddingAssignmentMaterialLists) {
    newState.paddingAssignmentMaterialLists =
        action.paddingAssignmentMaterialLists;
  }
  if (action is SetSubmitedAssignmenMaterialLists) {
    newState.submitedAssignmenMaterialLists =
        action.submitedAssignmenMaterialLists;
  }
  if (action is SetLoadFirstTimeReviewQuiz) {
    newState.loadFirstTimeReviewQuiz = action.loadFirstTimeReviewQuiz;
  }
  if (action is SetPaddingQuizMaterialLists) {
    newState.paddingQuizMaterialLists = action.paddingQuizMaterialLists;
  }
  if (action is SetSubmitedQuizMaterialLists) {
    newState.submitedQuizMaterialLists = action.submitedQuizMaterialLists;
  }
  if (action is SetLoadFirstTimeClass) {
    newState.loadFirstTimeClass = action.loadFirstTimeClass;
  }
  if (action is SetClassList) {
    newState.classList = action.classList;
  }
  if (action is SetLoadFirstTimeAssignment) {
    newState.loadFirstTimeAssignment = action.loadFirstTimeAssignment;
  }
  if (action is SetAssignmentList) {
    newState.assignmentList = action.assignmentList;
  }
  if (action is SetLoadFirstTimeQuiz) {
    newState.loadFirstTimeQuiz = action.loadFirstTimeQuiz;
  }
  if (action is SetQuizList) {
    newState.quizList = action.quizList;
  }
  if (action is SetLoadFirstTimeMaterial) {
    newState.loadFirstTimeMaterial = action.loadFirstTimeMaterial;
  }
  if (action is SetMaterialList) {
    newState.materialList = action.materialList;
  }
  if (action is SetLoadFirstTimeGrade) {
    newState.loadFirstTimeGrade = action.loadFirstTimeGrade;
  }
  if (action is SetGradeList) {
    newState.gradeList = action.gradeList;
  }
  if (action is SetLoadFirstTimeGradeStudents) {
    newState.loadFirstTimeGradeStudents = action.loadFirstTimeGradeStudents;
  }
  if (action is SetStudentsList) {
    newState.studentsList = action.studentsList;
  }
  return newState;
}
